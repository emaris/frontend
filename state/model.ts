import { BaseState, ChangeFunction } from "apprise-frontend/state/model";
import { initialBase } from "apprise-frontend/state/constants";
import { CampaignState, initialCampaign } from '../campaign/state';
import { initialProducts, ProductState } from "../product/state";
import { initialRequirements, RequirementState } from "../requirement/state";
import { initialSettings } from "apprise-frontend/settings/state";
import { MessageState, initialMessages } from "apprise-messages/state";
import { initialEvents } from "emaris-frontend/event/state";
import { EventState } from "emaris-frontend/event/state";



export type EmarisState = ChangeFunction<EmarisState> & BaseState & RequirementState & ProductState & CampaignState & MessageState & EventState


export const initialEmariState = { ...initialBase, ...initialMessages,  ...initialSettings, ...initialEvents, ...initialRequirements, ...initialProducts, ...initialCampaign,}
