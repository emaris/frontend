
import { useAppState } from "apprise-frontend/state/model"
import { EmarisState } from "./model"


export const useEmarisState = () => useAppState() as EmarisState

