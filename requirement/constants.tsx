import { Icon } from "antd";
import * as React from "react";
import { FiBox } from "react-icons/fi";


// note: moved here from module, to avoid cycles. after all, they're broadly used constants.
export const requirementSingular = "requirement.module.name_singular"
export const requirementPlural = "requirement.module.name_plural"

export const requirementType = "requirement"
export const requirementIcon = <Icon className="fi" component={FiBox} />
export const requirementRoute = "/requirement"

export const sourceTypeCategory = "TGC-requirement-sourcetype"
export const reqImportanceCategory = "TGC-requirement-importancescale"