import { any } from "apprise-frontend/iam/model"
import { requirementIcon, requirementType } from "./constants"

const baseAction = { icon:requirementIcon, type: requirementType, resource :any }

export const requirementActions = {  
    
    manage:  {...baseAction, labels:["manage"], shortName:"requirement.actions.manage.short",name:"requirement.actions.manage.name", description: "requirement.actions.manage.desc"}
    ,
    edit:  {...baseAction, labels:["manage","edit"], shortName:"requirement.actions.edit.short",name:"requirement.actions.edit.name", description: "requirement.actions.edit.desc"}
    ,
    assess:  {...baseAction, labels:["manage","assess"], shortName:"requirement.actions.assess.short",name:"requirement.actions.assess.name", description: "requirement.actions.assess.desc"}
       
}