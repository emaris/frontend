
import { callapi } from "apprise-frontend/call/call"
import { domainService } from "../constants"
import { EmarisState } from "../state/model"
import { Requirement, requirementmodelapi } from "./model"

const requirements = "/requirement"



export const requirementcalls = (s:EmarisState) => {

    const {at} = callapi(s)
    const { extern, intern } = requirementmodelapi(s)

    return {

        fetchAll: () => at(requirements,domainService).get<Requirement[]>().then(reqs=>reqs.map(intern))
        
        , 

        fetchOne: (id:String)  =>  at(`${requirements}/${id}`,domainService).get<Requirement>().then(intern) 
        
        ,

        fetchMany: (ids:string[])  =>  at(`${requirements}/search`,domainService).post<Requirement[]>({ids}).then(res => res.map(intern)) 
        
        ,

        add: (requirement:Requirement)  =>  at(requirements,domainService).post<Requirement>(extern(requirement)).then(intern)
        
        ,
    
        update: (requirement:Requirement)  => at(`${requirements}/${requirement.id}`,domainService).put<Requirement>(extern(requirement)).then(intern)
        
        , 

        delete: (id:string) => at(`${requirements}/${id}`,domainService).delete()
    }

}