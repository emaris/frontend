
import { any } from 'apprise-frontend/iam/model'
import { useLocale } from 'apprise-frontend/model/hooks'
import { noTenant } from 'apprise-frontend/tenant/constants'
import { UserPermissions } from 'apprise-frontend/user/UserPermissionTable'
import { ChangesProps } from 'apprise-frontend/iam/permission'
import { usePermissionDrawer } from 'apprise-frontend/iam/PermissionForm'
import { ResourceProps, StateProps, SubjectProps } from 'apprise-frontend/iam/PermissionTable'
import { User } from 'apprise-frontend/user/model'
import * as React from 'react'
import { useTranslation } from 'react-i18next'
import { requirementActions } from './actions'
import { useRequirements } from './api'
import { requirementPlural, requirementSingular, requirementType } from './constants'
import { RequirementLabel } from './Label'
import { Requirement } from './model'


type PermissionsProps = ChangesProps & Partial<ResourceProps<Requirement>> & Partial<SubjectProps<User>> & Partial<StateProps<User,Requirement>> & {
    edited? : User
}


//  wraps UserPermissions for Requirements, so as to inject appropriate defaults.

export const RequirementPermissions =  (props:PermissionsProps) => {

    const {t} = useTranslation()
    const {l} = useLocale()
    const requirements = useRequirements()

    return  <UserPermissions {...props}
                id="requirement-permissions" 
                subjectRange={props.subjectRange?.filter(s=>s.tenant===noTenant)}
                resourceSingular={props.resourceSingular || t(requirementSingular)}
                resourcePlural={props.resourcePlural || t(requirementPlural)}
                resourceText={t=>l(t.name)}
                renderResource={props.renderResource || ((t:Requirement) => <RequirementLabel requirement={t} /> )}
                resourceId={props.resourceId || ((r:Requirement) => r.id) }
                renderResourceOption={props.renderResourceOption || ((r:Requirement) => <RequirementLabel noLink requirement={r} />)} 
                resourceRange={props.resourceRange || requirements.all() } 
                resourceType={requirementType}

                actions={Object.values(requirementActions)} />
                
}


//  wraps the table in a drawer-based-form
export const useRequirementPermissions = ()=>usePermissionDrawer ( RequirementPermissions, { types:[requirementType, any] } )