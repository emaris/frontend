import { Placeholder } from "apprise-frontend/components/Placeholder";
import { iamType } from "apprise-frontend/iam/constants";
import { IamSlot } from "apprise-frontend/iam/module";
import { documentspec } from "apprise-frontend/layout/components/Document";
import { imagespec } from "apprise-frontend/layout/components/Image";
import { inputboxspec } from "apprise-frontend/layout/components/InputBox";
import { multichoicespec } from "apprise-frontend/layout/components/MultiChoice";
import { paragraphspec } from "apprise-frontend/layout/components/Paragraph";
import { cellspec, rowspec } from "apprise-frontend/layout/components/Row";
import { rowsectionspec } from "apprise-frontend/layout/components/RowSection";
import { separatorspec } from "apprise-frontend/layout/components/Separator";
import { titlespec } from "apprise-frontend/layout/components/Title";
import { layoutType } from "apprise-frontend/layout/constants";
import { LayoutSlot } from 'apprise-frontend/layout/module';
import { numspec } from "apprise-frontend/layout/parameters/Number";
import { tenantspec } from "apprise-frontend/layout/parameters/Tenant";
import { textspec } from "apprise-frontend/layout/parameters/Text";
import { themespec } from "apprise-frontend/layout/parameters/Theme";
import { Module } from "apprise-frontend/module";
import { pushEventType } from 'apprise-frontend/push/constants';
import { tagType } from "apprise-frontend/tag/constants";
import { TagSlot } from "apprise-frontend/tag/module";
import { noTenant, tenantType } from "apprise-frontend/tenant/constants";
import { TenantSlot } from "apprise-frontend/tenant/module";
import { userType } from "apprise-frontend/user/constants";
import { UserSlot } from "apprise-frontend/user/module";
import { eventType } from "emaris-frontend/event/constants";
import { EventSlot } from "emaris-frontend/event/module";
import { submissionfiledropspec } from 'emaris-frontend/layout/components/SubmissionFileDrop';
import { campaignspec } from "emaris-frontend/layout/parameters/Campaign";
import { dateyearspec } from "emaris-frontend/layout/parameters/DateYear";
import { partyspec } from "emaris-frontend/layout/parameters/Party";
import { requirementspec } from "emaris-frontend/layout/parameters/Requirement";
import * as React from "react";
import { requirementActions } from "./actions";
import { requirementIcon, requirementPlural, requirementSingular, requirementType } from "./constants";
import { RequirementLoader } from "./Loader";
import { pushRequirementSlot } from './pushevents';
import { RequirementPermissions } from "./RequirementPermissions";

export const requirementmodule : Module = {

    icon: requirementIcon,
    type: requirementType,

    dependencies: () => [tagType],
    
    nameSingular: requirementSingular,
    namePlural: requirementPlural,

    [tenantType]: {

        tenantResource:true


    } as TenantSlot

    ,

    [pushEventType] : pushRequirementSlot

    ,


    [iamType]: {
    
        actions: requirementActions,
     
     } as IamSlot,

    [tagType] :  {
         
        enable: true,
        // categories:requirementCategories,
        // tags: requirementstags
    
    } as TagSlot,

    [userType] : {

        renderIf: ({subjectRange}) => subjectRange?.some(s=>s.tenant===noTenant)        
        ,
        permissionComponent: props => <RequirementLoader placeholder={Placeholder.list}>
                                            <RequirementPermissions {...props} />
                                      </RequirementLoader>
  
    } as UserSlot
    
    ,

    [eventType] :  {
           
        enable: true
  
    } as EventSlot

    ,

    [layoutType] : {

        context: {type:requirementType},
        components : ()=>[paragraphspec,titlespec,inputboxspec,multichoicespec,submissionfiledropspec,rowspec,cellspec,rowsectionspec,separatorspec,documentspec,imagespec],
        parameters: ()=>[textspec,numspec,tenantspec,dateyearspec, themespec, campaignspec,partyspec,rowspec,cellspec,rowsectionspec,requirementspec]
    
    } as LayoutSlot

}