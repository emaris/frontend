
import { useAsyncRender } from "apprise-frontend/utils/hooks"
import * as React from "react"
import { useRequirements } from "./api"

type Props = React.PropsWithChildren<{

  placeholder?: React.ReactNode

}>


export const RequirementLoader = (props:Props) => {

    const {areReady,fetchAll} = useRequirements()

    const [render] = useAsyncRender({
      when:areReady(),
      task:fetchAll,
      content:props.children,
      placeholder:props.placeholder
  
    })
  
    return render 

  
  }