

import { Tooltip } from "antd"
import { Label, LabelProps, sameLabel, UnknownLabel } from "apprise-frontend/components/Label"
import { icns } from "apprise-frontend/icons"
import { useLocale } from "apprise-frontend/model/hooks"
import * as React from "react"
import { useTranslation } from "react-i18next"
import { Link, useLocation } from "react-router-dom"
import { requirementapi, useRequirements } from "./api"
import { requirementIcon, requirementRoute, requirementType } from "./constants"
import { Requirement } from "./model"


type Props = LabelProps & {

    requirement: string | Requirement | undefined
    lineage?: boolean
    showTitle?: boolean
    tipTitle?: boolean
}

const innerrouteRegexp = new RegExp(`${requirementRoute}\\/(.+?)(\\?|\\/|$)`)


export const RequirementLabel = (props: Props) => {

    const requirements = useRequirements()
    const location = useLocation()

    const {requirement} = props;

    const resolved = typeof requirement === 'string' ? requirements.lookup(requirement) : requirement

    if (!resolved)
        return <UnknownLabel tip={() => requirement as string} {...props} />

    return <PureRequirementLabel {...props} requirement={resolved} requirements={requirements} location={location} />

}

type PureProps = Omit<Props,'requirement'> & {

    requirement: Requirement
    requirements: ReturnType<typeof requirementapi>,
    location: ReturnType<typeof useLocation>
}


const PureRequirementLabel = React.memo((props: PureProps) => {

    const { l } = useLocale()
    const { t } = useTranslation()

    const { location, requirements, icon, requirement, lineage, linkTo, linkTarget, inactive, showTitle = false, tipTitle = false, tipPlacement = "topLeft", decorations = [], ...rest } = props

    const { pathname, search } = location
    const { routeTo, safeLookup } = requirements

     // like routeTo() if the current route is not a campaign route, otherwise replaces the current campaign with the target one.
    // undefined, if the two campaigns coincide.
    const innerRouteTo = (target: Requirement) => {

        const regexpmatch = pathname.match(innerrouteRegexp)?.[1]
        const linkname = target?.id

        return regexpmatch ?
            regexpmatch === linkname ? undefined : `${pathname.replace(regexpmatch, linkname!)}${search}`
            : requirements.routeTo(target)


    }

    const route = linkTo ?? innerRouteTo(requirement)

    const lineagelist = requirement?.lineage?.map(r => l(requirements.lookup(r)?.name)).join(', ')
    const lineageContent = requirement?.lineage?.[0] && lineage ?

        [<Tooltip title={t("requirement.misc.supersedes", { lineage: lineagelist })}>{
            requirement?.lineage?.length === 1 ?
                <Link to={() => routeTo(safeLookup(requirement!.lineage?.[0]))}>{icns.branch}</Link>
                : icns.branch}
        </Tooltip>]

        : []

    const noTip = props.noTip ?? (props.tip ? false : showTitle ? l(requirement.description) ? false : true : tipTitle ? l(requirement.description) ? false : true : true)
    const tip = props.tip ?? (() => showTitle ? l(requirement.name) : tipTitle ? l(requirement.description) : undefined)

    const decorationContent = [...lineageContent, ...decorations]

    const inactiveState = requirement.lifecycle.state === 'inactive'

    const label = (showTitle ? (l(requirement.description) ?? l(requirement.name)) : l(requirement.name))

    return <Label title={label} {...rest} inactive={inactive ?? inactiveState} tip={tip} noTip={noTip} tipPlacement={tipPlacement} linkTarget={linkTarget ?? requirementType} icon={icon || requirementIcon} decorations={decorationContent} linkTo={route} />


}, ($, $$) => $.requirement === $$.requirement && sameLabel($, $$))


export const NotAssessableLabel = (props: LabelProps & { emphasis?: boolean }) => {

    const { emphasis = true } = props
    const { t } = useTranslation()


    return <Label iconStyle={emphasis ? { color: 'white' } : undefined } style={emphasis ? { background: 'orange', color: 'white' } : undefined} mode='tag' title={t('submission.labels.not_assessable')} {...props} />
}