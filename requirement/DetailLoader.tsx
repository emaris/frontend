import { Placeholder } from 'apprise-frontend/components/Placeholder'
import { useAsyncRender } from 'apprise-frontend/utils/hooks'
import * as React from "react"
import { useRequirements } from './api'
import { Requirement } from './model'


export type Props = {

    id: string, 
    placeholder?: JSX.Element
    children: (loaded:Requirement)=>React.ReactElement
}


export const RequirementDetailLoader = (props: Props) => {

    const { id, placeholder=Placeholder.none, children } = props

    const { fetchOne, lookup, isLoaded } = useRequirements()

    const current = lookup(id)

    const initialValue = () => isLoaded(current) ? current : undefined

    const [fetched, setFetched] = React.useState<Requirement | undefined>(initialValue())

    const [render] = useAsyncRender({
        id: "fetchrequirement",
        when: !!fetched && id===fetched.id && isLoaded(current),
        task: () => fetchOne(id).then(setFetched),
        content: ()=>children(fetched!),
        placeholder

    })

    return render

}
