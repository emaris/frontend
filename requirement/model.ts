import { intlapi } from "apprise-frontend/intl/api"
import { Layout, newLayout } from "apprise-frontend/layout/model"
import { Lifecycle, newLifecycle } from "apprise-frontend/model/lifecycle"
import { compareMultilang, l, MultilangDto, newMultiLang, noMultilang } from "apprise-frontend/model/multilang"
import { settingsapi } from "apprise-frontend/settings/api"
import { Document } from "apprise-frontend/stream/model"
import { systemType } from "apprise-frontend/system/constants"
import { SystemAppSettings } from "apprise-frontend/system/model"
import { tagapi } from "apprise-frontend/tag/api"
import { TagExpression, Tagged } from "apprise-frontend/tag/model"
import { TenantAudience } from "apprise-frontend/tenant/AudienceList"
import { noTenant } from 'apprise-frontend/tenant/constants'
import { User } from "apprise-frontend/user/model"
import { deepclone, shortid } from "apprise-frontend/utils/common"
import { predefinedParameters, predefinedRequirementParameterId, predefinedRequirementParameters } from "emaris-frontend/layout/parameters/constants"
import { EmarisState } from "emaris-frontend/state/model"
import { requirementSingular } from "./constants"


export type RequirementDto = Tagged & {

    id: string
    
    predefined?:boolean
    
    lifecycle: Lifecycle<RequirementState>

    name: MultilangDto,
    description: MultilangDto

    audience: TagExpression
    audienceList?: TenantAudience
    userProfile: TagExpression

    lineage?: string[]
    
    documents?: Record<string, Document[]>

    properties: RequirementProperties
}

export type RequirementProperties = {

    source: RequirementSource,
    layout: Layout
    note?:Record<string, string>
    versionable: boolean | undefined
    editable: boolean | undefined
    assessed: boolean | undefined
  
  } //& {[key:string]:any}

export type RequirementSource = {

    title: MultilangDto
    
}

export type RequirementState = "active" | "inactive"

export type Requirement = RequirementDto





export const newRequirement : (_:EmarisState)=> Requirement = s => {
    const settings = ()=>settingsapi(s).get<SystemAppSettings>()[systemType] ?? {}

   return {

        tags: settings().defaultRequirementImportance ? ["TG-system-defaultcontext", ...settings().defaultRequirementImportance] : ["TG-system-defaultcontext"], //Default context set
        
        id:undefined!,
        name:newMultiLang(),
        description:newMultiLang(),
        lineage: [],
        audience: {terms:[]},
        userProfile: {terms:[]},
        audienceList: undefined!,
        lifecycle:newLifecycle('inactive'),

        properties: {

            title:newMultiLang(),
            source:{ type:undefined!, title:newMultiLang},
            layout: { ...newLayout(), parameters:[...predefinedParameters(s),...predefinedRequirementParameters(s)] } ,
            versionable: true,
            editable: true,
            assessed: true
           
        }

    }

}



export const noRequirement = (s:EmarisState) : Requirement  => {

    const t = intlapi(s).getT()
    
    return {
        ...newRequirement(s), 
        id: `${newRequirementId()}-unknown`, 
        name:noMultilang(t('common.labels.unknown_one',{singular:t(requirementSingular)}))
    }

}
export const completeOnAdd  = (requirement:Requirement, id?:string) => {

    const newreq = {...requirement, id: id ?? newRequirementId()}

    const hostparam = newreq.properties.layout.parameters.find(p=>p.id===predefinedRequirementParameterId)

    if (hostparam)
        hostparam.value = {id: newreq.id}

    return newreq

}

export const newRequirementId = () =>`R-${shortid()}`



export const requirementmodelapi = (s:EmarisState) =>  ({

    stringify: (r:Requirement | undefined) => r ? `${l(s)(r.name)} ${l(s)(r.description)??''}` : ``

    ,

    nameOf: (r:Requirement | undefined) => r ? `${l(s)(r.name)}` : ''

    ,

    intern: (dto:RequirementDto) => dto

    ,

    isLoaded: (req:Requirement|undefined) => !!req?.properties?.layout
    
    ,
    
    extern: (r:Requirement) => r
    
    ,

    clone: (r:Requirement) : Requirement => ({...deepclone(r), id: undefined!, predefined:false, lifecycle:newLifecycle(`inactive`), documents: {},})
    
    ,

    branch: (r:Requirement) : Requirement => ({...deepclone(r), id: undefined!, predefined:false, lifecycle:newLifecycle(`inactive`), lineage : [r.id], documents: {}})
    
    ,

    comparator: (o1:Requirement,o2:Requirement) => compareMultilang(s)(o1.name,o2.name)

    ,

    matches: (r:Requirement, u: User) => {

        const {given} = tagapi(s)
        
        return u.tenant === noTenant || given(u).matches(r.userProfile)
        
    },

    explicitLayoutParameters: (r:Requirement)=> r.properties.layout.parameters

    ,

    implicitLayoutParameters: (_:Requirement)=> []


})
