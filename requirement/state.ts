import { State } from "#app";
import { intlapi } from "apprise-frontend/intl/api";
import { systemapi } from "apprise-frontend/system/api";
import { utilapi } from "apprise-frontend/utils/api";
import { indexMap, through } from "apprise-frontend/utils/common";
import { notify, showAndThrow } from "apprise-frontend/utils/feedback";
import { requirementcalls } from "emaris-frontend/requirement/calls";
import { completeOnAdd, newRequirement, noRequirement, Requirement, RequirementDto, requirementmodelapi } from "emaris-frontend/requirement/model";
import { requirementPlural, requirementRoute, requirementSingular, requirementType } from "./constants";

type RequirementNext = {
    model: Requirement
}

export type RequirementState = {

    requirements: {
        all: Requirement[],
        map: Record<string, Requirement>
        next?: RequirementNext
    }
}

export const initialRequirements: RequirementState = {

    requirements: {
        all: undefined!,
        map: undefined!
    }

}

export const reqstateapi = (s: State) => {

    const model = requirementmodelapi(s)
    const call = requirementcalls(s)

    const { toggleBusy } = systemapi(s)

    const t = intlapi(s).getT()

    const type = requirementType
    const [singular, plural] = [t(requirementSingular), t(requirementPlural)].map(n => n.toLowerCase())

    const self = {

        areReady: () => !!s.requirements.all

        ,

        all: () => s.requirements.all

        ,

        allSorted: () => [...self.all()].sort(model.comparator)

        ,

        lookup: (id: string | undefined) => id ? s.requirements.map?.[id] : undefined

        ,

        safeLookup: (id: string | undefined) => self.lookup(id) ?? noRequirement(s)

        ,

        setAll: (requirements: Requirement[], props?: { noOverwrite: boolean }) => s.changeWith(s => {

            if (s.requirements.all && props?.noOverwrite)
                return

            s.requirements.all = requirements; s.requirements.map = indexMap(requirements).by(r => r.id)


        })

        ,


        fetchAll: (forceRefresh = false): Promise<Requirement[]> =>

            self.areReady() && !forceRefresh ? Promise.resolve(self.all())

                :

                toggleBusy(`${type}.fetchAll`, t("common.feedback.load", { plural }))

                    .then(_ => console.log(`fetching requirements...`))
                    .then(_ => call.fetchAll())
                    .then(through($ => self.setAll($, { noOverwrite: true })))


                    .catch(e => showAndThrow(e, t("common.calls.fetch_all_error", { plural })))
                    .finally(() => toggleBusy(`${type}.fetchAll`))

        ,

        routeTo: (r: Requirement) => r ? `${requirementRoute}/${r.id}` : requirementRoute

        ,


        fetchOne: (id: string, forceRefresh?: boolean): Promise<Requirement> => {

            const current = self.lookup(id)

            return current && model.isLoaded(current) && !forceRefresh ?

                Promise.resolve(current) :


                toggleBusy(`${type}.fetchOne`, t("common.feedback.load_one", { singular }))


                    .then(_ => call.fetchOne(id))
                    .then(through(fetched => { if (!fetched) throw Error(`Invalid resource lookup for ${id}, returned undefined.`) }))
                    .then(through(fetched => self.setAll(self.all().map(u => u.id === id ? fetched : u))))

                    .catch(e => showAndThrow(e, t("common.calls.fetch_one_error", { singular })))
                    .finally(() => toggleBusy(`${type}.fetchOne`))
        }
        
        ,

        fullFetch: (requirement: Requirement, force?: boolean): Promise<Requirement> =>

            model.isLoaded(requirement) && !force ? Promise.resolve(requirement) : self.fetchOne(requirement.id)



        ,

        fetchMany: (ids: string[], force?: boolean): Promise<Requirement[]> =>

            toggleBusy(`${type}.fetchMany`, t("common.feedback.load_many", { plural }))

                .then(() => self.fetchManySilently(ids, force))

                .catch(e => showAndThrow(e, t("common.calls.fetch_many_error", { plural })))
                .finally(() => toggleBusy(`${type}.fetchMany`))

        ,


        fetchManySilently: async (ids: string[], force?: boolean): Promise<Requirement[]> => {

            const filteredIds = ids.filter(id => force || !model.isLoaded(self.lookup(id)))

            // Prepare tasks that fetch requirements 10 at the time
            // (splice mutates the original array and returns the deleted entries.)
            let fetchtasks = [] as (() => Promise<RequirementDto[]>)[]
            while (filteredIds.length) {
                const group = filteredIds.splice(0,10)
                fetchtasks = [...fetchtasks, () => call.fetchMany(group)]
            }
            // Execute tasks 5 at the time in parallel.
            let taskgroups = [] as Promise<RequirementDto[]>[]
            while (fetchtasks.length) taskgroups = [...taskgroups, Promise.all(fetchtasks.splice(0,5).map(task => task())).then(reqs => reqs.flat()).then(fetched => fetched.filter(r => r))]

            const fetched = await taskgroups.reduce((prev, cur) => prev.then( reqs => cur.then(reqs2 => [...reqs, ...reqs2])), Promise.resolve([] as RequirementDto[]));

            self.setAll(self.all().map(u => fetched.find(uu => uu.id === u.id) ?? u))

            return fetched
        }
        ,

        next: () => {
            const next = s.requirements.next || { model: newRequirement(s) }
            return next;
        }

        ,

        resetNext: () => {
            self.setNext(undefined)
        }

        ,

        setNext: (t: RequirementNext | Requirement | undefined): RequirementNext | undefined => {
            const model = t ? t.hasOwnProperty('model') ? t as RequirementNext : { model: t as Requirement } : undefined

            s.changeWith(s => s.requirements.next = model)
            return model

        }

        ,


        save: (requirement: Requirement): Promise<Requirement> =>

            toggleBusy(`${type}.save`, t("common.feedback.save_changes"))


                .then(() => requirement.id ?

                    call.update(requirement)
                        .then(through(saved => self.setAll(self.all().map(r => r.id === requirement.id ? saved : r))))

                    :

                    call.add(completeOnAdd(requirement))
                        .then(through(saved => self.setAll([saved, ...self.all()])))

                )
                .then(through(() => notify(t('common.feedback.saved'))))

                .catch(e => showAndThrow(e, t("common.calls.save_one_error", { singular })))
                .finally(() => toggleBusy(`${type}.save`))

        ,

        remove: (id: string, onConfirm: () => void, challenge?: boolean) =>

            utilapi(s).askConsent({

                title: t('common.consent.remove_one_title', { singular }),
                content: t('common.consent.remove_one_msg', { singular }),
                okText: t('common.consent.remove_one_confirm', { singular }),

                okChallenge: challenge ? t('common.consent.remove_challenge', { singular }) : undefined,

                onOk: () => {


                    toggleBusy(`${type}.removeOne`, t("common.feedback.save_changes"))

                        .then(_ => call.delete(id))
                        .then(_ => self.all().filter(u => id !== u.id))
                        .then(self.setAll)
                        .then(_ => notify(t('common.feedback.saved')))
                        .then(_ => onConfirm && onConfirm())

                        .catch(e => showAndThrow(e, t("common.calls.remove_one_error", { singular })))
                        .finally(() => toggleBusy(`${type}.removeOne`))


                }
            }),

        removeAll: (list: string[], onConfirm?: () => void) =>

            utilapi(s).askConsent({


                title: t('common.consent.remove_many_title', { count: list.length, plural }),
                content: t('common.consent.remove_many_msg', { plural }),
                okText: t('common.consent.remove_many_confirm', { count: list.length, plural }),

                onOk: () => {


                    toggleBusy(`${type}.removeAll`, t("common.feedback.save_changes"))

                        .then(_ => list.forEach(id => call.delete(id)))
                        .then(_ => self.all().filter(u => !list.includes(u.id)))
                        .then(self.setAll)
                        .then(_ => notify(t('common.feedback.saved')))
                        .then(_ => onConfirm && onConfirm())

                        .catch(e => showAndThrow(e, t("common.calls.remove_one_error", { singular })))
                        .finally(() => toggleBusy(`${type}.removeAll`))


                }
            })

    }

    return self

}