
import { useEmarisState } from "emaris-frontend/state/hooks"
import { EmarisState } from "../state/model"
import { requirementActions } from "./actions"
import { requirementmodelapi } from "./model"
import { reqstateapi } from "./state"
import { requirementvalidationapi } from "./validation"

export const useRequirements = () => requirementapi(useEmarisState())

export const requirementapi = (s:EmarisState) => ({

    ...reqstateapi(s),
    ...requirementmodelapi(s),
    ...requirementvalidationapi(s),

    actions: requirementActions

})

export const requirements = {

    breadcrumbResolver: (id:string,s: EmarisState) => requirementapi(s).lookup(id)?.name
}


