import { askReloadOnPushChange, askReloadOnPushRemove } from 'apprise-frontend/push/constants';
import { PushEventSlot } from 'apprise-frontend/push/module';
import { tenantRoute } from 'apprise-frontend/tenant/constants';
import { campaignapi } from 'emaris-frontend/campaign/api';
import { dashboardRoute } from 'emaris-frontend/dashboard/constants';
import { EmarisState } from 'emaris-frontend/state/model';
import shortid from 'shortid';
import { requirementRoute, requirementSingular, requirementType } from './constants';
import { Requirement } from './model';
import { reqstateapi } from './state';

export type RequirementChangeEvent = {

    requirement: Requirement
    type: 'add' | 'remove' | 'change'

}


export const pushRequirementSlot: PushEventSlot<EmarisState, RequirementChangeEvent> = {

    onSubcribe: () => {

        console.log("subscribing for requirement changes...")

        return [{

            topics: [requirementType]

            ,

            onEvent: (event: RequirementChangeEvent, s: EmarisState, history, location) => {

                const { requirement, type } = event

                console.log(`received ${type} event for requirement ${requirement.name.en}...`, { event })

                const requirements = reqstateapi(s)
                const campaigns = campaignapi(s)

                const currentUrl = `${location.pathname}${location.search}`

                const detailOrDashBordOrTrailOnScreen = currentUrl.includes(requirement.id)
                const trailOnScreen = detailOrDashBordOrTrailOnScreen && currentUrl.includes(tenantRoute)
                const detailOnScreen = detailOrDashBordOrTrailOnScreen && !currentUrl.includes(dashboardRoute)

                const campaignId = location.pathname.split("/")[2]
                const campaign = campaigns.lookup(campaignId)
                const campaignArchived = campaign?.lifecycle.state === 'archived'


                switch (type) {

                    case 'add': {

                        requirements.setAll([...requirements.all(), requirement])

                        break
                    }


                    case 'change': {

                        const change = () => requirements.setAll(requirements.all().map(c => c.id === requirement.id ? requirement : c))
                        const changeAndRefresh = () => { change(); history.push(currentUrl, shortid()) } // uses location state to send down a signal for remount.

                        if ((detailOnScreen || trailOnScreen) && !campaignArchived)
                            askReloadOnPushChange(s, { singular: requirementSingular, onOk: changeAndRefresh })

                        else change()

                        break
                    }

                    case 'remove': {

                        const fallbackRoute = detailOnScreen ? requirementRoute : dashboardRoute

                        const remove = () => requirements.setAll(requirements.all().filter(r => r.id !== requirement.id))
                        const leaveAndRemove = () => { history.push(fallbackRoute); remove() }

                        if (detailOrDashBordOrTrailOnScreen && !campaignArchived)
                            askReloadOnPushRemove(s, { singular: requirementSingular, onOk: leaveAndRemove })

                        else remove()

                        break
                    }

                }

            }

        }]
    }
}