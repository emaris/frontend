import { eventapi } from "emaris-frontend/event/api";
import { eventType } from "emaris-frontend/event/constants";
import { eventcardinalities, EventDto } from "emaris-frontend/event/model";
import { EventSlot } from "emaris-frontend/event/module";
import { Lifecycle } from "apprise-frontend/model/lifecycle";
import { moduleRegistry } from "apprise-frontend/module";
import { globalstate } from "apprise-frontend/state/model";
import { defaultContext } from "apprise-frontend/system/constants";
import MockAdapter from "axios-mock-adapter/types";
import { randomIn } from "apprise-frontend/utils/common";
import { idify, mockRegistry, mockstore, MockStore, someMultiLang, somePastLifecycle } from "apprise-frontend/utils/mocks/mocks";
import { mockregistry } from "apprise-frontend/utils/mocks/registry";
import { mockContexts, randomTagRefsFor } from "apprise-frontend/utils/mocks/tags";
import { EmarisState } from "emaris-frontend/state/model";


mockregistry.register<EmarisState>({

    onLoad: state => {

        console.log("staging event types...")
        eventstore = mockstore(state.events.all)

    }
})



export const mockEvents = (mock: MockAdapter, size?: number) => {

    mockRegistry.register(eventType)

    mockCalls(mock, size)

}

export const mockevents: EventDto[] = []
var eventstore: MockStore<EventDto>

export const temporals: EventDto[] = [

    { "tags": [],
        cardinality: eventcardinalities["any"], 
        managed: true, 
        managedTypes: [], 
        "id": "EV-event-reminder", 
        "name": { "en": "Reminder", "fr": "Rappel" }, 
        "description": { 
            "en": "A reminder about the occurence of another event", 
            "fr": "Un rappel sur la survenance d'un autre événement" }, 
        "type": "event", 
        "lifecycle": { "state": "active", "created": 1584371426833 },
        "predefinedProperties": [],
        properties:{}
    }

]

export const events = (size: number = 0) => {

    if (eventstore)
        return eventstore

    const api = eventapi(globalstate)

    let events: EventDto[]

    if (api.areReady()) {
        events = api.all()
    }
    else {


        console.log("generating mock event types...")

        // generates mock names first, to enable references
        const mocknames = Array(size).fill(undefined).map((_, i) => `Sample Event ${i}`)

        events = [...temporals, ...mocknames.map(n => someEvent(n))]
        
        events.forEach(e => e.tags.push(defaultContext))

        events = [...events, ...mockevents]

    }

    eventstore = mockstore(events)

    return eventstore

}



const someEvent = (name: string): EventDto => ({

    id: idify(name),
    name: someMultiLang(name),
    description: someMultiLang("description"),
    cardinality: eventcardinalities["one"], 
    type: randomIn(moduleRegistry.allWith(eventType).filter(m => (m[eventType] as EventSlot).enable).map(m => m.type)),
    tags: [randomIn([defaultContext, ...mockContexts.map(t => t.id)])!, ...randomTagRefsFor(eventType)],
    lifecycle: somePastLifecycle("active") as Lifecycle<'active' | 'inactive'>,
    predefinedProperties: [],
    properties:{}

})



const mockCalls = (mock: MockAdapter, size?: number) => {


    mock.onGet("/admin/event").reply(() => [200, events(size).all()])
    mock.onGet(/\/admin\/event\/.+/).reply(({ url }) => [200, events().oneWith(url!.split('/').pop())])
    mock.onPost("/admin/event").reply(({ data }) => {

        const model = JSON.parse(data)
        return [200, events().add(model)]
    })

    mock.onPut("/admin/event").reply(({ data }) => {

        const model = JSON.parse(data)
        return [200, events().update(model)]
    })

    mock.onDelete(/\/admin\/event\/.+/).reply(({ url }) => [204, events().delete(url!.split('/').pop())])
}





//const mockTags = () => {

//     [
//         someCat('Category 1',eventType,['eventag 1','eventag 2','eventag 3']),
//         someCat('Category 2',eventType,['eventag 4','eventag 5']),
//         someCat('Category 3',eventType,['eventag 6','eventag 7']),
//         {cat: noCategory, tags: someTags(eventType,['eventag 8',`eventag 9`])} 
//     ]
//     .forEach(({cat,tags:ts}) =>{ 

//             if (cat!==noCategory)  tagCategories().add(cat); 
//             ts.forEach(t=>tags().add( cat===noCategory ? t : {...t,category: withTags.refOfCategory(cat)} )) 
//     })

// }