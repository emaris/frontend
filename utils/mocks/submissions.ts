import { newMultiLang } from "apprise-frontend/model/multilang";
import { globalstate } from "apprise-frontend/state/model";
import { withUser } from "apprise-frontend/user/model";
import { randomBoolean, randomIn, randomMomentBetween, randomNumberBetween } from "apprise-frontend/utils/common";
import { getLogged } from "apprise-frontend/utils/mocks/logged";
import { loremipsum, MockStore, mockstore } from "apprise-frontend/utils/mocks/mocks";
import { messages, someMessage } from "apprise-messages/mocks";
import MockAdapter from "axios-mock-adapter/types";
import { campaignapi } from "emaris-frontend/campaign/api";
import { campaignEndEvent, campaignStartEvent, requirementDeadlineEvent } from "emaris-frontend/campaign/constants";
import { EventInstanceDto } from "emaris-frontend/campaign/event/model";
import { partyinstapi } from "emaris-frontend/campaign/party/api";
import { PartyInstance, PartyInstanceDto } from "emaris-frontend/campaign/party/model";
import { productinstapi } from "emaris-frontend/campaign/product/api";
import { requirementinstapi } from "emaris-frontend/campaign/requirement/api";
import { submissionapi } from "emaris-frontend/campaign/submission/api";
import { SubmissionDto, SubmissionLifecycle, submissionmodelapi, TrailKey } from "emaris-frontend/campaign/submission/model";
import { requirementType } from "emaris-frontend/requirement/constants";
import moment from "moment-timezone";
import { complianceScaleCategory } from "../../campaign/constants";
import { Campaign, CampaignDto, CampaignInstance } from "../../campaign/model";
import { TrailDto } from "../../campaign/submission/model";
import { campaigns, predefinedTags } from "./campaigns";
import { absolute, eventInstances, partyInstances, productInstances, requirementInstances } from "./instances";





export const mockSubmissions = (mock: MockAdapter) => {

  mockCalls(mock)

}


export const passesRuleOfTenancy = (resource: TrailDto): boolean => {

  const logged = withUser(getLogged())

  // case 1)
  if (logged.hasNoTenant())
    return true


  return logged.tenant === resource.key.party || logged.isManagerOf(resource.key.party, 'direct')


}

const mockCalls = (mock: MockAdapter) => {

  // helpers
  //const trailsIn = url => trailsFor(campaignIn(url)).all().filter(passesRuleOfTenancy).filter(i=>i.key.campaign===campaignIn(url))
  const trailsInCampaign = campaign => trailsFor(campaign).all().filter(passesRuleOfTenancy).filter(i => i.key.campaign === campaign)

  // const trailIn = url => url.split('/')[5]
  const idIn = url => url.split("/")[3]
  const findTrail = id => Object.values(trailstore).map(store => store.oneWith(id)).find(trail => !!trail)
  // const submissionIn = url => url.split('/')[5]

  //mock.onGet(/\/domain\/campaign\/.+\/trail$/).reply( ({url}) =>  [200, trailsIn(url)] )

  mock.onPost('/domain/trail/search').reply(({ data }) => [200, trailsInCampaign(JSON.parse(data).campaign)])


  //mock.onGet(/\/domain\/campaign\/.+\/trail\/.+/).reply( ({url}) => [200, trailsFor(campaignIn(url)).oneWith(trailIn(url))] )
  mock.onGet(/\/domain\/trail\/.+/).reply(({ url }) => [200, findTrail(idIn(url))])


  mock.onPut(/\/domain\/trail\/.+/).reply(({ url, data }) => {

    const modified = JSON.parse(data) as TrailDto

    const trails = trailsFor(modified.key.campaign)

    const trail = trails.oneWith(modified.id)

    return [200, trail ? trails.update(modified) : trails.add(modified)]

  })

  mock.onDelete(/\/domain\/submission\/.+$/).reply(({ url }) => {

    const id = idIn(url)
    const submission = submissions().oneWith(id)

    if (!submission)
      return [404, { message: `no such submission ${id}` }]

    const trail = findTrail(submission.trail)

    if (!trail)
      return [404, { message: `no such trail ${submission.trail}` }]

    trail.submissions = trail.submissions?.filter(s => s.id !== id)

    submissions().delete(id)

    return [204]

  })


  mock.onPost('domain/trail').reply(({ data }) => {

    const { submission, key } = JSON.parse(data) as { submission: SubmissionDto, key: TrailKey }

    const now = moment()

    if (!submission.trail)
      return [404, { message: `no trail in submission` }]

    let trail = trailsFor(key.campaign).oneWith(submission.trail)

    // creates a trail if one doesn't exist.
    if (!trail)
      trail = trailsFor(key.campaign).add({ id: submission.trail, key, submissions: [], properties: {} })

    trail.submissions = [...trail.submissions ?? [], submission]

    submission.lifecycle.created = new Date().getTime()
    submission.lifecycle.lastModified = new Date().getTime()
    submission.lifecycle.lastModifiedBy = getLogged().username
    if (submission.lifecycle.state === 'submitted')
      submission.lifecycle.lastSubmitted = now.format()

    submissions().add(submission)

    return [200, { trail, submission }]

  })

  mock.onGet(/\/domain\/submission\/.+$/).reply(({ url }) => { return [200, submissions().oneWith(idIn(url))] })

  mock.onPost(/\/domain\/submission\/search$/).reply(({ data }) => {

    const { submissions: subs } = JSON.parse(data)

    return [200, submissions().all().filter(s => subs.includes(s.id))]

  })

  mock.onPut(/\/domain\/submission\/.+$/).reply(({ url, data }) => {

    const submission = JSON.parse(data) as SubmissionDto

    if (!submission.trail)
      return [404, { message: `missing trail` }]

    const trail = findTrail(submission.trail)

    if (!trail)
      return [404, { message: `no such trail ${submission.trail}` }]

    const current = trail.submissions.find(s => s.id === submission.id)

    if (!current)
      return [404, { message: `unknwon submission` }]


    const now = moment()

    if (current.lifecycle.state === 'draft' && submission.lifecycle.state === 'submitted') {

      submission.lifecycle.lastSubmitted = now.format()

    }
    if (current.lifecycle.state === 'pending' && submission.lifecycle.state === 'submitted') {

      submission.lifecycle.lastSubmitted = now.format()

      submission.lifecycle.lastApprovedBy = getLogged().username
    }

    else if (submission.lifecycle.compliance !== current.lifecycle.compliance)
      submission.lifecycle.compliance =
        submission.lifecycle.compliance ?
          { ...submission.lifecycle.compliance, lastAssessed: now.format(), lastAssessedBy: getLogged().username } :
          // {state:undefined, msg: undefined, lastAssessed : now.format(), lastAssessedBy: getLogged().username}
          undefined


    else {
      submission.lifecycle.lastModified = now.valueOf()
      submission.lifecycle.lastModifiedBy = getLogged().username
    }

    // update in storage
    trail.submissions = trail.submissions?.map(s => s.id === submission.id ? submission : s)


    submissions().update(submission)

    return [200, submission]

  })

}


var trailstore: Record<string, MockStore<TrailDto>>

export const trailsFor = (cid: string) => {

  if (trailstore && trailstore[cid])
    return trailstore[cid]

  const campaign = campaigns().oneWith(cid) as Campaign

  const api = campaign ? submissionapi(globalstate)(campaign) : undefined;

  let trails

  if (api && api.areTrailsReady())

    trails = api.allTrails()

  else if (!campaign) {

    console.log(`submission mock warn: campaign ${cid} doesn't exist, assuming it's just been created.`)
    trails = []

  }
  else {

    console.log("generating mocks trails...")

    const start = eventInstances(cid).all().find(e => e.campaign === cid && e.target === cid && e.source === campaignStartEvent)!
    const end = eventInstances(cid).all().find(e => e.campaign === cid && e.target === cid && e.source === campaignEndEvent)!

    trails = moment(absolute(start?.date)).isBefore(moment()) &&
      moment(absolute(end?.date)).isAfter(moment()) ?
      trailsForCampaign(campaign, start, end) : []
  }

  trailstore = { ...trailstore ?? {}, [cid]: mockstore(trails) }

  return trailstore[cid]

}


var submissionstore: MockStore<SubmissionDto>


export const submissions = () => {

  if (submissionstore)
    return submissionstore

  // init
  const submissions = [] as SubmissionDto[]

  submissionstore = mockstore(submissions)

  return submissionstore

}

const trailsForCampaign = (c: CampaignDto, start: EventInstanceDto, end: EventInstanceDto) => {

  const reqtrails = requirementInstances(c.id).all().filter(r => r.campaign === c.id).slice(0, 10).flatMap(i => {

    const deadline = eventInstances(c.id).all().find(ei => ei.campaign === c.id && ei.source === requirementDeadlineEvent && ei.target === i.source)

    return trailsForInstance(c, i, deadline, start, end)

  })
  const prodtrails = productInstances(c.id).all().filter(r => r.campaign === c.id).slice(0, 10).flatMap(i => trailsForInstance(c, i, undefined, start, end))

  const trails = [...reqtrails, ...prodtrails]

  return trails
}

const trailsForInstance = (c: CampaignDto, source: CampaignInstance, deadline: EventInstanceDto | undefined, start: EventInstanceDto, end: EventInstanceDto) => partyInstances(c.id).all().filter(r => r.campaign === source.campaign).slice(0, 10).flatMap(pi => someTrail(c, source, deadline, pi, start, end))

const someTrail = (c: CampaignDto, source: CampaignInstance, deadline: EventInstanceDto | undefined, party: PartyInstanceDto, start: EventInstanceDto, end: EventInstanceDto): TrailDto => {

  const trail = {

    id: `TR-${party.source}-${source.source}-${source.campaign}`,

    key: {

      campaign: source.campaign,
      asset: source.source,
      assetType: source.instanceType,
      party: party.source
    },

    properties: { adminCanEdit: true, adminCanSubmit: true }

  } as TrailDto

  // gen submitted
  let mocks = Array(randomNumberBetween(0, 2)).fill(undefined).map((_, i) => submissions().add(someSubmission(c, party, source, trail, i, deadline, start, false)))

  // optionally, gen latest unsubmitted
  // if (true) {

    const draft = someSubmission(c, party, source, trail, mocks.length, deadline, start, true);

    mocks.push(draft)

    submissions().add(draft)
  // }

  //  // gen submitted
  //  let mocks = Array(randomNumberBetween(0,2)).fill(undefined).map((_,i)=>submissions().add(someSubmission(trail,i,deadline,start,false)))

  //  // optionally, gen latest unsubmitted
  //  if (mocks.length===0 || randomBoolean()) {

  //   const draft = someSubmission(trail,mocks.length,deadline,start,true);

  //   mocks.push(draft)

  //   submissions().add(draft)
  // }

  trail.submissions = [...mocks]

  return trail
}

const someSubmission = (c: CampaignDto, p: PartyInstance, a: CampaignInstance, { id, key }: TrailDto, index: number, deadlineEvent: EventInstanceDto | undefined, start: EventInstanceDto, last: boolean) => {


  const now = moment()
  const deadline = moment(absolute(deadlineEvent?.date))
  const diff = deadline.diff(now, "days")

  // created after the start of the campaign and before the deadline, or before now if the deadline is in the future.
  const created = randomMomentBetween(moment(new Date(absolute(start.date))), diff >= 0 ? now : deadline)

  // last touched shorlty before or after the deadline, or shortly before now if the deadline is in the future.
  const lastModified = randomMomentBetween(created, diff >= 0 ? now.clone().subtract(randomNumberBetween(0, 20), 'days') : deadline.clone().add(randomNumberBetween(-20, Math.min(10, -diff)), 'days'))

  const lifecycle = {

    created: created.valueOf(),
    lastModified: lastModified.valueOf(),
    lastModifiedBy: getLogged().username,
    state: last ? randomBoolean(.7) ? 'draft' : 'pending' : 'submitted'

  } as SubmissionLifecycle

  if (lifecycle.state === 'submitted') {

    const lastSubmitted = moment.min(now, lastModified.clone().add(randomNumberBetween(1, 2), "days"))
    const lastAssessed = moment.min(now, lastSubmitted.clone().add(randomNumberBetween(5, 15), "days"))

    lifecycle.lastSubmitted = lastSubmitted.format()
    lifecycle.lastApprovedBy = getLogged().username

    if (campaigns().oneWith(key.campaign)?.properties.complianceScale) {

      const complianceTags = predefinedTags().filter(t => t.category === complianceScaleCategory)

      const state = randomBoolean(.7) ? randomIn(complianceTags).id : undefined

      lifecycle.compliance = { state, message: loremipsum, officialObservation: newMultiLang(), lastAssessed: lastAssessed.format(), lastAssessedBy: getLogged().username, timeliness: undefined! }

    }


  }


  const submission = {

    id: `SUB-${index}-${id}`,
    trail: id,
    content: { data: {}, resources: {} },
    lifecycle

  } as SubmissionDto

  const submissionTopics = submissionmodelapi(globalstate)(c as Campaign).topics(submission)
  const campaignTopics = campaignapi(globalstate).topics(c as Campaign)
  const partyTopics = partyinstapi(globalstate)(c as Campaign).topics(p)
  const assetTopics = a.instanceType === requirementType ? requirementinstapi(globalstate)(c as Campaign).topics(a) : productinstapi(globalstate)(c as Campaign).topics(a)



  Array.from({ length: 5 }).forEach(() => messages().add(someMessage([...campaignTopics, ...partyTopics, ...assetTopics, ...submissionTopics, { type: "some", name: "extra" }], p.source)))

  return submission
}