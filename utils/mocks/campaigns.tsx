import { Button } from "apprise-frontend/components/Button"
import { eventapi } from "emaris-frontend/event/api"
import { eventcardinalities, EventDto } from "emaris-frontend/event/model"
import { intlapi } from "apprise-frontend/intl/api"
import { globalstate } from "apprise-frontend/state/model"
import { systemapi } from "apprise-frontend/system/api"
import { defaultContext } from "apprise-frontend/system/constants"
import { cardinalities, Tag, TagCategory } from "apprise-frontend/tag/model"
import { noTenant, tenantType } from "apprise-frontend/tenant/constants"
import { User, UserDto } from "apprise-frontend/user/model"
import { randomIn } from "apprise-frontend/utils/common"
import { mockevents } from "emaris-frontend/utils/mocks/events"
import { getLogged } from "apprise-frontend/utils/mocks/logged"
import { idify, mockRegistry, MockStore, mockstore, someMultiLang, somePastLifecycle } from "apprise-frontend/utils/mocks/mocks"
import { mockcategories, mocktags, randomTagRefsFor } from "apprise-frontend/utils/mocks/tags"
import { users } from "apprise-frontend/utils/mocks/users"
import { messagecallapi } from "apprise-messages/calls"
import { messageFullIcon } from "apprise-messages/constants"
import { newMessage } from "apprise-messages/model"
import MockAdapter from "axios-mock-adapter/types"
import { campaignapi } from "emaris-frontend/campaign/api"
import { CampaignAndSummary } from "emaris-frontend/campaign/calls"
import { anyPartyTopic, campaignEndEvent, campaignStartEvent, campaignType, complianceScaleCategory, complianceType } from "emaris-frontend/campaign/constants"
import { partyinstapi } from "emaris-frontend/campaign/party/api"
import { productinstapi } from "emaris-frontend/campaign/product/api"
import { requirementinstapi } from "emaris-frontend/campaign/requirement/api"
import { submissionType } from "emaris-frontend/campaign/submission/constants"
import { submissionmodelapi } from "emaris-frontend/campaign/submission/model"
import { productapi } from "emaris-frontend/product/api"
import { productType } from "emaris-frontend/product/constants"
import { requirementapi } from "emaris-frontend/requirement/api"
import { requirementType } from "emaris-frontend/requirement/constants"
import { useEmarisState } from "emaris-frontend/state/hooks"
import randomParagraph from "random-paragraph"
import * as React from 'react'
import { Campaign, CampaignDto, newCampaignId } from "../../campaign/model"
import { eventInstances, partyInstances, pastDate, productInstances, requirementInstances } from "./instances"
import { trailsFor } from "./submissions"



export const mockCampaigns = (mock: MockAdapter, size?: number) => {

  mockRegistry.register(campaignType)

  mockCalls(mock, size)

  mockEvents()
  mockTags()

}

var campaignstore: MockStore<CampaignDto>

export const campaigns = (size: number = 5) => {

  if (campaignstore)
    return campaignstore

  let campaigns: CampaignDto[]

  const api = campaignapi(globalstate);

  if (api.areReady())

    campaigns = api.all()

  else {

    console.log("generating campaign mocks...")

    campaigns = generateMockCampaigns(size)

  }

  campaignstore = mockstore(campaigns)

  return campaignstore

}


export const generateMockCampaigns = (size: number = 5): CampaignDto[] => {

  const mocknames = Array(size).fill(undefined).map((_, i) => `Sample Campaign ${i}`)

  const mockcampaigns = mocknames.map(n => someCampaign(n))

  mockcampaigns.forEach(c => c.id = newCampaignId())

  return mockcampaigns
}



const mockCalls = (mock: MockAdapter, size?: number) => {

  mock.onGet("/domain/campaign").reply(_ => {

    // we load all templates before we generate campaigns and their instances.
    // if we didn't the instances may end up being based on mock templates, rather than real templates.
    // this is not what happens with real campaigns, where we load templates only if and when we access their instances.

    const loadAllAssets = requirementapi(globalstate).fetchAll()
      .then(() => productapi(globalstate).fetchAll())
      .then(() => eventapi(globalstate).fetchAll())

    return loadAllAssets.then(_ => [200, campaigns(size).all().map(update)])

  })

  mock.onPost("/domain/campaign").reply(({ data }) => {

    const model = JSON.parse(data)
    model.lifecycle = somePastLifecycle(model.lifecycle.state)

    // // makes sure isntances of existing campaigns are staged before we add a new one.
    // // otherwise the new campaign will be also pre-staged like the others.
    // partyInstances()
    // requirementInstances()
    // productInstances()
    // eventInstances()

    return [200, campaigns().add(model)]
  })

  mock.onPost("/domain/campaign/remove").reply(({ data }) => {

    (JSON.parse(data) as string[]).filter(id => campaigns().delete)

    return [200]
  })

  // TODO: why do we return a campaign with summary here?
  mock.onGet(/\/domain\/campaign\/[^/]+$/).reply(({ url }) => [200, update(campaigns().oneWith(url!.split('/').pop())).campaign])
  mock.onPut(/\/domain\/campaign\/[^/]+$/).reply(({ data }) => {

    const model = JSON.parse(data)
    model.lifecycle = somePastLifecycle(model.lifecycle.state)
    return [200, campaigns().update(model)]
  })
  mock.onDelete(/\/domain\/campaign\/[^/]+$/).reply(({ url }) => [204, campaigns().delete(url!.split('/').pop())])


  mock.onPost('/domain/campaign/search').reply(({ data }) => {

    const { type, id } = JSON.parse(data)
    

    const matchingIds = campaigns().all().filter(c => {

      let instances

      switch (type) {

        case tenantType: instances = partyInstances(c.id).all(); break
        case requirementType: instances = requirementInstances(c.id).all(); break
        case productType: instances = productInstances(c.id).all(); break
        default: instances = eventInstances(c.id).all();
      }

      return instances.find(inst => inst.campaign === c.id && inst.source === id)

    })
      .map(c => c.id)

    return [200, matchingIds]

  })


}

const mockTags = () => {

  predefinedCategories().forEach(c => mockcategories.push(c))
  otherPredefinedCategories.forEach(c => mockcategories.push(c))

  predefinedTags().forEach(t => mocktags.push(t))
  otherPredefinedTags.forEach(t => mocktags.push(t))

}



export const reminder: EventDto = { 
  "tags": [], 
  managed: false, 
  cardinality: eventcardinalities["one"], 
  "id": "reminder", 
  "name": { "en": "Reminder", "fr": "Rappel" }, 
  "description": { "en": "A reminder about the occurence of another event", "fr": "Un rappel sur la survenance d'un autre événement" }, 
  "type": "event", 
  "lifecycle": { "state": "active", "created": 1584371426833 },
  "predefinedProperties": [],
  "properties":{}
}


export const startEvent = () => ({
  id: campaignStartEvent, type: campaignType
  , name: someMultiLang("Campaign Start"), description: someMultiLang("The start of a campaign.")
  , lifecycle: somePastLifecycle('active'),
  cardinality: eventcardinalities["one"], 
  managed: true,
  predefined: true,
  tags: [],
  predefinedProperties: [],
  "properties":{}
}) as EventDto

export const endEvent = () => ({
  id: campaignEndEvent, type: campaignType
  , name: someMultiLang("Campaign End"), description: someMultiLang("The end of a campaign.")
  , lifecycle: somePastLifecycle('active'),
  cardinality: eventcardinalities["one"], 
  managed: true,
  predefined: true,
  tags: [],
  guarded: true,
  predefinedProperties: ['guarded'],
  "properties":{}
}) as EventDto

export const otherEvents = (): EventDto[] => [

  {
    id: "compliance-meeting", type: campaignType
    , name: someMultiLang("Compliance Committe Meeting"), description: someMultiLang("Compliance Committe Meeting.")
    , lifecycle: somePastLifecycle('active'),
    cardinality: eventcardinalities["one"], 
    managed: true,
    tags: [],
    predefinedProperties: [],
    "properties":{}
  }
  ,

  { "type": campaignType, 
    managed: false, 
    cardinality: eventcardinalities["any"], 
    "tags": [], 
    "id": "bday", 
    "name": { "en": "Fabio's Birthday", "fr": "Anniversaire de Fabio" }, 
    "description": { "en": "A day for reflection and thought.", "fr": "Une journée de réflexion." }, 
    "lifecycle": { "state": "inactive", "created": 1584481283072 },
    "predefinedProperties" : [],
    "properties":{}
  }


]

export const campaignEvents = (): EventDto[] => [startEvent(), endEvent(), ...otherEvents()]



const mockEvents = () => {

  [...campaignEvents()].forEach(e => { e.tags.push(defaultContext); mockevents.push(e) })
}


const startDate = (c: CampaignDto) => pastDate(c, campaignStartEvent, c.id)
const endDate = (c: CampaignDto) => pastDate(c, campaignEndEvent, c.id)

const summaryOf = (c: CampaignDto) => ({

  startDate: startDate(c),
  endDate: endDate(c)

})

const update = (c: CampaignDto | undefined): CampaignAndSummary => {

  if (!c)
    return undefined!

  const summary = summaryOf(c)

  const state = summary.startDate ? summary.endDate ? 'closed' : 'open' : c.lifecycle.state
  const dto: CampaignDto = { ...c, lifecycle: { ...c.lifecycle, state } as any }


  return { campaign: dto, summary }

}


export const someCampaign = (name: string): CampaignDto => {

  return {
    id: idify(name),
    name: someMultiLang(name),
    description: someMultiLang("description"),
    lifecycle: somePastLifecycle(),
    tags: [
      randomIn(systemapi(globalstate).allContexts()).id, ...randomTagRefsFor(campaignType)
    ],
    properties: { statisticalHorizon: 30, complianceScale: complianceScaleCategory, approveCycle: 'all', adminCanEdit: undefined, adminCanSubmit: undefined },

  }
}






export const predefinedCategories = (): TagCategory[] => {

  // const t = intlapi(globalstate).getT()

  return [{

    id: complianceScaleCategory,
    type: complianceType,
    name: { en: "Default Scale", fr: "Échelle par Défaut" },
    description: { en: "A predefined vocabulary to assess the compliance of a party to one or more obligations.", fr: "Vocabulaire prédéfini pour évaluer la conformité d'une partie à une ou plusieurs obligations." },
    lifecycle: { state: 'active' } as any,
    predefined: true,
    properties: {
      color: "#e65100",
      field: {
        title: { en: "Level", fr: "Niveau" },
        type: 'radio',
        enabled: true,
        message: { en: "Select one compliance level.", fr: "Sélectionnez un niveau de conformité." },
        help: { en: "A compliance level measures the extent and quality of the actions taken by a party to meet its obligations.", fr: "Un niveau de conformité mesure l'étendue et la qualité des mesures prises par une partie pour s'acquitter de ses obligations." }
      }
    },
    cardinality: cardinalities.one,


  }]

}





export const predefinedTags = (): Tag[] => {

  const t = intlapi(globalstate).getT()

  return [
    {
      id: "TG-prod-default-not_compliant",
      type: complianceType,
      lifecycle: { state: 'active' } as any,
      name:
        { "en": t("campaign.tags.not_compliant.name.en"), "fr": t("campaign.tags.not_compliant.name.fr") },
      description:
        { "en": t("campaign.tags.not_compliant.description.en"), "fr": t("campaign.tags.not_compliant.description.fr") },
      category: complianceScaleCategory,
      properties: { value: 0 }
    }
    ,
    {
      id: "TG-prod-default-partially_compliant",
      type: complianceType,
      lifecycle: { state: 'active' } as any,
      name:
        { "en": t("campaign.tags.partially_compliant.name.en"), "fr": t("campaign.tags.partially_compliant.name.fr") },
      description:
        { "en": t("campaign.tags.partially_compliant.description.en"), "fr": t("campaign.tags.partially_compliant.description.fr") },
      category: complianceScaleCategory,
      properties: { value: 10 }
    }
    ,
    {
      id: "TG-prod-default-compliant",
      type: complianceType,
      name: { "en": t("campaign.tags.compliant.name.en"), "fr": t("campaign.tags.compliant.name.fr") },
      description:
        { "en": t("campaign.tags.compliant.description.en"), "fr": t("campaign.tags.compliant.description.fr") },
      lifecycle: { state: 'active' } as any,
      category: complianceScaleCategory,
      properties: { value: 30 }
    }

  ]

}




const otherPredefinedCategories: TagCategory[] = [


  {
    "type": complianceType,
    "name": {
      "en": "Other Scale",
      "fr": "Autre échelle"
    },
    "description": {
      "en": "This is a compliance scale for tests",
      "fr": "Il s'agit d'une échelle de conformité pour les tests"
    },
    lifecycle: { state: 'active' } as any,
    "predefined": false,
    "id": "TGC-compliance-testscale",
    properties: {
      "color": "#008aa7",
      "field": {
        "type": 'radio',
        "enabled": true,
        "title": { "en": "Level", "fr": "Niveau" },
        "message": { "en": "Select one compliance level", "fr": " Sélectionnez un niveau de conformité." },
        "help": { "en": "A compliance level measures the extent and quality of the actions taken by a party to meet its obligations.", "fr": "Un niveau de conformité mesure l'étendue et la qualité des mesures prises par une partie pour s'acquitter de ses obligations." }
      }
    },
    "cardinality": { "min": 1, "max": 1 }
  }

]



const otherPredefinedTags: Tag[] = [


  { lifecycle: { state: 'active' } as any, "type": complianceType, "name": { "en": "Not Compliant test", "fr": "Not Compliant test" }, "description": { "fr": "Not Compliant value", "en": "Not Compliant value" }, "id": "TG-prod-test-notcomplianttest", "category": `TGC-compliance-testscale`, properties: { value: 0 } }

  ,

  { lifecycle: { state: 'active' } as any, "type": complianceType, "name": { "en": "Partially Compliant test", "fr": "Partially Compliant test" }, "description": { "fr": "Partially Compliant value", "en": "Partially Compliant value" }, "id": "TG-prod-test-partiallycomplianttest", "category": `TGC-compliance-testscale`, properties: { value: 5 } }

  ,

  { lifecycle: { state: 'active' } as any, "type": complianceType, "name": { "en": "Quite Compliant test", "fr": "Quite Compliant test" }, "description": { "fr": "Quite Compliant value", "en": "Quite Compliant value" }, "id": "TG-prod-test-quitecomplianttest", "category": `TGC-compliance-testscale`, properties: { value: 10 } }

  ,

  { lifecycle: { state: 'active' } as any, "type": complianceType, "name": { "en": "Almost Compliant test", "fr": "Almost Compliant test" }, "description": { "fr": "Almost Compliant value", "en": "Almost Compliant value" }, "id": "TG-prod-test-almostcomplianttest", "category": `TGC-compliance-testscale`, properties: { value: 20 } }

  ,

  { lifecycle: { state: 'active' } as any, "type": complianceType, "name": { "en": "Compliant test", "fr": "Compliant test" }, "description": { "fr": "Compliant value", "en": "Compliant value" }, "id": "TG-prod-test-complianttest", "category": `TGC-compliance-testscale`, properties: { value: 50 } }

]


export const useSimulatedMessage = () => {

  const state = useEmarisState()

  const campaignsapi = campaignapi(state)

  const postMessage = (type: typeof campaignType | typeof tenantType | typeof requirementType | typeof productType | typeof submissionType) => () => {

    const campaign = randomIn(campaigns().all().map(c => update(c).campaign as Campaign).filter(c => summaryOf(c).startDate && !summaryOf(c).endDate))
    const party = randomIn(partyInstances(campaign.id).all())!
    const requirement = randomIn(requirementInstances(campaign.id).all().filter(r => r.campaign === campaign.id))!
    const product = randomIn(productInstances(campaign.id).all().filter(p => p.campaign === campaign.id))!
    const trail = randomIn(trailsFor(campaign.id).all().filter(trail => trail.key.campaign === campaign.id && trail.submissions.length > 0 && (trail.key.asset === requirement.source || trail.key.asset === product.source) && trail.key.party === party.source))
    const submission = randomIn(trail.submissions)

    const campaignTopics = campaignsapi.topics(campaign)
    const partyTopics = partyinstapi(state)(campaign).topics(party)
    const requirementTopics = requirementinstapi(state)(campaign).topics(requirement)
    const productTopics = productinstapi(state)(campaign).topics(product)
    const submissionTopics = submissionmodelapi(state)(campaign).topics(submission)

    const submissionAssetTopics = trail.key.assetType === requirementType ? requirementTopics : productTopics

    const topics = type === campaignType ? campaignTopics :
      type === tenantType ? [...campaignTopics, ...partyTopics] :
        type === requirementType ? [...campaignTopics, ...requirementTopics, anyPartyTopic] :
          type === productType ? [...campaignTopics, ...productTopics, anyPartyTopic] :
            type === submissionType ? [...campaignTopics, ...partyTopics, ...submissionAssetTopics, ...submissionTopics] : []


    // This simulates a user from a party that makes sense for a message
    // const postingTenant = type === tenantType ? (u:UserDto) => u.tenant === party.source : 
    //                       type === submissionType ? (u:UserDto) => u.tenant === party.source || u.tenant === noTenant : 
    //                       (u:UserDto) => u.tenant === noTenant

    // Here is always IOTC
    const postingTenant = (u: UserDto) => u.tenant === noTenant

    const postingUser = randomIn(users().all().filter(u => postingTenant(u) && u.username !== getLogged().username))! as User

    const message = newMessage(`${postingUser.details.firstname} ${postingUser.details.lastname} says: ${randomParagraph({ min: 3, max: 15 })}`, topics, postingUser)

    messagecallapi(state).postMessage(message)

  }

  return <div style={{ display: "flex", flexDirection: 'column', alignItems: "flex-start" }}>
    <Button onClick={postMessage(campaignType)} iconLeft icn={messageFullIcon} noborder xsize="xsmall" >Simulate message for campaign.</Button>
    <Button onClick={postMessage(tenantType)} iconLeft icn={messageFullIcon} noborder xsize="xsmall" >Simulate message for party.</Button>
    <Button onClick={postMessage(requirementType)} iconLeft icn={messageFullIcon} noborder xsize="xsmall" >Simulate message for requirement.</Button>
    <Button onClick={postMessage(productType)} iconLeft icn={messageFullIcon} noborder xsize="xsmall" >Simulate message for product.</Button>
    <Button onClick={postMessage(submissionType)} iconLeft icn={messageFullIcon} noborder xsize="xsmall" >Simulate message for submission.</Button>
  </div>



}