import { globalstate } from "apprise-frontend/state/model"
import { defaultContext } from "apprise-frontend/system/constants"
import { cardinalities, Tag, TagCategory } from "apprise-frontend/tag/model"
import { deepclone, randomBoolean, randomIn } from "apprise-frontend/utils/common"
import { idify, mockRegistry, MockStore, mockstore, someMultiLang, somePastLifecycle } from "apprise-frontend/utils/mocks/mocks"
import { mockcategories, mockContexts, mocktags, randomTagRefsFor } from "apprise-frontend/utils/mocks/tags"
import MockAdapter from "axios-mock-adapter/types"
import { productDeadlineEvent } from "emaris-frontend/campaign/constants"
import { eventcardinalities, EventDto } from "emaris-frontend/event/model"
import { predefinedParameters, predefinedProductParameters } from "emaris-frontend/layout/parameters/constants"
import { productapi } from "emaris-frontend/product/api"
import { prodImportanceCategory, productType } from "emaris-frontend/product/constants"
import { completeOnAdd, Product, ProductDto } from "emaris-frontend/product/model"
import { mockevents } from "emaris-frontend/utils/mocks/events"
import { someLayout } from "./mocks"


export const mockProducts = (mock: MockAdapter, size?: number) => {

  mockRegistry.register(productType)

  mockCalls(mock, size)

  mockTags()

  mockEvents()

}


var productstore: MockStore<ProductDto>

export const products = (size?: number) => {

  if (productstore)
    return productstore

  let products: ProductDto[]

  const api = productapi(globalstate)

  if (api.areReady())
    products = api.all()

  else {

   console.log("generating mock products...")
  
   products = generateMockProducts(size)
 
  }

  productstore = mockstore(products)

  return productstore

}

const generateMockProducts = (size:number = 10) => {


  const mocknames = Array(size).fill(undefined).map((_, i) => `Sample Report ${i}`)

  const mockproducts = mocknames.map(n => someProduct(n))

  const products = [...distinguishedProducts, ...mockproducts]
  
  products.forEach(p => completeOnAdd(p, idify(p.name.en!)))

  return products
}


const mockTags = () => {

  predefinedCategories().forEach(c=>mockcategories.push(c))
  predefinedTags().forEach(t=>mocktags.push(t))

  const data: { cat: TagCategory, tags: Tag[] }[] = [
    {
      cat: { "type": "product", "name": { "en": "Sharing Level", "fr": "Niveau de partage" }, "description": { "en": "The sharing level of a report.", "fr": "Le niveau de partage d'un rapport." }, lifecycle: {state:'active'} as any,"predefined": true, "id": "TGC-prod-sharinglevel", properties: { "color": "#e65100", "field": { "enabled": true, "title": { "en": "Sharing Level", "fr": "Niveau de partage" }, "message": { "en": "Select one option.", "fr": " Sélectionnez une option." }, "help": { "en": "Shared reports are visible to all CPCs. Private report are only visible to the target CPC.", "fr": "Les rapports partagés sont visibles par tous les CPC. Les rapports privés ne sont visibles que par le CPC cible." } } }, "cardinality": { "min": 1, "max": 1 } }
      ,
      tags: [

        { lifecycle: {state:'active'} as any,"type": "product", "name": { "en": "Private", "fr": "Privé" }, "description": { "en": "Private to the target CPC.", "fr": "Privé au CPC cible." }, "id": "TG-prod-private", "category": "TGC-prod-sharinglevel", properties: {} }

        ,

        { lifecycle: {state:'active'} as any,"type": "product", "name": { "en": "Public", "fr": "Public" }, "description": { "fr": "Visible par tous les CPC", "en": "Visible to all CPCs" }, "id": "TG-prod-public", "category": "TGC-prod-sharinglevel", properties: {} }


      ]
    }
  ]

  data.forEach(({ cat, tags: ts }) => {

    mockcategories.push(cat)
    ts.forEach(t=>mocktags.push(t))

  })

}

export const PDeadlineEvent = () => ({
  id: productDeadlineEvent, type: productType
  , name: someMultiLang("Report Deadline"), description: someMultiLang("The deadline to submit a report.")
  , lifecycle: somePastLifecycle('active'),
  managed: true,
  cardinality: eventcardinalities["one"], 
  predefined: true,
  tags: [],
  predefinedProperties: [],
  properties:{}
}) as EventDto


export const productEvents = (): EventDto[] => [


  PDeadlineEvent()

]

const mockEvents = () => {

  productEvents().forEach(e => { e.tags.push(defaultContext); mockevents.push(e) })
}

const mockCalls = (mock: MockAdapter, size: number = 10) => {

  mock.onGet("/domain/product").reply(_ => [200, products(size).all()])

  mock.onPost("/domain/product").reply(({ data }) => {

    const model = JSON.parse(data)
    model.lifecycle = somePastLifecycle(model.lifecycle.state)
    return [200, products().add(model)]
  })

  mock.onPost("/domain/product/remove").reply(({ data }) => {

    (JSON.parse(data) as string[]).filter(id => products().delete)

    return [200]
  })
  mock.onGet(/\/domain\/product\/.+/).reply(({ url }) => [200, products().oneWith(url!.split('/').pop())])
  mock.onPut(/\/domain\/product\/.+/).reply(({ data }) => {

    const model = JSON.parse(data)
    model.lifecycle = somePastLifecycle(model.lifecycle.state)
    return [200, products().update(model)]
  })
  mock.onDelete(/\/domain\/product\/.+/).reply(({ url }) => [204, products().delete(url!.split('/').pop())])
}





export const someProduct = (name: string): Product => {

  const layout = someLayout(productType)

  const product = {
    
    id: idify(name),
    
    name: someMultiLang(name),
    description: someMultiLang("description"),
    
    lifecycle: somePastLifecycle(randomBoolean(.8) ? "active" : "inactive"),

    tags: [
      randomIn([defaultContext, ...mockContexts.map(t => t.id)
      ])!, ...randomTagRefsFor(productType)],

      // requirements: randomSlice(requirements().all(), 0, 10).map(r => r.id),

    audience: { terms: [] },
    userProfile: { terms: [] },
    
    properties: {
      layout,
      dependencies:[],
      versionable: true,
      editable: true,
      assessed: true
    }

  } as Product

  const predefinedparams = [...predefinedProductParameters(globalstate), ...predefinedParameters(globalstate)]

  predefinedparams.forEach(p => layout.parameters.unshift(deepclone(p)))

  return product
}


export const predefinedCategories = (): TagCategory[] => {

  return [
    {
      id: prodImportanceCategory,
      name: { en: "Importance", fr: "Importance" },
      description: { en: "Importance scale.", fr: "Échelle d'importance." },
      type: productType,
      properties: {
        color: "#2A8800",
        field: { enabled: true }
      },
      cardinality: cardinalities.one,
      lifecycle: {state:'active'} as any,
      predefined: true,

    }

  ]
}

export const predefinedTags = (): Tag[] => {

  return [

    {
      id: "TG-product-importancescale",
      type: productType,
      lifecycle: {state:'active'} as any,
      name:
        { en: "Very Important", fr: "Très important" },
      description: {},
      category: prodImportanceCategory,
      properties: { value: 100 }
    }
    ,
    {
      id: "TG-product-important",
      type: productType,
      lifecycle: {state:'active'} as any,
      name: { en: "Important", fr: "Important" },
      description: {},
      category: prodImportanceCategory,
      properties: { value: 75 }
    }
    ,
    {
      id: "TG-product-normal",
      type: productType,
      lifecycle: {state:'active'} as any,
      name: { en: "Normal", fr: "Ordinaire" },
      description: {},
      category: prodImportanceCategory,
      properties: { value: 50 }
    }
    ,

    {
      id: "TG-product-notveryimportant",
      type: productType,
      lifecycle: {state:'active'} as any,
      name:
        { en: "Not Very Important", fr: "Pas très important" },
      description: {},
      category: prodImportanceCategory,
      properties: { value: 10 }
    }
  ]

}



const distinguishedProducts: any[] = [

  {
    "audience": {
      "terms": [
        {
          "category": "tenant@state-type-roles",
          "op": "anyOf",
          "tags": [
            "TG-tenant-flag",
            "TG-tenant-port",
            "TG-tenant-market",
            "TG-tenant-coastal",
            "TG-tenant-crosstype"
          ]
        }
      ]
    },
    "description": {
      "en": "Status report for the year 2019",
      "fr": "Rapport d’état pour l’année 2019"
    },
    "id": "P-Cw_mXX8f",
    "lifecycle": {
      "created": 1569527995896,
      "lastModified": 1571860795896,
      "lastModifiedBy": "otholite",
      "state": "active"
    },
    "name": {
      "en": "Implementation Report 2019",
      "fr": "Rapport de mise en œuvre 2019"
    },
    "requirements": [],
    "tags": [
      "TG-system-defaultcontext",
      "TG-product-important",
      "TG-prod-public"
    ],
    "properties": {
      "layout": {
        "components": {
          "children": [
            {
              "children": [
                {
                  "id": "title-1584438022192",
                  "spec": "title",
                  "style": {
                    "spacing": {
                      "bottom": 0
                    },
                    "text": {
                      "alignment": "center",
                      "color": "#0b7278",
                      "sizeBaseline": "xxl"
                    }
                  },
                  "text": {
                    "en": "<strong>IOTC Agreement Article X Report of Implementation for the year <span class=\"mention\" contenteditable=\"false\" data-value=\"year\">@year</span></strong>",
                    "fr": "<strong>Accord de la CTOI Article X Rapport de mise en œuvre pour l’année <span class=\"mention\" contenteditable=\"false\" data-value=\"year\">@year</span></strong>"
                  }
                },
                {
                  "id": "title-1584439427277",
                  "spec": "title",
                  "style": {
                    "background": {
                      "allStyles": {
                        "background": {
                          "allStyles": {
                            "spacing": {
                              "bottom": 25
                            },
                            "text": {
                              "alignment": "center",
                              "color": "#0b7278",
                              "size": "sm",
                              "sizeBaseline": "xxl"
                            }
                          },
                          "color": "#fff9c4",
                          "id": "background",
                          "valueprops": {}
                        },
                        "spacing": {
                          "bottom": 25
                        },
                        "text": {
                          "alignment": "center",
                          "color": "#ffffff",
                          "size": "sm",
                          "sizeBaseline": "xxl"
                        }
                      },
                      "color": "#b71c1c",
                      "id": "background",
                      "valueprops": {
                        "allStyles": {
                          "spacing": {
                            "bottom": 25
                          },
                          "text": {
                            "alignment": "center",
                            "color": "#0b7278",
                            "size": "sm",
                            "sizeBaseline": "xxl"
                          }
                        },
                        "color": "#fff9c4",
                        "id": "background",
                        "valueprops": {}
                      }
                    },
                    "spacing": {
                      "bottom": 7,
                      "top": 9
                    },
                    "text": {
                      "alignment": "center",
                      "color": "#ffffff",
                      "size": "sm",
                      "sizeBaseline": "xxl"
                    }
                  },
                  "text": {
                    "en": "Deadline for Submission of the Report: <span class=\"mention\" contenteditable=\"false\" data-value=\"deadline\">@deadline</span>",
                    "fr": "Date Limite De Soumission Du Rapport: <span class=\"mention\" contenteditable=\"false\" data-value=\"deadline\">@deadline</span>"
                  }
                },
                {
                  "id": "paragraph-1584438057223",
                  "spec": "paragraph",
                  "style": {
                    "spacing": {
                      "bottom": 0,
                      "top": 5
                    }
                  },
                  "text": {
                    "en": "Reporting CPC: <strong><span class=\"mention\" contenteditable=\"false\" data-value=\"party.name\">@party.name</span></strong>",
                    "fr": "Reporting CPC: <strong><span class=\"mention\" contenteditable=\"false\" data-value=\"party.name\">@party.name</span></strong>"
                  }
                },
                {
                  "id": "paragraph-1584438104875",
                  "spec": "paragraph",
                  "style": {
                    "background": {
                      "allStyles": {
                        "background": {
                          "allStyles": {},
                          "color": "#b71c1c",
                          "id": "background",
                          "valueprops": {}
                        },
                        "text": {}
                      },
                      "id": "background",
                      "valueprops": {
                        "allStyles": {},
                        "color": "#b71c1c",
                        "id": "background",
                        "valueprops": {}
                      }
                    },
                    "spacing": {
                      "bottom": 5,
                      "top": 0
                    },
                    "text": {}
                  },
                  "text": {
                    "en": "Date of submission: <strong><span class=\"mention\" contenteditable=\"false\" data-value=\"submission-date\">@submission-date</span></strong>"
                  }
                },
                {
                  "id": "paragraph-1584438142165",
                  "spec": "paragraph",
                  "style": {},
                  "text": {
                    "en": "<strong>Please NOTE:</strong> this document is composed of 3 sections to report on the implementation of IOTC Resolutions.",
                    "fr": "<strong>NOTE</strong>: ce document est composé de 3 sections pour rapporter sur la mise en œuvre des résolutions de la CTOI."
                  }
                },
                {
                  "id": "paragraph-1584438393465",
                  "spec": "paragraph",
                  "style": {},
                  "text": {
                    "en": "<strong>PART A: </strong>Describe the actions taken, under national legislation, in the previous year to implement conservation and management measures adopted by the Commission at its XX Session.",
                    "fr": "<strong>PART A: </strong>Décrire les actions prises au cours de l’année écoulée, dans le cadre de la législation nationale, pour appliquer les mesures de conservation et de gestion adoptées par la Commission lors de sa vingt-et-unième session.&nbsp;"
                  }
                },
                {
                  "id": "paragraph-1584486933660",
                  "spec": "paragraph",
                  "style": {
                    "background": {
                      "allStyles": {
                        "background": {
                          "allStyles": {
                            "text": {
                              "color": "#000000"
                            }
                          },
                          "color": "#607d8b",
                          "id": "background",
                          "valueprops": {}
                        },
                        "text": {
                          "color": "#000000"
                        }
                      },
                      "color": "#90a4ae",
                      "id": "background",
                      "valueprops": {
                        "allStyles": {
                          "text": {
                            "color": "#000000"
                          }
                        },
                        "color": "#607d8b",
                        "id": "background",
                        "valueprops": {}
                      }
                    },
                    "border": {
                      "all": 7,
                      "bottom": 7,
                      "color": "#000000",
                      "left": 7,
                      "right": 7,
                      "top": 7
                    },
                    "height": {
                      "allStyles": {
                        "background": {
                          "allStyles": {
                            "background": {
                              "allStyles": {
                                "text": {
                                  "color": "#000000"
                                }
                              },
                              "color": "#607d8b",
                              "id": "background",
                              "valueprops": {}
                            },
                            "text": {
                              "color": "#000000"
                            }
                          },
                          "color": "#90a4ae",
                          "id": "background",
                          "valueprops": {
                            "allStyles": {
                              "text": {
                                "color": "#000000"
                              }
                            },
                            "color": "#607d8b",
                            "id": "background",
                            "valueprops": {}
                          }
                        },
                        "height": {
                          "allStyles": {
                            "background": {
                              "allStyles": {
                                "background": {
                                  "allStyles": {
                                    "text": {
                                      "color": "#000000"
                                    }
                                  },
                                  "color": "#607d8b",
                                  "id": "background",
                                  "valueprops": {}
                                },
                                "text": {
                                  "color": "#000000"
                                }
                              },
                              "color": "#90a4ae",
                              "id": "background",
                              "valueprops": {
                                "allStyles": {
                                  "text": {
                                    "color": "#000000"
                                  }
                                },
                                "color": "#607d8b",
                                "id": "background",
                                "valueprops": {}
                              }
                            },
                            "text": {
                              "color": "#000000"
                            }
                          },
                          "id": "height",
                          "value": 6,
                          "valueprops": {}
                        },
                        "text": {
                          "color": "#000000"
                        }
                      },
                      "id": "height",
                      "value": 10,
                      "valueprops": {
                        "allStyles": {
                          "background": {
                            "allStyles": {
                              "background": {
                                "allStyles": {
                                  "text": {
                                    "color": "#000000"
                                  }
                                },
                                "color": "#607d8b",
                                "id": "background",
                                "valueprops": {}
                              },
                              "text": {
                                "color": "#000000"
                              }
                            },
                            "color": "#90a4ae",
                            "id": "background",
                            "valueprops": {
                              "allStyles": {
                                "text": {
                                  "color": "#000000"
                                }
                              },
                              "color": "#607d8b",
                              "id": "background",
                              "valueprops": {}
                            }
                          },
                          "text": {
                            "color": "#000000"
                          }
                        },
                        "id": "height",
                        "value": 6,
                        "valueprops": {}
                      }
                    },
                    "spacing": {
                      "bottom": 0,
                      "left": 0,
                      "top": 2
                    },
                    "text": {
                      "color": "#000000"
                    },
                    "width": {
                      "allStyles": {
                        "background": {
                          "allStyles": {
                            "background": {
                              "allStyles": {
                                "text": {
                                  "color": "#000000"
                                }
                              },
                              "color": "#607d8b",
                              "id": "background",
                              "valueprops": {}
                            },
                            "text": {
                              "color": "#000000"
                            }
                          },
                          "color": "#90a4ae",
                          "id": "background",
                          "valueprops": {
                            "allStyles": {
                              "text": {
                                "color": "#000000"
                              }
                            },
                            "color": "#607d8b",
                            "id": "background",
                            "valueprops": {}
                          }
                        },
                        "border": {
                          "all": 7,
                          "bottom": 7,
                          "color": "#000000",
                          "left": 7,
                          "right": 7,
                          "top": 7
                        },
                        "height": {
                          "allStyles": {
                            "background": {
                              "allStyles": {
                                "background": {
                                  "allStyles": {
                                    "text": {
                                      "color": "#000000"
                                    }
                                  },
                                  "color": "#607d8b",
                                  "id": "background",
                                  "valueprops": {}
                                },
                                "text": {
                                  "color": "#000000"
                                }
                              },
                              "color": "#90a4ae",
                              "id": "background",
                              "valueprops": {
                                "allStyles": {
                                  "text": {
                                    "color": "#000000"
                                  }
                                },
                                "color": "#607d8b",
                                "id": "background",
                                "valueprops": {}
                              }
                            },
                            "height": {
                              "allStyles": {
                                "background": {
                                  "allStyles": {
                                    "background": {
                                      "allStyles": {
                                        "text": {
                                          "color": "#000000"
                                        }
                                      },
                                      "color": "#607d8b",
                                      "id": "background",
                                      "valueprops": {}
                                    },
                                    "text": {
                                      "color": "#000000"
                                    }
                                  },
                                  "color": "#90a4ae",
                                  "id": "background",
                                  "valueprops": {
                                    "allStyles": {
                                      "text": {
                                        "color": "#000000"
                                      }
                                    },
                                    "color": "#607d8b",
                                    "id": "background",
                                    "valueprops": {}
                                  }
                                },
                                "text": {
                                  "color": "#000000"
                                }
                              },
                              "id": "height",
                              "value": 6,
                              "valueprops": {}
                            },
                            "text": {
                              "color": "#000000"
                            }
                          },
                          "id": "height",
                          "value": 10,
                          "valueprops": {
                            "allStyles": {
                              "background": {
                                "allStyles": {
                                  "background": {
                                    "allStyles": {
                                      "text": {
                                        "color": "#000000"
                                      }
                                    },
                                    "color": "#607d8b",
                                    "id": "background",
                                    "valueprops": {}
                                  },
                                  "text": {
                                    "color": "#000000"
                                  }
                                },
                                "color": "#90a4ae",
                                "id": "background",
                                "valueprops": {
                                  "allStyles": {
                                    "text": {
                                      "color": "#000000"
                                    }
                                  },
                                  "color": "#607d8b",
                                  "id": "background",
                                  "valueprops": {}
                                }
                              },
                              "text": {
                                "color": "#000000"
                              }
                            },
                            "id": "height",
                            "value": 6,
                            "valueprops": {}
                          }
                        },
                        "spacing": {
                          "bottom": 5,
                          "left": 0,
                          "top": 5
                        },
                        "text": {
                          "color": "#000000"
                        },
                        "width": {
                          "allStyles": {
                            "background": {
                              "allStyles": {
                                "background": {
                                  "allStyles": {
                                    "text": {
                                      "color": "#000000"
                                    }
                                  },
                                  "color": "#607d8b",
                                  "id": "background",
                                  "valueprops": {}
                                },
                                "text": {
                                  "color": "#000000"
                                }
                              },
                              "color": "#90a4ae",
                              "id": "background",
                              "valueprops": {
                                "allStyles": {
                                  "text": {
                                    "color": "#000000"
                                  }
                                },
                                "color": "#607d8b",
                                "id": "background",
                                "valueprops": {}
                              }
                            },
                            "border": {
                              "all": 7,
                              "bottom": 7,
                              "color": "#000000",
                              "left": 7,
                              "right": 7,
                              "top": 7
                            },
                            "height": {
                              "allStyles": {
                                "background": {
                                  "allStyles": {
                                    "background": {
                                      "allStyles": {
                                        "text": {
                                          "color": "#000000"
                                        }
                                      },
                                      "color": "#607d8b",
                                      "id": "background",
                                      "valueprops": {}
                                    },
                                    "text": {
                                      "color": "#000000"
                                    }
                                  },
                                  "color": "#90a4ae",
                                  "id": "background",
                                  "valueprops": {
                                    "allStyles": {
                                      "text": {
                                        "color": "#000000"
                                      }
                                    },
                                    "color": "#607d8b",
                                    "id": "background",
                                    "valueprops": {}
                                  }
                                },
                                "height": {
                                  "allStyles": {
                                    "background": {
                                      "allStyles": {
                                        "background": {
                                          "allStyles": {
                                            "text": {
                                              "color": "#000000"
                                            }
                                          },
                                          "color": "#607d8b",
                                          "id": "background",
                                          "valueprops": {}
                                        },
                                        "text": {
                                          "color": "#000000"
                                        }
                                      },
                                      "color": "#90a4ae",
                                      "id": "background",
                                      "valueprops": {
                                        "allStyles": {
                                          "text": {
                                            "color": "#000000"
                                          }
                                        },
                                        "color": "#607d8b",
                                        "id": "background",
                                        "valueprops": {}
                                      }
                                    },
                                    "text": {
                                      "color": "#000000"
                                    }
                                  },
                                  "id": "height",
                                  "value": 6,
                                  "valueprops": {}
                                },
                                "text": {
                                  "color": "#000000"
                                }
                              },
                              "id": "height",
                              "value": 10,
                              "valueprops": {
                                "allStyles": {
                                  "background": {
                                    "allStyles": {
                                      "background": {
                                        "allStyles": {
                                          "text": {
                                            "color": "#000000"
                                          }
                                        },
                                        "color": "#607d8b",
                                        "id": "background",
                                        "valueprops": {}
                                      },
                                      "text": {
                                        "color": "#000000"
                                      }
                                    },
                                    "color": "#90a4ae",
                                    "id": "background",
                                    "valueprops": {
                                      "allStyles": {
                                        "text": {
                                          "color": "#000000"
                                        }
                                      },
                                      "color": "#607d8b",
                                      "id": "background",
                                      "valueprops": {}
                                    }
                                  },
                                  "text": {
                                    "color": "#000000"
                                  }
                                },
                                "id": "height",
                                "value": 6,
                                "valueprops": {}
                              }
                            },
                            "spacing": {
                              "bottom": 5,
                              "left": 0,
                              "top": 5
                            },
                            "text": {
                              "color": "#000000"
                            }
                          },
                          "id": "width",
                          "percentage": 89,
                          "valueprops": {}
                        }
                      },
                      "id": "width",
                      "percentage": 100,
                      "valueprops": {
                        "allStyles": {
                          "background": {
                            "allStyles": {
                              "background": {
                                "allStyles": {
                                  "text": {
                                    "color": "#000000"
                                  }
                                },
                                "color": "#607d8b",
                                "id": "background",
                                "valueprops": {}
                              },
                              "text": {
                                "color": "#000000"
                              }
                            },
                            "color": "#90a4ae",
                            "id": "background",
                            "valueprops": {
                              "allStyles": {
                                "text": {
                                  "color": "#000000"
                                }
                              },
                              "color": "#607d8b",
                              "id": "background",
                              "valueprops": {}
                            }
                          },
                          "border": {
                            "all": 7,
                            "bottom": 7,
                            "color": "#000000",
                            "left": 7,
                            "right": 7,
                            "top": 7
                          },
                          "height": {
                            "allStyles": {
                              "background": {
                                "allStyles": {
                                  "background": {
                                    "allStyles": {
                                      "text": {
                                        "color": "#000000"
                                      }
                                    },
                                    "color": "#607d8b",
                                    "id": "background",
                                    "valueprops": {}
                                  },
                                  "text": {
                                    "color": "#000000"
                                  }
                                },
                                "color": "#90a4ae",
                                "id": "background",
                                "valueprops": {
                                  "allStyles": {
                                    "text": {
                                      "color": "#000000"
                                    }
                                  },
                                  "color": "#607d8b",
                                  "id": "background",
                                  "valueprops": {}
                                }
                              },
                              "height": {
                                "allStyles": {
                                  "background": {
                                    "allStyles": {
                                      "background": {
                                        "allStyles": {
                                          "text": {
                                            "color": "#000000"
                                          }
                                        },
                                        "color": "#607d8b",
                                        "id": "background",
                                        "valueprops": {}
                                      },
                                      "text": {
                                        "color": "#000000"
                                      }
                                    },
                                    "color": "#90a4ae",
                                    "id": "background",
                                    "valueprops": {
                                      "allStyles": {
                                        "text": {
                                          "color": "#000000"
                                        }
                                      },
                                      "color": "#607d8b",
                                      "id": "background",
                                      "valueprops": {}
                                    }
                                  },
                                  "text": {
                                    "color": "#000000"
                                  }
                                },
                                "id": "height",
                                "value": 6,
                                "valueprops": {}
                              },
                              "text": {
                                "color": "#000000"
                              }
                            },
                            "id": "height",
                            "value": 10,
                            "valueprops": {
                              "allStyles": {
                                "background": {
                                  "allStyles": {
                                    "background": {
                                      "allStyles": {
                                        "text": {
                                          "color": "#000000"
                                        }
                                      },
                                      "color": "#607d8b",
                                      "id": "background",
                                      "valueprops": {}
                                    },
                                    "text": {
                                      "color": "#000000"
                                    }
                                  },
                                  "color": "#90a4ae",
                                  "id": "background",
                                  "valueprops": {
                                    "allStyles": {
                                      "text": {
                                        "color": "#000000"
                                      }
                                    },
                                    "color": "#607d8b",
                                    "id": "background",
                                    "valueprops": {}
                                  }
                                },
                                "text": {
                                  "color": "#000000"
                                }
                              },
                              "id": "height",
                              "value": 6,
                              "valueprops": {}
                            }
                          },
                          "spacing": {
                            "bottom": 5,
                            "left": 0,
                            "top": 5
                          },
                          "text": {
                            "color": "#000000"
                          }
                        },
                        "id": "width",
                        "percentage": 89,
                        "valueprops": {}
                      }
                    }
                  },
                  "text": {
                    "en": "Resolution 17/03"
                  }
                },
                {
                  "id": "paragraph-1584488488360",
                  "spec": "paragraph",
                  "style": {
                    "spacing": {
                      "bottom": 5,
                      "top": 0
                    }
                  },
                  "text": {
                    "en": "<strong><em><span class=\"mention\" contenteditable=\"false\" data-value=\"17/03.source\">@17/03.source</span></em></strong>",
                    "fr": "<strong><em><span class=\"mention\" contenteditable=\"false\" data-value=\"17/03.source\">@17/03.source</span></em></strong>"
                  }
                },
                {
                  "id": "paragraph-1584488888520",
                  "spec": "paragraph",
                  "style": {
                    "spacing": {
                      "bottom": 5,
                      "top": 0
                    }
                  },
                  "text": {
                    "en": "EU Member States and EU Operators in the Indian Ocean have been officially notified of this resolution and must comply with its provisions and with any related Conservation and Management Measure adopted by the IOTC. The EU has transmitted to the Secretariat relevant information on alleged IUU fishing activities in the IOTC area of competence, notably collected from information provided by the EU Naval Force operating in the Indian Ocean or from other sources of information collected by the IUU fishing experts in DG MARE. In 2018 the EU might propose the inclusion of some vessels in the IOTC IUU list.&nbsp;",
                    "fr": "Les États membres de l'UE et les opérateurs de l'UE dans l'océan Indien ont été officiellement notifiés de cette résolution et doivent se conformer à ses dispositions et à toute mesure de conservation et de gestion connexe adoptée par la CTOI. L’UE a transmis au Secrétariat les informations pertinentes sur des activités de pêche INN présumées dans la zone de compétence de la CTOI, compilées notamment à partir des données provenant de la Force navale de l’UE opérant dans l’Océan Indien ou d’autres sources d’informations recueillies par les experts sur la pêche INN de la DG MARE. En 2018, l’UE pourrait proposer l’inclusion de certains navires dans la liste INN de la CTOI.&nbsp;"
                  }
                },
                {
                  "id": "paragraph-1584488456733",
                  "spec": "paragraph",
                  "style": {
                    "background": {
                      "allStyles": {
                        "background": {
                          "allStyles": {
                            "text": {
                              "color": "#000000"
                            }
                          },
                          "color": "#607d8b",
                          "id": "background",
                          "valueprops": {}
                        },
                        "text": {
                          "color": "#000000"
                        }
                      },
                      "color": "#90a4ae",
                      "id": "background",
                      "valueprops": {
                        "allStyles": {
                          "text": {
                            "color": "#000000"
                          }
                        },
                        "color": "#607d8b",
                        "id": "background",
                        "valueprops": {}
                      }
                    },
                    "border": {
                      "all": 7,
                      "bottom": 7,
                      "color": "#000000",
                      "left": 7,
                      "right": 7,
                      "top": 7
                    },
                    "height": {
                      "allStyles": {
                        "background": {
                          "allStyles": {
                            "background": {
                              "allStyles": {
                                "text": {
                                  "color": "#000000"
                                }
                              },
                              "color": "#607d8b",
                              "id": "background",
                              "valueprops": {}
                            },
                            "text": {
                              "color": "#000000"
                            }
                          },
                          "color": "#90a4ae",
                          "id": "background",
                          "valueprops": {
                            "allStyles": {
                              "text": {
                                "color": "#000000"
                              }
                            },
                            "color": "#607d8b",
                            "id": "background",
                            "valueprops": {}
                          }
                        },
                        "height": {
                          "allStyles": {
                            "background": {
                              "allStyles": {
                                "background": {
                                  "allStyles": {
                                    "text": {
                                      "color": "#000000"
                                    }
                                  },
                                  "color": "#607d8b",
                                  "id": "background",
                                  "valueprops": {}
                                },
                                "text": {
                                  "color": "#000000"
                                }
                              },
                              "color": "#90a4ae",
                              "id": "background",
                              "valueprops": {
                                "allStyles": {
                                  "text": {
                                    "color": "#000000"
                                  }
                                },
                                "color": "#607d8b",
                                "id": "background",
                                "valueprops": {}
                              }
                            },
                            "text": {
                              "color": "#000000"
                            }
                          },
                          "id": "height",
                          "value": 6,
                          "valueprops": {}
                        },
                        "text": {
                          "color": "#000000"
                        }
                      },
                      "id": "height",
                      "value": 10,
                      "valueprops": {
                        "allStyles": {
                          "background": {
                            "allStyles": {
                              "background": {
                                "allStyles": {
                                  "text": {
                                    "color": "#000000"
                                  }
                                },
                                "color": "#607d8b",
                                "id": "background",
                                "valueprops": {}
                              },
                              "text": {
                                "color": "#000000"
                              }
                            },
                            "color": "#90a4ae",
                            "id": "background",
                            "valueprops": {
                              "allStyles": {
                                "text": {
                                  "color": "#000000"
                                }
                              },
                              "color": "#607d8b",
                              "id": "background",
                              "valueprops": {}
                            }
                          },
                          "text": {
                            "color": "#000000"
                          }
                        },
                        "id": "height",
                        "value": 6,
                        "valueprops": {}
                      }
                    },
                    "spacing": {
                      "bottom": 0,
                      "left": 0,
                      "top": 2
                    },
                    "text": {
                      "color": "#000000"
                    },
                    "width": {
                      "allStyles": {
                        "background": {
                          "allStyles": {
                            "background": {
                              "allStyles": {
                                "text": {
                                  "color": "#000000"
                                }
                              },
                              "color": "#607d8b",
                              "id": "background",
                              "valueprops": {}
                            },
                            "text": {
                              "color": "#000000"
                            }
                          },
                          "color": "#90a4ae",
                          "id": "background",
                          "valueprops": {
                            "allStyles": {
                              "text": {
                                "color": "#000000"
                              }
                            },
                            "color": "#607d8b",
                            "id": "background",
                            "valueprops": {}
                          }
                        },
                        "border": {
                          "all": 7,
                          "bottom": 7,
                          "color": "#000000",
                          "left": 7,
                          "right": 7,
                          "top": 7
                        },
                        "height": {
                          "allStyles": {
                            "background": {
                              "allStyles": {
                                "background": {
                                  "allStyles": {
                                    "text": {
                                      "color": "#000000"
                                    }
                                  },
                                  "color": "#607d8b",
                                  "id": "background",
                                  "valueprops": {}
                                },
                                "text": {
                                  "color": "#000000"
                                }
                              },
                              "color": "#90a4ae",
                              "id": "background",
                              "valueprops": {
                                "allStyles": {
                                  "text": {
                                    "color": "#000000"
                                  }
                                },
                                "color": "#607d8b",
                                "id": "background",
                                "valueprops": {}
                              }
                            },
                            "height": {
                              "allStyles": {
                                "background": {
                                  "allStyles": {
                                    "background": {
                                      "allStyles": {
                                        "text": {
                                          "color": "#000000"
                                        }
                                      },
                                      "color": "#607d8b",
                                      "id": "background",
                                      "valueprops": {}
                                    },
                                    "text": {
                                      "color": "#000000"
                                    }
                                  },
                                  "color": "#90a4ae",
                                  "id": "background",
                                  "valueprops": {
                                    "allStyles": {
                                      "text": {
                                        "color": "#000000"
                                      }
                                    },
                                    "color": "#607d8b",
                                    "id": "background",
                                    "valueprops": {}
                                  }
                                },
                                "text": {
                                  "color": "#000000"
                                }
                              },
                              "id": "height",
                              "value": 6,
                              "valueprops": {}
                            },
                            "text": {
                              "color": "#000000"
                            }
                          },
                          "id": "height",
                          "value": 10,
                          "valueprops": {
                            "allStyles": {
                              "background": {
                                "allStyles": {
                                  "background": {
                                    "allStyles": {
                                      "text": {
                                        "color": "#000000"
                                      }
                                    },
                                    "color": "#607d8b",
                                    "id": "background",
                                    "valueprops": {}
                                  },
                                  "text": {
                                    "color": "#000000"
                                  }
                                },
                                "color": "#90a4ae",
                                "id": "background",
                                "valueprops": {
                                  "allStyles": {
                                    "text": {
                                      "color": "#000000"
                                    }
                                  },
                                  "color": "#607d8b",
                                  "id": "background",
                                  "valueprops": {}
                                }
                              },
                              "text": {
                                "color": "#000000"
                              }
                            },
                            "id": "height",
                            "value": 6,
                            "valueprops": {}
                          }
                        },
                        "spacing": {
                          "bottom": 5,
                          "left": 0,
                          "top": 5
                        },
                        "text": {
                          "color": "#000000"
                        },
                        "width": {
                          "allStyles": {
                            "background": {
                              "allStyles": {
                                "background": {
                                  "allStyles": {
                                    "text": {
                                      "color": "#000000"
                                    }
                                  },
                                  "color": "#607d8b",
                                  "id": "background",
                                  "valueprops": {}
                                },
                                "text": {
                                  "color": "#000000"
                                }
                              },
                              "color": "#90a4ae",
                              "id": "background",
                              "valueprops": {
                                "allStyles": {
                                  "text": {
                                    "color": "#000000"
                                  }
                                },
                                "color": "#607d8b",
                                "id": "background",
                                "valueprops": {}
                              }
                            },
                            "border": {
                              "all": 7,
                              "bottom": 7,
                              "color": "#000000",
                              "left": 7,
                              "right": 7,
                              "top": 7
                            },
                            "height": {
                              "allStyles": {
                                "background": {
                                  "allStyles": {
                                    "background": {
                                      "allStyles": {
                                        "text": {
                                          "color": "#000000"
                                        }
                                      },
                                      "color": "#607d8b",
                                      "id": "background",
                                      "valueprops": {}
                                    },
                                    "text": {
                                      "color": "#000000"
                                    }
                                  },
                                  "color": "#90a4ae",
                                  "id": "background",
                                  "valueprops": {
                                    "allStyles": {
                                      "text": {
                                        "color": "#000000"
                                      }
                                    },
                                    "color": "#607d8b",
                                    "id": "background",
                                    "valueprops": {}
                                  }
                                },
                                "height": {
                                  "allStyles": {
                                    "background": {
                                      "allStyles": {
                                        "background": {
                                          "allStyles": {
                                            "text": {
                                              "color": "#000000"
                                            }
                                          },
                                          "color": "#607d8b",
                                          "id": "background",
                                          "valueprops": {}
                                        },
                                        "text": {
                                          "color": "#000000"
                                        }
                                      },
                                      "color": "#90a4ae",
                                      "id": "background",
                                      "valueprops": {
                                        "allStyles": {
                                          "text": {
                                            "color": "#000000"
                                          }
                                        },
                                        "color": "#607d8b",
                                        "id": "background",
                                        "valueprops": {}
                                      }
                                    },
                                    "text": {
                                      "color": "#000000"
                                    }
                                  },
                                  "id": "height",
                                  "value": 6,
                                  "valueprops": {}
                                },
                                "text": {
                                  "color": "#000000"
                                }
                              },
                              "id": "height",
                              "value": 10,
                              "valueprops": {
                                "allStyles": {
                                  "background": {
                                    "allStyles": {
                                      "background": {
                                        "allStyles": {
                                          "text": {
                                            "color": "#000000"
                                          }
                                        },
                                        "color": "#607d8b",
                                        "id": "background",
                                        "valueprops": {}
                                      },
                                      "text": {
                                        "color": "#000000"
                                      }
                                    },
                                    "color": "#90a4ae",
                                    "id": "background",
                                    "valueprops": {
                                      "allStyles": {
                                        "text": {
                                          "color": "#000000"
                                        }
                                      },
                                      "color": "#607d8b",
                                      "id": "background",
                                      "valueprops": {}
                                    }
                                  },
                                  "text": {
                                    "color": "#000000"
                                  }
                                },
                                "id": "height",
                                "value": 6,
                                "valueprops": {}
                              }
                            },
                            "spacing": {
                              "bottom": 5,
                              "left": 0,
                              "top": 5
                            },
                            "text": {
                              "color": "#000000"
                            }
                          },
                          "id": "width",
                          "percentage": 89,
                          "valueprops": {}
                        }
                      },
                      "id": "width",
                      "percentage": 100,
                      "valueprops": {
                        "allStyles": {
                          "background": {
                            "allStyles": {
                              "background": {
                                "allStyles": {
                                  "text": {
                                    "color": "#000000"
                                  }
                                },
                                "color": "#607d8b",
                                "id": "background",
                                "valueprops": {}
                              },
                              "text": {
                                "color": "#000000"
                              }
                            },
                            "color": "#90a4ae",
                            "id": "background",
                            "valueprops": {
                              "allStyles": {
                                "text": {
                                  "color": "#000000"
                                }
                              },
                              "color": "#607d8b",
                              "id": "background",
                              "valueprops": {}
                            }
                          },
                          "border": {
                            "all": 7,
                            "bottom": 7,
                            "color": "#000000",
                            "left": 7,
                            "right": 7,
                            "top": 7
                          },
                          "height": {
                            "allStyles": {
                              "background": {
                                "allStyles": {
                                  "background": {
                                    "allStyles": {
                                      "text": {
                                        "color": "#000000"
                                      }
                                    },
                                    "color": "#607d8b",
                                    "id": "background",
                                    "valueprops": {}
                                  },
                                  "text": {
                                    "color": "#000000"
                                  }
                                },
                                "color": "#90a4ae",
                                "id": "background",
                                "valueprops": {
                                  "allStyles": {
                                    "text": {
                                      "color": "#000000"
                                    }
                                  },
                                  "color": "#607d8b",
                                  "id": "background",
                                  "valueprops": {}
                                }
                              },
                              "height": {
                                "allStyles": {
                                  "background": {
                                    "allStyles": {
                                      "background": {
                                        "allStyles": {
                                          "text": {
                                            "color": "#000000"
                                          }
                                        },
                                        "color": "#607d8b",
                                        "id": "background",
                                        "valueprops": {}
                                      },
                                      "text": {
                                        "color": "#000000"
                                      }
                                    },
                                    "color": "#90a4ae",
                                    "id": "background",
                                    "valueprops": {
                                      "allStyles": {
                                        "text": {
                                          "color": "#000000"
                                        }
                                      },
                                      "color": "#607d8b",
                                      "id": "background",
                                      "valueprops": {}
                                    }
                                  },
                                  "text": {
                                    "color": "#000000"
                                  }
                                },
                                "id": "height",
                                "value": 6,
                                "valueprops": {}
                              },
                              "text": {
                                "color": "#000000"
                              }
                            },
                            "id": "height",
                            "value": 10,
                            "valueprops": {
                              "allStyles": {
                                "background": {
                                  "allStyles": {
                                    "background": {
                                      "allStyles": {
                                        "text": {
                                          "color": "#000000"
                                        }
                                      },
                                      "color": "#607d8b",
                                      "id": "background",
                                      "valueprops": {}
                                    },
                                    "text": {
                                      "color": "#000000"
                                    }
                                  },
                                  "color": "#90a4ae",
                                  "id": "background",
                                  "valueprops": {
                                    "allStyles": {
                                      "text": {
                                        "color": "#000000"
                                      }
                                    },
                                    "color": "#607d8b",
                                    "id": "background",
                                    "valueprops": {}
                                  }
                                },
                                "text": {
                                  "color": "#000000"
                                }
                              },
                              "id": "height",
                              "value": 6,
                              "valueprops": {}
                            }
                          },
                          "spacing": {
                            "bottom": 5,
                            "left": 0,
                            "top": 5
                          },
                          "text": {
                            "color": "#000000"
                          }
                        },
                        "id": "width",
                        "percentage": 89,
                        "valueprops": {}
                      }
                    }
                  },
                  "text": {
                    "en": "Resolution 01/06"
                  }
                },
                {
                  "id": "paragraph-1584438537690",
                  "spec": "paragraph",
                  "style": {
                    "spacing": {
                      "bottom": 5,
                      "top": 0
                    }
                  },
                  "text": {
                    "en": "<strong><em><span class=\"mention\" contenteditable=\"false\" data-value=\"01/06.source\">@01/06.source</span></em></strong>",
                    "fr": "<strong><em><span class=\"mention\" contenteditable=\"false\" data-value=\"01/06.source\">@01/06.source</span></em></strong>"
                  }
                },
                {
                  "id": "paragraph-1584488987488",
                  "spec": "paragraph",
                  "style": {
                    "spacing": {
                      "bottom": 5,
                      "top": 0
                    }
                  },
                  "text": {
                    "en": "CPCs which export bigeye tuna shall examine export data upon receiving the import data from the Secretary, and report the results to the Commission annually [<em>A template report exists</em>].&nbsp;",
                    "fr": "Les CPC qui exportent du thon obèse doivent examiner les données d’exportation une fois les données d’importation transmises par le Secrétaire et faire rapport annuellement sur les résultats de cet examen. [<em>Un modèle de rapport ex</em>iste]&nbsp;"
                  }
                },
                {
                  "id": "paragraph-1584489464322",
                  "spec": "paragraph",
                  "style": {
                    "spacing": {
                      "bottom": 5,
                      "left": 5,
                      "top": 5
                    }
                  },
                  "text": {
                    "en": "<strong>Nil report, specify the reason:</strong>",
                    "fr": "<strong>Rapport NUL, spécifier la raison:&nbsp;</strong>"
                  }
                },
                {
                  "id": "select-box-1584489522153",
                  "inline": true,
                  "label": {},
                  "msg": {},
                  "multi": false,
                  "options": [
                    {
                      "default": false,
                      "id": "0",
                      "name": {
                        "en": "No Large scale longline vessels on the IOTC Record of Authorised Vessels (RAV)",
                        "fr": "Aucun grand navire palangrier sur le Registre de la CTOI"
                      },
                      "type": "default"
                    },
                    {
                      "default": false,
                      "id": "1",
                      "name": {
                        "en": "Do not export frozen big eye tuna",
                        "fr": "N’exporte pas de thons obèses congelés"
                      },
                      "type": "default"
                    }
                  ],
                  "required": true,
                  "spec": "select-box",
                  "style": {
                    "spacing": {
                      "left": 9
                    }
                  }
                },
                {
                  "id": "paragraph-1584438733721",
                  "spec": "paragraph",
                  "style": {
                    "spacing": {
                      "bottom": 5,
                      "left": 5,
                      "top": 5
                    }
                  },
                  "text": {
                    "en": "<strong>The report has already been provided to the IOTC Secretariat :</strong>&nbsp;",
                    "fr": "<strong>Le rapport a déjà été fourni au secrétariat de la CTO:</strong>"
                  }
                },
                {
                  "id": "select-box-1584438970045",
                  "inline": true,
                  "label": {},
                  "msg": {},
                  "multi": false,
                  "options": [
                    {
                      "default": false,
                      "id": "0",
                      "name": {
                        "en": "Yes",
                        "fr": "Oui"
                      },
                      "type": "default"
                    },
                    {
                      "default": false,
                      "id": "1",
                      "name": {
                        "en": "No",
                        "fr": "Non"
                      },
                      "type": "default"
                    }
                  ],
                  "required": true,
                  "spec": "select-box",
                  "style": {
                    "spacing": {
                      "left": 9
                    }
                  }
                },
                {
                  "id": "input-box-1584489912735",
                  "label": {
                    "en": "Date of Reporting"
                  },
                  "msg": {},
                  "multi": false,
                  "placeholder": [],
                  "required": true,
                  "rich": false,
                  "spec": "input-box",
                  "style": {
                    "spacing": {
                      "left": 7
                    }
                  }
                },
                {
                  "id": "input-box-1584489782859",
                  "label": {
                    "en": "Additional Information",
                    "fr": "Informations supplémentaires"
                  },
                  "msg": {},
                  "multi": false,
                  "required": false,
                  "rich": false,
                  "spec": "input-box",
                  "style": {}
                }
              ],
              "id": "page-1584438022192",
              "name": {},
              "spec": "page"
            }
          ],
          "id": "layout",
          "spec": "layout"
        },
        "parameters": [
          {
            "fixed": true,
            "id": "party",
            "name": "party",
            "required": true,
            "spec": "tenant"
          },
          {
            "fixed": true,
            "id": "campaign",
            "name": "campaign",
            "required": true,
            "spec": "campaign"
          },
          {
            "fixed": false,
            "id": "requirement-1584488430636",
            "name": "01/06",
            "required": false,
            "spec": "requirement",
            "value": { id: "bigeyetunaimport0106" }
          },
          {
            "fixed": false,
            "id": "requirement-1584486807570",
            "name": "17/03",
            "required": false,
            "spec": "requirement",
            "value": { id: "iuulistedvessels1703" }
          },
          {
            "fixed": false,
            "id": "number-1584484666105",
            "name": "year",
            "required": false,
            "spec": "number",
            "value": "2019"
          },
          {
            "fixed": false,
            "id": "text-1584484689418",
            "name": "deadline",
            "required": false,
            "spec": "text",
            "value": {}
          },
          {
            "fixed": false,
            "id": "text-1584485701489",
            "name": "submission-date",
            "required": false,
            "spec": "text",
            "value": {}
          }
        ]
      }
    }
  }
]