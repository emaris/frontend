import { globalstate } from "apprise-frontend/state/model"
import { noTenant, tenantType } from "apprise-frontend/tenant/constants"
import { Tenant } from "apprise-frontend/tenant/model"
import { withUser } from "apprise-frontend/user/model"
import { randomBoolean, randomNumber, randomNumberBetween, randomSlice } from "apprise-frontend/utils/common"
import { getLogged } from "apprise-frontend/utils/mocks/logged"
import { loremipsum, mockRegistry, mockstore, MockStore } from "apprise-frontend/utils/mocks/mocks"
import { tenants } from "apprise-frontend/utils/mocks/tenants"
import MockAdapter from "axios-mock-adapter/types"
import { eventinstapi } from "emaris-frontend/campaign/event/api"
import { Changes } from "emaris-frontend/campaign/event/calls"
import { absoluteOf, asDuration, EventDate, EventInstance, EventInstanceDto, isRelative } from "emaris-frontend/campaign/event/model"
import { partyinstapi } from "emaris-frontend/campaign/party/api"
import { productinstapi } from "emaris-frontend/campaign/product/api"
import { requirementinstapi } from "emaris-frontend/campaign/requirement/api"
import { eventType } from "emaris-frontend/event/constants"
import { EventDto } from "emaris-frontend/event/model"
import { events } from "emaris-frontend/utils/mocks/events"
import moment from "moment-timezone"
import shortid from "shortid"
import { campaignEndEvent, campaignStartEvent, splitId } from "../../campaign/constants"
import { AbsoluteDate } from "../../campaign/event/model"
import { Campaign, CampaignDto } from "../../campaign/model"
import { newPartyProperties, PartyInstance, PartyInstanceDto } from "../../campaign/party/model"
import { ProductInstance, ProductInstanceDto } from "../../campaign/product/model"
import { RequirementInstance, RequirementInstanceDto } from "../../campaign/requirement/model"
import { productType } from "../../product/constants"
import { Product } from "../../product/model"
import { requirementType } from "../../requirement/constants"
import { Requirement } from "../../requirement/model"
import { campaigns } from "./campaigns"
import { products } from "./products"
import { requirements } from "./requirements"


export const mockInstances = (mock: MockAdapter) => {

    mockRegistry.register("instances")

    mockPartyInstances(mock)
    mockRequirementInstances(mock)
    mockProductInstances(mock)
    mockEventInstances(mock)


}


const identifiedPartyInstance = (i: PartyInstanceDto): PartyInstanceDto => ({ ...i, id: `PI-${shortid()}` })


/* Mocking Party Instances */


export const somePartyInstance = (party: Tenant, campaign: CampaignDto): PartyInstance => {

    return identifiedPartyInstance({

        id: undefined!,
        instanceType: tenantType,
        source: party.id,
        campaign: campaign.id,
        tags: [...party.tags],
        properties: {

            note: { en: randomBoolean(.3) ? loremipsum : undefined },
            ...newPartyProperties()
        }

    })
}

var partyinstancestore: Record<string, MockStore<PartyInstanceDto>>


export const partyInstances = (cid: string, size: number = 5) => {

    if (partyinstancestore && partyinstancestore[cid])
        return partyinstancestore[cid]


    const campaign = campaigns().oneWith(cid) as Campaign

    const api = partyinstapi(globalstate)(campaign)


    let instances

    if (api.areReady())
        instances = api.all()
    else
        instances = tenants().all().filter(t => t.id !== "T-BRP").slice(0, 5).concat(randomSlice(tenants().all().filter(t => t.id !== "T-BRP").slice(5), 3, size)).map(p => somePartyInstance(p, campaign));


    partyinstancestore = { ...partyinstancestore ?? {}, [cid]: mockstore(instances) }

    return partyinstancestore[cid]

}

export const mockPartyInstances = (mock: MockAdapter) => {

    const logged = withUser(getLogged())

    const visibleTenants = logged.managedTenants()

    // helper
    const partiesFor = campaign => partyInstances(campaign).all().filter(i => i.campaign === campaign).filter(pi => getLogged().tenant === noTenant || logged.tenant === pi.source || visibleTenants.includes(pi.source))

    mock.onPost("/domain/partyinstance/search").reply(({ data }) => [200, partiesFor(JSON.parse(data).campaign)])
    mock.onGet(/\/domain\/partyinstance\/.+/).reply(({ url }) => {

        const [campaign, source] = splitId(url?.split('/')[3])

        return [200, partiesFor(campaign).filter(pi => pi.source === source)]

    })

    //add many
    mock.onPost('/domain/partyinstance').reply(({ data }) => {

        const identified = (JSON.parse(data) as PartyInstanceDto[]).map(identifiedPartyInstance);

        identified.forEach(i => partyInstances(i.campaign).add(i))

        return [200, identified]

    })
    //delete one
    mock.onPut(/\/domain\/partyinstance\/[^/]+$/).reply(({ data }) => {

        const instance = JSON.parse(data) as PartyInstance

        return [200, partyInstances(instance.campaign).update(instance)]
    })

    //delete many
    mock.onPost("domain/partyinstance/remove").reply(({ data }) => {

        const ids = JSON.parse(data) as string[]

        Object.keys(partyinstancestore).forEach(cid =>

            ids.forEach(id => partyInstances(cid).delete(id))
        )

        return [200]

    })

    //delete one
    mock.onDelete(/\/domain\/partyinstance\/[^/]+$/).reply(({ url }) => {

        const id = url?.split('/')[3]

        Object.keys(partyinstancestore).forEach(c => partyInstances(c).delete(id))

        return [200]

    })

}


/* Mocking Requirement Instances */

const identifiedRequirementInstance = (i: RequirementInstanceDto): RequirementInstanceDto => ({ ...i, id: `RQI-${shortid()}` })

export const someRequirementsInstance = (requirement: Requirement, campaign: CampaignDto): RequirementInstance => {


    return identifiedRequirementInstance({

        id: undefined!,
        instanceType: requirementType,
        source: requirement.id,
        campaign: campaign.id,
        tags: [...requirement.tags],
        properties: {
            note: requirement.properties.note
        }

    })
}

var requirementinstancestore: Record<string, MockStore<RequirementInstanceDto>>

export const requirementInstances = (cid: string, size: number = 10) => {

    if (requirementinstancestore && requirementinstancestore[cid])
        return requirementinstancestore[cid]

    const campaign = campaigns().oneWith(cid) as Campaign

    const api = requirementinstapi(globalstate)(campaign)

    let instances

    if (api.areReady())
        instances = api.all()
    else {
        console.log("mocking requirement instances for ",cid)
        instances = requirements().all().slice(0, 5).concat(randomSlice(requirements().all().slice(5), 3, size)).map(p => someRequirementsInstance(p, campaign))
    }

    requirementinstancestore = { ...requirementinstancestore ?? {}, [cid]: mockstore(instances) }

    return requirementinstancestore[cid]

}

export const mockRequirementInstances = (mock: MockAdapter) => {

    const logged = withUser(getLogged())

    const visibleTenants = logged.managedTenants()

    // helper
    const requirementsFor = campaign => requirementInstances(campaign).all().filter(i => i.campaign === campaign).filter(ri => getLogged().tenant === noTenant || logged.tenant === ri.source || visibleTenants.includes(ri.source))

    mock.onPost("/domain/requirementinstance/search").reply(({ data }) => [200, requirementsFor(JSON.parse(data).campaign)])

    mock.onGet(/\/domain\/requirementinstance\/.+/).reply(({ url }) => {

        const [campaign, source] = splitId(url?.split('/')[3])

        return [200, requirementsFor(campaign).filter(pi => pi.source === source)]

    })

    //add many
    mock.onPost("/domain/requirementinstance").reply(({ data }) => {

        const identified = (JSON.parse(data) as RequirementInstanceDto[]).map(identifiedRequirementInstance)

        identified.forEach(i => requirementInstances(i.campaign).add(i))

        return [200, identified]

    })

    mock.onPut(/\/domain\/requirementinstance\/[^/]+$/).reply(({ data }) => {

        const instance = JSON.parse(data) as RequirementInstanceDto

        return [200, requirementInstances(instance.campaign).update(instance)]
    })

    //delete many
    mock.onPost("domain/requirementinstance/remove").reply(({ data }) => {

        const ids = JSON.parse(data) as string[]

        Object.keys(requirementinstancestore).forEach(cid =>

            ids.forEach(id => requirementInstances(cid).delete(id))
        )

        return [200]

    })


    //delete one
    mock.onDelete(/\/domain\/requirementinstance\/[^/]+$/).reply(({ url }) => {

        const id = url?.split('/')[3]

        Object.keys(requirementinstancestore).forEach(c => requirementInstances(c).delete(id))

        return [200]

    })

}


/* Mocking Product Instances */

var productinstancestore: Record<string, MockStore<ProductInstanceDto>>


export const productInstances = (cid: string, size: number = 10) => {

    if (productinstancestore && productinstancestore[cid])
        return productinstancestore[cid]

    const campaign = campaigns().oneWith(cid) as Campaign

    const api = productinstapi(globalstate)(campaign)

    let instances

    if (api.areReady())
        instances = api.all()
    else {
        console.log("mocking product instances for ",cid)
        instances = products().all().slice(0, 5).concat(randomSlice(products().all().slice(5), 3, 10)).map(p => someProductInstance(p, campaign))
    }

    productinstancestore = { ...productinstancestore ?? {}, [cid]: mockstore(instances) }

    return productinstancestore[cid]

}


const identifiedProductInstance = (i: ProductInstanceDto): ProductInstanceDto => ({ ...i, id: `PRI-${shortid()}` })


export const someProductInstance = (product: Product, campaign: CampaignDto): ProductInstance => {

    return identifiedProductInstance({

        id: undefined!,
        instanceType: productType,
        source: product.id,
        campaign: campaign.id,
        tags: [...product.tags],
        properties: {
            note: product.properties.note
        }

    })
}


export const mockProductInstances = (mock: MockAdapter) => {

    const logged = withUser(getLogged())

    const visibleTenants = logged.managedTenants()

    // helper
    const productsFor = campaign => productInstances(campaign).all().filter(i => i.campaign === campaign).filter(ri => getLogged().tenant === noTenant || logged.tenant === ri.source || visibleTenants.includes(ri.source))

    mock.onPost("/domain/productinstance/search").reply(({ data }) => [200, productsFor(JSON.parse(data).campaign)])

    mock.onGet(/\/domain\/productinstance\/.+/).reply(({ url }) => {

        const [campaign, source] = splitId(url?.split('/')[3])

        return [200, productsFor(campaign).filter(pi => pi.source === source)]

    })

    //add many
    mock.onPost("domain/productinstance").reply(({ data }) => {

        const identified = (JSON.parse(data) as ProductInstanceDto[]).map(identifiedProductInstance)

        identified.forEach(i => productInstances(i.campaign).add(i))

        return [200, identified]

    })

    mock.onPut(/\/domain\/productinstance\/[^/]+$/).reply(({ data }) => {

        const instance = JSON.parse(data) as ProductInstanceDto

        return [200, productInstances(instance.campaign).update(instance)]
    })


    //delete many
    mock.onPost("domain/productinstance/remove").reply(({ data }) => {

        const ids = JSON.parse(data) as string[]

        Object.keys(productinstancestore).forEach(cid =>

            ids.forEach(id => productInstances(cid).delete(id))
        )

        return [200]

    })

    //delete one
    mock.onDelete(/\/domain\/productinstance\/[^/]+$/).reply(({ url }) => {

        const id = url?.split('/')[3]

        Object.keys(productinstancestore).forEach(c => productInstances(c).delete(id))

        return [200]

    })


}






// Mocking event instances



export const mockEventInstances = (mock: MockAdapter) => {

    mockCalls(mock)

}


const identifiedEventInstance = (i: EventInstanceDto): EventInstanceDto => ({ ...i, id: `EVI-${shortid()}` })



const mockCalls = (mock: MockAdapter) => {

    // helper
    const eventsFor = campaign => eventInstances(campaign).all().filter(i => i.campaign === campaign)


    mock.onPost("domain/eventinstance/search").reply(({ data }) => [200, eventsFor(JSON.parse(data).campaign)])

    //add many
    mock.onPost("/domain/eventinstance").reply(({ data }) => {

        const identified = (JSON.parse(data) as EventInstanceDto[]).map(identifiedEventInstance)

        identified.forEach(i => eventInstances(i.campaign).add(i))

        return [200, identified]

    })

    //update one
    mock.onPut(/\/domain\/eventinstance\/[^/]+$/).reply(({ data }) => {

        const instance = JSON.parse(data) as EventInstanceDto

        return [200, eventInstances(instance.campaign).update(instance)]
    })

    //update many
    mock.onPut("domain/eventinstance").reply(({ data }) => {

        const changes = JSON.parse(data) as Changes

        const updated = changes.updated.map(i=>eventInstances(i.campaign).update(i))
        const added = changes.added.map(identifiedEventInstance).map(i=>eventInstances(i.campaign).add(i))
        changes.removed.forEach(r=>Object.keys(eventinstancestore).forEach(cid=>eventinstancestore[cid].delete(r)))

        return [200, [...added, ...updated]]

    })

    //delete many
    mock.onPost("domain/eventinstance/remove").reply(({ data }) => {

        const ids = JSON.parse(data) as string[]

        Object.keys(eventinstancestore).forEach(cid =>

            ids.forEach(id => eventInstances(cid).delete(id))
        )

        return [200]

    })

}


var eventinstancestore: Record<string, MockStore<EventInstanceDto>>

export const eventInstances = (cid: string, size: number = 25) => {

    if (eventinstancestore && eventinstancestore[cid])
        return eventinstancestore[cid]

    const campaign = campaigns().oneWith(cid) as Campaign


    const api = campaign ? eventinstapi(globalstate)(campaign) : undefined;

    let instances

    if (api && api.areReady())

        instances = api.all()

    else if (!campaign) {

        console.log(`mock warn: campaign ${cid} doesn't exist, assuming it's just been created.`)
        instances = []

    }
    else {

        let count = 0;

        const startDate = randomBoolean(.7) ? moment().subtract(randomNumberBetween(3, 9), 'months').format() : moment().add(randomNumber(30), 'days').format()
        const endDate = moment(startDate).add(12, 'months').format()

        instances = [

            // for realism: add instances for each campaign event
            ...[someEventInstance(events().oneWith(campaignStartEvent)!, campaign, campaign.id, ++count, startDate)],

            ...[someEventInstance(events().oneWith(campaignEndEvent)!, campaign, campaign.id, ++count, endDate)],

            ...events().all().filter(e => !e.type && e.lifecycle.state === 'active').map(e => someEventInstance(e, campaign, campaign.id, ++count, undefined, startDate)),

            // for realism: add event instances for each requirement in the campaign
            ...requirementInstances(campaign.id).all().flatMap(i => events().all().filter(e => e.type === requirementType).map(e => someEventInstance(e, campaign, i.source, ++count, undefined, startDate))),


            // for realism: add event instances for each product in the campaign
            ...productInstances(campaign.id).all().flatMap(i => events().all().filter(e => e.type === productType).map(e => someEventInstance(e, campaign, i.source, ++count, undefined, startDate))),


        ]


        // generate temporals for all instances in the campaign with a managed type
        instances.push(...instances.filter(i => i.type)
            .flatMap(i => events().all()
                .filter(t => t.type === eventType && (t.managedTypes?.length === 0 || t.managedTypes?.includes(i.type)))
                .map(e => someEventInstance(e, campaign, i.id, ++count, someDateRelativeTo(i)))
            ))



    }

    eventinstancestore = { ...eventinstancestore ?? {}, [cid]: mockstore(instances) }


    return eventinstancestore[cid]

}


const someEventInstance = (event: EventDto, campaign: CampaignDto, target: string, i: number, date?: string, start?: string): EventInstance => {


    return identifiedEventInstance({

        id: undefined!,
        instanceType: eventType,
        source: event.id,
        type: event.type,
        target,
        campaign: campaign.id,
        date: absoluteOf(date) ?? (randomBoolean(.9) ? absoluteOf(moment(start).add(randomNumber(365), 'days').format()) : undefined),
        tags: [...event.tags],
        properties: {
            note: event.properties.note
        }

    })

}



// ------------------------------------------------ utils



export function campaignIn(url) {

    return url.split('/')[3]

}

const someDateRelativeTo = (ei: EventInstance): string => {


    const relative = moment(absolute(ei.date)).subtract(randomNumber(90), 'days').format()

    //console.log("id",ei.id,"relative to ",ei.date,"produced",relative)

    return relative

}

export const absolute = (date: EventDate | string) => {

    if (!date)
        return undefined

    if (typeof date === 'string')
        return date

    if (!isRelative(date))
        return (date as AbsoluteDate).value

    if (!date?.period?.number)
        return undefined

    const target = Object.values(eventinstancestore).flatMap(store=>store.all()).find(ei => ei.id === date.target)

    // resolve target date 'recursively'.
    const targetDate = absolute(target?.date)

    if (!targetDate)
        return undefined

    return moment(new Date(targetDate)).add(asDuration(date.period)).format()

}


export const pastDate = (campaign: CampaignDto, source: string, target?: string) => eventInstances(campaign.id).all()
    .filter(ei => ei.campaign === campaign.id
        && ei.source === source
        && (target === undefined || ei.target === target))
    .map(e => absolute(e.date))
    .find(d => d && moment(d).isBefore(moment()))




