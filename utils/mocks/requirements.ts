import { eventcardinalities, EventDto } from "emaris-frontend/event/model"
import { Lifecycle } from "apprise-frontend/model/lifecycle"
import { globalstate } from "apprise-frontend/state/model"
import { defaultContext } from "apprise-frontend/system/constants"
import { cardinalities, Tag, TagCategory } from "apprise-frontend/tag/model"
import { deepclone, randomBoolean, randomIn, randomNumber } from "apprise-frontend/utils/common"
import { mockevents } from "emaris-frontend/utils/mocks/events"
import { idify, mockRegistry, MockStore, mockstore, someMultiLang, somePastLifecycle } from "apprise-frontend/utils/mocks/mocks"
import { mockregistry } from "apprise-frontend/utils/mocks/registry"
import { mockcategories, mockContexts, mocktags, randomTagRefsFor } from "apprise-frontend/utils/mocks/tags"
import MockAdapter from "axios-mock-adapter/types"
import { requirementDeadlineEvent } from "emaris-frontend/campaign/constants"
import { predefinedParameters, predefinedRequirementParameters } from "emaris-frontend/layout/parameters/constants"
import { requirementapi } from "emaris-frontend/requirement/api"
import { reqImportanceCategory, requirementType, sourceTypeCategory } from "emaris-frontend/requirement/constants"
import { completeOnAdd, RequirementDto, RequirementState } from "emaris-frontend/requirement/model"
import { EmarisState } from "emaris-frontend/state/model"
import { someLayout } from "./mocks"

mockregistry.register<EmarisState>({

  onLoad: state => {

    console.log("staging mock requirements...")
    reqstore = mockstore(state.requirements.all)

  }
})

export const mockRequirements = (mock: MockAdapter, size?: number) => {

  mockRegistry.register(requirementType)

  mockCalls(mock, size)
  
  mockEvents()
  mockTags()

}

var reqstore: MockStore<RequirementDto>


export const requirements = (size?: number) => {

  if (reqstore)
    return reqstore

  const api = requirementapi(globalstate)

  let requirements: RequirementDto[]

  if (api.areReady()) {
    requirements = api.all()
  }
  else {

    console.log("generating mock requirements...")

    requirements = generateMockRequirements(size)

  }


  reqstore = mockstore(requirements)

  return reqstore

}

const generateMockRequirements = (size: number = 50) => {

  //   generates mock names first, to enable references
  const mocknames = Array(size).fill(undefined).map((_, i) => `Requirement ${i}`)

  const otherrequirements = mocknames.map(n => someRequirement(n, mocknames))

  const requirements = [...distinguishedRequirements, ...otherrequirements]

  return requirements.map(r => completeOnAdd(r, idify(r.name.en!)))


}


const distinguishedRequirements: RequirementDto[] = [

  {
    "audience": { terms: [] }, "userProfile": {"terms" : []}, "description": { "en": "<strong>Paragraph 10</strong>: <p>The flag State of a vessel included on the Draft IUU Vessel List may transmit to the IOTC Executive Secretary at least 15 days before the Annual Meeting of the Compliance Committee, any comments and information about listed vessels and their activities, including information pursuant to Paragraph 9.a) and 9.b) and information showing that the listed vessels either have or have not: </p><ul><li>conducted fishing activities in a manner consistent with IOTC Conservation and Management Measures in force; or </li></ul><p><br></p><ul><li>conducted fishing activities in a manner consistent with the laws and regulations of a coastal State when fishing in the waters under the jurisdiction of that State, and with the law and regulations of the flag State and the Authorisation to Fish; or </li></ul><p><br></p><ul><li>conducted fishing activities exclusively for species that are not covered by the IOTC Agreement or IOTC Conservation and Management Measures i-Sheet - Resolution </li></ul><p><br></p><p><a href=\"http://www.google.com/\" rel=\"noopener noreferrer\" target=\"_blank\" style=\"background-color: rgb(255, 255, 255);\">17/03 - List of presumed IUU Vessels</a></p>" }, "id": "R-lcL47O49", "lifecycle": { "state": "active", "created": 1572154932868, "lastModified": 1580449332868, "lastModifiedBy": "gerard.dominigue" }, "lineage": ["requirement13"], "name": { "en": "IUU Listed Vessels 17/03", "fr": "IUU Navires Listés 17/03" }, "tags": ["TG-system-defaultcontext", "TG-requirement-important", "TG-requirement-cmmtype", "TG-req-newcmm", "TG-req-assessed", "TG-req-mandatory", "TG-req-domain-compliance"], "properties": {
    "versionable": true, "editable": true, "assessed": true,
      "title": { "en": "Comments & information about IUU listed vessels and their activities", "fr": "Commentaire & information au sujet des navires listés et de leurs activités" }, "source": { "title": { "en": "On establishing a list of vessels presumed to have carried out illegal, unregulated and unreported fishing in the IOTC area." } }, "layout": { "components": { "children": [{ "children": [{ "id": "title-1584349830473", "spec": "title", "style": { "text": { "alignment": "center", "color": "#0b7278", "sizeBaseline": "xxl" } }, "text": { "en": "<span class=\"mention\" contenteditable=\"false\" data-value=\"requirement.title\">@requirement.title</span>", "fr": "<span class=\"mention\" contenteditable=\"false\" data-value=\"requirement.title\">@requirement.title</span>" } }, { "id": "separator-1584351380981", "spec": "separator", "style": { "line": { "color": "#0b7278", "visible": true, "type": "dashed", "thickness": 3 }, "width": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": { "line": { "color": "#0b7278", "visible": true, "type": "dashed" } }, "percentage": 37 }, "allStyles": { "line": { "color": "#0b7278", "visible": true, "type": "dashed" }, "width": { "id": "width", "valueprops": {}, "allStyles": { "line": { "color": "#0b7278", "visible": true, "type": "dashed" } }, "percentage": 37 } }, "percentage": 100 }, "height": { "id": "height", "valueprops": { "id": "height", "valueprops": {}, "allStyles": { "line": { "color": "#0b7278", "visible": true, "type": "dashed" }, "width": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": { "line": { "color": "#0b7278", "visible": true, "type": "dashed" } }, "percentage": 37 }, "allStyles": { "line": { "color": "#0b7278", "visible": true, "type": "dashed" }, "width": { "id": "width", "valueprops": {}, "allStyles": { "line": { "color": "#0b7278", "visible": true, "type": "dashed" } }, "percentage": 37 } }, "percentage": 100 } }, "value": 43 }, "allStyles": { "line": { "color": "#0b7278", "visible": true, "type": "dashed" }, "width": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": { "line": { "color": "#0b7278", "visible": true, "type": "dashed" } }, "percentage": 37 }, "allStyles": { "line": { "color": "#0b7278", "visible": true, "type": "dashed" }, "width": { "id": "width", "valueprops": {}, "allStyles": { "line": { "color": "#0b7278", "visible": true, "type": "dashed" } }, "percentage": 37 } }, "percentage": 100 }, "height": { "id": "height", "valueprops": {}, "allStyles": { "line": { "color": "#0b7278", "visible": true, "type": "dashed" }, "width": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": { "line": { "color": "#0b7278", "visible": true, "type": "dashed" } }, "percentage": 37 }, "allStyles": { "line": { "color": "#0b7278", "visible": true, "type": "dashed" }, "width": { "id": "width", "valueprops": {}, "allStyles": { "line": { "color": "#0b7278", "visible": true, "type": "dashed" } }, "percentage": 37 } }, "percentage": 100 } }, "value": 43 } }, "value": 0 }, "spacing": { "left": 3, "right": 3 } }, "withLine": true }, { "id": "paragraph-1584349873480", "spec": "paragraph", "style": { "border": { "left": 0 }, "spacing": { "bottom": 5, "left": 5, "right": 5, "top": 5 }, "text": { "alignment": "left", "size": "l" }, "width": { "allStyles": { "text": { "alignment": "left" }, "width": { "allStyles": { "text": { "alignment": "left" }, "width": { "allStyles": { "text": { "alignment": "left" }, "width": { "allStyles": { "text": { "alignment": "left" } }, "id": "width", "percentage": 90, "valueprops": {} } }, "id": "width", "percentage": 89, "valueprops": { "allStyles": { "text": { "alignment": "left" } }, "id": "width", "percentage": 90, "valueprops": {} } } }, "id": "width", "percentage": 88, "valueprops": { "allStyles": { "text": { "alignment": "left" }, "width": { "allStyles": { "text": { "alignment": "left" } }, "id": "width", "percentage": 90, "valueprops": {} } }, "id": "width", "percentage": 89, "valueprops": { "allStyles": { "text": { "alignment": "left" } }, "id": "width", "percentage": 90, "valueprops": {} } } } }, "id": "width", "percentage": 100, "valueprops": { "allStyles": { "text": { "alignment": "left" }, "width": { "allStyles": { "text": { "alignment": "left" }, "width": { "allStyles": { "text": { "alignment": "left" } }, "id": "width", "percentage": 90, "valueprops": {} } }, "id": "width", "percentage": 89, "valueprops": { "allStyles": { "text": { "alignment": "left" } }, "id": "width", "percentage": 90, "valueprops": {} } } }, "id": "width", "percentage": 88, "valueprops": { "allStyles": { "text": { "alignment": "left" }, "width": { "allStyles": { "text": { "alignment": "left" } }, "id": "width", "percentage": 90, "valueprops": {} } }, "id": "width", "percentage": 89, "valueprops": { "allStyles": { "text": { "alignment": "left" } }, "id": "width", "percentage": 90, "valueprops": {} } } } } }, "text": { "en": "<span class=\"mention\" contenteditable=\"false\" data-value=\"requirement.description\">@requirement.description</span>", "fr": "<span class=\"mention\" contenteditable=\"false\" data-value=\"requirement.description\">@requirement.description</span>" } }], "id": "page-1584349830473", "name": {}, "spec": "page" }], "id": "layout", "spec": "layout" }, "parameters": [{ "id": "campaign", "spec": "campaign", "required": true, "name": "campaign", "fixed": true }, { "id": "party", "spec": "tenant", "required": true, "name": "party", "fixed": true, "flag": true }, { "id": "requirement", "spec": "requirement", "required": true, "name": "requirement", "value": "R-lcL47O49", "fixed": true }] }

    }
  } as RequirementDto,

  {
    "tags": ["TG-system-defaultcontext", "TG-requirement-important", "TG-req-newcmm", "TG-req-mandatory", "TG-req-domain-compliance", "TG-req-assessed", "TG-requirement-cmmtype"], "id": "R-ZOiozOSHH", "name": { "en": "Bigeye Tuna Import 01/06", "fr": "Importation de patudo 01/06" }, "description": { "en": "<strong>Paragraph 5:</strong><p>For CPCs that import bigeye tuna, shall transmit import data for the period 1 July – 31 December (2017) of the previous year.</p><p><br></p><p>i-Sheet - Resolution 01/06 - Big-eye statistical document programme (Amendments to IOTC stats forms)</p><p><br></p>" }, "lineage": [], "audience": { "terms": [] }, "userProfile": {"terms" : []}, "lifecycle": { "state": "active", "created": 1548425349927, "lastModified": 1550498949927, "lastModifiedBy": "floriangiroux" }, "properties": {
      "versionable": true, "editable": true, "assessed": true,"title": { "en": "Second semester report - import of bigeye tuna", "fr": "Rapport du second semestre – données d’importation de patudo" }, "source": { "title": { "en": "Concerning the IOTC bigeye tuna statistical document programme." } }, "layout": { "parameters": [{ "id": "campaign", "spec": "campaign", "required": true, "name": "campaign", "fixed": true }, { "id": "party", "spec": "tenant", "required": true, "name": "party", "fixed": true, "flag": true }, { "id": "requirement", "spec": "requirement", "required": true, "name": "requirement", "value": "R-ZOiozOSHH", "fixed": true }], "components": { "spec": "layout", "id": "layout", "children": [{ "id": "page-1584353017560", "spec": "page", "children": [{ "id": "title-1584353017560", "spec": "title", "style": { "text": { "color": "#0b7278", "sizeBaseline": "xxl", "alignment": "center" } }, "text": { "en": "<span class=\"mention\" contenteditable=\"false\" data-value=\"requirement.title\">@requirement.title</span>", "fr": "<span class=\"mention\" contenteditable=\"false\" data-value=\"requirement.title\">@requirement.title</span>" } }, { "id": "separator-1584353039572", "spec": "separator", "style": { "line": { "color": "#0b7278", "visible": true, "thickness": 2, "type": "dashed" }, "spacing": { "left": 3, "right": 3 } }, "withLine": true }, { "id": "paragraph-1584353067489", "spec": "paragraph", "style": { "text": { "alignment": "center", "size": "l" } }, "text": { "en": "<span class=\"mention\" contenteditable=\"false\" data-value=\"requirement.description\">@requirement.description</span>", "fr": "<span class=\"mention\" contenteditable=\"false\" data-value=\"requirement.description\">@requirement.description</span>" } }, { "id": "separator-1584353296594", "spec": "separator", "style": { "spacing": { "bottom": 21 } }, "withLine": false }, { "id": "select-box-1584353123839", "spec": "select-box", "msg": {}, "label": {}, "style": { "spacing": { "left": 5 } }, "options": [{ "type": "default", "id": "0", "name": { "en": "No large scale longline vessels on the IOTC Record of Authorised Vessels (RAV)", "fr": "No large scale longline vessels on the IOTC Record of Authorised Vessels (RAV)" }, "default": false }, { "type": "default", "id": "1", "name": { "en": "Do not export frozen big eye tuna", "fr": "Do not export frozen big eye tuna" }, "default": false }], "multi": false, "inline": true, "required": true }, { "id": "paragraph-1584353195284", "spec": "paragraph", "style": { "spacing": { "top": 5, "bottom": 2, "left": 5 } }, "text": { "en": "Please upload a file here below", "fr": "Please upload a file here below" } }, { "id": "filedrop-1584353185707", "spec": "filedrop", "msg": {}, "label": {}, "style": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 86 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 86 } }, "percentage": 87 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 86 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 86 } }, "percentage": 87 } }, "percentage": 88 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 86 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 86 } }, "percentage": 87 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 86 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 86 } }, "percentage": 87 } }, "percentage": 88 } }, "percentage": 89 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 86 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 86 } }, "percentage": 87 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 86 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 86 } }, "percentage": 87 } }, "percentage": 88 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 86 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 86 } }, "percentage": 87 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 86 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 86 } }, "percentage": 87 } }, "percentage": 88 } }, "percentage": 89 } }, "percentage": 90 }, "alignment": { "value": "center" } }, "required": false, "mode": "multi", "allowedMimeTypes": [] }, { "id": "input-box-1584353243126", "spec": "input-box", "msg": {}, "label": {}, "style": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 } }, "percentage": 88 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 } }, "percentage": 88 } }, "percentage": 89 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 } }, "percentage": 88 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 } }, "percentage": 88 } }, "percentage": 89 } }, "percentage": 90 }, "alignment": { "value": "center" }, "height": { "id": "height", "valueprops": { "id": "height", "valueprops": { "id": "height", "valueprops": {}, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 } }, "percentage": 88 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 } }, "percentage": 88 } }, "percentage": 89 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 } }, "percentage": 88 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 } }, "percentage": 88 } }, "percentage": 89 } }, "percentage": 90 }, "alignment": { "value": "center" } }, "value": 5 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 } }, "percentage": 88 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 } }, "percentage": 88 } }, "percentage": 89 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 } }, "percentage": 88 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 } }, "percentage": 88 } }, "percentage": 89 } }, "percentage": 90 }, "alignment": { "value": "center" }, "height": { "id": "height", "valueprops": {}, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 } }, "percentage": 88 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 } }, "percentage": 88 } }, "percentage": 89 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 } }, "percentage": 88 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 } }, "percentage": 88 } }, "percentage": 89 } }, "percentage": 90 }, "alignment": { "value": "center" } }, "value": 5 } }, "value": 3 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 } }, "percentage": 88 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 } }, "percentage": 88 } }, "percentage": 89 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 } }, "percentage": 88 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 } }, "percentage": 88 } }, "percentage": 89 } }, "percentage": 90 }, "alignment": { "value": "center" }, "height": { "id": "height", "valueprops": { "id": "height", "valueprops": {}, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 } }, "percentage": 88 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 } }, "percentage": 88 } }, "percentage": 89 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 } }, "percentage": 88 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 } }, "percentage": 88 } }, "percentage": 89 } }, "percentage": 90 }, "alignment": { "value": "center" } }, "value": 5 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 } }, "percentage": 88 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 } }, "percentage": 88 } }, "percentage": 89 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 } }, "percentage": 88 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 } }, "percentage": 88 } }, "percentage": 89 } }, "percentage": 90 }, "alignment": { "value": "center" }, "height": { "id": "height", "valueprops": {}, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 } }, "percentage": 88 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 } }, "percentage": 88 } }, "percentage": 89 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 } }, "percentage": 88 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 85 } }, "percentage": 88 } }, "percentage": 89 } }, "percentage": 90 }, "alignment": { "value": "center" } }, "value": 5 } }, "value": 3 } }, "value": 0 } }, "multi": false, "required": true, "rich": false, "placeholder": [] }], "name": {} }] } }
    }
  } as RequirementDto,

  { "tags": ["TG-system-defaultcontext", "TG-requirement-important", "TG-req-newcmm", "TG-req-mandatory", "TG-req-domain-compliance", "TG-req-assessed", "TG-requirement-cmmtype"], "id": "R-GkSrzprw0", "name": { "en": "Import of Tuna 10/10", "fr": "Importation de Thon 10/10" }, "description": { "en": "<strong>Paragraph 1:</strong><p>For CPCs that import tuna and tuna-like fish products, or in whose ports those products are landed or tran- shipped, should report, a range of information annu- ally (e.g. information on vessels / owners, product data (species, weight), point of export).</p><p><br></p><p>i-Sheet - Resolution 01/06 - Big-eye statistical document programme (Amendments to IOTC stats forms)</p>" }, "lineage": [], "audience": { "terms": [] },  "userProfile": {"terms" : []}, "lifecycle": { "state": "active", "created": 1570759023671, "lastModified": 1576637823671, "lastModifiedBy": "otholite" }, "properties": {
    "versionable": true, "editable": true, "assessed": true,"title": { "en": "Report on import, landing and transhipment of tuna and tuna-like fish products", "fr": "Report on import, landing and transhipment of tuna and tuna-like fish products" }, "source": { "title": { "en": "Resolution 10/10 Concerning market related measures" } }, "layout": { "parameters": [{ "id": "campaign", "spec": "campaign", "required": true, "name": "campaign", "fixed": true }, { "id": "party", "spec": "tenant", "required": true, "name": "party", "fixed": true, "flag": true }, { "id": "requirement", "spec": "requirement", "required": true, "name": "requirement", "value": "R-GkSrzprw0", "fixed": true }], "components": { "spec": "layout", "id": "layout", "children": [{ "id": "page-1584353993042", "spec": "page", "children": [{ "id": "title-1584353993042", "spec": "title", "style": { "text": { "color": "#0b7278", "sizeBaseline": "xxl", "alignment": "center" } }, "text": { "en": "<span class=\"mention\" contenteditable=\"false\" data-value=\"requirement.title\">@requirement.title</span>", "fr": "<span class=\"mention\" contenteditable=\"false\" data-value=\"requirement.title\">@requirement.title</span>" } }, { "id": "separator-1584354013639", "spec": "separator", "style": { "line": { "color": "#0b7278", "visible": true, "thickness": 3, "type": "dashed" }, "spacing": { "left": 3, "right": 3 } }, "withLine": true }, { "id": "paragraph-1584354047277", "spec": "paragraph", "style": { "text": { "alignment": "center", "size": "l" }, "spacing": { "top": 5, "bottom": 5, "left": 5, "right": 5 } }, "text": { "en": "<span class=\"mention\" contenteditable=\"false\" data-value=\"requirement.description\">@requirement.description</span>", "fr": "<span class=\"mention\" contenteditable=\"false\" data-value=\"requirement.description\">@requirement.description</span>" } }, { "id": "separator-1584354128248", "spec": "separator", "style": { "height": { "id": "height", "valueprops": { "id": "height", "valueprops": { "id": "height", "valueprops": { "id": "height", "valueprops": { "id": "height", "valueprops": {}, "allStyles": {}, "value": 32 }, "allStyles": { "height": { "id": "height", "valueprops": {}, "allStyles": {}, "value": 32 } }, "value": 27 }, "allStyles": { "height": { "id": "height", "valueprops": { "id": "height", "valueprops": {}, "allStyles": {}, "value": 32 }, "allStyles": { "height": { "id": "height", "valueprops": {}, "allStyles": {}, "value": 32 } }, "value": 27 } }, "value": 30 }, "allStyles": { "height": { "id": "height", "valueprops": { "id": "height", "valueprops": { "id": "height", "valueprops": {}, "allStyles": {}, "value": 32 }, "allStyles": { "height": { "id": "height", "valueprops": {}, "allStyles": {}, "value": 32 } }, "value": 27 }, "allStyles": { "height": { "id": "height", "valueprops": { "id": "height", "valueprops": {}, "allStyles": {}, "value": 32 }, "allStyles": { "height": { "id": "height", "valueprops": {}, "allStyles": {}, "value": 32 } }, "value": 27 } }, "value": 30 } }, "value": 31 }, "allStyles": { "height": { "id": "height", "valueprops": { "id": "height", "valueprops": { "id": "height", "valueprops": { "id": "height", "valueprops": {}, "allStyles": {}, "value": 32 }, "allStyles": { "height": { "id": "height", "valueprops": {}, "allStyles": {}, "value": 32 } }, "value": 27 }, "allStyles": { "height": { "id": "height", "valueprops": { "id": "height", "valueprops": {}, "allStyles": {}, "value": 32 }, "allStyles": { "height": { "id": "height", "valueprops": {}, "allStyles": {}, "value": 32 } }, "value": 27 } }, "value": 30 }, "allStyles": { "height": { "id": "height", "valueprops": { "id": "height", "valueprops": { "id": "height", "valueprops": {}, "allStyles": {}, "value": 32 }, "allStyles": { "height": { "id": "height", "valueprops": {}, "allStyles": {}, "value": 32 } }, "value": 27 }, "allStyles": { "height": { "id": "height", "valueprops": { "id": "height", "valueprops": {}, "allStyles": {}, "value": 32 }, "allStyles": { "height": { "id": "height", "valueprops": {}, "allStyles": {}, "value": 32 } }, "value": 27 } }, "value": 30 } }, "value": 31 } }, "value": 35 } }, "withLine": false }, { "id": "select-box-1584354190714", "spec": "select-box", "msg": {}, "label": {}, "style": { "spacing": { "left": 5 } }, "options": [{ "type": "default", "id": "1", "name": { "en": "No transhipment by foreign vessels in national ports", "fr": "Aucun transbordement de navires étrangers dans les ports nationaux" }, "default": false }, { "type": "default", "id": "0", "name": { "en": "No landing from foreign vessels in national ports", "fr": "Aucun débarquement de navires étrangers dans les ports nationaux" }, "default": false }, { "id": "2", "type": "default", "name": { "en": "Do not import tuna and tuna-like fish products", "fr": "N’importe pas de thons et des produits du thon et des espèces apparentées" }, "default": false }], "multi": false, "inline": true, "required": true }, { "id": "paragraph-1584354150530", "spec": "paragraph", "style": { "spacing": { "top": 5, "bottom": 2, "left": 5 } }, "text": { "en": "Please upload a file here below", "fr": "Please upload a file here below" } }, { "id": "filedrop-1584354172701", "spec": "filedrop", "msg": {}, "label": {}, "style": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 } }, "percentage": 95 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 } }, "percentage": 95 } }, "percentage": 93 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 } }, "percentage": 95 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 } }, "percentage": 95 } }, "percentage": 93 } }, "percentage": 92 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 } }, "percentage": 95 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 } }, "percentage": 95 } }, "percentage": 93 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 } }, "percentage": 95 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 } }, "percentage": 95 } }, "percentage": 93 } }, "percentage": 92 } }, "percentage": 91 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 } }, "percentage": 95 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 } }, "percentage": 95 } }, "percentage": 93 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 } }, "percentage": 95 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 } }, "percentage": 95 } }, "percentage": 93 } }, "percentage": 92 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 } }, "percentage": 95 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 } }, "percentage": 95 } }, "percentage": 93 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 } }, "percentage": 95 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 } }, "percentage": 95 } }, "percentage": 93 } }, "percentage": 92 } }, "percentage": 91 } }, "percentage": 92 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 } }, "percentage": 95 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 } }, "percentage": 95 } }, "percentage": 93 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 } }, "percentage": 95 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 } }, "percentage": 95 } }, "percentage": 93 } }, "percentage": 92 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 } }, "percentage": 95 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 } }, "percentage": 95 } }, "percentage": 93 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 } }, "percentage": 95 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 } }, "percentage": 95 } }, "percentage": 93 } }, "percentage": 92 } }, "percentage": 91 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 } }, "percentage": 95 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 } }, "percentage": 95 } }, "percentage": 93 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 } }, "percentage": 95 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 } }, "percentage": 95 } }, "percentage": 93 } }, "percentage": 92 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 } }, "percentage": 95 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 } }, "percentage": 95 } }, "percentage": 93 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 } }, "percentage": 95 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 97 } }, "percentage": 95 } }, "percentage": 93 } }, "percentage": 92 } }, "percentage": 91 } }, "percentage": 92 } }, "percentage": 90 }, "alignment": { "value": "center" } }, "required": false, "mode": "multi", "allowedMimeTypes": [] }, { "id": "input-box-1584354276585", "spec": "input-box", "msg": {}, "label": {}, "style": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 98 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 98 } }, "percentage": 97 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 98 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 98 } }, "percentage": 97 } }, "percentage": 95 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 98 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 98 } }, "percentage": 97 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 98 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 98 } }, "percentage": 97 } }, "percentage": 95 } }, "percentage": 93 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 98 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 98 } }, "percentage": 97 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 98 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 98 } }, "percentage": 97 } }, "percentage": 95 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 98 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 98 } }, "percentage": 97 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 98 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 98 } }, "percentage": 97 } }, "percentage": 95 } }, "percentage": 93 } }, "percentage": 92 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 98 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 98 } }, "percentage": 97 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 98 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 98 } }, "percentage": 97 } }, "percentage": 95 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 98 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 98 } }, "percentage": 97 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 98 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 98 } }, "percentage": 97 } }, "percentage": 95 } }, "percentage": 93 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 98 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 98 } }, "percentage": 97 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 98 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 98 } }, "percentage": 97 } }, "percentage": 95 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 98 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 98 } }, "percentage": 97 }, "allStyles": { "width": { "id": "width", "valueprops": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 98 }, "allStyles": { "width": { "id": "width", "valueprops": {}, "allStyles": {}, "percentage": 98 } }, "percentage": 97 } }, "percentage": 95 } }, "percentage": 93 } }, "percentage": 92 } }, "percentage": 90 }, "alignment": { "value": "center" } }, "multi": false, "required": false, "rich": false, "placeholder": [] }], "name": {} }] } } } } as RequirementDto
]





export const requirementEvents = (): EventDto[] => [


  {
    id: requirementDeadlineEvent,
    type: requirementType,
    cardinality: eventcardinalities["one"], 
    name: someMultiLang("Requirement Deadline"), description: someMultiLang("The deadline of a requirement."),
    lifecycle: somePastLifecycle('active'),
    managed: true,
    predefined: true,
    tags: [],
    predefinedProperties: [],
    properties:{}
  }

]


export const mockEvents = () => {

  requirementEvents().forEach(e => { e.tags.push(defaultContext); mockevents.push(e) })
}


const mockTags = () => {

  // predefined mocks

  predefinedCategories().forEach(c => mockcategories.push(c))
  predefinedTags().forEach(t => mocktags.push(t))

  //  additional mocks

  const data: { cat: TagCategory, tags: Tag[] }[] = [
    {
      cat: {
        lifecycle: { state: 'active' } as any, "type": "requirement", "name": { "en": "Domain", "fr": "Domaine" }, "description": { "en": "The broad category of the requirement.", "fr": "La catégorie générale de l'obligation." }, "id": "TGC-req-domain", properties: {
          "color": "#f06292",
          "field": { "enabled": true, "title": { "en": "Domain", "fr": "Domaine" }, "help": { "en": "The broad category of the requirement.", "fr": "La catégorie générale de l'obligation." }, "message": { "en": "Select one domain.", "fr": "Sélectionnez un domaine." } }
        }, "cardinality": { "min": 1, "max": 1 }, "predefined": true
      },
      tags: [
        { lifecycle: { state: 'active' } as any, "type": "requirement", "name": { "en": "Science", "fr": "Science" }, "description": { "en": "Scientific requirement.", "fr": "Obligation Scientifique" }, "id": "TG-req-domain-science", "category": "TGC-req-domain", properties: {} },
        { lifecycle: { state: 'active' } as any, "type": "requirement", "name": { "en": "Compliance", "fr": "Conformité" }, "description": { "en": "Compliance requirement.", "fr": "Obligation de la conformitè" }, "id": "TG-req-domain-compliance", "category": "TGC-req-domain", properties: {} }
      ]
    }

    ,

    {
      cat: {
        lifecycle: { state: 'active' } as any, "type": "requirement", "name": { "en": "Constraint", "fr": "Contrainte" }, "description": { "en": "The reporting constraint of the requirement.", "fr": "La contrainte de dèclaration de l'obligation." }, "id": "TGC-req-constraint",
        properties: {
          "color": "#7986cb",
          "field": { "enabled": true, "title": { "en": "Reporting Constraint", "fr": "Contrainte de Dèclaration" }, "message": { "en": "Select an option.", "fr": " Sélectionnez une option." }, "help": { "en": "A requirement may be mandatory or optional.", "fr": "Un obligation peut être obligatoire ou facultative." } }
        }, "cardinality": { "min": 1, "max": 1 }, "predefined": true
      },
      tags: [

        { lifecycle: { state: 'active' } as any, "type": "requirement", "name": { "en": "Mandatory", "fr": "Oligatoire" }, "description": { "en": "Mandatory requirement.", "fr": "Obligation Obligatoire." }, "id": "TG-req-mandatory", "category": "TGC-req-constraint", properties: {} }
        ,
        { lifecycle: { state: 'active' } as any, "type": "requirement", "name": { "en": "Optional", "fr": "Facultative" }, "description": { "en": "Optional requirement.", "fr": "Obligation facultative." }, "id": "TG-req-optional", "category": "TGC-req-constraint", properties: {} }
      ]
    }
    ,

    {

      cat: {
        lifecycle: { state: 'active' } as any, "type": "requirement", "name": { "en": "Assessment", "fr": "Évaluation" }, "description": { "en": "The assessment status of the requirement.", "fr": "Le statut d'évaluation de l'obligation." }, "id": "TGC-req-assessment",
        properties: {
          "color": "#4db6ac", "field": { "enabled": true, "title": { "en": "Assessment Status", "fr": "Statut d'Èvaluation" }, "message": { "en": "Select one option.", "fr": " Sélectionnez une option." }, "help": { "en": "The assessment status of the requirement.", "fr": "Le statut d'évaluation de l'obligation." } }
        }, "cardinality": { "min": 1, "max": 1 }, "predefined": true
      }

      ,

      tags: [

        { lifecycle: { state: 'active' } as any, "type": "requirement", "name": { "en": "Assessed", "fr": "Èvalué" }, "description": { "en": "Assessed requirement.", "fr": "Obligation évalué." }, "id": "TG-req-assessed", "category": "TGC-req-assessment", properties: {} }
        ,
        { lifecycle: { state: 'active' } as any, "type": "requirement", "name": { "en": "Not Assessed", "fr": "Non Èvalué" }, "description": { "en": "Unassessed Requirement.", "fr": "Obligation Non Èvalué." }, "id": "TG-req-notassessed", "category": "TGC-req-assessment", properties: {} }

      ]
    },

    {
      cat: {
        lifecycle: { state: 'active' } as any, "type": "requirement", "name": { "en": "Reporting Type", "fr": "Type de rapport" }, "description": { "en": "The reporting type of the requirement.", "fr": "Type de rapport de l'obligation. " }, "id": "TGC-req-type",
        properties: {
          "color": "#455a64",
          "field": {
            "enabled": true
          }
        }, "cardinality": { "min": 1, "max": 1 }, "predefined": true
      }

      ,

      tags: [
        { lifecycle: { state: 'active' } as any, "type": "requirement", "name": { "en": "New CMM", "fr": "CMM Nouvelle" }, "description": { "en": "Reporting on a new CMM.", "fr": "Déclaration pour une nouvelle CMM." }, "id": "TG-req-newcmm", "category": "TGC-req-type", properties: {} }
        ,
        { lifecycle: { state: 'active' } as any, "type": "requirement", "name": { "en": "Previous CMM", "fr": "CMM Précédente" }, "description": { "en": "Reporting on previous CMM.", "fr": "Dèclaration pour une CMM précédente." }, "id": "TG-req-existingcmm", "category": "TGC-req-type", properties: {} }
      ]

    },


  ]


  data.forEach(({ cat, tags: ts }) => {

    mockcategories.push(cat)
    ts.forEach(t => mocktags.push(t))

  })



}

const mockCalls = (mock: MockAdapter, size?: number) => {

  mock.onGet("/domain/requirement").reply(_ => [200, requirements(size).all()])

  mock.onPost("/domain/requirement").reply(({ data }) => {

    const model = JSON.parse(data)
    model.lifecycle = somePastLifecycle(model.lifecycle.state)
    return [200, requirements().add(model)]
  })

  mock.onPost("/domain/requirement/remove").reply(({ data }) => {

    (JSON.parse(data) as string[]).filter(id => requirements().delete)

    return [200]
  })

  mock.onGet(/\/domain\/requirement\/.+/).reply(({ url }) => [200, requirements().oneWith(url!.split('/').pop())])


  mock.onPut(/\/domain\/requirement\/.+/).reply(({ data }) => {

    const model = JSON.parse(data)
    model.lifecycle = somePastLifecycle(model.lifecycle.state)
    return [200, requirements().update(model)]
  })

  mock.onDelete(/\/domain\/requirement\/.+/).reply(({ url }) => [204, requirements().delete(url!.split('/').pop())])


}


const someRequirement = (name: string, others: string[]): RequirementDto => {

  const layout = someLayout(requirementType)

  const predefinedparams = [...predefinedRequirementParameters(globalstate), ...predefinedParameters(globalstate)]

  predefinedparams.forEach(p => layout.parameters.unshift(deepclone(p)))

  return {

    id: idify(name),
    name: someMultiLang(name),
    description: someMultiLang("description"),
    audience: { terms: [] },
    userProfile: {terms : []},
    lifecycle: somePastLifecycle(randomBoolean(.8) ? "active" : "inactive") as Lifecycle<RequirementState>,
    tags: [randomIn([defaultContext, ...mockContexts.map(t => t.id)])!, ...randomTagRefsFor(requirementType)],
    lineage: randomBoolean(.3) ? Array.from({ length: randomNumber(3) }).map(_ => idify(randomIn(others)!)) : undefined!,
    properties: {
      source: { title: someMultiLang("source title") },
      layout,
      versionable: true, editable: true, assessed: true,

    }

  }

}




////


export const predefinedCategories = (): TagCategory[] => {

  return [

    {
      id: sourceTypeCategory,
      name: { en: "Source Type", fr: "Type de Source" },
      description: {
        en: "The type of the original source of the requirement.",
        fr: "Le type de la source d'origine de l'exigence"
      },
      type: requirementType,
      properties: {
        color: "#1976D2",
        field: { enabled: true }
      },
      cardinality: cardinalities.one,
      lifecycle: { state: 'active' } as any,
      predefined: true

    }

    ,

    {
      id: reqImportanceCategory,
      name: { en: "Importance", fr: "Importance" },
      description: { en: "Importance scale.", fr: "Échelle d'importance." },
      type: requirementType,
      properties: {
        color: "#2A8800",
        field: { enabled: true }
      },
      cardinality: cardinalities.one,
      lifecycle: { state: 'active' } as any,
      predefined: true,

    }

  ]
}


export const predefinedTags = (): Tag[] => {

  return [{
    id: "TG-requirement-veryimportant",
    type: requirementType,
    lifecycle: { state: 'active' } as any,
    name:
      { en: "Very Important", fr: "Très Important" },
    description: {},
    category: reqImportanceCategory,
    properties: { value: 100 }
  }
    ,
  {
    id: "TG-requirement-important",
    type: requirementType,
    lifecycle: { state: 'active' } as any,
    name: { en: "Important", fr: "Important" },
    description: {},
    category: reqImportanceCategory,
    properties: { value: 75 }
  }
    ,
  {
    id: "TG-requirement-normal",
    type: requirementType,
    lifecycle: { state: 'active' } as any,
    name: { en: "Normal", fr: "Ordinaire" },
    description: {},
    category: reqImportanceCategory,
    properties: { value: 50 }
  }
    ,

  {
    id: "TG-requirement-notveryimportant",
    type: requirementType,
    lifecycle: { state: 'active' } as any,
    name:
      { en: "Not Very Important", fr: "Pas très Important" },
    description: {},
    category: reqImportanceCategory,
    properties: { value: 10 }
  },


  {
    id: "TG-requirement-cmmtype",
    type: requirementType,
    lifecycle: { state: 'active' } as any,
    name: { en: "CMM", fr: "CMM" },
    description: { en: "Conservation Management Measure.", fr: "Mesure de conservation et de gestion." },
    category: sourceTypeCategory,
    properties: {}
  }
    ,
  {
    id: "TG-requirement-roptype",
    type: requirementType,
    lifecycle: { state: 'active' } as any,
    name: { en: "ROP", "fr": "ROP" },
    description: { en: "Rule of Procedure.", fr: "Règle de procédure." },
    category: sourceTypeCategory,
    properties: {}
  }

    ,
  {
    id: "TG-requirement-agreementtype",
    type: requirementType,
    name: { en: "Agreement", fr: "Agreement" },
    description: { en: "Agreement", fr: "Agreement" },
    lifecycle: { state: 'active' } as any,
    category: sourceTypeCategory,
    properties: {}
  }
  ]
}

