import { intlapi } from "apprise-frontend/intl/api"
import { filedropspec } from "apprise-frontend/layout/components/FileDrop"
import { inputboxspec } from "apprise-frontend/layout/components/InputBox"
import { multichoicespec } from "apprise-frontend/layout/components/MultiChoice"
import { pagespec } from "apprise-frontend/layout/components/Page"
import { paragraphspec } from "apprise-frontend/layout/components/Paragraph"
import { separatorspec } from "apprise-frontend/layout/components/Separator"
import { titlespec } from "apprise-frontend/layout/components/Title"
import { newLayout } from "apprise-frontend/layout/model"
import { newParameterFrom, Parameter } from "apprise-frontend/layout/parameters/model"
import { tenantspec } from "apprise-frontend/layout/parameters/Tenant"
import { themespec } from "apprise-frontend/layout/parameters/Theme"
import { layoutRegistry } from "apprise-frontend/layout/registry"
import { withMultilang } from "apprise-frontend/model/multilang"
import { globalstate } from "apprise-frontend/state/model"
import { randomBoolean, randomIn } from "apprise-frontend/utils/common"
import MockAdapter from "axios-mock-adapter/types"
import { campaignspec } from "emaris-frontend/layout/parameters/Campaign"
import { partyspec } from "emaris-frontend/layout/parameters/Party"
import { productspec } from "emaris-frontend/layout/parameters/Product"
import { requirementspec } from "emaris-frontend/layout/parameters/Requirement"
import { productType } from "emaris-frontend/product/constants"
import { requirementType } from "emaris-frontend/requirement/constants"
import { requirements } from "./requirements"

export const mockInfo = (mock: MockAdapter) => {

  mock.onGet(/\/(admin|domain)\/info/).reply(200, {

    name: "admin",
    build: {
      version: "1.0.2-SNAPSHOT",
      timestamp: "2019-08-13T10:12:30+0200",
      commit: "258487b",
      user: "John Doe",
      message: "builds also a tarball with jar and dependencies.",
      time: "2019-06-13T01:02:50+0200",
    },
    toggles: ["DEV", "FEATUREX"]

  })

}


export const someLayout = (context: string) => {


  const layout = newLayout()

  const page = { ...pagespec.generate(), id: "mock-page" }

  page.children = [{ ...titlespec.generate(), id: "mock-title" },
  { ...separatorspec.generate(), id: "mock-separator" },
  { ...paragraphspec.generate(), id: "mock-para" },
  { ...inputboxspec.generate(), id: "mock-input" },
  { ...multichoicespec.generate(), id: "mock-multi" },
  { ...filedropspec.generate(), id: "mock-filedrop" }]

  layout.components.children = [page]

  return {

      ...layout

      ,

      parameters: Array(5).fill(0).map((_, i) => someParam(i + 1, context))

  }


}


const someParam = (i: number, context: string) => {

  let excludes = [tenantspec.id, themespec.id, campaignspec.id, partyspec]

  if (context === requirementType)
      excludes.push(requirementspec.id)

  if (context === productType)
      excludes.push(productspec.id)

  const spec = randomIn(layoutRegistry(context).allParameters().filter(spec => !excludes.includes(spec.id)))!

  const t = intlapi(globalstate).getT()

  const value = (context !== requirementType && spec.id === requirementspec.id) ? randomIn(requirements().all())?.id : spec.randomValue(globalstate)
  const name = context !== requirementType && spec.id === requirementspec.id ? withMultilang(globalstate)(requirements().oneWith(value)!.name).inCurrent() : `${t(spec.name).toLowerCase()}-${i}`

  return {

      ...newParameterFrom(spec),

      id: `${i}`,
      name,
      required: randomBoolean(.5),
      value

  } as Parameter

}

