
import { Component, isContainer } from 'apprise-frontend/layout/components/model'
import { Layout, newLayout } from "apprise-frontend/layout/model"
import { Lifecycle, Lifecycled, newLifecycle } from "apprise-frontend/model/lifecycle"
import { compareMultilang, l, MultilangDto } from "apprise-frontend/model/multilang"
import { settingsapi } from "apprise-frontend/settings/api"
import { Document } from "apprise-frontend/stream/model"
import { systemType } from "apprise-frontend/system/constants"
import { SystemAppSettings } from "apprise-frontend/system/model"
import { newTagged, Tagged } from "apprise-frontend/tag/model"
import { TenantAudience } from "apprise-frontend/tenant/AudienceList"
import { deepclone, shortid } from "apprise-frontend/utils/common"
import { AssetSection, NamedAsset } from 'emaris-frontend/layout/components/AssetSection'
import { predefinedParameters, predefinedProductParameterId, predefinedProductParameters } from "emaris-frontend/layout/parameters/constants"
import { requirementType } from "emaris-frontend/requirement/constants"
import { EmarisState } from "../state/model"


export type ProductDto = Tagged & Lifecycled<ProductState> & {

    id: string

    predefined?: boolean

    name: MultilangDto
    description: MultilangDto
    lifecycle: Lifecycle<ProductState>
    audience: { terms: any[] },
    audienceList?: TenantAudience,
    userProfile: { terms: any[] },

    lineage?: string[]
    
    documents?: Record<string, Document[]>
    properties: ProductProperties

}

export type ProductProperties = {

    layout: Layout
    dependencies: NamedAsset[]
    note?: Record<string, string>
    versionable: boolean | undefined
    editable: boolean | undefined
    assessed: boolean | undefined

} & { [key: string]: any }

export type ProductState = "active" | "inactive"

export type Product = ProductDto



export const newProduct: (_: EmarisState) => Product = s => {
    
    const settings = ()=>settingsapi(s).get<SystemAppSettings>()[systemType] ?? {}

    const product = {


        ...newTagged,

        id: undefined!,
        name: { en: undefined },
        description: { en: undefined },
        lifecycle: newLifecycle('inactive'),
        audience: { terms: [] },
        userProfile: { terms: [] },
        
        tags: settings().defaultProductImportance ? ["TG-system-defaultcontext", ...settings().defaultProductImportance] : ["TG-system-defaultcontext"], //Default context set


        properties: {
            layout: { ...newLayout() },
            dependencies:[],
            versionable: true,
            editable: true,
            assessed: true
        },

    } as Product

    product.properties.layout.parameters = [...predefinedParameters(s), ...predefinedProductParameters(s)]

    return product

}

export const completeOnAdd = (product: Product, id?: string) => {

    const newreq = { ...product, id: id ?? newProductId() }

    const hostparam = newreq.properties.layout.parameters.find(p => p.id === predefinedProductParameterId)

    if (hostparam)
        hostparam.value = { id: newreq.id }

    return newreq

}

export const noProduct = (s: EmarisState): Product => ({ ...newProduct(s), id: 'unknown' })

export const newProductId = () => `P-${shortid()}`

export const productmodelapi = (s: EmarisState) => {

    const self = {

        stringify: (p: Product | undefined) => p ? `${l(s)(p.name)} ${l(s)(p.description) ?? ''}` : ''

        ,

        nameOf: (p: Product | undefined) => p ? `${l(s)(p.name)}` : ''

        ,

        intern: (dto: ProductDto) => dto

        ,

        extern: (p: ProductDto) => p

        ,

        clone: (p: Product): Product => ({ ...deepclone(p), id: undefined!, predefined: false, lifecycle: newLifecycle(`inactive`), documents:{} })

        ,

        branch: (p: Product): Product => ({ ...deepclone(p), id: undefined!, predefined: false, lifecycle: newLifecycle(`inactive`), lineage: [p.id], documents:{} })

        ,

        isLoaded: (prod:Product|undefined) => !!prod?.properties?.layout

        ,


        comparator: (o1: Product, o2: Product) => compareMultilang(s)(o1.name, o2.name)

        ,

        dependenciesOf: (product: Product) => {

            const flatten =  (c: Component): Component[] => isContainer(c) ? [c, ...c.children.flatMap(flatten)] : [c]

            return  flatten(product.properties.layout?.components ?? []).flatMap(c => (c as AssetSection)?.assets ?? [])
        }

        ,

        requirementsFor: (p: Product) => p.properties.layout.parameters.filter(param => param.spec === requirementType && param.value !== undefined).map(r => r.value.id)

    }

    return self


}
