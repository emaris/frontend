
import { callapi } from "apprise-frontend/call/call"
import { domainService } from "../constants"
import { EmarisState } from "../state/model"
import { Product, productmodelapi } from "./model"

const products = "/product"


export const productcalls = (s:EmarisState) => {

    const {at} = callapi(s)
    const { extern, intern } = productmodelapi(s)

    return {


        fetchAll: () => at(products,domainService).get<Product[]>()
        
        , 

        fetchOne: (id:String) =>  at(`${products}/${id}`,domainService).get<Product>().then(intern) 
        
        ,
        
        add: (product:Product) =>  at(products,domainService).post<Product>(extern(product)).then(intern)
        
        ,
    
        update: (product:Product) => at(`${products}/${product.id}`,domainService).put<Product>(extern(product)).then(intern)
        
        , 

        delete: (id:string) => at(`${products}/${id}`,domainService).delete()
  

    }

}