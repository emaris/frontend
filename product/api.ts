
import { useEmarisState } from "emaris-frontend/state/hooks"
import { EmarisState } from "../state/model"
import { productActions } from "./actions"
import { productmodelapi } from "./model"
import { productstateapi } from "./state"
import { productvalidationapi } from "./validation"

export const useProducts = () => productapi(useEmarisState())

export const productapi = (s:EmarisState) => ({

    ...productstateapi(s),
    ...productvalidationapi(s),
    ...productmodelapi(s),

    actions: productActions

})

export const products = {

    breadcrumbResolver: (id:string,s: EmarisState) => productapi(s).lookup(id)?.name
}
