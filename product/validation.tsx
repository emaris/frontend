

import { State } from "#app";
import { configapi } from "apprise-frontend/config/state";
import { intlapi } from "apprise-frontend/intl/api";
import { contextCategory } from "apprise-frontend/system/constants";
import { tagapi } from "apprise-frontend/tag/api";
import { tenantPlural } from "apprise-frontend/tenant/constants";
import { userPlural } from 'apprise-frontend/user/constants';
import { check, checkIt, notdefined, requireLanguages, reservedKeyword, stringLength, uniqueLanguages, withReport } from "apprise-frontend/utils/validation";
import { productPlural, productSingular, productType } from "emaris-frontend/product/constants";
import { Product } from "emaris-frontend/product/model";
import { productapi } from "./api";

export type ProductValidation = ReturnType<typeof productvalidationapi>

export const productvalidationapi = (s: State) => ({

    validateProduct: (edited: Product) => {

        const t = intlapi(s).getT()
        const { validateCategories } = tagapi(s)

        const products = productapi(s).all();
        const requiredLangs = configapi(s).get().intl.required || [];

        const singular = t(productSingular).toLowerCase()
        const plural = t(productPlural).toLowerCase()
        const partyAudiencePlural = t(tenantPlural).toLowerCase()
        const userProfilePlural = t(userPlural).toLowerCase()

        return withReport({

            active: checkIt().nowOr(t("common.fields.active.msg"), t("common.fields.active.help", { plural })),

            name: check(edited.name).with(notdefined(t))
                .with(requireLanguages(requiredLangs, t))
                .with(stringLength(t))
                .with(reservedKeyword(t))
                .with(uniqueLanguages(products.filter(t => t.id !== edited.id).map(t => t.name), t)).nowOr(
                    t("common.fields.name_multi.msg"),
                    t("common.fields.name_multi.help", { plural, requiredLangs })),

            tags: checkIt().nowOr(
                t("common.fields.tags.msg"),
                t("common.fields.tags.help", { singular, plural })
            ),

            title: checkIt().nowOr(
                    t("common.fields.title.msg",{singular}),
                    t("common.fields.title.help",{plural,requiredLangs}))
                    
            ,

            // title: check(edited.description).with(notdefined(t))
            // .with(requireLanguages(requiredLangs,t))
            // .with(uniqueLanguages(products.filter(t=>t.id!==edited.id).map(t=>t.description ?? {}),t)).nowOr(
            //         t("common.fields.title.msg",{singular}),
            //         t("common.fields.title.help",{plural,requiredLangs}))
                    
            // ,
        

            audience: checkIt().nowOr(
                t("common.fields.audience.msg"),
                t("common.fields.audience.help", { singular, plural: partyAudiencePlural })
            ),

            audienceList: checkIt().nowOr(
                t("common.fields.audience_list.msg"),
                t("common.fields.audience_list.help", { singular, plural: partyAudiencePlural })
            )

            ,

            userProfile: checkIt().nowOr(
                t("common.fields.user_profile.msg"),
                t("common.fields.user_profile.help", { singular, plural: userProfilePlural })
            )

            ,

            lineage: checkIt().nowOr(
                t("common.fields.lineage.msg", { singular }),
                t("product.fields.lineage.help"),
            )

            ,

            note: check(edited.properties.note ? edited.properties.note[s.logged.tenant] : '')
                .with(reservedKeyword(t)).nowOr(t("common.fields.note.msg"), t("common.fields.note.help", { singular }))

            ,

            editable: checkIt().nowOr(
                t("common.fields.editable.msg"), t("common.fields.editable.help", { plural, parties: partyAudiencePlural })
            )

            ,

            versionable: checkIt().nowOr(
                t("common.fields.versionable.msg"), t("common.fields.versionable.help", { plural, parties: partyAudiencePlural })
            )

            ,

            assessed: checkIt().nowOr(
                t("common.fields.assessed.msg"), t("common.fields.assessed.help", { plural })
            )

            ,

            ...validateCategories(edited.tags).include(contextCategory)

            ,

            ...validateCategories(edited.tags).for(productType)
        })


    }

})


