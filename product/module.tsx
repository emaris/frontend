import { Placeholder } from "apprise-frontend/components/Placeholder";
import { iamType } from "apprise-frontend/iam/constants";
import { IamSlot } from "apprise-frontend/iam/module";
import { documentspec } from "apprise-frontend/layout/components/Document";
import { filedropspec } from "apprise-frontend/layout/components/FileDrop";
import { imagespec } from "apprise-frontend/layout/components/Image";
import { inputboxspec } from "apprise-frontend/layout/components/InputBox";
import { multichoicespec } from "apprise-frontend/layout/components/MultiChoice";
import { paragraphspec } from "apprise-frontend/layout/components/Paragraph";
import { cellspec, rowspec } from "apprise-frontend/layout/components/Row";
import { rowsectionspec } from "apprise-frontend/layout/components/RowSection";
import { separatorspec } from "apprise-frontend/layout/components/Separator";
import { titlespec } from "apprise-frontend/layout/components/Title";
import { layoutType } from "apprise-frontend/layout/constants";
import { LayoutSlot } from 'apprise-frontend/layout/module';
import { numspec } from "apprise-frontend/layout/parameters/Number";
import { tenantspec } from "apprise-frontend/layout/parameters/Tenant";
import { textspec } from "apprise-frontend/layout/parameters/Text";
import { themespec } from "apprise-frontend/layout/parameters/Theme";
import { Module } from "apprise-frontend/module";
import { pushEventType } from 'apprise-frontend/push/constants';
import { tagType } from "apprise-frontend/tag/constants";
import { TagSlot } from "apprise-frontend/tag/module";
import { noTenant, tenantType } from "apprise-frontend/tenant/constants";
import { TenantSlot } from "apprise-frontend/tenant/module";
import { userType } from "apprise-frontend/user/constants";
import { UserSlot } from "apprise-frontend/user/module";
import { eventType } from "emaris-frontend/event/constants";
import { EventSlot } from "emaris-frontend/event/module";
import { assetsectionspec } from "emaris-frontend/layout/components/AssetSection";
import { reqsectionspec } from 'emaris-frontend/layout/components/RequirementSection';
import { assetspec } from "emaris-frontend/layout/parameters/Asset";
import { campaignspec } from "emaris-frontend/layout/parameters/Campaign";
import { dateyearspec } from "emaris-frontend/layout/parameters/DateYear";
import { partyspec } from "emaris-frontend/layout/parameters/Party";
import { productspec } from "emaris-frontend/layout/parameters/Product";
import { referencespec } from "emaris-frontend/layout/parameters/Reference";
import { requirementspec } from "emaris-frontend/layout/parameters/Requirement";
import * as React from "react";
import { productActions } from "./actions";
import { productIcon, productPlural, productSingular, productType } from "./constants";
import { ProductLoader } from "./Loader";
import { ProductPermissions } from "./ProductPermissions";
import { pushProductSlot } from './pushevents';


export const productmodule : Module = {

    icon: productIcon,
    type: productType,

    dependencies: () => [tagType],
    
    nameSingular: productSingular,
    namePlural: productPlural,


    [tenantType]: {

        
        tenantResource:true

    } as TenantSlot

    ,

    [pushEventType]:  pushProductSlot

    ,
    

    [iamType]: {

        actions: productActions,
     
     } as IamSlot,

    [tagType] :  {
         
          enable: true
    
    } as TagSlot,


    [eventType] :  {
         
        enable: true
  
    } as EventSlot,

    [userType] : {

        renderIf: ({subjectRange}) => subjectRange?.some(s=>s.tenant===noTenant)   
        
        ,

        permissionComponent: props => <ProductLoader placeholder={Placeholder.list}>
                                            <ProductPermissions {...props} />
                                      </ProductLoader>
  
    } as UserSlot


    ,


    [layoutType] : {

        context: {type:productType},
        components : ()=>[paragraphspec,titlespec,reqsectionspec, assetsectionspec, inputboxspec,multichoicespec, filedropspec, rowspec, cellspec, rowsectionspec,separatorspec, documentspec, imagespec],
        parameters: ()=>[textspec,numspec, productspec, dateyearspec, requirementspec, assetspec, tenantspec, themespec, partyspec, rowspec, cellspec, rowsectionspec, campaignspec, referencespec]
    
    } as LayoutSlot

}