import { State } from "#app";
import { intlapi } from "apprise-frontend/intl/api";
import { systemapi } from "apprise-frontend/system/api";
import { utilapi } from "apprise-frontend/utils/api";
import { indexMap, through } from "apprise-frontend/utils/common";
import { notify, showAndThrow } from "apprise-frontend/utils/feedback";
import { productcalls } from "./calls";
import { productPlural, productRoute, productSingular, productType } from "./constants";
import { completeOnAdd, newProduct, noProduct, Product, ProductDto, productmodelapi } from "./model";

type ProductNext = {
    model: Product
}

export type ProductState = {

    products: {
        all: Product[]
        map: Record<string, Product>
        next?: ProductNext
    }
}

export const initialProducts: ProductState = {

    products: {
        all: undefined!,
        map: undefined!
    }

}

export const productstateapi = (s: State) => {

    const model = productmodelapi(s)
    const call = productcalls(s)

    const { toggleBusy } = systemapi(s)
    const t = intlapi(s).getT()

    const type = productType
    const [singular, plural] = [t(productSingular), t(productPlural)].map(n => n.toLowerCase())


    const self = {


        areReady: () => !!s.products.all

        ,

        all: () => s.products.all

        ,

        allSorted: () => {

            return [...self.all()].sort(model.comparator)

        }

        ,

        lookup: (id: string | undefined) => id ? s.products.map?.[id] : undefined

        ,

        safeLookup: (id: string | undefined) => self.lookup(id) ?? noProduct(s)

        ,

        setAll: (products: Product[], props?: { noOverwrite: boolean }) => s.changeWith(s => {

            if (s.products.all && props?.noOverwrite)
                return

            s.products.all = products;
            s.products.map = indexMap(products).by(r => r.id)
        }

        )

        ,


        fetchAll: (forceRefresh = false): Promise<Product[]> =>

            self.areReady() && !forceRefresh ? Promise.resolve(self.all())

                :

                toggleBusy(`${type}.fetchAll`, t("common.feedback.load", { plural }))

                    .then(_ => console.log(`fetching products...`))
                    .then(_ => call.fetchAll())
                    .then(through($ => self.setAll($, { noOverwrite: true })))


                    .catch(e => showAndThrow(e, t("common.calls.fetch_all_error", { plural })))
                    .finally(() => toggleBusy(`${type}.fetchAll`))

        ,


        fetchOne: (id: string, forceRefresh?: boolean): Promise<Product> => {

            const current = self.lookup(id)

            return current && model.isLoaded(current) && !forceRefresh ?

                Promise.resolve(current) :

                toggleBusy(`${type}.fetchOne`, t("common.feedback.load_one", { singular }))

                    .then(_ => call.fetchOne(id))
                    .then(through(fetched => { if (!fetched) throw Error(`Invalid resource lookup for ${id}, returned undefined.`) }))
                    .then(through(fetched => self.setAll(self.all().map(u => u.id === id ? fetched : u))
                    ))

                    .catch(e => showAndThrow(e, t("common.calls.fetch_one_error", { singular })))
                    .finally(() => toggleBusy(`${type}.fetchOne`))

        }

        ,

        fullFetch: (product: Product, force?: boolean): Promise<Product> =>

            model.isLoaded(product) && !force ? Promise.resolve(product) : self.fetchOne(product.id)



        ,




        fetchManySilently: (ids: string[], force?: boolean): Promise<Product[]> => {

            const filteredIds = ids.filter(id => force || !model.isLoaded(self.lookup(id)))

            const fetchFiltered = filteredIds.map(id =>

                call.fetchOne(id)

                    .catch(e => {

                        console.warn(`warning: couldn't fetch product ${id}:`, e)
                        return Promise.resolve(undefined)

                    }))

            return Promise.all(fetchFiltered)
                .then(fetched => fetched.filter(p => p) as ProductDto[])
                .then(
                    through(fetched => self.setAll(self.all().map(u => fetched.find(uu => uu.id === u.id) ?? u)))
                )
        }
        ,

        routeTo: (p: Product) => p ? `${productRoute}/${p.id}` : productRoute

        ,

        next: () => {
            const next = s.products.next || { model: newProduct(s) }
            return next;
        },

        resetNext: () => {
            self.setNext(undefined)
        }
        ,

        setNext: (t: ProductNext | Product | undefined): ProductNext | undefined => {
            const model = t ? t.hasOwnProperty('model') ? t as ProductNext : { model: t as Product } : undefined

            s.changeWith(s => s.products.next = model)
            return model

        },

        save: (p: Product): Promise<Product> => {

            const product: Product = { ...p, properties: { ...p.properties, dependencies: productmodelapi(s).dependenciesOf(p) } }


            return toggleBusy(`${type}.save`, t("common.feedback.save_changes"))

                .then(() => product.id ?

                    call.update(product)
                        .then(through(saved => self.setAll(self.all().map(c => c.id === product.id ? saved : c))))

                    :

                    call.add(completeOnAdd(product))
                        .then(through(saved => self.setAll([saved, ...self.all()])))

                )
                .then(through(() => notify(t('common.feedback.saved'))))

                .catch(e => showAndThrow(e, t("common.calls.save_one_error", { singular })))
                .finally(() => toggleBusy(`${type}.save`))
        }
        ,


        remove: (id: string, onConfirm: () => void, challenge?: boolean) =>

            utilapi(s).askConsent({

                title: t('common.consent.remove_one_title', { singular }),
                content: t('common.consent.remove_one_msg', { singular }),
                okText: t('common.consent.remove_one_confirm', { singular }),

                okChallenge: challenge ? t('common.consent.remove_challenge', { singular }) : undefined,

                onOk: () => {


                    toggleBusy(`${type}.removeOne`, t("common.feedback.save_changes"))

                        .then(_ => call.delete(id))
                        .then(_ => self.all().filter(u => id !== u.id))
                        .then(self.setAll)
                        .then(_ => notify(t('common.feedback.saved')))
                        .then(_ => onConfirm && onConfirm())

                        .catch(e => showAndThrow(e, t("common.calls.remove_one_error", { singular })))
                        .finally(() => toggleBusy(`${type}.removeOne`))


                }
            }),

        removeAll: (list: string[], onConfirm?: () => void) =>

            utilapi(s).askConsent({


                title: t('common.consent.remove_many_title', { count: list.length, plural }),
                content: t('common.consent.remove_many_msg', { plural }),
                okText: t('common.consent.remove_many_confirm', { count: list.length, plural }),

                onOk: () => {


                    toggleBusy(`${type}.removeAll`, t("common.feedback.save_changes"))

                        .then(_ => list.forEach(id => call.delete(id)))
                        .then(_ => self.all().filter(u => !list.includes(u.id)))
                        .then(self.setAll)
                        .then(_ => notify(t('common.feedback.saved')))
                        .then(_ => onConfirm && onConfirm())

                        .catch(e => showAndThrow(e, t("common.calls.remove_one_error", { singular })))
                        .finally(() => toggleBusy(`${type}.removeAll`))


                }
            })


    }

    return self

}