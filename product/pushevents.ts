import { askReloadOnPushChange, askReloadOnPushRemove } from 'apprise-frontend/push/constants';
import { PushEventSlot } from 'apprise-frontend/push/module';
import { tenantRoute } from 'apprise-frontend/tenant/constants';
import { campaignapi } from 'emaris-frontend/campaign/api';
import { dashboardRoute } from 'emaris-frontend/dashboard/constants';
import { EmarisState } from 'emaris-frontend/state/model';
import shortid from 'shortid';
import { productRoute, productSingular, productType } from './constants';
import { Product } from './model';
import { productstateapi } from './state';

export type ProductChangeEvent = {

    product: Product
    type: 'add' | 'remove' | 'change'

}


export const pushProductSlot: PushEventSlot<EmarisState, ProductChangeEvent> = {

    onSubcribe: () => {

        console.log("subscribing for product changes...")

        return [{

            topics: [productType]

            ,

            onEvent: (event: ProductChangeEvent, s: EmarisState, history, location) => {

                const { product, type } = event

                console.log(`received ${type} event for product ${product.name.en}...`, { event })

                const products = productstateapi(s)
                const campaigns = campaignapi(s)

                const currentUrl = `${location.pathname}${location.search}`

                const detailOrDashBordOrTrailOnScreen = currentUrl.includes(product.id)
                const trailOnScreen = detailOrDashBordOrTrailOnScreen && currentUrl.includes(tenantRoute)
                const detailOnScreen = detailOrDashBordOrTrailOnScreen && !currentUrl.includes(dashboardRoute)

                let campaignArchived = false

                if (trailOnScreen) {
                    const campaignId = location.pathname.split("/")[2]
                    const campaign = campaigns.lookup(campaignId)
                    campaignArchived = campaignArchived || campaign?.lifecycle.state === 'archived'
                }

                switch (type) {

                    case 'add': {

                        products.setAll([...products.all(), product])

                        break
                    }

                    case 'change': {

                        const change = () => products.setAll(products.all().map(c => c.id === product.id ? product : c))
                        const changeAndRefresh = () => { change(); history.push(currentUrl, shortid()) } // uses location state to send down a signal for remount.

                        if (detailOnScreen || (trailOnScreen && !campaignArchived))
                            askReloadOnPushChange(s, { singular: productSingular, onOk: changeAndRefresh })

                        else change()

                        break
                    }

                    case 'remove': {

                        // when designing, we go back to list. in dshboard we go back to summary current campaign.
                        const fallbackRoute = detailOnScreen ? productRoute : dashboardRoute

                        const remove = () => products.setAll(products.all().filter(r => r.id !== product.id))
                        const leaveAndRemove = () => { history.push(fallbackRoute); remove() }

                        if (detailOrDashBordOrTrailOnScreen && !campaignArchived)
                            askReloadOnPushRemove(s, { singular: productSingular, onOk: leaveAndRemove })

                        else remove()

                        break
                    }
                }

            }

        },


        ]
    }
}