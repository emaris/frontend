import { Placeholder } from 'apprise-frontend/components/Placeholder'
import { Component, isContainer } from 'apprise-frontend/layout/components/model'
import { useAsyncRender } from 'apprise-frontend/utils/hooks'
import { AssetSection, assetsectionspec, sectionAssetOf } from 'emaris-frontend/layout/components/AssetSection'
import { reqsectionspec, RequirementSection } from 'emaris-frontend/layout/components/RequirementSection'
import { useRequirements } from 'emaris-frontend/requirement/api'
import { requirementType } from 'emaris-frontend/requirement/constants'
import * as React from "react"
import { useProducts } from './api'
import { productType } from './constants'
import { Product, ProductDto } from './model'


export type Props = {

    id: string,
    placeholder?: JSX.Element
    children: (product: Product) => React.ReactElement
}


export const ProductDetailLoader = (props: Props) => {

    const { id, placeholder=Placeholder.none,  children } = props

    const prods = useProducts()
    const { fetchOne, lookup, isLoaded } = prods

    const current = lookup(id)

    const initialValue = () => isLoaded(current) ? current : undefined

    const [fetched, setFetched] = React.useState<Product | undefined>(initialValue())

    const [render] = useAsyncRender({
        id: "fetchproduct",
        when: !!fetched && id === fetched.id && isLoaded(current),
        task: () => fetchOne(id).then(setFetched),
        content: ()=><ProductDepLoader product={fetched!} placeholder={placeholder}>{children(fetched!)}</ProductDepLoader>,
        placeholder

    })

    return render

}

type ProductDepLoaderProps = React.PropsWithChildren<{
    product: ProductDto,
    placeholder?: JSX.Element
}>

export const ProductDepLoader = (props: ProductDepLoaderProps) => {

    const { product, placeholder, children } = props

    const prods = useProducts()
    const reqs = useRequirements()

    const [fetchedDependencies, fetchedDependenciesSet] = React.useState<boolean>(false)
    const fetchAllDependencies = (fetched: Product) => {

        const flattencontainers = (c: Component): Component[] => isContainer(c) ? [c, ...c.children.flatMap(flattencontainers)] : []

        const referencedReqs = flattencontainers(fetched.properties.layout.components).filter(c => c.spec === reqsectionspec.id).flatMap(c => (c as RequirementSection).requirements)
        const referencedAssets = flattencontainers(fetched.properties.layout.components).filter(c => c.spec === assetsectionspec.id).flatMap(c => (c as AssetSection).assets)

        const assetReq = [...referencedReqs, ...referencedAssets.filter(a=>a.type===requirementType)].map(sectionAssetOf).reduce( (acc, cur) => (acc.includes(cur) ? acc : [...acc, cur]), [] as string[])
        const assetProd = referencedAssets.filter(a=>a.type===productType).map(sectionAssetOf)

        // fetch those never fetched before

        // NOTE: (fallback to id for retro compatibility: parameter.id used to be `requirement id` before becoming a stable, independent id)
        return Promise.all([reqs.fetchManySilently(assetReq), prods.fetchManySilently(assetProd)]) //.then(r => r.flatMap(r=>r))
        

    }

    const [render] = useAsyncRender({
        id: "fetchproductdeps",
        when: fetchedDependencies,
        task: () => fetchAllDependencies(product).then(() => fetchedDependenciesSet(true)),
        content: children,
        placeholder

    })

    return render

}
