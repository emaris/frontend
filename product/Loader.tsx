
import { Placeholder } from "apprise-frontend/components/Placeholder"
import { useAsyncRender } from "apprise-frontend/utils/hooks"
import { RequirementLoader } from "emaris-frontend/requirement/Loader"
import * as React from "react"
import { useProducts } from "./api"

type Props = React.PropsWithChildren<{

  placeholder?: React.ReactNode

}>


export const ProductLoader = (props:Props) => {

    const {areReady,fetchAll} = useProducts()

    const [render] = useAsyncRender({
      when:areReady(),
      task:fetchAll,
      content:props.children,
      placeholder:props.placeholder
  
    })
  
    return <RequirementLoader placeholder={Placeholder.none}>
              {render}
          </RequirementLoader>  
  
  }