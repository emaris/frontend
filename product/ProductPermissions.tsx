
import { any } from 'apprise-frontend/iam/model'
import { useLocale } from 'apprise-frontend/model/hooks'
import { noTenant } from 'apprise-frontend/tenant/constants'
import { UserPermissions } from 'apprise-frontend/user/UserPermissionTable'
import { ChangesProps } from 'apprise-frontend/iam/permission'
import { usePermissionDrawer } from 'apprise-frontend/iam/PermissionForm'
import { ResourceProps, StateProps, SubjectProps } from 'apprise-frontend/iam/PermissionTable'
import { User } from 'apprise-frontend/user/model'
import * as React from 'react'
import { useTranslation } from 'react-i18next'
import { productActions } from './actions'
import { useProducts } from './api'
import { productPlural, productSingular, productType } from './constants'
import { ProductLabel } from './Label'
import { Product } from './model'


type PermissionsProps = ChangesProps & Partial<ResourceProps<Product>> & Partial<SubjectProps<User>> & Partial<StateProps<User,Product>> & {
    edited? : User
}


//  wraps UserPermissions for Requirements, so as to inject appropriate defaults.

export const ProductPermissions =  (props:PermissionsProps) => {

    const {t} = useTranslation()
    const {l} = useLocale()
    const products = useProducts()

    return  <UserPermissions {...props}
                id="product-permissions" 
                subjectRange={props.subjectRange?.filter(s=>s.tenant===noTenant)}
                resourceSingular={props.resourceSingular || t(productSingular)}
                resourcePlural={props.resourcePlural || t(productPlural)}
                renderResource={props.renderResource || ((t:Product) => <ProductLabel product={t} /> )}
                resourceText={t=>l(t.name)}
                resourceId={props.resourceId || ((r:Product) => r.id) }
                renderResourceOption={props.renderResourceOption || ((r:Product) => <ProductLabel noLink product={r} />)} 
                resourceRange={props.resourceRange || products.all() } 
                resourceType={productType}

                actions={Object.values(productActions)} />
                
}


export const useProductPermissions = ()=>usePermissionDrawer (ProductPermissions, { types:[productType, any] }  )