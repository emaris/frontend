import { EmarisState } from "emaris-frontend/state/model"
import { tenantType } from "apprise-frontend/tenant/constants"
import { productType } from "emaris-frontend/product/constants"
import { requirementType } from "emaris-frontend/requirement/constants"
import { campaignapi } from "emaris-frontend/campaign/api"
import { Campaign } from "emaris-frontend/campaign/model"
import { eventType } from "emaris-frontend/event/constants"


export type TargetType = typeof tenantType | typeof productType | typeof requirementType

export type FilterSelection = {[requirementType]:string[], [productType]:string[], [tenantType]:string[], [eventType]:string[], 'action': string[]}

export type DashboardState = {

    dashboard : {

        summaryCard?: number,
        summaryRateView?: TargetType
        partySummaryCard?: number,
        campaignFilter?: string
        requirementFilter?: string,
        productFilter?: string
        partyFilter?:string
        submissionHorizon?: number

        calendarFilters?: {[key:string]:FilterSelection | undefined}

        
    }
}

export const initialDashboard = {


    dashboard : {}
}


export const dashboardstateapi = (s:EmarisState) => {
   

    const self = {


        summaryCard: () =>  s.campaigns.dashboard.summaryCard

        ,
    
        setSummaryCard: (card:number) => s.changeWith(s => s.campaigns.dashboard.summaryCard=card)

        ,

        summaryRateView: ()  =>  s.campaigns.dashboard.summaryRateView

        ,
    
        setSummaryRateView: (view:TargetType) => s.changeWith(s => s.campaigns.dashboard.summaryRateView=view)

        ,

        partySummaryCard: () =>  s.campaigns.dashboard.partySummaryCard

        ,
    
        setPartySummaryCard: (card:number) => s.changeWith(s => s.campaigns.dashboard.partySummaryCard=card)

        ,

        submissionHorizon: (campaign:Campaign) =>  s.campaigns.dashboard.submissionHorizon ?? campaignapi(s).statisticalHorizon(campaign)

        ,
    
        setSubmissionHorizon: (horizon:number) => s.changeWith(s => s.campaigns.dashboard.submissionHorizon=horizon)

    
    }

    return self;

}