
import { useLocale } from "apprise-frontend/model/hooks"
import { useTenants } from "apprise-frontend/tenant/api"
import { tenantType } from "apprise-frontend/tenant/constants"
import { useCurrentCampaign } from "emaris-frontend/campaign/hooks"
import { usePartyInstances } from "emaris-frontend/campaign/party/api"
import { PartyInstance } from "emaris-frontend/campaign/party/model"
import * as React from "react"
import { useHistory } from "react-router-dom"
import { DashboardPage } from "./DashboardPage"
import { useCurrentAsset, useDashboard } from "./hooks"
import { PartyStatsTable } from "./PartyStatsTable"
import { HorizonDecoration } from './SubmissionHorizon'


export const PartyStatsView = () => {

        const { l } = useLocale()

        const history = useHistory()
        const tenants = useTenants()
    
        const dashboard = useDashboard()

        const campaign = useCurrentCampaign()

        const parties = usePartyInstances(campaign)

        const horizon = dashboard.submissionHorizon(campaign)

        const { asset, source } = useCurrentAsset()

        const partyRoute = (p: PartyInstance) => `${history.location.pathname}/${dashboard.partyParam(p.source)}`

        const summary = dashboard.summaries[tenantType]

        const data = parties.allSorted().map(p => ({ ...p, summary: summary[p.source] }))

        return <DashboardPage title={!!asset && l(source(asset.source)?.name)} tab={tenantType}>
                <PartyStatsTable route={partyRoute} data={data} horizon={horizon}
                        filterBy={r => l(tenants.safeLookup(r.instance.source)?.name)}
                        decorations={[<HorizonDecoration /> ]} />

        </DashboardPage>
}