import { Icon } from "antd";
import * as React from "react";
import { MdSpaceDashboard } from 'react-icons/md';

export const dashboardIcon=<Icon component={MdSpaceDashboard} />

export const dashboardName = "dashboard.name"

export const dashboardRoute="/dashboard"

export const dashboardCampaignParam="campaign" 
export const dashboardPartyParam="party" 
export const dashboardViewParam="view" 
export const dashboardAllPartiesParam="all"
