import { Label } from "apprise-frontend/components/Label"
import { OptionMenu } from 'apprise-frontend/components/OptionMenu'
import { useLocale } from "apprise-frontend/model/hooks"
import { useSystem } from 'apprise-frontend/system/api'
import { useTenants } from 'apprise-frontend/tenant/api'
import { tenantIcon, tenantSingular, tenantType } from "apprise-frontend/tenant/constants"
import { clock, wait } from "apprise-frontend/utils/common"
import { showAndThrow } from 'apprise-frontend/utils/feedback'
import { useFilterState } from 'apprise-frontend/utils/filter'
import { campaignSingular, campaignType } from "emaris-frontend/campaign/constants"
import { useEventInstances } from 'emaris-frontend/campaign/event/api'
import { useCurrentCampaign } from "emaris-frontend/campaign/hooks"
import { AssetInstance } from 'emaris-frontend/campaign/model'
import { usePartyInstances } from 'emaris-frontend/campaign/party/api'
import { PartyInstance, PartyInstanceDto } from 'emaris-frontend/campaign/party/model'
import { useProductInstances } from 'emaris-frontend/campaign/product/api'
import { ProductInstance, ProductInstanceDto } from 'emaris-frontend/campaign/product/model'
import { useRequirementInstances } from 'emaris-frontend/campaign/requirement/api'
import { RequirementInstance } from 'emaris-frontend/campaign/requirement/model'
import { useSubmissions } from 'emaris-frontend/campaign/submission/api'
import { eventIcon, eventType } from "emaris-frontend/event/constants"
import 'emaris-frontend/event/styles.scss'
import { useProducts } from 'emaris-frontend/product/api'
import { productIcon, productSingular, productType } from "emaris-frontend/product/constants"
import { useRequirements } from 'emaris-frontend/requirement/api'
import { requirementIcon, requirementSingular, requirementType } from "emaris-frontend/requirement/constants"
import { TFunction } from 'i18next'
import * as React from "react"
import { useTranslation } from "react-i18next"
import { CalendarContextType, useCalendarContext } from "./calendar"
import { CalendarRow } from './CalendarTable'
import { useCalendar } from "./hooks"
import { FilterSelection } from './state'


const typeMap = (t: TFunction) => ({
    'other': t('event.types.reminder_singular'),
    [requirementType]: t(requirementSingular),
    [productType]: t(productSingular),
    [campaignType]: t(campaignSingular),
    'submitted': t("dashboard.calendar.filters.submission"),
    'assessed': t("dashboard.calendar.filters.assessment")
})

type ComputedCalendarType = {
    calendarData: CalendarRow[]
    computeCalendar: () => Promise<void>
    currentlySelected: FilterSelection
    initiallySelected: FilterSelection
    filters: JSX.Element[]
}


export const useCampaignCalendar = (props: { campaignCalendarGroup: string }): ComputedCalendarType => {

    const { campaignCalendarGroup } = props

    const { t } = useTranslation()
    const { l } = useLocale()

    const { eventInstanceToCalendarRow } = useCalendar()

    const campaign = useCurrentCampaign()
    const requirements = useRequirements()
    const products = useProducts()
    const tenants = useTenants()
    const eventInstances = useEventInstances(campaign)
    const events = eventInstances.all()
    const partyInstances = usePartyInstances(campaign)
    const requirementInstances = useRequirementInstances(campaign)
    const productInstances = useProductInstances(campaign)
    const allPartyInstances = partyInstances.all()
    const allRequirementInstances = requirementInstances.all()
    const allProductInstances = productInstances.all()

    const ctx = useFilterState(campaignCalendarGroup)

    const { calendars = {} as CalendarContextType, setCalendars } = useCalendarContext()

    const regenerate = calendars.regenerate

    React.useEffect(() => {

        if (regenerate)
            computeAndSetCalendarData()

        //eslint-disable-next-line
    }, [regenerate])

    const [calendarData, calendarDataSet] = React.useState<CalendarRow[]>(calendars.general!)

    const eventOptions = [campaignType, productType, requirementType]
    const actionOptions = ["assessed", "submitted"]

    const computeCalendarRows = async (): Promise<CalendarRow[]> => {
        const noTemporalEvents = events.filter(t => t.type !== eventType)

        // const submissionRows: CalendarRow[] = clock('building calendar submission rows', () => buildCalendarRows(trails, 'all'))

        const eventRows: CalendarRow[] = clock('building calendar event rows', () => [...noTemporalEvents.filter(e => e.date !== undefined).map(event => eventInstanceToCalendarRow(event, noTemporalEvents))].filter(row => !!row.date))

        // const calendarRows: CalendarRow[] = clock('merging submissions and events calendar rows', () => [...eventRows/*, ...submissionRows!*/])

        setCalendars({ ...calendars, general: eventRows })

        return eventRows
    }

    const initiallySelected = {
        [tenantType]: allPartyInstances.map(p => p.source),
        [requirementType]: allRequirementInstances.map(r => r.source),
        [productType]: allProductInstances.map(p => p.source),
        [eventType]: eventOptions,
        'action': actionOptions
    }

    const currentlySelected = ctx.get('toggles') ?? initiallySelected

    const EventFilter = <OptionMenu placeholderIcon={eventIcon} placeholder={t("campaign.labels.filter_event_types")}
        setSelected={types => {
            const { [eventType]: events, ...rest } = ctx.get('toggles') ?? {}
            ctx.set('toggles')(types ? { ...rest, [eventType]: types } : rest)
        }}
        selected={currentlySelected[eventType] ?? initiallySelected[eventType]}>
        {[campaignType, requirementType, productType].map(o => <OptionMenu.Option key={o} value={o} label={<Label icon={eventIcon} title={typeMap(t)[o]} />} />)}
    </OptionMenu>

    // const ActionFilter = <OptionMenu placeholderIcon={eventIcon} placeholder={t("campaign.labels.filter_action_types")}
    //     setSelected={types => {
    //         const { 'action': actions, ...rest } = ctx.get('toggles') ?? {}
    //         ctx.set('toggles')(types ? { ...rest, 'action': types } : rest)
    //     }}
    //     selected={currentlySelected['action'] ?? initiallySelected['action']}>
    //     {['assessed', 'submitted'].map(o => <OptionMenu.Option key={o} value={o} label={<Label icon={eventIcon} title={typeMap(t)[o]} />} />)}
    // </OptionMenu>


    // let filters = [EventFilter, ActionFilter]
    let filters = [EventFilter]

    if (allRequirementInstances.length > 0)
        filters.push(
            <OptionMenu placeholderIcon={requirementIcon} placeholder={t(requirementSingular)}
                setSelected={selected => {
                    const { [requirementType]: requirements, ...rest } = ctx.get('toggles') ?? {}
                    ctx.set(`toggles`)(selected ? { ...rest, [requirementType]: selected } : rest)
                }}
                selected={currentlySelected[requirementType] ?? initiallySelected[requirementType]}>
                {allRequirementInstances.map(o => <OptionMenu.Option key={o.source} value={o.source} label={<Label icon={requirementIcon} title={l(requirements.lookup(o.source)?.name)} />} />)}
            </OptionMenu>)

    if (allProductInstances.length > 0)
        filters.push(
            <OptionMenu placeholderIcon={productIcon} placeholder={t(productSingular)}
                setSelected={(selected) => {
                    const { [productType]: products, ...rest } = ctx.get('toggles') ?? {}
                    ctx.set(`toggles`)(selected ? { ...rest, [productType]: selected } : rest)
                }}
                selected={currentlySelected[productType] ?? initiallySelected[productType]}>
                {allProductInstances.map(o => <OptionMenu.Option key={o.source} value={o.source} label={<Label icon={productIcon} title={l(products.lookup(o.source)?.name)} />} />)}
            </OptionMenu>)

    if (allPartyInstances.length > 0)
        filters.push(
            <OptionMenu placeholderIcon={tenantIcon} placeholder={t(tenantSingular)}
                setSelected={(selected) => {
                    const { [tenantType]: parties, ...rest } = ctx.get('toggles') ?? {}
                    ctx.set(`toggles`)(selected ? { ...rest, [tenantType]: selected } : rest)
                }}
                selected={currentlySelected[tenantType] ?? initiallySelected[tenantType]}>
                {allPartyInstances.map(o => <OptionMenu.Option key={o.source} value={o.source} label={<Label icon={tenantIcon} title={l(tenants.lookup(o.source)?.name)} />} />)}
            </OptionMenu>)

    const computeAndSetCalendarData = () => Promise.resolve().then(wait(150)).then(computeCalendarRows).then(calendarDataSet)

    const { toggleBusy, } = useSystem()

    const computeCalendar = () => toggleBusy(`computecampaigncalendar`, t("dashboard.calendar.generating"))

        .then(computeAndSetCalendarData)
        .catch(e => showAndThrow(e, t("dashboard.calendar.generating_error", { campaign: l(campaign.name) })))
        .finally(() => toggleBusy(`computecampaigncalendar`))

    return { calendarData, computeCalendar, initiallySelected, currentlySelected, filters } as const
}


export const usePartyCalendar = (props: { party: PartyInstance, partyCalendarGroup: string }): ComputedCalendarType => {

    const { party, partyCalendarGroup } = props

    const partyId = party.source

    const { t } = useTranslation()
    const { l } = useLocale()

    const requirements = useRequirements()
    const products = useProducts()
    const campaign = useCurrentCampaign()
    const requirementInstances = useRequirementInstances(campaign)
    const productInstances = useProductInstances(campaign)
    const eventInstances = useEventInstances(campaign)
    const submissions = useSubmissions(campaign)

    const allRequirementInstances = requirementInstances.all()
    const allProductInstances = productInstances.all()

    const ctx = useFilterState(partyCalendarGroup)

    const { calendars = {} as CalendarContextType, setCalendars } = useCalendarContext()

    const regenerate = calendars.regenerate

    React.useEffect(() => {
        if (regenerate)
            computeAndSetCalendarData()

        //eslint-disable-next-line
    }, [regenerate])


    const [calendarData, calendarDataSet] = React.useState<CalendarRow[]>(calendars.party?.[partyId]!)

    const { buildCalendarRows, eventInstanceToCalendarRow } = useCalendar()

    const eventOptions = [campaignType, productType, requirementType]
    const actionOptions = ["assessed", "submitted"]

    const [partyRelatedRequirements, partyRelatedRequirementsSet] = React.useState<AssetInstance[]>(requirementInstances.allForParty(party))
    const [partyRelatedProducts, partyRelatedProductsSet] = React.useState<ProductInstanceDto[]>(productInstances.allForParty(party))

    //eslint-disable-next-line
    React.useEffect(() => partyRelatedRequirementsSet(requirementInstances.allForParty(party)), [allRequirementInstances])

    //eslint-disable-next-line
    React.useEffect(() => partyRelatedProductsSet(productInstances.allForParty(party)), [allProductInstances])

    const computeCalendarRows = async (): Promise<CalendarRow[]> => {

        /* All assets visible to party (audiance) */
        const partyRelatedAssets = [...partyRelatedRequirements, ...partyRelatedProducts]

        /* All events about campaign and visible assets */
        const partyRelatedEvents = [...eventInstances.allAbout(...partyRelatedAssets.map(a => a.source)), ...eventInstances.allOf(campaignType)]

        /* All trails for this party */
        const allPartyTrails = submissions.allTrailsForParty(party)

        const eventTrailsMap = partyRelatedEvents.reduce((acc, cur) => ({ ...acc, [cur.id]: submissions.latestIn(allPartyTrails.find(pt => pt.key.asset === cur.target)) }), {})

        const submissionRows: CalendarRow[] = clock('building calendar submission rows', () => buildCalendarRows(allPartyTrails, 'party'))

        const eventRows: CalendarRow[] = clock('building calendar event rows', () => [...partyRelatedEvents.filter(e => e.date !== undefined).map(event => eventInstanceToCalendarRow(event, partyRelatedEvents, eventTrailsMap[event.id], party)).filter(row => !!row.date)])

        const calendarRows: CalendarRow[] = clock('merging submission and events calendar rows', () => [...eventRows, ...submissionRows!])

        const calendarParty = calendars.party ?? {}

        setCalendars({ ...calendars, party: { ...calendarParty, [partyId]: calendarRows } })

        return calendarRows

    }

    const initiallySelected = {
        [tenantType]: [party.source],
        [requirementType]: partyRelatedRequirements.map(r => r.source),
        [productType]: partyRelatedProducts.map(r => r.source),
        [eventType]: eventOptions,
        'action': []
        //eslint-disable-next-line
    }

    const currentlySelected = ctx.get('toggles') ?? initiallySelected

    const EventFilter = <OptionMenu placeholderIcon={eventIcon} placeholder={t("campaign.labels.filter_event_types")}
        setSelected={types => {
            const { [eventType]: events, ...rest } = ctx.get('toggles') ?? {}
            ctx.set('toggles')(types ? { ...rest, [eventType]: types } : rest)
        }}
        selected={currentlySelected[eventType] ?? initiallySelected[eventType]}>
        {[campaignType, requirementType, productType].map(o => <OptionMenu.Option key={o} value={o} label={<Label icon={eventIcon} title={typeMap(t)[o]} />} />)}
    </OptionMenu>

    const ActionFilter = <OptionMenu placeholderIcon={eventIcon} placeholder={t("campaign.labels.filter_action_types")}
        allOptionsAsUndef={false}
        setSelected={types => {
            const { 'action': actions, ...rest } = ctx.get('toggles') ?? {}
            ctx.set('toggles')(types ? { ...rest, 'action': types } : rest)
        }}
        selected={currentlySelected['action'] ?? initiallySelected['action']}>
        {actionOptions.map(o => <OptionMenu.Option key={o} value={o} label={<Label icon={eventIcon} title={typeMap(t)[o]} />} />)}
    </OptionMenu>


    let filters = [EventFilter, ActionFilter]


    if (partyRelatedRequirements.length > 0)
        filters.push(
            <OptionMenu placeholderIcon={requirementIcon} placeholder={t(requirementSingular)}
                setSelected={selected => {
                    const { [requirementType]: requirements, ...rest } = ctx.get('toggles') ?? {}
                    ctx.set(`toggles`)(selected ? { ...rest, [requirementType]: selected } : rest)
                }}
                selected={currentlySelected[requirementType] ?? initiallySelected[requirementType]}>
                {partyRelatedRequirements.map(o => <OptionMenu.Option key={o.source} value={o.source} label={<Label icon={requirementIcon} title={l(requirements.lookup(o.source)?.name)} />} />)}
            </OptionMenu>)

    if (partyRelatedProducts.length > 0)
        filters.push(
            <OptionMenu placeholderIcon={productIcon} placeholder={t(productSingular)}
                setSelected={(selected) => {
                    const { [productType]: products, ...rest } = ctx.get('toggles') ?? {}
                    ctx.set(`toggles`)(selected ? { ...rest, [productType]: selected } : rest)
                }}
                selected={currentlySelected[productType] ?? initiallySelected[productType]}>
                {partyRelatedProducts.map(o => <OptionMenu.Option key={o.source} value={o.source} label={<Label icon={productIcon} title={l(products.lookup(o.source)?.name)} />} />)}
            </OptionMenu>)

    const computeAndSetCalendarData = () => Promise.resolve().then(wait(150)).then(computeCalendarRows).then(calendarDataSet)

    const { toggleBusy, } = useSystem()

    const computeCalendar = () => toggleBusy(`computepartycalendar`, t("dashboard.calendar.generating"))

        .then(computeAndSetCalendarData)
        .catch(e => showAndThrow(e, t("dashboard.calendar.generating_error", { campaign: l(campaign.name) })))
        .finally(() => toggleBusy(`computepartycalendar`))

    return { calendarData, computeCalendar, currentlySelected, initiallySelected, filters } as const

}

export const useAssetCalendar = (props: { asset: RequirementInstance | ProductInstance, assetCalendarGroup: string }): ComputedCalendarType => {

    const { asset, assetCalendarGroup } = props

    const assetId = asset.source

    const { t } = useTranslation()
    const { l } = useLocale()

    const tenants = useTenants()
    const campaign = useCurrentCampaign()

    const eventInstances = useEventInstances(campaign)
    const submissions = useSubmissions(campaign)
    const partyInstances = usePartyInstances(campaign)

    const ctx = useFilterState(assetCalendarGroup)

    const { calendars = {} as CalendarContextType, setCalendars } = useCalendarContext()

    const regenerate = calendars.regenerate

    React.useEffect(() => {
        if (regenerate)
            computeAndSetCalendarData()

        //eslint-disable-next-line
    }, [regenerate])

    const [calendarData, calendarDataSet] = React.useState<CalendarRow[]>(calendars.asset?.[assetId]!)

    const { buildCalendarRows, eventInstanceToCalendarRow } = useCalendar()

    const eventOptions = [campaignType]
    const actionOptions = ["assessed", "submitted"]

    const allPartyInstances = partyInstances.all()

    const [realatedPartyInstances, realatedPartyInstancesSet] = React.useState<PartyInstanceDto[]>(allPartyInstances)

    //eslint-disable-next-line
    React.useEffect(() => realatedPartyInstancesSet(allPartyInstances), [allPartyInstances])

    const computeCalendarRows = async (): Promise<CalendarRow[]> => {

        const assetRelatedEvents = [...eventInstances.allAbout(asset.source), ...eventInstances.allOf(campaignType)]

        /* All trails for this asset */
        const allAssetTrails = submissions.allTrailsForAsset(asset)

        const submissionRows: CalendarRow[] = clock('building calendar submission rows', () => buildCalendarRows(allAssetTrails, 'asset'))

        const eventRows: CalendarRow[] = clock('building calendar event rows', () => [...assetRelatedEvents.filter(e => e.date !== undefined).map(event => eventInstanceToCalendarRow(event, assetRelatedEvents)).filter(row => row.date)])

        const calendarRows: CalendarRow[] = clock('merging submissions and events calendar rows', () => [...eventRows, ...submissionRows!])

        const assetCalendar = { ...calendars.asset ?? {}, [assetId]: calendarRows }

        setCalendars({ ...calendars, asset: assetCalendar })

        return calendarRows

    }

    const initiallySelected = {
        [tenantType]: realatedPartyInstances.map(p => p.source),
        [requirementType]: asset.instanceType === requirementType ? [asset.source] : [],
        [productType]: asset.instanceType === productType ? [asset.source] : [],
        [eventType]: eventOptions,
        'action': actionOptions
    }

    const currentlySelected = ctx.get('toggles') ?? initiallySelected

    const EventFilter = <OptionMenu placeholderIcon={eventIcon} placeholder={t("campaign.labels.filter_event_types")}
        setSelected={types => {
            const { [eventType]: events, ...rest } = ctx.get('toggles') ?? {}
            ctx.set('toggles')(types ? { ...rest, [eventType]: types } : rest)
        }}
        selected={currentlySelected[eventType] ?? initiallySelected[eventType]}>
        {eventOptions.map(o => <OptionMenu.Option key={o} value={o} label={<Label icon={eventIcon} title={typeMap(t)[o]} />} />)}
    </OptionMenu>

    const ActionFilter = <OptionMenu placeholderIcon={eventIcon} placeholder={t("campaign.labels.filter_action_types")}
        setSelected={types => {
            const { 'action': actions, ...rest } = ctx.get('toggles') ?? {}
            ctx.set('toggles')(types ? { ...rest, 'action': types } : rest)
        }}
        selected={currentlySelected['action'] ?? initiallySelected['action']}>
        {actionOptions.map(o => <OptionMenu.Option key={o} value={o} label={<Label icon={eventIcon} title={typeMap(t)[o]} />} />)}
    </OptionMenu>

    let filters = [EventFilter, ActionFilter]

    if (realatedPartyInstances.length > 0)
        filters.push(
            <OptionMenu placeholderIcon={tenantIcon} placeholder={t(tenantSingular)}
                setSelected={(selected) => {
                    const { [tenantType]: parties, ...rest } = ctx.get('toggles') ?? {}
                    ctx.set('toggles')(selected ? { ...rest, [tenantType]: selected } : rest)
                }}
                selected={currentlySelected[tenantType] ?? initiallySelected[tenantType]}>
                {realatedPartyInstances.map(o => <OptionMenu.Option key={o.source} value={o.source} label={<Label icon={tenantIcon} title={l(tenants.lookup(o.source)?.name)} />} />)}
            </OptionMenu>)

    const computeAndSetCalendarData = () => Promise.resolve().then(wait(150)).then(computeCalendarRows).then(calendarDataSet)

    const { toggleBusy, } = useSystem()

    const computeCalendar = () => toggleBusy(`computeassetcalendar`, t("dashboard.calendar.generating"))

        .then(computeAndSetCalendarData)
        .catch(e => showAndThrow(e, t("dashboard.calendar.generating_error", { campaign: l(campaign.name) })))
        .finally(() => toggleBusy(`computeassetcalendar`))

    return { calendarData, computeCalendar, currentlySelected, initiallySelected, filters } as const

}