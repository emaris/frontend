
import { Badge, Tooltip } from "antd"
import { Button, Buttons } from "apprise-frontend/components/Button"
import { Label } from 'apprise-frontend/components/Label'
import { useConfig } from "apprise-frontend/config/state"
import { VSelectBox } from "apprise-frontend/form/VSelectBox"
import { specialise } from "apprise-frontend/iam/model"
import { icns } from "apprise-frontend/icons"
import { useLocale } from "apprise-frontend/model/hooks"
import { PushGuard } from 'apprise-frontend/push/PushGuard'
import { Page } from "apprise-frontend/scaffold/Page"
import { Titlebar } from "apprise-frontend/scaffold/PageHeader"
import { Tab } from "apprise-frontend/scaffold/Tab"
import { Topbar } from "apprise-frontend/scaffold/Topbar"
import { useSettingsDrawer } from "apprise-frontend/settings/hooks"
import { useSystem } from "apprise-frontend/system/api"
import { ContextAwareSelectBox } from "apprise-frontend/system/ContextAwareSelectBox"
import { TagList } from "apprise-frontend/tag/Label"
import { useTenants } from 'apprise-frontend/tenant/api'
import { tenantIcon, tenantPlural, tenantSingular, tenantType } from "apprise-frontend/tenant/constants"
import { useUsers } from "apprise-frontend/user/api"
import { messageIcon, messagePlural, messageType } from "apprise-messages/constants"
import { useFlowCount } from "apprise-messages/VirtualFlow"
import { useCampaigns } from "emaris-frontend/campaign/api"
import { campaignType, runningIcon } from "emaris-frontend/campaign/constants"
import { useCurrentCampaign } from "emaris-frontend/campaign/hooks"
import { ArchiveLabel, CampaignLabel, MutedLabel, SuspendedLabel } from "emaris-frontend/campaign/Label"
import { usePartyInstances } from "emaris-frontend/campaign/party/api"
import { PartyInstanceLabel } from "emaris-frontend/campaign/party/Label"
import { LivePartySettings } from "emaris-frontend/campaign/party/Settings"
import { eventType } from "emaris-frontend/event/constants"
import { productIcon, productPlural, productType } from "emaris-frontend/product/constants"
import { requirementIcon, requirementPlural, requirementType } from "emaris-frontend/requirement/constants"
import * as React from "react"
import { useTranslation } from "react-i18next"
import { useHistory, useLocation } from "react-router-dom"
import { StatisticsContext, useCurrentAsset, useCurrentParty, useDashboard } from "./hooks"
import { useAssetMessages, useCampaignMessages, usePartyMessages } from "./MessageView"
import { sharedDocumentsName, sharedDocumentsRoute, summaryRoute } from "./ViewRouter"


type Props = React.PropsWithChildren<{

    className?: string
    title?: React.ReactNode
    decorations?: JSX.Element[]
    tab: string

}>

export const DashboardPage = (props: Props) => {

    const { t } = useTranslation()
    const { l } = useLocale()

    const history = useHistory()
    const { search } = useLocation()

    const config = useConfig()
    const users = useUsers()
    const system = useSystem()
    const campaigns = useCampaigns()

    const logged = users.logged

    const dashboard = useDashboard()
    const campaign = useCurrentCampaign()

    const { type, asset, route, stringify, label, allSorted } = useCurrentAsset()
    const { party, tenant } = useCurrentParty()

    const { startedCampaigns } = React.useContext(StatisticsContext)

    const tenants = useTenants()
    const parties = usePartyInstances(campaign)

    const { SettingsDrawer, settingsButton, closeSettings } = useSettingsDrawer()

    const { title, tab, decorations, children, className } = props

    const context = system.contextOf(campaign)

    const routeToDesignPage = () => party ? parties.route(party) :
        asset ? route(asset) :
            campaigns.routeTo(campaign)

    const designBtn = config.get().routedTypes?.includes(campaignType) && <Button iconLeft icn={icns.edit} onClick={() => history.push(routeToDesignPage())}>{t("campaign.labels.campaign_view.design")}</Button>

    const settingsTitle = t("settings.preferences_one", { one: t(tenantSingular) })
    const customSettingsBtn = React.cloneElement(settingsButton, { type: 'primary', title: settingsTitle })

    const campaignPicker = <ContextAwareSelectBox key={startedCampaigns.length} className='campaign-picker' light
        style={{ width: 250 }}
        currentContext={context}
        searchOption={c => l(campaigns.lookup(c.id)?.name)}
        options={startedCampaigns}
        onChange={c => history.push(dashboard.preserveRouteTo(c))}
        renderOption={c => <CampaignLabel mode='option' campaign={c} />}
        lblTxt={c => l(c.name)}
        optionId={c => c.id}>

        {[campaign]}

    </ContextAwareSelectBox>

    const allParties = party ? parties.allSorted() : []

    ///// Real logic
    // const partyPicker =  (party && !logged.isSingleTenantUser() && allParties.length > 1) && <VSelectBox className='party-picker' light 
    //                 style={{width:170}}
    //                 searchOption={parties.stringify}
    //                 options={allParties}  onChange={pi=>history.push(`${dashboard.routeToParty(pi)}/${type}`)} 
    //                 renderOption={p=><PartyInstanceLabel mode='option' instance={p} />}
    //                 optionId={p=>p.source }>

    //                 {[party]}

    //                 </VSelectBox>

    ///// For demo 25/06/2020
    const partyPicker = party && logged.hasNoTenant() && <VSelectBox className='party-picker' light
        style={{ width: 170 }}
        searchOption={parties.stringify}
        options={allParties} onChange={pi => history.push(`${dashboard.routeToParty(pi)}/${history.location.pathname.substring(history.location.pathname.lastIndexOf('/') + 1)}`)}
        renderOption={p => <PartyInstanceLabel mode='option' instance={p} />}
        optionId={p => p.source}
        lblTxt={p => l(tenants.lookup(p.source)?.name)}>

        {[party]}

    </VSelectBox>

    const assetPicker = type && asset && <VSelectBox className='asset-picker' light
        style={{ width: 250 }}
        searchOption={a => stringify(a)}
        options={allSorted()} onChange={a => history.push(`${dashboard.routeToAsset(a)}/${history.location.pathname.substring(history.location.pathname.lastIndexOf('/') + 1)}`)}
        renderOption={a => label(a, { mode: 'option' })}
        optionId={p => p.source}>

        {[asset]}

    </VSelectBox>


    const routeType = (type: string) => {

        history.push(

            // navigate at party level
            party ? `${dashboard.routeToParty(party)}/${type}${search}` :
                // navigate at asset level
                asset ? `${dashboard.routeToAsset(asset)}/${type}${search}` :

                    // navigate at top level
                    `${dashboard.routeToCampaign()}/${type}${search}`

        )

    }


    const campaignMessages = useCampaignMessages()
    const partyMessages = usePartyMessages(party!)
    const assetMessages = useAssetMessages(asset!)

    const messages = party ? partyMessages : asset ? assetMessages : campaignMessages

    const manage = party ? logged.managesSomeTenant() : logged.can(specialise(campaigns.actions.manage, campaign.id))

    const unreadMessages = useFlowCount(messages.id, messages.flow)
    const unreadBadge = <Badge dot={unreadMessages > 0} className='unread-message-badge'>{t(messagePlural)}</Badge>

    const campaignPills = <React.Fragment>
        <CampaignLabel mode='tag' statusOnly campaign={campaign} />
        <ArchiveLabel campaign={campaign} />
        <SuspendedLabel campaign={campaign} />
        <MutedLabel campaign={campaign} />
        <TagList taglist={campaign.tags} />
    </React.Fragment>

    const partyPills = <React.Fragment>
        <ArchiveLabel campaign={campaign} />
        <SuspendedLabel campaign={campaign} />
        <MutedLabel campaign={campaign} />
        <TagList taglist={tenant.tags} />
    </React.Fragment>

    return <PushGuard>

        <Page className={className}>

            <Topbar activeTab={tab} onTabChange={routeType}>


                <Titlebar title={title || l(campaign.name)}>
                    <div style={{ display: "flex", alignItems: "center" }}>
                        {asset ?

                            <React.Fragment>
                                <ArchiveLabel campaign={campaign} />
                                <SuspendedLabel campaign={campaign} />
                                <MutedLabel campaign={campaign} />
                                {label(asset, { mode: 'tag', deadlineOnly: true, accentScheme: "weeks" })}
                                {asset.properties.assessed === false && <Label iconStyle={{ color: 'white' }} style={{ background: 'orange', color: 'white' }} mode='tag' title={t('submission.labels.not_assessable')} />}
                                <TagList taglist={asset.tags} />
                            </React.Fragment>

                            :

                            party ?

                                logged.hasNoTenant() ? partyPills : campaignPills
                                :
                                campaignPills
                        }

                    </div>
                </Titlebar>


                <Tab id={summaryRoute} icon={runningIcon} name={t("dashboard.labels.summary")} />
                <Tab id={eventType} icon={icns.calendar} name={t("dashboard.calendar.name")} />

                {!!party ||
                    <Tab id={tenantType} icon={tenantIcon} name={t(tenantPlural)} />
                }

                {!!asset || <Tab id={requirementType} icon={requirementIcon} name={t(requirementPlural)} />}
                {!!asset || <Tab id={productType} icon={productIcon} name={t(productPlural)} />}


                <Tab id={messageType} icon={messageIcon} name={unreadMessages > 0 ? <Tooltip title={t('message.unread_count', { count: unreadMessages })}>{unreadBadge}</Tooltip> : unreadBadge} />


                {!asset && <Tab id={sharedDocumentsRoute} icon={icns.share} name={t(sharedDocumentsName)} />}

                <div className="dashboard-pickers">
                    {decorations && decorations.map((d, i) => <div key={`decoration_${i}`}>{d}</div>)}
                    {assetPicker}
                    {partyPicker}
                    {campaignPicker}
                    <Buttons>
                        {designBtn}
                        {party && customSettingsBtn}
                    </Buttons>
                </div>

            </Topbar>

            {children}

            {party &&

                <SettingsDrawer readonly={!manage} title={settingsTitle}>
                    <LivePartySettings party={party} onSave={closeSettings} />
                </SettingsDrawer>

            }

        </Page>
    </PushGuard>
    
}
