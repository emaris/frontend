
import { Label } from 'apprise-frontend/components/Label'
import { OptionMenu } from 'apprise-frontend/components/OptionMenu'
import { icns } from 'apprise-frontend/icons'
import { useLocale } from 'apprise-frontend/model/hooks'
import { useTime } from 'apprise-frontend/time/api'
import { useUsers } from 'apprise-frontend/user/api'
import { useFilterState } from 'apprise-frontend/utils/filter'
import { useCampaigns } from 'emaris-frontend/campaign/api'
import { complianceIcon } from 'emaris-frontend/campaign/constants'
import { useCurrentCampaign } from 'emaris-frontend/campaign/hooks'
import { submissionIcon } from 'emaris-frontend/campaign/submission/constants'
import { ComplianceProfiles, managedStates, SubmissionStatus, TimelinessProfiles } from 'emaris-frontend/campaign/submission/model'
import { eventIcon } from 'emaris-frontend/event/constants'
import * as React from 'react'
import { useTranslation } from 'react-i18next'
import { AssetRow } from './AssetTrailTable'
import { PartyRow } from './PartyTrailTable'


type Row = AssetRow | PartyRow

export type SubmissionFilterProps<T extends Row> = {

    key: string,
    group: string
    

    rows: T[]
    view: 'asset' | 'party',

    complianceProfiles?: ComplianceProfiles

    timelinessProfiles?: TimelinessProfiles
   
}

export const useSubmissionFilter = <T extends Row> (props: SubmissionFilterProps<T>) => {

    const { rows=[], view } = props

    const campaigns = useCampaigns()
    const campaign = useCurrentCampaign()
    const withCompliance = campaigns.currentComplianceScale(campaign) 
    const withTimeliness = campaigns.currentTimelinessScale(campaign) 

    const { Filter: StatusFilter, filteredData: statusFilteredData } = useStatusFilter({ ...props, key: `${props.key}-status-filter`, rows: rows })

    const { Filter: ComplianceFilter, filteredData: complianceFilteredData } = useComplianceFilter({ ...props, key: `${props.key}-compliance-filter`, rows: statusFilteredData })

    const { Filter: TimelinessFilter, filteredData: timelinessFilteredData } = useTimelinessFilter({ ...props, key: `${props.key}-timeliness-filter`, rows: complianceFilteredData })

    const { Filter: DueDateFilter, filteredData: dueDateFilteredData } = useDueDateFilter({ ...props, key: `${props.key}-duedate-filter`, rows: timelinessFilteredData, dueDate: row => row.summary.dueDate })

    const Filters = view === 'asset' ? [StatusFilter, ...withCompliance ? [ComplianceFilter] : [] , ...withTimeliness ? [TimelinessFilter] : [], DueDateFilter] : [StatusFilter, ComplianceFilter, ...withTimeliness ? [TimelinessFilter] : []]
    
    return { Filters, data: dueDateFilteredData }


}

const useStatusFilter = <T extends Row> (props: { rows: T[], group: string, key: string }) => {

    const { t } = useTranslation()
    
    const { rows, group, key } = props

    const ctx = useFilterState(group)

    const ctxValues = ctx.get(key)

    const logged = useUsers().logged
 
    const hasNoTenant = logged.hasNoTenant()

    const selected = React.useMemo(() => 
    
        ctxValues ?? (hasNoTenant ? ['submitted', 'pending', 'draft', 'managed', 'published', 'missing', 'nostatus'] : ['submitted', 'pending', 'draft', 'missing', 'nostatus'])
    
    , [ctxValues, hasNoTenant])

    const Filter = <OptionMenu className='submission-filter-status' selected={selected} setSelected={ctx.set(key)} placeholder={t("dashboard.filters.status_filter")} placeholderIcon={submissionIcon}>
        <OptionMenu.Option value="submitted" key="submitted" label={<Label className='submission-label state-only submitted' mode='tag' icon={submissionIcon} title={t("submission.status.submitted")} />} />
        <OptionMenu.Option value="pending" key="pending" label={<Label className='submission-label state-only pending' mode='tag' icon={icns.pause} title={t("submission.status.pending")} />} />
        <OptionMenu.Option value="draft" key="draft" label={<Label className='submission-label state-only draft' mode='tag' icon={icns.edit} title={t("submission.status.draft")} />} />
        {hasNoTenant && <OptionMenu.Option value="managed" key="managed" label={<Label className='submission-label state-only managed' mode='tag' icon={icns.admin} title={t("submission.status.managed")} />} />}
        {hasNoTenant && <OptionMenu.Option value="published" key="published" label={<Label className='submission-label state-only published' mode='tag' icon={icns.publish} title={t("submission.status.published")} />} />}
        <OptionMenu.Option value="missing" key="missing" label={<Label className='submission-label state-only missing' mode='tag' icon={submissionIcon} title={t("submission.status.missing")} />} />
        <OptionMenu.Option value="nostatus" key="nostatus" label={<Label className='submission-label state-only nostatus' mode='tag' icon={icns.warn} title={t("submission.status.nostatus")} />} />
    </OptionMenu>


    const equalUpToManaged = (selectedState, rowState: SubmissionStatus | undefined) => 
    
        (logged.isTenantUser() && selectedState==='submitted') ? ['submitted',...managedStates].includes(rowState ?? '') : selectedState===rowState

    const filter = React.useCallback(

        (row: Row) =>  row.summary?.lastRevision ? selected.some( v => equalUpToManaged(v, row.summary?.lastRevision?.lifecycle.state)) : selected.includes('nostatus')
        
    // eslint-disable-next-line
    , [selected])

    const filteredData = React.useMemo(() => rows.filter(filter), [rows, filter])

    return { Filter, filteredData }

}

// const useTimelinessFilter = <T extends Row> (props: { rows: T[], group: string, key: string }) => {

//     const { t } = useTranslation()
    
//     const { rows, group, key } = props
    
//     const ctx = useFilterState(group)

//     const ctxValues = ctx.get(key)

//     const selected = React.useMemo(() => ctxValues ?? ['ontime', 'late', 'notdue'], [ctxValues])

//     const Filter = <OptionMenu selected={selected} setSelected={ctx.set(key)} placeholder={t("dashboard.filters.timeliness_filter")} placeholderIcon={eventIcon}>
//         <OptionMenu.Option value="ontime" key="ontime" label={<Label mode='tag' className="timeliness-ontime" icon={eventIcon} title={t("dashboard.labels.timeliness.ontime")} />} />
//         <OptionMenu.Option value="late" key="late" label={<Label mode='tag' className="timeliness-late" icon={eventIcon} title={t("dashboard.labels.timeliness.late")} />} />
//         <OptionMenu.Option value="notdue" key="notdue" label={<Label mode='tag' className="timeliness-notdue" icon={eventIcon} title={t("dashboard.labels.timeliness.notdue")} />} />
//     </OptionMenu>

//     const filter = React.useCallback((row: Row) =>  (selected.includes('notdue') && !row.summary.computedTimeliness) || selected.some(v => v === row.summary.computedTimeliness), [selected])

//     const filteredData = React.useMemo(() => rows.filter(filter), [rows, filter])

//     return { Filter, filteredData }

// }

const useTimelinessFilter = <T extends Row> (props: { rows: T[], group: string, key: string, timelinessProfiles?: TimelinessProfiles }) => {
    
    const { t } = useTranslation()
    
    const { l } = useLocale()
    
    const { rows, group, key, timelinessProfiles } = props

    const ctx = useFilterState(group)

    const sortedTimelinessProfiles = timelinessProfiles ? timelinessProfiles.sort((a, b) => a.properties.value <= b.properties.value ? 1 : -1) : undefined

    const ctxValues = ctx.get(key)

    const selected = React.useMemo(() => ctxValues ?? [...(sortedTimelinessProfiles ?? []).map(cp => cp.id), 'notdue'], [ctxValues, sortedTimelinessProfiles])

    const submissionFiltersTimelinessProfiles = sortedTimelinessProfiles ?
        [
            ...sortedTimelinessProfiles.map((cp, i) => 
            
                <OptionMenu.Option key={`timelinessprofile_${i}`} value={cp.id} label={<Label mode='tag' className={`submission-label timeliness ${cp.timelinessClass}`} icon={eventIcon} title={l(cp.name)} />} />),
           
                <OptionMenu.Option value="notdue" key="notdue" label={<Label mode='tag' className="timeliness-notdue" icon={eventIcon} title={t("dashboard.labels.timeliness.notdue")} />} />
        ]
        : []

    const Filter = <OptionMenu selected={selected} setSelected={ctx.set(key)} placeholder={t("dashboard.filters.timeliness_filter")} placeholderIcon={eventIcon}>
        {submissionFiltersTimelinessProfiles}
    </OptionMenu>

    const filter = React.useCallback(
        
        (row: Row) => {

            const matchesTag = selected.includes( row.summary.timeliness?.tag?.id )
            const noTimeliness = selected.includes('notdue') && !row.summary.timeliness 

            return noTimeliness || matchesTag        
        
        }, [selected]
    )

    const filteredData = React.useMemo(() => rows.filter(filter), [rows, filter])

    return { Filter, filteredData }

}

export const useDueDateFilter = <T extends any> (props: { 
    
    initiallySelected? : ('due'|'next')[], 
    rows: T[],  
    dueDate: (t:T) => string | undefined, 
    group: string, 
    key: string }) => {
   
    
    const { t } = useTranslation()
   
    const time = useTime()

    const now = time.current()

    const { initiallySelected = ["due", "next"], rows = [], dueDate, group, key } = props
    
    const ctx = useFilterState(group)

    const selected = ctx.get(key) ??  initiallySelected

    const Filter = <OptionMenu selected={selected} setSelected={ctx.set(key)} placeholder={t("dashboard.filters.duedate_filter")} placeholderIcon={eventIcon}>
        <OptionMenu.Option value="due" key="due" label={<Label icon={submissionIcon} mode='tag' title={t("dashboard.labels.past_requirements")} />} />
        <OptionMenu.Option value="next" key="next" label={<Label icon={submissionIcon} mode='tag' title={t("dashboard.labels.upcoming_requirements")} />} />
    </OptionMenu>

    const filter = React.useCallback(
        
        //This will catch missing due dates as next
        (row: T) => (selected.includes('due') && dueDate(row) && now.isAfter(dueDate(row))) 
                        || 
                     (selected.includes('next') && (!dueDate(row) || !now.isAfter(dueDate(row))))
        //eslint-disable-next-line        
        , [selected])

    const filteredData = React.useMemo(() => rows.filter(filter), [rows, filter])

    return { Filter, filteredData }

}


const useComplianceFilter = <T extends Row> (props: { rows: T[], group: string, key: string, complianceProfiles?: ComplianceProfiles }) => {
    
    const { t } = useTranslation()
    
    const { l } = useLocale()
    
    const { rows, group, key, complianceProfiles } = props

    const ctx = useFilterState(group)

    const sortedComplianceProfiles = complianceProfiles ? complianceProfiles.sort((a, b) => a.properties.value <= b.properties.value ? 1 : -1) : undefined

    const ctxValues = ctx.get(key)

    const selected = React.useMemo(() => ctxValues ?? [...(sortedComplianceProfiles ?? []).map(cp => cp.id), 'noCompliance', 'noAssessable'], [ctxValues, sortedComplianceProfiles])

    const submissionFiltersComplianceProfiles = sortedComplianceProfiles ?
        [
            ...sortedComplianceProfiles.map((cp, i) => 
            
                <OptionMenu.Option key={`complianceprofile_${i}`} value={cp.id} label={<Label mode='tag' className={`submission-label compliance ${cp.complianceClass}`} icon={complianceIcon} title={l(cp.name)} />} />),
           
            <OptionMenu.Option key="noCompliance" value={'noCompliance'} label={<Label mode='tag' className={`submission-label compliance `} icon={complianceIcon} title={t("common.labels.not_assessed")} />} />,
            <OptionMenu.Option key="noAssessable" value={'noAssessable'} label={<Label mode='tag' className={`submission-label compliance `} icon={complianceIcon} title={t("common.labels.not_assessable")} />} />
        ]
        : []

    const Filter = <OptionMenu selected={selected} setSelected={ctx.set(key)} placeholder={t("dashboard.filters.compliance_filter")} placeholderIcon={complianceIcon}>
        {submissionFiltersComplianceProfiles}
    </OptionMenu>

    const filter = React.useCallback(
        
        (row: Row) => {

            const isNotAssessable = selected.includes('noAssessable') && !row.summary.assessable 
            const matchesTag = selected.includes( row.summary.compliance?.tag?.id )
            const noCompliance = selected.includes( 'noCompliance') && !row.summary.compliance 

            return noCompliance || isNotAssessable || matchesTag        
        
        }, [selected]
    )

    const filteredData = React.useMemo(() => rows.filter(filter), [rows, filter])

    return { Filter, filteredData }

}
