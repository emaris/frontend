
import '@brainhubeu/react-carousel/lib/style.css'
import { Icon } from "antd"
import { Label } from "apprise-frontend/components/Label"
import { Placeholder } from 'apprise-frontend/components/Placeholder'
import { TableProps } from 'apprise-frontend/components/VirtualTable'
import { SelectBox } from 'apprise-frontend/form/SelectBox'
import { icns } from "apprise-frontend/icons"
import { useLocale } from "apprise-frontend/model/hooks"
import { useTenants } from 'apprise-frontend/tenant/api'
import { useUsers } from 'apprise-frontend/user/api'
import { group } from "apprise-frontend/utils/common"
import { useAsyncRender } from 'apprise-frontend/utils/hooks'
import { paramsInQuery, updateQuery } from "apprise-frontend/utils/routes"
import { useCampaigns } from 'emaris-frontend/campaign/api'
import { useCurrentCampaign } from "emaris-frontend/campaign/hooks"
import { PartyInstance } from 'emaris-frontend/campaign/party/model'
import { calendarOptions } from 'emaris-frontend/campaign/preferences/model'
import { ProductInstance } from 'emaris-frontend/campaign/product/model'
import { RequirementInstance } from 'emaris-frontend/campaign/requirement/model'
import { eventType } from "emaris-frontend/event/constants"
import 'emaris-frontend/event/styles.scss'
import { useProducts } from 'emaris-frontend/product/api'
import { useRequirements } from 'emaris-frontend/requirement/api'
import { requirementType } from "emaris-frontend/requirement/constants"
import moment from "moment-timezone"
import * as React from "react"
import { useTranslation } from "react-i18next"
import { AiOutlineCalendar, AiOutlineUnorderedList } from 'react-icons/ai'
import { useHistory, useLocation } from "react-router-dom"
import { useAssetCalendar, useCampaignCalendar, usePartyCalendar } from './calendarHooks'
import { CalendarListView } from './CalendarListView'
import { CalendarMonthlyView } from './CalendarMonthlyView'
import { CalendarRow } from './CalendarTable'
import { DashboardPage } from "./DashboardPage"
import { useCurrentAsset, useCurrentParty } from "./hooks"
import { FilterSelection } from './state'

const calendarModeParam = "calendar-mode"
const listMode = "list"
const monthMode = "month"

const defaultComparator = (i1, i2) => i1.name.localeCompare(i2.name)


export const CalendarView = () => {

    const { party } = useCurrentParty()
    const { asset } = useCurrentAsset()

    return party ? <PartyCalendarView party={party} /> :
        asset ? <AssetCalendarView asset={asset} /> :
            <CampaignCalendarView />


}


const CampaignCalendarView = () => {

    const campaign = useCurrentCampaign()

    const campaignCalendarGroup = `${campaign.id}-calendar`

    const { calendarData, computeCalendar, currentlySelected, initiallySelected, filters } = useCampaignCalendar({ campaignCalendarGroup })

    const [render] = useAsyncRender({

        when: !!calendarData,

        task: computeCalendar,

        content: () => <InnerView data={calendarData} filterSelections={currentlySelected} initialSelection={initiallySelected} filterGroup={campaignCalendarGroup} filters={filters} />,

        placeholder: Placeholder.page

    })

    return render

}

const PartyCalendarView = (props: { party: PartyInstance }) => {

    const { party } = props

    const { l } = useLocale()

    const users = useUsers()
    const tenants = useTenants()
    const campaign = useCurrentCampaign()

    const partyCalendarGroup = `${campaign.id}-${party.source}-calendar`

    const { calendarData, computeCalendar, currentlySelected, initiallySelected, filters } = usePartyCalendar({ party, partyCalendarGroup })

    const pageTitle = users.logged.hasNoTenant() ? l(tenants.safeLookup(party.source)?.name) : undefined

    const [render] = useAsyncRender({

        when: !!calendarData,

        task: computeCalendar,

        content: () => <InnerView data={calendarData} filterSelections={currentlySelected} initialSelection={initiallySelected} filterGroup={partyCalendarGroup} filters={filters} pageTitle={pageTitle} />,

        placeholder: Placeholder.page

    })


    return render

}

const AssetCalendarView = (props: { asset: RequirementInstance | ProductInstance }) => {
    const { asset } = props

    const { l } = useLocale()

    const requirements = useRequirements()
    const products = useProducts()
    const campaign = useCurrentCampaign()

    const assetCalendarGroup = `${campaign.id}-${asset.source}-calendar`
    const { calendarData, computeCalendar, currentlySelected, initiallySelected, filters } = useAssetCalendar({ asset, assetCalendarGroup })

    const pageTitle = asset.instanceType === requirementType ? l(requirements.safeLookup(asset.source).name) : l(products.safeLookup(asset.source).name)

    const [render] = useAsyncRender({

        when: !!calendarData,

        task: computeCalendar,

        content: () => <InnerView data={calendarData} filterSelections={currentlySelected} initialSelection={initiallySelected as any} filterGroup={assetCalendarGroup} filters={filters} pageTitle={pageTitle} />,

        placeholder: Placeholder.page

    })

    return render
}



type InnerProps = Partial<TableProps<CalendarRow>> & {
    pageTitle?: React.ReactNode
    filterSelections: FilterSelection
    initialSelection?: FilterSelection
}

const InnerView = (props: InnerProps) => {

    const { data = [], pageTitle, filterSelections, initialSelection = {}, filters, ...rest } = props

    const history = useHistory()
    const { t } = useTranslation()

    const { search } = useLocation()

    const defaultCalendarMode = useCampaigns().userPreferences().calendarView()


    const { [calendarModeParam]: mode = defaultCalendarMode } = paramsInQuery(search)
    const directives = views[mode as string]


    const sortedRows = React.useMemo(() => data.sort((o1, o2) => o1.date.diff(o2.date)), [data])

    const minDate = sortedRows[0]?.date
    const maxDate = sortedRows[sortedRows.length - 1]?.date


    //eslint-disable-next-line
    const beforeMonths = React.useMemo(() => moment().diff(minDate, 'months') >= 0 ? moment().diff(minDate, 'months') + 1 : 0, [minDate])

    //eslint-disable-next-line
    const afterMonths = React.useMemo(() => moment(maxDate).diff(moment(), 'months') + 1 >= 0 ? moment(maxDate).diff(moment(), 'months') + 1 : 0, [maxDate])


    const filteredCalendarRows = React.useMemo(() => sortedRows.filter(i => i.filter({ ...initialSelection, ...filterSelections })), [sortedRows, filterSelections, initialSelection])


    //eslint-disable-next-line
    const groupsForSelection = React.useMemo(() => group(filteredCalendarRows).by(i => directives.groupBy(i, t), directives.orderBy ?? defaultComparator), [filteredCalendarRows])

    //eslint-disable-next-line
    const selectionTotal = React.useMemo(() => groupsForSelection.reduce((acc, cur) => acc = acc + cur.group.length, 0), [])

    //In the case of monthly view we want all the groups from start of end of campaign, even if the groups are empty
    const emptyGroup = React.useMemo(() => {

        const monthDates = [
            // now for the months before.
            ...Array.from(Array(beforeMonths).keys()).map(i => ({ date: moment().subtract(i + 1, 'months') })),

            // now
            { date: moment() },

            // now for the months after.
            ...Array.from(Array(afterMonths).keys()).map(i => ({ date: moment().add(i + 1, 'months') }))

        ]

        // const groups = group(monthDates).by(
        return group(monthDates).by(

            date => directives.groupBy(date, t),  // key

            directives.orderBy ?? ((i1, i2) => i1.name.localeCompare(i2.name)) // sort

        )

        //eslint-disable-next-line
    }, [])


    const groupsForMonthly = React.useMemo(() => emptyGroup.map(g => {
        const match = groupsForSelection.findIndex(gs => gs.key.name === g.key.name)
        return { key: g.key, group: match >= 0 ? [...groupsForSelection[match].group] : [] }
        //eslint-disable-next-line
    }), [groupsForSelection])

    const modePicker = <SelectBox
        fieldStyle={{ minWidth: 150 }}
        standalone
        onChange={m => history.push(`${history.location.pathname}?${updateQuery(history.location.search).with(p => { p[calendarModeParam] = m })}`)}
        getlbl={m => <Label icon={m === monthMode ? <Icon component={AiOutlineCalendar} /> : <Icon component={AiOutlineUnorderedList} />} title={t(`dashboard.calendar.${m}`)} />}
        selectedKey={mode as string}
        getkey={o => o} >
        {Object.keys(calendarOptions)}
    </SelectBox>

    return <DashboardPage title={pageTitle} tab={eventType} decorations={[modePicker]} className={`calendar-${mode}`} >
        {
            mode === listMode ?
                <CalendarListView groups={groupsForSelection} total={selectionTotal + groupsForSelection.length} filters={filters} {...rest} />
                :
                <CalendarMonthlyView groups={groupsForMonthly} total={data.length + groupsForMonthly.length} filters={filters} {...rest} />
        }
    </DashboardPage>
}




const orderBy = (t1, t2) => t1.year - t2.year || t1.month - t2.month

const groupBy = (i: CalendarRow, t) => ({ month: i.date.month(), year: i.date.year(), name: `${t(`time.months.${i.date.month() + 1}`)} ${i.date.year()}`, icon: icns.calendar })


const views = {

    [listMode]: {
        name: "campaign.labels.event_view.flat",
        orderBy,
        groupBy
    },

    [monthMode]: {
        name: "campaign.labels.event_view.calendar",
        orderBy,
        groupBy
    }
}
