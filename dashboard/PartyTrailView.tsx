
import { useLocale } from "apprise-frontend/model/hooks"
import { useTenants } from "apprise-frontend/tenant/api"
import { tenantType } from "apprise-frontend/tenant/constants"
import { useCurrentCampaign } from "emaris-frontend/campaign/hooks"
import { usePartyInstances } from "emaris-frontend/campaign/party/api"
import { PartyInstance } from "emaris-frontend/campaign/party/model"
import * as React from "react"
import { useHistory } from "react-router-dom"
import { DashboardPage } from "./DashboardPage"
import { useCurrentAsset, useDashboard } from "./hooks"
import { PartyTrailTable } from "./PartyTrailTable"


export const PartyTrailView = () => {

        const { l } = useLocale()

        const history = useHistory()

        const tenants = useTenants()

        const dashboard = useDashboard()

        const campaign = useCurrentCampaign()

        const parties = usePartyInstances(campaign)
       
        const { asset, source } = useCurrentAsset()
        
        const partyRoute = (p: PartyInstance) => `${history.location.pathname}/${dashboard.partyParam(p.source)}`
        
        const rows = parties.allForAsset(asset!)

        return <DashboardPage title={l(source(asset!.source)?.name)} tab={tenantType}>

                <PartyTrailTable data={rows}
                        route={partyRoute}
                        filterBy={r => l(tenants.safeLookup(r.source)?.name)}/>

        </DashboardPage>
}