
import { NoSuchRoute } from 'apprise-frontend/components/NoSuchRoute'
import { Placeholder } from 'apprise-frontend/components/Placeholder'
import { parentIn } from 'apprise-frontend/utils/routes'
import { CampaignInstance } from 'emaris-frontend/campaign/model'
import { PartyInstance } from 'emaris-frontend/campaign/party/model'
import { useSubmissions } from 'emaris-frontend/campaign/submission/api'
import { SubmissionLoader } from 'emaris-frontend/campaign/submission/Loader'
import { useCurrentAsset, useCurrentParty } from 'emaris-frontend/dashboard/hooks'
import { ProductDetailLoader } from 'emaris-frontend/product/DetailLoader'
import { requirementType } from 'emaris-frontend/requirement/constants'
import { RequirementDetailLoader } from 'emaris-frontend/requirement/DetailLoader'
import * as React from 'react'
import { Redirect, useLocation } from 'react-router-dom'
import { useCurrentCampaign } from '../campaign/hooks'
import { latestSubmission } from "../campaign/submission/constants"
import { SubmissionDetail } from '../campaign/submission/Detail'
import { newSubmission, newTrail, TrailKey } from '../campaign/submission/model'

export type Props = {

    submission: string

}

// resolves a submission from the context and pushes it down.
// (split into outer and inner component or we'd have to use hooks conditionally.)
export const SubmissionRouter = (props: Props) => {

    const { submission: id } = props

    const { party } = useCurrentParty()
    const { asset } = useCurrentAsset()

    // remounts if asset type changes.
    // this cover cases wheen we move from report to linked requirements
    return id && asset && party ? <InnerSubmissionRouter key={asset.instanceType} {...props} party={party} asset={asset} /> : <NoSuchRoute />
}


export type InnerProps = {

    submission: string
    party: PartyInstance
    asset: CampaignInstance


}

export const InnerSubmissionRouter = (props: InnerProps) => {

    const { submission: id, party,asset } = props

    const campaign = useCurrentCampaign()
    const submissions = useSubmissions(campaign)
    const { pathname, search } = useLocation()

    // buidls a key for the trail in context.
    const key = { campaign: campaign.id, party: party?.source, asset: asset?.source, assetType: asset?.instanceType } as TrailKey

    // resolves trail from the key, or creates a new one with it.
    let trail = submissions.lookupTrailKey(key) ?? newTrail(key)

    // stabilises new submission in case we need to work with the very first of an empty trail.
    const newSub = React.useRef(newSubmission(trail?.id)) 

    const latest = submissions.latestIn(trail)

    if (id === latest?.id)
        return <Redirect to={`${parentIn(pathname)}/${latestSubmission}${search}`} />
   
    // resolves route to submission.
    const submission = id === latestSubmission ? (latest ?? newSub.current) : trail?.submissions?.find(s => s.id === id)

    // if we had an id and still couldn't resolve it.
    if (!submission)
        return <NoSuchRoute msg={id} />


    // at this point we have 1) an existing trail with an existing or new submission, or 2) a new trail with a new submission.

    const AssetLoader = trail.key.assetType === requirementType ? RequirementDetailLoader : ProductDetailLoader



    return <AssetLoader id={trail.key.asset} placeholder={Placeholder.page}  >{
        () =>

            // note: when we discard the latest submission we need to load a new submission.  
            // but the route remains @ 'latest' but  and the router doesn't get remounted.
            // so we force-unmount the loader instead, using the submission id as the key.
            <SubmissionLoader key={submission.id} trail={trail} submission={submission} campaign={campaign}>

                {(loaded, related, onReload) => <SubmissionDetail

                    submission={loaded}
                    relatedSubmissions={related}
                    trail={trail}
                    campaign={campaign}
                    asset={asset}
                    party={party}
                    onReload={onReload} />}

            </SubmissionLoader>

    }</AssetLoader>
}