import { configapi } from "apprise-frontend/config/state";
import { tenantapi } from "apprise-frontend/tenant/api";
import { tenantType } from "apprise-frontend/tenant/constants";
import { timeapi } from "apprise-frontend/time/api";
import { campaignapi } from "emaris-frontend/campaign/api";
import { campaignType } from "emaris-frontend/campaign/constants";
import { partyinstapi } from "emaris-frontend/campaign/party/api";
import { PartyInstance } from "emaris-frontend/campaign/party/model";
import { productinstapi } from "emaris-frontend/campaign/product/api";
import { requirementinstapi } from "emaris-frontend/campaign/requirement/api";
import { submissionapi } from "emaris-frontend/campaign/submission/api";
import { SubmissionStub, Trail, TrailDto } from "emaris-frontend/campaign/submission/model";
import { submissionstateapi } from "emaris-frontend/campaign/submission/state";
import { productType } from "emaris-frontend/product/constants";
import { requirementType } from "emaris-frontend/requirement/constants";
import { useEmarisState } from "emaris-frontend/state/hooks";
import { EmarisState } from "emaris-frontend/state/model";
import moment from "moment-timezone";
import { Campaign, CampaignInstance, campaignmodelapi } from "../campaign/model";
import { dashboardRoute } from "./constants";
import { dashboardstateapi } from "./state";

export const useDashboards = () => dashboardapi(useEmarisState())


export const dashboardapi = (s: EmarisState) => {

    const self = {


        routeTo: (c: Campaign) => self.given(c).routeToCampaign()

        ,

        given: (c: Campaign) => dashboardcampaignApi(s)(c)

        ,

        routableCampaigns: () => {
            const { routedTypes = [] } = configapi(s).get()
            const canDesignCampaign = routedTypes.includes(campaignType) 
            return canDesignCampaign ?

                campaignapi(s).all().filter(campaignapi(s).startDate).sort(campaignapi(s).startedComparator)
                :
                campaignapi(s).allStarted()
        }
        
        ,

        defaultCampaign: () => {
            const routableCampaigns = self.routableCampaigns()
            const userPreferences = campaignapi(s).userPreferences()

            return routableCampaigns.find(c => c.id === userPreferences.lastVisitedCampaign())
            ?? routableCampaigns.find(campaignmodelapi(s).isRunning) 
            ?? routableCampaigns[0]
        }

    }

    return self
}


const dashboardcampaignApi = (s: EmarisState) => (c: Campaign) => {

    const self = {


        ...dashboardstateapi(s)

        ,

        resolveParty: (source?: string) => source ? partyinstapi(s)(c).lookupBySource(source) : undefined

        ,

        resolveAsset: (type: string, source: string) => {

            switch (type) {

                case requirementType: return requirementinstapi(s)(c).lookupBySource(source)
                case productType: return productinstapi(s)(c).lookupBySource(source)
                default: return undefined

            }
        }

        ,

        routeTo: (c: Campaign) => `${dashboardRoute}/${c?.id}`

        ,


        preserveRouteTo: (c: Campaign) =>{
            
            const innerrouteRegexp = new RegExp(`(${dashboardRoute}/(.+?))(/|$)`.replace("/", "\\/"))

            const {pathname,search} = window.location

            const routeMatch = pathname.match(innerrouteRegexp)?.[1]
            const campaignMatch = pathname.match(innerrouteRegexp)?.[2]

            const linkname = c.id
    
            return routeMatch ? `${routeMatch.replace(campaignMatch!, linkname!)}${search}` : self.routeTo(c)

                    
        
        }

        ,

        routeToCampaign: () => self.routeTo(c)

        ,

        partyParam: (source: string) => {

            const param = tenantapi(s).lookup(source)?.id

            return param ? encodeURIComponent(param) : source
        }

        ,

        routeToParty: (instance: PartyInstance | string) => `${self.routeToCampaign()}/${tenantType}/${self.partyParam(typeof instance === 'string' ? instance :instance.source)}`

        ,

        assetParam: (type: string, source: string) => {

            const param = submissionapi(s)(c).profileOf(type).source(source)?.id

            return param ?? source
        }

        ,

        routeToAsset: (instance: CampaignInstance) => `${self.routeToCampaign()}/${instance.instanceType}/${self.assetParam(instance.instanceType, instance.source)}`

        ,

        routeToAssetWith: (type:string,source:string) => `${self.routeToCampaign()}/${type}/${self.assetParam(type, source)}`

        ,

        assetRouteToSubmission: (submission: SubmissionStub) => {

            const {key:{assetType,asset,party}} = submissionstateapi(s)(c).lookupTrail(submission.trail) ?? {} as Trail

            return `${self.routeToCampaign()}/${assetType}/${self.assetParam(assetType,asset)}/${tenantType}/${self.partyParam(party)}`

        }

        ,

        assetRouteToSubmissionWith: (type:string,source:string,party:string) => {

            return `${self.routeToCampaign()}/${type}/${self.assetParam(type,source)}/${tenantType}/${self.partyParam(party)}`

        }

        ,

        partyRouteToSubmission: (submission: SubmissionStub) => {

            const trail = submissionstateapi(s)(c).lookupTrail(submission.trail) ?? {} as Trail

            return self.partyRouteToSubmissionWithTrail(submission, trail)

        }

        ,

        partyRouteToSubmissionWith: (type:string,source:string,party:string) => {

    
            return `${self.routeToCampaign()}/${tenantType}/${self.partyParam(party)}/${type}/${self.assetParam(type,source)}`

        }


        ,

        partyRouteToSubmissionWithTrail: (submission: SubmissionStub,trail: TrailDto) => {

            const campaignRoute = self.routeToCampaign()
            const partyParam = self.partyParam(trail.key.party)
            const assetParam = self.assetParam(trail.key.assetType, trail.key.asset)
    
            return `${campaignRoute}/${tenantType}/${partyParam}/${trail.key.assetType}/${assetParam}/${submission.id}`

        }


        ,

        campaignStatistics: (party?: PartyInstance) => {

            const campaign = c;
            const campaigns = campaignapi(s)

            const now = timeapi(s).current()

            const start = campaigns.startDate(c)
            const started = moment(start).isBefore(now)

            const end = campaigns.endDate(c)
            const ended = end && moment(end).isBefore(now)

            const days = Math.max(0, (end ? moment(end) : moment(start).add(1.5,'year')).diff(start, 'days'))
            const progress = Math.min(days, moment(now).diff(start, 'days'))
            const progressRate = ended ? 100 : Math.round(progress * 100 / Math.max(1, days))

            const parties = partyinstapi(s)(campaign)
            const requirements = requirementinstapi(s)(campaign)
            const requirementPastDue = requirements.pastDue(party).length

            const products = productinstapi(s)(campaign)
            const productsPastDue = products.pastDue(party).length

            const totalRequirements = (party ? requirements.allForParty(party,false) : requirements.all()).length
            const totalProducts = (party ? products.allForParty(party,false) : products.all()).length

            return {

                start, 
                started,
                end, 
                ended, 

                days, 
                progress, 
                progressRate, 
                
                parties, 
                requirements, 
                products, 
                
                totalRequirements,
                totalProducts,

                requirementPastDue, 
                productsPastDue

            }

        }

    }

    return self

}



export type CampaignStats = {

    start: string,
    end: string,
    ended: boolean,
    days: number,
    progress: number,
    progressRate: number,
    parties: ReturnType<ReturnType<typeof partyinstapi>>,
    requirements: ReturnType<ReturnType<typeof requirementinstapi>>,
    products: ReturnType<ReturnType<typeof productinstapi>>,
    requirementPastDue: number,
    productsPastDue: number

}
