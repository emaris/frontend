
import { Button, Carousel, Icon, Tooltip } from "antd";
import { AccordionChildProps, AccordionLabel, useAccordion } from "apprise-frontend/components/Accordion";
import { ThrottledAutoResizer } from "apprise-frontend/components/ThrottledAutoResizez";
import { useLocale } from 'apprise-frontend/model/hooks';
import { useUsers } from 'apprise-frontend/user/api';
import { paramsInQuery } from "apprise-frontend/utils/routes";
import { messageIcon } from 'apprise-messages/constants';
import { campaignIcon } from "emaris-frontend/campaign/constants";
import React from 'react';
import { unstable_batchedUpdates } from "react-dom";
import { useTranslation } from "react-i18next";
import { AiOutlinePieChart } from "react-icons/ai";
import { ImClipboard } from 'react-icons/im';
import { RiCheckboxMultipleFill } from "react-icons/ri";
import { useHistory } from "react-router-dom";
import { DashboardPage } from "./DashboardPage";
import { useCurrentAsset, useCurrentParty, useDashboard } from "./hooks";
import { AssetMessageFlow, CampaignMessageFlow, PartyMessageFlow } from "./MessageView";
import { ComplianceCard } from "./SummaryComplianceCards";
import { ProgressCard } from "./SummaryProgressCards";
import { QuickList } from "./SummaryQuicklist";
import { SubmissionCard } from "./SummarySubmissionCards";


export const SummaryView = () => {

	const { l } = useLocale()

	const { logged } = useUsers()
	const { party, tenant } = useCurrentParty()
	const { asset, nameOf } = useCurrentAsset()
	const title = party && logged.hasNoTenant() ? l(tenant.name) : asset ? nameOf(asset) : undefined

	const { Accordion: AccordionMessage, toggleCollapsed: toggleMsg, collapsed: collapsedMsg } = useAccordion({ key:'messages',axes: 'height' });
	const { Accordion: AccordionQuick, toggleCollapsed: toggleQuick, collapsed: collapsedQuick } = useAccordion({ key:'quicklist', axes: 'height' });
	const { Accordion: AccordionCard, toggleCollapsed: toggleCard, collapsed: collapsedCard } = useAccordion({ key:'cards', collapsedSize: 40 });

	return <DashboardPage className="summary-page" title={title} tab={'summary'} >

		<ThrottledAutoResizer>{({ width, height }) => {

			return <div className='summary campaign-summary' style={{ width, height, maxHeight: height }}>

				<AccordionCard>
					<Cards onCollapseToggle={toggleCard} collapsed={collapsedCard}/>
				</AccordionCard>

				<div className='summary-lists'>

						<AccordionQuick>
							<QuickList onCollapseToggle={toggleQuick} collapsed={collapsedQuick}/>
						</AccordionQuick>

						<AccordionMessage>
							<MessageList onCollapseToggle={toggleMsg} collapsed={collapsedMsg}/>
						</AccordionMessage>

				</div>
			</div>
		}}
		</ThrottledAutoResizer>

	</DashboardPage>

}

const assetCards = ["submissions", "compliance"]
const generalCards = ["submissions", "compliance", "progress"]

const Cards = (props: AccordionChildProps = {}) => {

	const { onCollapseToggle, collapsed = false } = props;

	const history = useHistory()

	const { pathname, search } = history.location

	const { asset } = useCurrentAsset()

	// the goal is to control the carousel and make it routable on load (which helps also with testing).
	// we can't control the carousel via props, and must use a ref to change the slide on button clicks.
	// the carousel tells us back when that happens, and we keep the slide index in state to highlight its button.
	// then we externalise this state so that we can navigate back to it from wherever in the app.
	// and finally use and mantain a query param to make it routable, changing state and route in one single render (using unstable_batchedUpdates()).
	// TODO: does it need to be so messy?
	const { card: initialCard = '0' } = paramsInQuery(search)

	const dashboard = useDashboard()

	const card = dashboard.summaryCard() ?? parseInt(initialCard as string)

	const changeCard = (card: number) => {

		unstable_batchedUpdates(() => {
			dashboard.setSummaryCard(card)
			history.push(`${pathname}?card=${card}`)
		})

	}

	const carouselRef = React.useRef<any>()

	const cardorder = asset ? assetCards : generalCards
	const cards = asset ? [<SubmissionCard active={card === 0} key="0" />, <ComplianceCard active={card === 1} key="1" />] : [<SubmissionCard active={card === 0} key="0" />, <ComplianceCard active={card === 1} key="1" />, <ProgressCard active={card === 2} key="2" />]

	return <div className='summary-section cards-section'>

		<CardsHeader onCollapseToggle={onCollapseToggle} collapsed={collapsed} cardorder={cardorder} card={card} carouselRef={carouselRef} asset={!!asset} />

		<div className='section-content cards-content'>

			<Carousel initialSlide={card} infinite={false} dots={false} ref={carouselRef} beforeChange={(_, slide) => changeCard(slide)} >
				{cards}
			</Carousel>

		</div>

	</div>

}

type CardsHeaderProps = {

	cardorder: string[]
	card: number
	carouselRef: React.MutableRefObject<any>
	asset: boolean
} & AccordionChildProps

const CardsHeader = React.memo((props: CardsHeaderProps) => {

	const { t } = useTranslation()
	const { carouselRef, cardorder, card, asset, onCollapseToggle, collapsed } = props

	return <div className='section-header'>
		<AccordionLabel
			className='summary-stats-title'
			title={t(`dashboard.summary.cards.${cardorder[card]}`)}
			icon={ <Icon component={AiOutlinePieChart} /> }
			onCollapseToggle={ onCollapseToggle }
			collapsed={ collapsed }
		/>

		<div className='header-icons'>
			<Tooltip title={t(`dashboard.summary.cards.submissions`)}>
				<Button onClick={() => carouselRef.current.goTo(cardorder.indexOf("submissions"))} className={cardorder[card] === 'submissions' ? 'icon-selected' : undefined} type='link'>{<Icon component={RiCheckboxMultipleFill} />}</Button>
			</Tooltip>
			<Tooltip title={t(`dashboard.summary.cards.compliance`)}>
				{/* <Button onClick={() => carouselRef.current.goTo(cardorder.indexOf("compliance"))} className={cardorder[card] === 'compliance' ? 'icon-selected' : undefined} type='link'>{complianceIcon}</Button> */}
				<Button onClick={() => carouselRef.current.goTo(cardorder.indexOf("compliance"))} className={cardorder[card] === 'compliance' ? 'icon-selected' : undefined} type='link'>{<Icon component={ImClipboard} />}</Button>
			</Tooltip>

			{asset ||

				<Tooltip title={t(`dashboard.summary.cards.progress`)}>
					<Button onClick={() => carouselRef.current.goTo(cardorder.indexOf("progress"))} className={cardorder[card] === 'progress' ? 'icon-selected' : undefined} type='link'>{campaignIcon}</Button>
				</Tooltip>
			}
		</div>

	</div>

})


const MessageList = (props: AccordionChildProps = {}) => {

	const { t } = useTranslation()
	const { party } = useCurrentParty()
	const { asset } = useCurrentAsset()
	const { onCollapseToggle, collapsed = false } = props;

	return <div className="summary-section list-messages-section">

		<div className='section-header'>

			<AccordionLabel
				className='summary-stats-title'
				title={t('message.latest_messages')}
				icon={ messageIcon }
				onCollapseToggle={ onCollapseToggle }
				collapsed={ collapsed }
			/>
		</div>


		<div className="section-content">

			{party ?
				<PartyMessageFlow party={party} summary />
				: asset ? <AssetMessageFlow asset={asset} summary />
					: <CampaignMessageFlow summary />}

		</div>

	</div>


}




