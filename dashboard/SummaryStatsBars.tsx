
import { Col, Progress, Row, Statistic, Tooltip } from "antd"
import { SubmissionLabel } from "emaris-frontend/campaign/submission/Label"
import { SubmissionStub } from "emaris-frontend/campaign/submission/model"
import { AggregateSummary } from "emaris-frontend/campaign/submission/statistics/aggregateOne"
import * as React from "react"
import { useTranslation } from "react-i18next"

// renders a SubmissionSummary as a monodimensional or bidemensional graph of submission or compliance data.

type Props = {

        className?: string

        range: 'party' | 'asset'
        route: (_: SubmissionStub) => () => string
        theme?: 'dark'


        stats: AggregateSummary

}

export const SubmissionStatsBar = (props: Props) => {

        const { className, stats } = props

        return <Tooltip overlayClassName='progress-rate-tooltip' title={<SubmissionStatsSheet {...props} />}>

                <Progress className={`progress-rate submission-rate ${stats.submissionProfile.submissionClass} ${className}`}
                        showInfo={false} size='small'
                        successPercent={stats.submissionProfile.absoluteLateRate()}
                        percent={stats.submissionProfile.rate} />

        </Tooltip>
}


export const ComplianceStatsBar = (props: Props) => {

        const { stats } = props

        return <Tooltip placement="left" overlayClassName='progress-rate-tooltip' title={<ComplianceStatsSheet {...props} />}>

                <Progress className={`progress-rate compliance-rate ${stats.complianceProfile.complianceClass ?? ''}`}
                        showInfo={false} size='small' percent={stats.complianceProfile.rate ?? 0} />

        </Tooltip>
}

export const AssessmentStatsBar = (props: Props) => {

        const { stats: { assessmentProfile } } = props

        return <Tooltip overlayClassName='progress-rate-tooltip' title={<AssessmentStatsSheet {...props} />}>

                <Progress className={`progress-rate assessment-rate ${assessmentProfile.assessmentClass ?? ''}`}
                        showInfo={false} size='small' percent={assessmentProfile.rate ?? 0} />
        </Tooltip>

}



const SubmissionStatsSheet = (props: Props) => {

        const { t } = useTranslation()

        const { stats, theme = 'dark', route, range } = props

        const rate = stats.submissionProfile.rate
        const missingRate = stats.submissionProfile.missingRate
        const lateRate = stats.submissionProfile.lateRate()

        return <React.Fragment>

                <div className={`stats-card stats-${theme}`}>

                        <Row >

                                <StatsSheetProp range={range} route={route} subs={stats.submitted.map(s => s.lastSubmitted!)}>
                                        <Col span={8}>
                                                <Statistic className="stats-primary" title={t("dashboard.labels.submission_count")} value={stats.submitted.length} suffix={`/${stats.dueSubmissions}`} />
                                        </Col>
                                </StatsSheetProp>

                                <StatsSheetProp range={range} route={route} subs={stats.submittedAsMissing.map(s => s.lastRevision!)}>
                                        <Col span={8}>
                                                <Statistic className="stats-error" title={t("dashboard.summary.performance.submission.missing")} value={isNaN(missingRate) ? '--' : `${missingRate}%`} />
                                        </Col>
                                </StatsSheetProp>

                                <StatsSheetProp range={range} route={route} subs={stats.late().map(s => s.lastSubmitted!)}>
                                        <Col span={8}>
                                                <Statistic className="stats-warning" title={t("dashboard.summary.performance.submission.late")} value={isNaN(lateRate) ? '0%' : `${lateRate}%`} />
                                        </Col>
                                </StatsSheetProp>

                        </Row>

                        <Row style={{ marginTop: 10 }}>

                                <StatsSheetProp range={range} placement='bottom' route={route} subs={stats.drafts.map(s => s.lastRevision!)}>
                                        <Col span={8}>
                                                <Statistic title={t("dashboard.labels.submission_draft")} value={stats.drafts.length} />
                                        </Col>
                                </StatsSheetProp>

                                <StatsSheetProp range={range} placement='bottom' route={route} subs={stats.pending.map(s => s.lastRevision!)}>
                                        <Col span={8}>
                                                <Statistic title={t("dashboard.labels.submission_pending")} value={stats.pending.length} />
                                        </Col>
                                </StatsSheetProp>

                                <Col span={8}>
                                        <Statistic className={`stats-rate ${stats.submissionProfile.submissionClass}`} title={t("dashboard.labels.current_rate")} value={isNaN(rate) ? '--' : `${rate}%`} />
                                </Col>

                        </Row>

                </div>


        </React.Fragment>

}


const ComplianceStatsSheet = (props: Props) => {

        const { t } = useTranslation()

        const { stats, theme = 'dark', route, range } = props

        return <React.Fragment>

                <div className={`stats-card stats-${theme}`}>

                        <Row >

                                <StatsSheetProp range={range} route={route} subs={stats.assessed.map(s => s.lastSubmitted!)}>

                                        <Col span={8}>
                                                <Statistic title={t("dashboard.labels.assessed_count")} value={stats.assessed.length} suffix={`/${stats.assessable.length}`} />
                                        </Col>

                                </StatsSheetProp>

                                <StatsSheetProp range={range} route={route} subs={stats.submittedAsMissing.map(s => s.lastRevision!)}>

                                        <Col span={8}>
                                                <Statistic className="stats-warning" title={t("dashboard.labels.missing_count")} value={stats.submittedAsMissing.length} suffix={`/${stats.assessed.length}`} />
                                        </Col>

                                </StatsSheetProp>


                                <Col span={8}>
                                        <Statistic className={`stats-rate ${stats.complianceProfile.complianceClass}`} title={t("dashboard.labels.current_rate")} value={isNaN(stats.complianceProfile.rate) ? "--" : stats.complianceProfile.rate} suffix={`%`} />
                                </Col>


                        </Row>

                </div>


        </React.Fragment>

}

const AssessmentStatsSheet = (props: Props) => {

        const { t } = useTranslation()

        const { stats, theme = 'dark', route, range } = props


        const assessed = stats.assessed.map(s => s.lastSubmitted!)
        const notAssesed = stats.assessable.filter(s => !stats.assessed.includes(s)).map(s => s.lastSubmitted!)

        return <React.Fragment>

                <div className={`stats-card stats-${theme}`}>

                        <Row >

                                <StatsSheetProp range={range} route={route} subs={assessed}>

                                        <Col span={8}>
                                                <Statistic title={t("dashboard.labels.assessed_count")} value={assessed.length} />
                                        </Col>

                                </StatsSheetProp>

                                <StatsSheetProp range={range} route={route} subs={notAssesed}>

                                        <Col span={8}>
                                                <Statistic title={t("dashboard.labels.not_assessed_count")} value={notAssesed.length} />
                                        </Col>

                                </StatsSheetProp>

                                <Col span={8}>
                                        <Statistic className={`stats-rate ${stats.assessmentProfile.assessmentClass}`} title={t("dashboard.labels.current_rate")} value={isNaN(stats.assessmentProfile.rate) ? "--" : stats.assessmentProfile.rate} suffix={`%`} />
                                </Col>


                        </Row>

                </div>


        </React.Fragment>

}


type TrailStatisticProps = {

        className?: string,
        subs: SubmissionStub[],
        range: 'party' | 'asset',
        placement?: 'top' | 'bottom'
        route: (_: SubmissionStub) => () => string,
        children: any

}

export const StatsSheetProp = (props: TrailStatisticProps) => {

        const { className, subs, route, placement = 'top', range, children } = props

        const drillstyle = { maxHeight: 100, padding: 5, overflow: 'auto' }

        return subs.length ?

                <Tooltip className={className} placement={placement} title={

                        <div style={drillstyle}>
                                {subs.map((sub, i) =>
                                        <SubmissionLabel key={i} linkTo={route(sub)} displayMode={range} submission={sub} />)}
                        </div>}>

                        {children}

                </Tooltip>

                :

                children

}