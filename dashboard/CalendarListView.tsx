import { TableProps } from 'apprise-frontend/components/VirtualTable'
import * as React from 'react'
import { CalendarRow, CalendarTable, Group } from './CalendarTable'
import { useCalendar } from './hooks'

export type CalendarListViewProps = Partial<TableProps<CalendarRow>> & { 
    groups: Group[] 
}


export const CalendarListView = (props: CalendarListViewProps) => {

    const { data,groups, ...rest } = props
    
    const grouped: any = groups.length === 1 ? groups[0].group : groups.flatMap(({ key, group }) => [{ id: key.name, title: key }, ...group])


    const {closestGroupToNow} = useCalendar()
    
    //eslint-disable-next-line    
    const computedClosestGroupToNow = React.useMemo( () => closestGroupToNow(grouped), [])

    //eslint-disable-next-line  
    const closestIndex = React.useMemo(()=>grouped.findIndex(g => Object.keys(g).includes('title') ? g['title'].year + g['title'].month === computedClosestGroupToNow : false), [])

    return <CalendarTable data={grouped} startingRow={closestIndex ? closestIndex : 0} {...rest} > </CalendarTable>

}