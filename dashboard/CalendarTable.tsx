import { Tooltip } from 'antd'
import { useListState } from 'apprise-frontend/components/hooks'
import { Label } from 'apprise-frontend/components/Label'
import { Column, TableProps, VirtualTable } from 'apprise-frontend/components/VirtualTable'
import { icns } from 'apprise-frontend/icons'
import { tenantType } from 'apprise-frontend/tenant/constants'
import { TenantDto } from 'apprise-frontend/tenant/model'
import { useTime } from 'apprise-frontend/time/api'
import { TimeLabel } from 'apprise-frontend/time/Label'
import { compareDates } from 'apprise-frontend/utils/common'
import { useEventInstances } from 'emaris-frontend/campaign/event/api'
import { useCurrentCampaign } from 'emaris-frontend/campaign/hooks'
import { eventPlural, eventType } from 'emaris-frontend/event/constants'
import { productType } from 'emaris-frontend/product/constants'
import { ProductDto } from 'emaris-frontend/product/model'
import { requirementType } from 'emaris-frontend/requirement/constants'
import { RequirementDto } from 'emaris-frontend/requirement/model'
import moment from 'moment-timezone'
import * as React from 'react'
import { useTranslation } from 'react-i18next'

export type Group = {
    key: any,
    group: CalendarRow[]
}
export type CalendarRow = {
    id: string
    label: JSX.Element
    date: moment.Moment
    type: string
    stringify: (f: string, i: CalendarRow) => boolean
    filter: (selection: { [requirementType]: string[], [eventType]: string[], [productType]: string[], [tenantType]: string[], 'action' : string[] }) => boolean
    party?: TenantDto
    target?: ProductDto | RequirementDto
    targetType?: typeof requirementType | typeof productType
}

export type GroupedRow = CalendarRow | {
    id: any;
    title: any;
}


export type CalendarTableProps = TableProps<any> & {
    startingRow?: number
    flexGrowFirstColumn?: boolean
    firstColumnWidth?: number
    dateFormat?: string
}

export const CalendarTable = (props: CalendarTableProps) => {
    
    const { t } = useTranslation()

    const { data, startingRow, firstColumnWidth=200, dateFormat=t("time.default_format_short") /*"MMMM DD, dddd"*/, ...rest } = props

    const time = useTime()
    const campaign = useCurrentCampaign()
    const campaignTimeZone = campaign.properties.timeZone ? {'original' : campaign.properties.timeZone} : undefined
    const api = useEventInstances(campaign)
    const { absolute } = api
    const plural = t(eventPlural)
    const liststate = useListState<CalendarRow>()

    return <VirtualTable<CalendarRow> data={data} state={liststate} rowKey={'id'}
        filterWith={(f, i) => (i as any).title || i.stringify(f, i)}
        filterPlaceholder={t("common.components.table.filter_placeholder", { plural: plural.toLowerCase() })}
        customProperties={({ rowData }) => ({ title: (rowData as any).title })}
        customRowRenderer={InstanceRow}
        selectable={false}
        scrollToRow={startingRow}
        countFilter={t=> (t && t.date) ? true : false}
        {...rest}>

        <Column<CalendarRow> 
            sortable={false} 
            width={firstColumnWidth} 
            title={t("common.fields.date.name")} 
            comparator={compareDates} 
            dataKey="t.date" 
            dataGetter={t => t.date} 
            refreshOn={data}
            cellRenderer={({ rowData: e, rowIndex: i }) => {
                    if (e.date) {
                        var format = time.preferredFormatFor(e.date,campaignTimeZone)

                        //We are showing on the table absolute date (probably UTC) rather than localized... NOT THE HOVERED ONE BUT THE ONE ON THE TABLE
                        const sameDate = i > 0 && data[i-1].date &&  e.date.tz(format.zone).isSame(moment(new Date(absolute(data[i-1].date)!)).tz(format.zone), 'date')
                        
                        const date = <span className={`day ${sameDate?'same-day':''}`}>{e.date.locale(moment.locale()).tz(format.zone).format(dateFormat)}</span>
                        const tooltip = <Tooltip title={() => <TimeLabel icon={icns.calendar} value={e.date} timezones={campaignTimeZone} />}>
                            {date}
                        </Tooltip>
                        
                        return tooltip
                    }
                }
                
            }/>

        <Column<CalendarRow> sortable={false} flexGrow={1} title={t("common.fields.name_multi.name")} dataKey="t.label" 
                        dataGetter={e => e.label} 
                        cellRenderer={row => row.rowData.label} />
    
    </VirtualTable>

}

const InstanceRow = props => {

    const { title, ...rest } = props
    const { t } = useTranslation()

    if (!title)
        return <div {...rest} />

    return <div {...rest} className={`${rest.className} event-table-title`}>
        <Label icon={title.icon} title={t(title.name)} />
    </div>

}
