
import { Column, TableProps, VirtualTable } from "apprise-frontend/components/VirtualTable"
import { useLocale } from "apprise-frontend/model/hooks"
import { useTenants } from "apprise-frontend/tenant/api"
import { tenantType } from "apprise-frontend/tenant/constants"
import { compareDates, compareNumbers } from "apprise-frontend/utils/common"
import { useCampaigns } from "emaris-frontend/campaign/api"
import { useCurrentCampaign } from "emaris-frontend/campaign/hooks"
import { PartyInstanceLabel } from "emaris-frontend/campaign/party/Label"
import { PartyInstance } from "emaris-frontend/campaign/party/model"
import { useSubmissions } from "emaris-frontend/campaign/submission/api"
import { SubmissionLabel } from "emaris-frontend/campaign/submission/Label"
import { SubmissionStub } from "emaris-frontend/campaign/submission/model"
import { PartySummary } from "emaris-frontend/campaign/submission/statistics/party"
import * as React from "react"
import { useTranslation } from "react-i18next"
import { useHistory } from "react-router-dom"
import { submissionType } from '../campaign/submission/constants'
import { useCurrentAsset, useDashboard } from "./hooks"
import { AssessmentStatsBar, ComplianceStatsBar, SubmissionStatsBar, StatsSheetProp } from "./SummaryStatsBars"

export type Row = { 
        instance: PartyInstance, 
        summary: PartySummary, 
        staticSummary: PartySummary     // used for assessment and (currently at least) compliance. 
}

export type Props = Partial<Omit<TableProps<Row>, 'data'>> & {

        horizon?: number
        mode?: 'full' | 'summary'
        route: (_: PartyInstance) => string
        data: PartyInstance[]
}


export const PartyStatsTable = (props: Props) => {

        const { t } = useTranslation()
        const { l } = useLocale()
        const history = useHistory()

        const { data, route, mode = 'full', horizon,...rest } = props

        const tenants = useTenants()
        const campaigns = useCampaigns()

        const dashboard = useDashboard()

        const campaign = useCurrentCampaign()
        const submissions = useSubmissions(campaign)

        const { asset } = useCurrentAsset()

        const assetRoute = (r: Row) => (sub: SubmissionStub) => {

                const trail = submissions.lookupTrail(sub.trail)!
                const type = submissions.lookupTrail(sub.trail)!.key.assetType
                return () => `${route(r.instance)}/${type}/${dashboard.assetParam(type, trail.key.asset)}/${sub.id}`
        }

        const summary = dashboard.summaries[tenantType]

        // uses full cache summary to replace compliance profile with a stable one
        const staticSummary = dashboard.summaries.staticparties

        const rows : Row[] = data.filter(p=>summary[p.source]).map(p => ({id:p.source, instance:p, staticSummary: staticSummary[p.source], summary: summary[p.source]  }))
     
        const partyStatsGroup = `${campaign.id}-${tenantType}-stats`

        return <VirtualTable<Row> selectable={false}
                filtered={mode === 'full' || data.length > 5}
                filterGroup={partyStatsGroup}
                data={rows} total={data.length}
                onDoubleClick={p => history.push(route(p.instance))}
                {...rest}>


                <Column<Row> flexGrow={1} refreshOn={asset?.source} dataKey="party"

                        title={t("common.fields.name_multi.name")}

                        dataGetter={r => l(tenants.safeLookup(r.instance.source)?.name)}
                        cellRenderer={({ rowData: p }) => <PartyInstanceLabel noDecorations linkTarget={submissionType} linkTo={() => route(p.instance)} instance={p.instance} />} />


                <Column<Row> flexGrow={1} dataKey="submissionrate"

                        title={t("dashboard.labels.submission_rate.title")}
                        headerTooltip={t('dashboard.labels.submission_rate.party_tip', { days: horizon })}

                        dataGetter={r => r.summary.submissionProfile.rate}
                        cellRenderer={({ rowData: r }) => <SubmissionStatsBar className="submissions-due" range='asset' route={assetRoute(r)} stats={r.summary} />
                        } />

                {campaigns.currentComplianceScale(campaign) &&


                        <Column<Row> flexGrow={1} dataKey="assessmentscore"

                                title={t("dashboard.labels.assessment_rate.title")}
                                headerTooltip={t('dashboard.labels.assessment_rate.party_tip')}
                                comparator={(r1,r2) => compareNumbers(r2,r1)}
                                dataGetter={r => r.staticSummary.assessmentProfile.rate!}
                                cellRenderer={({ rowData: r }) => <AssessmentStatsBar range='asset' route={assetRoute(r)} stats={r.staticSummary} />

                                } />
                }

                {campaigns.currentComplianceScale(campaign) &&


                        <Column<Row> flexGrow={1} dataKey="compliancescore"

                                title={t("dashboard.labels.compliance_rate.title")}
                                headerTooltip={t('dashboard.labels.compliance_rate.party_tip')}
                                comparator={(r1,r2) => compareNumbers(r2,r1)}
                                dataGetter={r => r.summary.complianceProfile.rate}
                                cellRenderer={({ rowData: r }) => <ComplianceStatsBar range='asset' route={assetRoute(r)} stats={r.staticSummary} />

                                } />}

                <Column<Row> width={130} dataKey="upcomingstats"

                        title={t("dashboard.labels.upcoming_stats.title")}
                        headerTooltip={t('dashboard.labels.upcoming_stats.tip', { days: horizon })}

                        dataGetter={r => r.summary.next.submitted.length}
                        cellRenderer={({ rowData: r }) => <span className='next-work-summary'>

                                <StatsSheetProp range='asset' route={assetRoute(r)} subs={r.summary.next.submitted.map(s => s.lastSubmitted!)}>
                                        <span className='next-submitted'>{r.summary.next.submitted.length}<span className='symbol'>S</span></span>
                                </StatsSheetProp>

                                &nbsp;

                                <StatsSheetProp range='asset' route={assetRoute(r)} subs={r.summary.next.pending.map(s=>s.lastRevision!)}>
                                        <span className='next-pending'>{r.summary.next.pending.length}<span className='symbol'>P</span></span>
                                </StatsSheetProp>

                                &nbsp;

                                <StatsSheetProp range='asset' route={assetRoute(r)} subs={r.summary.next.drafts.map(s=>s.lastRevision!)}>
                                        <span className='next-drafts'>{r.summary.next.drafts.length}<span className='symbol'>D</span></span>
                                </StatsSheetProp>


                        </span>}
                />



                 <Column<Row> width={150} dataKey="latestsubmission"

                                title={t("dashboard.labels.submitted.aggregated_title")}
                                headerTooltip={t('dashboard.labels.submitted.parties_tip')}

                                comparator={[(d1,d2) => compareDates(d1,d2,false), (d1,d2) => compareDates(d1,d2,true)]}

                                dataGetter={r => r.summary.lastSubmitted?.lifecycle.lastSubmitted}
                                cellRenderer={({ rowData: r }) => r.summary.lastSubmitted && r.summary.lastSubmitted.lifecycle.state !== 'missing' && <SubmissionLabel tipMode='asset' linkTo={assetRoute(r)(r.summary.lastSubmitted)} submission={r.summary.lastSubmitted} noPseudoNames />} />


                <Column<Row> width={150} dataKey="latestedit"

                                title={t("dashboard.labels.last_edit.aggregated_title")}
                                headerTooltip={t('dashboard.labels.last_edit.parties_tip')}

                                comparator={[(d1,d2) => compareDates(d1,d2,false), (d1,d2) => compareDates(d1,d2,true)]}


                                dataGetter={r => r.summary.last?.lifecycle.lastModified}
                                cellRenderer={({ rowData: r }) => r.summary.last && <SubmissionLabel tipMode='asset' linkTo={assetRoute(r)(r.summary.last)} submission={r.summary.last} noPseudoNames />} />

                




        </VirtualTable>

}