
import { Tooltip } from "antd"
import { Column, TableProps, VirtualTable } from "apprise-frontend/components/VirtualTable"
import { useLocale } from "apprise-frontend/model/hooks"
import { useTenants } from "apprise-frontend/tenant/api"
import { TimeLabel } from "apprise-frontend/time/Label"
import { compareDates, compareTags } from "apprise-frontend/utils/common"
import { useCampaigns } from "emaris-frontend/campaign/api"
import { useCurrentCampaign } from "emaris-frontend/campaign/hooks"
import { PartyInstanceLabel } from "emaris-frontend/campaign/party/Label"
import { PartyInstance } from "emaris-frontend/campaign/party/model"
import { latestSubmission } from "emaris-frontend/campaign/submission/constants"
import { SubmissionLabel } from "emaris-frontend/campaign/submission/Label"
import { trailSummary, TrailSummary } from 'emaris-frontend/campaign/submission/statistics/trail'
import { TimelinessLabel } from "emaris-frontend/campaign/submission/TimelinessLabel"
import { eventIcon } from "emaris-frontend/event/constants"
import { NotAssessableLabel } from 'emaris-frontend/requirement/Label'
import React from 'react'
import { useTranslation } from "react-i18next"
import { Link, useHistory } from "react-router-dom"
import { submissionType } from '../campaign/submission/constants'
import { useCurrentAsset, useDashboard, useTrail } from "./hooks"
import { useSubmissionFilter } from "./SubmissionFilter"


export type PartyRow = PartyInstance & { summary: TrailSummary }


export type Props = Partial<Omit<TableProps<PartyRow>, 'data'>> & {

    mode?: 'full' | 'summary'
    route: (_: PartyInstance) => string
    data: PartyInstance[]
}

export const PartyTrailTable = (props: Props) => {

    const { t } = useTranslation()
    const { l } = useLocale()

    const { mode = 'full', data, route, ...rest } = props

    const history = useHistory()

    

    const tenants = useTenants()
    const campaigns = useCampaigns()

    const campaign = useCurrentCampaign()
    const { summaries } = useDashboard()

    const campaignTimeZone = campaign.properties.timeZone ? { 'original': campaign.properties.timeZone } : undefined

    const { asset } = useCurrentAsset()
    const { submissions } = useTrail()

    const routeToSubmitted = (a: PartyRow) => `${route(a)}/${a.summary.lastSubmitted?.id ?? 'unknown'}`
    const routeToHistory = (a: PartyRow) => `${route(a)}/${latestSubmission}?tab=history`

    const partyTrailGroup = `${campaign.id}-${asset?.source}-trail`

    const summaryOf = (party: PartyInstance) => summaries.trails[party.source]?.find(s => s.trail.key.asset === asset?.source) ?? trailSummary(undefined!)

    const unfilteredrows: PartyRow[] = React.useMemo(() =>

        data.map(party => ({ ...party, summary: summaryOf(party) } as PartyRow))

        //eslint-disable-next-line
        , [data, summaries.trails])


    const { Filters: SubmissionFilters, data: rows } = useSubmissionFilter({
        rows: unfilteredrows,
        key: 'submission-filter',
        group: partyTrailGroup,
        complianceProfiles: submissions.allComplianceProfiles(),
        timelinessProfiles: submissions.allTimelinessProfiles(),
        view: 'party'
    })

    return <VirtualTable<PartyRow> rowKey='source'
        selectable={false}
        filtered={mode === 'full'}
        {...rest}
        data={rows}
        total={data.length}
        filterGroup={partyTrailGroup}
        filters={mode === 'full' ? [...props.filters ?? [], ...SubmissionFilters] : props.filters}
        onDoubleClick={p => history.push(route(p))}>

        <Column<PartyRow> min-width={200} refreshOn={asset?.source} flexGrow={1} title={t("common.fields.name_multi.name")} decorations={[]}
            dataKey="party"
            dataGetter={r => l(tenants.safeLookup(r.source)?.name)}
            cellRenderer={({ rowData: p }) => <PartyInstanceLabel noDecorations linkTo={() => route(p)} instance={p} linkTarget={submissionType} />} />



        {/* <Column<PartyRow> minWidth={110} className='trail-timeliness' dataKey="timeliness"

            title={t("dashboard.labels.timeliness.title")}
            headerTooltip={t("dashboard.labels.timeliness.party_tip")}

            dataGetter={r => r.summary.computedTimeliness}
            cellRenderer={({ rowData: r }) => r.summary.computedTimeliness &&

                <Label mode='tag' className={`timeliness-${r.summary.computedTimeliness}`}
                    icon={eventIcon}
                    title={t(`dashboard.labels.timeliness.${r.summary.computedTimeliness}`)} />} /> */}

        <Column<PartyRow> minWidth={110} className='trail-timeliness' dataKey="timeliness"

            title={t("dashboard.labels.timeliness.title")}
            headerTooltip={t("dashboard.labels.timeliness.party_tip")}

            dataGetter={r => r.summary.timeliness}
            comparator={(t1,t2) => compareTags(t1?.tag, t2?.tag)}
            cellRenderer={({ rowData: r }) => r.summary.timeliness && <TimelinessLabel party={r} />} />


        <Column<PartyRow> minWidth={150} dataKey="submitted"

            title={t("dashboard.labels.submitted.title")}
            headerTooltip={t("dashboard.labels.submitted.party_tip")}

            dataGetter={r => r.summary.lastSubmitted?.lifecycle.lastSubmitted}
            comparator={[(d1, d2) => compareDates(d1, d2, false), (d1, d2) => compareDates(d1, d2, true)]}
            cellRenderer={({ rowData: r }) => r.summary.lastSubmitted && r.summary.lastSubmitted.lifecycle.state !== 'missing' &&
                <SubmissionLabel tipMode='author' noPseudoNames
                    linkTo={routeToSubmitted(r)}   // if submission is last revision, this is the same as row's link.
                    submission={r.summary.lastSubmitted} />} />


        {(asset?.properties.assessed ?? true) && campaigns.currentComplianceScale(campaign) &&

            <Column<PartyRow> minWidth={150} dataKey="compliance"

                title={t("dashboard.labels.compliance.title")}
                headerTooltip={t("dashboard.labels.compliance.party_tip")}
                dataGetter={r => r.summary.assessable ? l(r.summary.compliance?.name) ?? "zz1": "zz2"}
                cellRenderer={({ rowData: r }) => {

                    if (r.summary.assessable === false) return <NotAssessableLabel emphasis={false} />

                    const submission = r.summary.lastSubmitted

                    return submission && <SubmissionLabel displayMode='compliance-rating' submission={submission} />

                }} />

        }


        <Column<PartyRow> minWidth={130} dataKey="status"

            title={t("dashboard.labels.status.title")}
            headerTooltip={t("dashboard.labels.status.party_tip")}

            dataGetter={r => r.summary.lastRevision?.lifecycle.state}
            cellRenderer={({ rowData: r }) => r.summary.lastRevision && <SubmissionLabel displayMode='state-only' noTip submission={r.summary.lastRevision} />} />



        {/* {

            mode === 'summary' ||

            <Column<Row> minWidth={150} dataKey="lastEdited"

                title={t("dashboard.labels.last_edit.title")}
                headerTooltip={t("dashboard.labels.last_edit.party_tip")}

                dataGetter={r => r.summary.last?.lifecycle.lastModified}
                cellRenderer={({ rowData: r }) => r.summary.last && <SubmissionLabel noPseudoNames tipMode='author' submission={r.summary.last} />} />

        } */}

        {

            mode === 'summary' ||

            <Column<PartyRow> minWidth={150} dataKey="lastUpdated"

                title={t("dashboard.labels.last_update.title")}
                headerTooltip={t("dashboard.labels.last_update.party_tip")}
                comparator={[(d1, d2) => compareDates(d1, d2, false), (d1, d2) => compareDates(d1, d2, true)]}

                dataGetter={r => r.summary.lastUpdate}
                cellRenderer={({ rowData: r }) => r.summary.lastUpdate && <TimeLabel icon={eventIcon} displayMode='relative' value={r.summary.lastUpdate} timezones={campaignTimeZone} />} />

        }

        {

            mode === 'summary' ||


            <Column<PartyRow> align='center' minWidth={100} dataKey="revisionCount"

                title={t("dashboard.labels.revisions.title")}
                headerTooltip={t("dashboard.labels.revisions.party_tip")}

                dataGetter={r => r.summary.revisions}
                cellRenderer={({ rowData: r }) => r.summary.lastRevision?.lifecycle.state !== 'missing' &&
                    <Link to={routeToHistory(r)}>
                        <Tooltip mouseEnterDelay={0.3} title={t("dashboard.labels.revisions.count_tip")}>
                            {r.summary.revisions && r.summary.revisions}
                        </Tooltip>
                    </Link>} />

        }

    </VirtualTable>
}