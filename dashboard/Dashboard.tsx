

import { AccordionProvider } from 'apprise-frontend/components/Accordion'
import { NoSuchRoute } from "apprise-frontend/components/NoSuchRoute"
import { Placeholder } from "apprise-frontend/components/Placeholder"
import { useLocale } from 'apprise-frontend/model/hooks'
import { useSystem } from 'apprise-frontend/system/api'
import { tenantType } from "apprise-frontend/tenant/constants"
import { useTime } from "apprise-frontend/time/api"
import { useUsers } from "apprise-frontend/user/api"
import { clock, wait } from "apprise-frontend/utils/common"
import { showAndThrow } from 'apprise-frontend/utils/feedback'
import { useAsyncRender } from "apprise-frontend/utils/hooks"
import { useCampaigns } from "emaris-frontend/campaign/api"
import { campaignType } from "emaris-frontend/campaign/constants"
import { useEventInstances } from "emaris-frontend/campaign/event/api"
import { CurrentCampaignContext, useCurrentCampaign } from "emaris-frontend/campaign/hooks"
import { CampaignLoader } from "emaris-frontend/campaign/Loader"
import { usePartyInstances } from "emaris-frontend/campaign/party/api"
import { PartyInstanceDto } from 'emaris-frontend/campaign/party/model'
import { useProductInstances } from "emaris-frontend/campaign/product/api"
import { useRequirementInstances } from "emaris-frontend/campaign/requirement/api"
import { useSubmissions } from "emaris-frontend/campaign/submission/api"
import { TrailKey } from 'emaris-frontend/campaign/submission/model'
import { TrailSummary } from 'emaris-frontend/campaign/submission/statistics/trail'
import { productType } from "emaris-frontend/product/constants"
import { requirementType } from "emaris-frontend/requirement/constants"
import { homeRoute } from "#home/constants"
import moment from "moment-timezone"
import * as React from "react"
import { useTranslation } from "react-i18next"
import { Redirect, Route, Switch } from "react-router-dom"
import { Campaign } from "../campaign/model"
import { useDashboards } from "./api"
import { CalendarContext } from './calendar'
import { dashboardRoute } from "./constants"
import { StatisticsContext, Summaries } from "./hooks"
import "./styles.scss"
import { ViewRouter } from "./ViewRouter"



export const Dashboard = () => {

    return <CampaignLoader placeholder={Placeholder.page}>
        <CampaignRouter />
    </CampaignLoader>

}

const CampaignRouter = () => {

    const campaigns = useCampaigns()
    const dashboard = useDashboards()
    const { t } = useTranslation()


    return <Switch>

        <Route exact path={homeRoute} render={() => <Redirect to={`${dashboardRoute}`} />} />

        <Route exact path={dashboardRoute} render={() => {

            const defaultCamapaign = dashboard.defaultCampaign()

            return defaultCamapaign ? <Redirect to={dashboard.routeTo(defaultCamapaign)} /> : <NoSuchRoute fullMsg={t("dashboard.no_campaign")} />
        }} />



        <Route path={`${dashboardRoute}/:id`} render={({ match: { params } }) => {

            const resolved = campaigns.lookup(params?.id as string)

            return resolved ? <CampaignDataLoader campaign={resolved} /> : <NoSuchRoute />

        }} />

    </Switch>

}


const CampaignDataLoader = ({ campaign }: { campaign: Campaign }) => {

    const campaigns = useCampaigns()

    const [fetched, setFetched] = React.useState<Campaign | undefined>(undefined)

    React.useEffect(() => {

        setFetched(campaign)

    }, [campaign])

    const [render] = useAsyncRender({

        when: !!fetched && fetched.id === campaign.id,

        // fetch only if it hasn't been already loaded.
        task: () => Promise.resolve(campaigns.isLoaded(campaign.id) ? campaign : campaigns.fetchOne(campaign)).then(setFetched),

        // We mount the campaign both for reading and for read/write (e.g for Settings)
        content: () => <CurrentCampaignContext campaign={fetched!}>

            <ContextBuilder />
        </CurrentCampaignContext>,

        placeholder: Placeholder.page

    })

    return render

}

const ContextBuilder = () => {

    const users = useUsers()
    const campaigns = useCampaigns()

    const campaign = useCurrentCampaign()

    // updates user preferences with last visited campaigns

    const newVisit = campaign.id !== users.logged.details.preferences.lastVisitedCampaign

    const updatePreferences = React.useCallback(() => {

        if (newVisit) {
            console.log("updating user preferences with last visited campaign")
            campaigns.userPreferences().setLastVisitedCampaign(users.logged, campaign.id)
        }


    }, [newVisit, campaign.id, users.logged, campaigns])


    React.useEffect(() => updatePreferences(), [updatePreferences])

    const [summaries, computeSummaries] = useSummaries()

    // const {calendars={} as CalendarContextType} = useCalendarContext() ?? {}
        
    // const generalCalendar = React.useMemo(() => calendars.general, [calendars])
    // const partyCalendar = React.useMemo(() => calendars.party, [calendars])
    // const assetCalendar = React.useMemo(() => calendars.asset, [calendars])

    const [render] = useAsyncRender({

        when: !!summaries,

        task: computeSummaries,

        content: () => <StatisticsContext.Provider value={summaries}>
            <CalendarContext >
                <AccordionProvider>
                    <ViewRouter />
                </AccordionProvider>
            </CalendarContext>
        </StatisticsContext.Provider >,

        placeholder: Placeholder.page

    })

    return render

}


const useSummaries = () => {

    const time = useTime()
    const campaigns = useCampaigns()
    const dashboard = useDashboards()

    const allCampaigns = campaigns.all()
    const allInstances = campaigns.allInstances()

    const campaign = useCurrentCampaign()

    // builds a cache of expensively derived data.

    const submissions = useSubmissions(campaign)

    // at the moment we might not need to recompute stats when the deps below change, because changes require navigating away
    // from the dashboard and this cache, so that when we get back here stats are recomputed on mount.

    // yet we may effect some changes from the dashboard now or in the future (eg settings). other changes may also come
    // from server pushes whilst we remain in this component tree. so it's safer to stay in sycn with these deps

    const requirementInstances = useRequirementInstances(campaign)
    const productInstances = useProductInstances(campaign)

    const events = useEventInstances(campaign).all()
    const parties = usePartyInstances(campaign).all()
    const requirements = requirementInstances.all()
    const products = productInstances.all()
    const trails = submissions.allTrails()

    const deps = [campaign, events, parties, requirements, products, trails] as const

    type ExtendedSummaries = Summaries & {
        partyTrails: [PartyInstanceDto, TrailSummary[]][]
    }

    const [summaries, setSummaries] = React.useState<ExtendedSummaries>(undefined!)

    const campaignDashboard = dashboard.given(campaign);

    const horizon = campaignDashboard.submissionHorizon(campaign)

    const computeSummaries = async () => {

        const dueDates = { ...requirementInstances.allDueDatesByRequirementId(), ...productInstances.allDueDatesByProductId() }

        const now = new Date()

        const [allTrailSummaries, summaryMap] = clock('trail summaries', () => {

            const summaries = {} as Record<string, TrailSummary[]>

            const summaryMap = {} as Record<string, TrailSummary>


            submissions.allTrails().forEach(trail => {

                const summary = submissions.trailSummaryNoDeps({ now, trail, due: dueDates[trail.key.asset] })

                const assetSummaries = summaries[trail.key.asset]
                if (assetSummaries) assetSummaries.push(summary)
                else summaries[trail.key.asset] = [summary]

                const partySummaries = summaries[trail.key.party]
                if (partySummaries) partySummaries.push(summary)
                else summaries[trail.key.party] = [summary]

                summaryMap[trail.id] = summary


            })

            return [summaries, summaryMap]
        })


        const campaignstats = clock(`summary campaign stats`, () => dashboard.given(campaign).campaignStatistics())

        const requirementstats = clock(`requirement stats`, () => submissions.allRequirementsSummary(allTrailSummaries, dueDates))

        const productstats = clock(`product stats`, () => submissions.allProductsSummary(allTrailSummaries, dueDates))

        //eslint-disable-next-line
        const partyTrails = submissions.partiesWithTrailSummaries(allTrailSummaries)

        const horizonToEndOfCampaign = time.current().add(campaignstats.days - campaignstats.progress, 'days')

        // party stats to end of campaign (horizon-independent).
        const staticPartystats = clock("static party stats", () => submissions.allPartiesSummary(partyTrails, horizonToEndOfCampaign))

        const trailSummaryOf = (key: TrailKey) => allTrailSummaries[key.asset]?.find(s => s.trail.key.party === key.party) ?? submissions.trailSummary(key, dueDates[key.asset])

        return {

            dueDates,
            trails: allTrailSummaries,
            trailSummaryMap: summaryMap, 
            trailSummaryOf,

            [campaignType]: campaignstats,
            [requirementType]: requirementstats,
            [productType]: productstats,
            partyTrails,
            staticparties: staticPartystats,
            ...computePartyStats(partyTrails),
            ...computeStartedCampaigns()
        }

    }

    const computePartyStats = (partyTrails: ExtendedSummaries['partyTrails']) => {

        const stats = clock("dynamic party stats", () => submissions.allPartiesSummary(partyTrails, moment(time.current()).add(horizon, 'days')))

        return { [tenantType]: stats }

    }


    const computeStartedCampaigns = () => ({ startedCampaigns: dashboard.routableCampaigns() })

    const computeAndSetSummaries = () => Promise.resolve().then(wait(150)).then(computeSummaries).then(computed => setSummaries(s => ({ ...s, ...computed })))

    React.useEffect(() => {

        if (summaries)
            computeAndSetSummaries()

        //eslint-disable-next-line
    }, deps)

    React.useEffect(() => {

        if (summaries)
            setSummaries(s => ({ ...s, ...computePartyStats(summaries.partyTrails) }))

        //eslint-disable-next-line
    }, [horizon])


    React.useEffect(() => {

        if (summaries)
            setSummaries(s => ({ ...s, ...computeStartedCampaigns() }))

        //eslint-disable-next-line
    }, [allCampaigns, allInstances])


    const { t } = useTranslation()
    const { l } = useLocale()
    const { toggleBusy, } = useSystem()
    // const {  } = useFeedback()

    const computeTask = () => toggleBusy(`computestats`, t("dashboard.statistics.generating"))

        .then(computeAndSetSummaries)
        .catch(e => showAndThrow(e, t("dashboard.statistics.generating_error", { campaign: l(campaign.name) })))
        .finally(() => toggleBusy(`computestats`))

    return [summaries, computeTask] as const
}
