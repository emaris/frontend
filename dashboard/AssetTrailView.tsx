
import { useLocale } from "apprise-frontend/model/hooks"
import { useUsers } from "apprise-frontend/user/api"
import { useCurrentCampaign } from "emaris-frontend/campaign/hooks"
import { useSubmissions } from "emaris-frontend/campaign/submission/api"
import { productType } from "emaris-frontend/product/constants"
import { requirementType } from "emaris-frontend/requirement/constants"
import * as React from "react"
import { useHistory } from "react-router-dom"
import { CampaignInstance } from "../campaign/model"
import { AssetTrailTable } from "./AssetTrailTable"
import { DashboardPage } from "./DashboardPage"
import { useCurrentAsset, useCurrentParty, useDashboard } from "./hooks"


export const AssetTrailView = () => {

        const {l} = useLocale()

        const history = useHistory()

        const {logged} = useUsers()
        const dashboard = useDashboard()

        const campaign = useCurrentCampaign()
        const submissions = useSubmissions(campaign)

        const profiles = { 
        
                [requirementType] : submissions.profileOf(requirementType),
                [productType] : submissions.profileOf(productType)
            
        }

        const {party,tenant} = useCurrentParty()
        const {type=requirementType,allForParty} = useCurrentAsset()
 
       
        const title = logged.isSingleTenantUser() ? l(campaign.name) : party && l(tenant.name)


        const filterId = `${campaign.id}-${type}`

        const isRowEnabled = (ci: CampaignInstance) => profiles[type].isForParty(party!, ci)


        return  <DashboardPage title={title} tab={type} >
                        <AssetTrailTable key={type} group='asset-list'
                                filterKey = {filterId}
                                filterGroup = {filterId}
                                filterBy = {r=>l(profiles[r.instanceType].source(r.source)?.name)}
                                data={allForParty(party!)} 
                                route={a=> `${history.location.pathname}/${dashboard.assetParam(a.instanceType,a.source)}`}
                                isRowEnabled = {isRowEnabled}
                        />

                </DashboardPage>
        }


