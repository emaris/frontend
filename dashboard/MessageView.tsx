import { TParams } from 'apprise-frontend/mail/model'
import { useLocale } from 'apprise-frontend/model/hooks'
import { useTenants } from 'apprise-frontend/tenant/api'
import { noTenant, tenantType } from 'apprise-frontend/tenant/constants'
import { useUsers } from 'apprise-frontend/user/api'
import { messageType } from 'apprise-messages/constants'
import { Flow, FLowMailProfile, Message, MessageDto } from 'apprise-messages/model'
import { FlowViewer } from 'apprise-messages/VirtualFlow'
import { useCampaigns } from 'emaris-frontend/campaign/api'
import { anyPartyTopic } from 'emaris-frontend/campaign/constants'
import { useCurrentCampaign } from 'emaris-frontend/campaign/hooks'
import { CampaignInstance } from 'emaris-frontend/campaign/model'
import { usePartyInstances } from 'emaris-frontend/campaign/party/api'
import { PartyInstance } from 'emaris-frontend/campaign/party/model'
import { useProductInstances } from 'emaris-frontend/campaign/product/api'
import { useRequirementInstances } from 'emaris-frontend/campaign/requirement/api'
import { useSubmissions } from 'emaris-frontend/campaign/submission/api'
import { useProducts } from 'emaris-frontend/product/api'
import { productType } from 'emaris-frontend/product/constants'
import { useRequirements } from 'emaris-frontend/requirement/api'
import { requirementType } from 'emaris-frontend/requirement/constants'
import * as React from 'react'
import { useTranslation } from 'react-i18next'
import { DashboardPage } from './DashboardPage'
import { useCurrentAsset, useCurrentParty } from './hooks'

export const MessageViewer = () => {

    const { party } = useCurrentParty()
    const { asset } = useCurrentAsset()

    const component = party ? <PartyMessageViewer party={party} /> : asset ? <AssetMessageViewer asset={asset} /> : <CampaignMessageViewer />

    return component
}




// ----------------------------------   campaign flow

const CampaignMessageViewer = () => {

    return <DashboardPage tab={messageType}  >
        <CampaignMessageFlow />
    </DashboardPage>
}

type CampaignMessageFlowProps = {


    summary?: boolean
}

const useTopicToMailProfile = () => {

    const campaign = useCurrentCampaign()
    const tenants = useTenants()
    const parties = usePartyInstances(campaign)
    const requirements = useRequirements()
    const reqinstances = useRequirementInstances(campaign)
    const prodinstances = useProductInstances(campaign)
    const products = useProducts()

    return (message: MessageDto): FLowMailProfile => {

        const source = tenants.lookup(message.topics?.find(t => t.type === tenantType)?.name.split(':')?.[1])
        const requirement = message.topics?.find(t => t.type === requirementType)?.name.split(':')?.[1]
        const product = message.topics?.find(t => t.type === productType)?.name.split(':')?.[1]

        const asset = requirement ? requirements.lookup(requirement) : products.lookup(product)
        const assetInstance = requirement ? reqinstances.lookupBySource(requirement) : prodinstances.lookupBySource(product)


        const partyTargets = source ? [source.id] : assetInstance ? parties.allForAsset(assetInstance).map(p => p.source) : parties.all().map(p => p.source) 

        const parameters: TParams = {
            campaign: campaign.name,
            campaignId: campaign.id,

        }

        if (source) {
            parameters.tenantId = source.id
            parameters.tenant = source.name
        }

        if (asset) {
            parameters.assetType = requirement ? requirementType : productType
            parameters.asset = asset?.name
            parameters.assetId = asset?.id
            parameters.assetTitle = asset?.description
        }

        return {
            targets: [noTenant, ...partyTargets],
            parameters
        }
    }

}

export const useCampaignMessages = (summary?: boolean) => {

    const campaigns = useCampaigns()
    const campaign = useCurrentCampaign()

    const profileOfMessage = useTopicToMailProfile()


    const flowId = campaign.id

    const flow: Flow = {
        scope: campaign.id,
        mailProfile: profileOfMessage

        ,

        channels: [{
            name: "main",
            channel: {
                read: campaigns.topics(campaign),
                noPost: !!summary,
                noReply: !!summary
            }
        }]
    }

    return { id: flowId, flow }
}

export const CampaignMessageFlow = (props: CampaignMessageFlowProps) => {

    const { t } = useTranslation()

    const { summary } = props

    const { id, flow } = useCampaignMessages(summary)

    const logged = useUsers().logged
    const campaigns = useCampaigns()
    const campaign = useCurrentCampaign()


    const readonly = !logged.isAdmin() || campaigns.isArchived(campaign) || campaigns.isMuted(campaign)

    return <FlowViewer id={id} flow={flow} multiTenant collapsed={!summary} collapsedMessage={t('campaign.labels.broadcast_message')} readOnly={readonly} />
}






// ----------------------------------   party flow

export const usePartyMessages = (party: PartyInstance, summary?: boolean) => {

    const campaign = useCurrentCampaign()
    const campaignTopics = useCampaigns().topics(campaign)

    const parties = usePartyInstances(campaign)
    const requirements = useRequirements()
    const products = useProducts()
    const tenants = useTenants()

    const profileOfMessage = useTopicToMailProfile()

    let flowId
    let flow: Flow | undefined


    if (party) {

        const source = tenants.safeLookup(party.source)
     
        flowId = party.id
        const partyTopics = parties.topics(party)

        flow = {

            scope: campaign.id,
            recipient: party.source,

            mailProfile: profileOfMessage,

            channels: [

                {
                    name: "party",
                    channel: {
                        read: [...campaignTopics, ...partyTopics],
                        noPost: !!summary,
                        noReply: !!summary
                    }
                }

                ,

                {
                    name: "broadcast-asset",
                    channel: {
                        read: [...campaignTopics, anyPartyTopic],
                        // on replies, replaces anyparty topic with specific party
                        onReply: m => ({ ...m, topics: m.topics.flatMap(t => t.type === tenantType && t.name === anyPartyTopic.name ? partyTopics : [t]) }),
                        noPost: true,
                        noReply: !!summary,

                        // must discover from topics and add the asset of this broadcast as an email parameter.
                        // so it can be referenced in the target mail template.
                        mailProfile: (message: Message) => {


                            const assetTopic = message.topics.find(t => t.type === requirementType || t.type === productType)

                            if (!assetTopic)
                                throw new Error(`no asset topic in ${message}`)


                            const asset = (assetTopic.type === requirementType ? requirements : products).lookup(assetTopic.name.split(':')?.[1])

                            if (!asset)
                                throw new Error(`can't resolve asset for topic ${assetTopic}`)

                            return {
                                targets: [noTenant, party.source],
                                parameters: {
                                    campaign: campaign.name,
                                    campaignId: campaign.id,
                                    tenant: source.name,
                                    tenantId: source.id,
                                    aassetType: assetTopic.type,
                                    asset: asset.name,
                                    assetId: asset.id,
                                    assetTitle: asset.description
                                }
                            }
                        }

                    }
                }

                ,

                {
                    name: "broadcast",
                    channel: {
                        read: campaignTopics,
                        write: [...campaignTopics, ...partyTopics],
                        mode: 'strict',
                        noPost: true,
                        noReply: !!summary
                    }
                }
            ]
        } as Flow

    }


    return { id: flowId, flow: flow! }
}

type PartyProps = {
    party: PartyInstance
}

type PartyMessageFlowProps = PartyProps & {

    summary?: boolean
}


export const PartyMessageFlow = (props: PartyMessageFlowProps) => {

    const { summary } = props

    const { party } = props

    const { id, flow } = usePartyMessages(party, summary)

    const logged = useUsers().logged
    const active = logged.isTenantManager() || logged.isManagerOf(party.id) || logged.isManagerOf(party.id, 'direct') || logged.isAdmin() || logged.isMultiManager() || logged.hasNoTenant()

    const campaigns = useCampaigns()
    const campaign = useCurrentCampaign()

    const readonly = !active || campaigns.isArchived(campaign) || campaigns.isMuted(campaign)

    return <FlowViewer id={id} key={party.source} flow={flow} readOnly={readonly}
    // splitWith={summary ? undefined : party.source} 
    />
}

const PartyMessageViewer = (props: PartyProps) => {

    const { l } = useLocale()

    const party = useCurrentParty()

    const users = useUsers()

    const title = users.logged.hasNoTenant() ? l(party?.tenant.name) : undefined

    return <DashboardPage title={title} tab={messageType}  >
        <PartyMessageFlow {...props} />
    </DashboardPage>
}







// ----------------------------------   asset flow

export const useAssetMessages = (asset: CampaignInstance, summary?: boolean) => {

    const campaign = useCurrentCampaign()
    const campaignTopics = useCampaigns().topics(campaign)

    const { topics: requirementTopics } = useRequirementInstances(campaign)
    const { topics: productTopics } = useProductInstances(campaign)

    const profileOfMessage = useTopicToMailProfile()

    let flowId
    let flow: Flow

    if (asset) {


        flowId = asset.id
        const assettopics = asset.instanceType === requirementType ? requirementTopics(asset) : productTopics(asset)

        flow = {

            scope: campaign.id,

            mailProfile: profileOfMessage,

            channels: [
                {
                    name: asset.instanceType,
                    channel: {
                        read: [...campaignTopics, ...assettopics],
                        noPost: true,
                        noReply: !!summary
                    }
                }
                ,
                {
                    name: "broadcast",
                    channel: {
                        read: [...campaignTopics, ...assettopics],
                        noPost: !!summary,
                        noReply: !!summary,
                        write: [...campaignTopics, ...assettopics, anyPartyTopic]
                    }
                }
            ]
        } as Flow

    }

    return { id: flowId, flow: flow! }
}

type AssetProps = {
    asset: CampaignInstance
}

const AssetMessageViewer = (props: AssetProps) => {

    const asset = useCurrentAsset()

    const users = useUsers()

    const title = users.logged.hasNoTenant() ? asset.nameOf(asset.asset!) : undefined

    return <DashboardPage title={title} tab={messageType}  >
        <AssetMessageFlow {...props} />
    </DashboardPage>

}

type AssetMessageFlowProps = AssetProps & {

    summary?: boolean
}


export const AssetMessageFlow = (props: AssetMessageFlowProps) => {


    const { t } = useTranslation()
    const { l } = useLocale()

    const campaign = useCurrentCampaign()
    const { profileOf } = useSubmissions(campaign)
    const campaigns = useCampaigns()

    const { asset, summary } = props

    const { id, flow } = useAssetMessages(asset, summary)
    const logged = useUsers().logged
    const active = logged.isAdmin() || logged.isMultiManager()

    const readonly = !active || campaigns.isArchived(campaign) || campaigns.isMuted(campaign)

    return <FlowViewer id={id} key={asset.source} flow={flow} multiTenant collapsed={!summary} collapsedMessage={t('campaign.labels.broadcast_message_about', { topic: l(profileOf(asset.instanceType).source(asset.source).name) })} readOnly={readonly} />
}