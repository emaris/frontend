import { Row, Tooltip } from 'antd'
import { SliderBox } from 'apprise-frontend/form/SliderBox'
import { useCurrentCampaign } from 'emaris-frontend/campaign/hooks'
import { useTranslation } from 'react-i18next'
import { useDashboard } from './hooks'
import { tipDelay } from './SummaryCard'

type HorizonProps = {

    disabled?: boolean
}

const step = 5

const calculateDefaultHorizon = (currentHorizon: number, days: number, progress: number): {max: number, horizon: number} => {

    const togo = days - progress
    const adjustment = togo % step
    const max = adjustment ? togo + (step - adjustment) : togo

    const horizon = currentHorizon < max ? currentHorizon : max

    return {max, horizon}

}

export const SubmissionHorizon = (props: HorizonProps) => {


    const { t } = useTranslation()

    const { disabled } = props

    const dashboard = useDashboard()

    const campaign = useCurrentCampaign()

    const campaignstats = dashboard.campaignStatistics()

    const currentHorizon = dashboard.submissionHorizon(campaign) ?? 0

    const {max, horizon} = calculateDefaultHorizon(currentHorizon, campaignstats.days, campaignstats.progress)

    return <SliderBox className='horizon-slider'
        disabled={disabled} standalone step={step} max={max} showValues={false}
        tipFormatter={days => {
            return days === 0 ? t("common.labels.today") : (campaignstats.end && days === max) ? t("campaign.labels.end_of_campaign") : t("dashboard.labels.horizon.slider_tip", { days })
        }}
        defaultValue={horizon}
        onChange={v => dashboard.setSubmissionHorizon(v as number)} />

}



export const HorizonRow = (props: HorizonProps) => {

    const { t } = useTranslation()

    const { disabled } = props

    const dashboard = useDashboard()

    const campaign = useCurrentCampaign()

    const campaignstats = dashboard.campaignStatistics()

    const currentHorizon = dashboard.submissionHorizon(campaign) ?? 0

    const {horizon} = calculateDefaultHorizon(currentHorizon, campaignstats.days, campaignstats.progress)

    // note: tooltip used internally on title or will pop up as the slider is dragged.

    return <Row className="submission-horizon part-horizon">

        <div className="horizon-title">
            <Tooltip mouseEnterDelay={tipDelay} title={t('dashboard.summary.performance.horizon.tip')}>
                {t('dashboard.summary.performance.horizon.name')}
            </Tooltip>
            <span className="current-horizon"> ({t("dashboard.summary.performance.horizon.reminder", { horizon })}) </span>
        </div>

        <SubmissionHorizon disabled={disabled} />

    </Row >

}

export const HorizonDecoration = () => {

    const { t } = useTranslation()

    const dashboard = useDashboard()

    const campaign = useCurrentCampaign()

    const horizon = dashboard.submissionHorizon(campaign) ?? 0

    return <div className='submission-horizon horizon-decoration'>

        <div className="horizon-title">
        
            <Tooltip mouseEnterDelay={0.5} title={t('dashboard.summary.performance.horizon.tip')}>
                {t('dashboard.summary.performance.horizon.name')}
            </Tooltip>

            <span className="current-horizon"> ({t("dashboard.summary.performance.horizon.reminder", { horizon })}) </span>
        
        </div>

        <SubmissionHorizon />

    </div>

}