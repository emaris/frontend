
import { useLocale } from "apprise-frontend/model/hooks"
import { requirementType } from "emaris-frontend/requirement/constants"
import * as React from "react"
import { useHistory } from "react-router-dom"
import { CampaignInstance } from "../campaign/model"
import { AssetStatsTable } from "./AssetStatsTable"
import { DashboardPage } from "./DashboardPage"
import { useCurrentAsset, useDashboard } from "./hooks"


export const AssetStatsView = () => {

    const {l} = useLocale()
    const history = useHistory()
   
    const dashboard = useDashboard()

    const {type=requirementType,allSorted,source} = useCurrentAsset()
   
    const assetRoute = (a:CampaignInstance) => `${history.location.pathname}/${dashboard.assetParam(type,a.source)}`  
    
    return <DashboardPage tab={type} >
            <AssetStatsTable key={`asset-${type}`} route={assetRoute} data={allSorted()}
                                    type={type}
                                    filterBy={r=>l(source(r.instance.source)?.name)}
                                    sortBy={[['duedate', 'asc']]} />
        </DashboardPage>
        
}

