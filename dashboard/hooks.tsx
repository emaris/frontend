
import { useTenants } from "apprise-frontend/tenant/api"
import { tenantType } from "apprise-frontend/tenant/constants"
import { campaignRoute, campaignType } from "emaris-frontend/campaign/constants"
import { useCurrentCampaign } from "emaris-frontend/campaign/hooks"
import { Campaign, CampaignInstance } from "emaris-frontend/campaign/model"
import { PartyInstance } from "emaris-frontend/campaign/party/model"
import { useSubmissions } from "emaris-frontend/campaign/submission/api"
import { TrailKey } from "emaris-frontend/campaign/submission/model"
import { AllAssetsSummary } from 'emaris-frontend/campaign/submission/statistics/asset'
import { AllPartiesSummary } from 'emaris-frontend/campaign/submission/statistics/party'
import { TrailSummary } from 'emaris-frontend/campaign/submission/statistics/trail'
import { productType } from "emaris-frontend/product/constants"
import { requirementType } from "emaris-frontend/requirement/constants"
import { useEmarisState } from 'emaris-frontend/state/hooks'
import { EmarisState } from "emaris-frontend/state/model"
import * as React from "react"
import { useLocation, useParams } from "react-router-dom"
import { dashboardapi } from "./api"
import { dashboardcalendarapi } from "./calendar"
import { dashboardRoute } from "./constants"



export const StatisticsContext = React.createContext<Summaries>(null!)

export const useDashboard = () => {

    const campaign = useCurrentCampaign()
    const api = dashboardapi(useEmarisState())
    const statistics = React.useContext (StatisticsContext) ?? {}

    return {...api.given(campaign),summaries:statistics}
}

export type CampaignMode = 'design' | 'dashboard' | 'off'

export const useCampaignMode = () : CampaignMode => {

    const {pathname} = useLocation()
    
    if (pathname.startsWith(campaignRoute) )
        return 'design'
    
    if (pathname.startsWith(dashboardRoute) )
        return "dashboard"

    return "off"

}

export const useCalendar = () => {
    const campaign = useCurrentCampaign()
    const state = useEmarisState()
    const api = dashboardcalendarapi( state as EmarisState)(campaign)
    return {...api}
}

export const useCurrentParty = () => {

    const tenants = useTenants()
    const dashboard = useDashboard()
    const {party:partyId} = useParams<{party: string}>()

    const party = dashboard.resolveParty(partyId)
    const tenant = tenants.safeLookup(partyId)
   
    return { party, partyParam:partyId, tenant}


}

export const useCurrentAsset = () => {


    const dashboard = useDashboard()
    const params = useParams<{ type: string, asset: string}>()
    
    const asset = params.asset ? dashboard.resolveAsset(params.type,params.asset) : undefined
    const type = params.type

    const campaign = useCurrentCampaign()
    const submissions= useSubmissions(campaign)

    return { asset,type,assetParam:params.asset, ...submissions.profileOf(type) }


}


export const useTrail = () => {

    const {party} = useCurrentParty()
    const {asset} = useCurrentAsset()
    const campaign = useCurrentCampaign()
    const submissions = useSubmissions(campaign)

    const key = {
        campaign: campaign.id
    }

    const keyWithParty = (p:PartyInstance) : TrailKey => ({...key,party:p.source,assetType:asset?.instanceType!,asset:asset?.source!})
    const keyWithAsset = (a:CampaignInstance) : TrailKey => ({...key,party:party?.source!,assetType:a.instanceType!,asset:a.source!})
    

    return {keyWithParty,keyWithAsset, submissions}
}


export type Summaries = {

    startedCampaigns: Campaign[]
    dueDates: Record<string,string>
    trails: Record<string, TrailSummary[]>
    trailSummaryMap: Record<string, TrailSummary>
    trailSummaryOf: (key:TrailKey) => TrailSummary
    [campaignType]:  ReturnType<ReturnType<ReturnType<typeof dashboardapi>['given']>['campaignStatistics']>,
    [requirementType]: AllAssetsSummary,
    [productType]: AllAssetsSummary,
    [tenantType] : AllPartiesSummary
    staticparties: AllPartiesSummary

}

