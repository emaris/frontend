import { Label } from "apprise-frontend/components/Label";
import { intlapi } from "apprise-frontend/intl/api";
import { withMultilang } from "apprise-frontend/model/multilang";
import { tagapi } from "apprise-frontend/tag/api";
import { tenantType } from "apprise-frontend/tenant/constants";
import { TenantLabel } from "apprise-frontend/tenant/Label";
import { TenantDto } from "apprise-frontend/tenant/model";
import { tenantstateapi } from "apprise-frontend/tenant/state";
import { userapi } from "apprise-frontend/user/api";
import { UserLabel } from "apprise-frontend/user/Label";
import { clock } from "apprise-frontend/utils/common";
import { campaignSingular, campaignType } from "emaris-frontend/campaign/constants";
import { eventinstapi, useEventInstances } from "emaris-frontend/campaign/event/api";
import { EventInstanceLabel } from "emaris-frontend/campaign/event/Label";
import { EventInstance, EventInstanceDto } from "emaris-frontend/campaign/event/model";
import { useCurrentCampaign } from "emaris-frontend/campaign/hooks";
import { Campaign } from "emaris-frontend/campaign/model";
import { usePartyInstances } from "emaris-frontend/campaign/party/api";
import { PartyInstanceDto } from "emaris-frontend/campaign/party/model";
import { productinstapi, useProductInstances } from "emaris-frontend/campaign/product/api";
import { ProductInstanceLabel } from "emaris-frontend/campaign/product/Label";
import { requirementinstapi, useRequirementInstances } from "emaris-frontend/campaign/requirement/api";
import { RequirementInstanceLabel } from "emaris-frontend/campaign/requirement/Label";
import { submissionapi, useSubmissions } from "emaris-frontend/campaign/submission/api";
import { SubmissionLabel } from "emaris-frontend/campaign/submission/Label";
import { SubmissionStub, TrailDto } from "emaris-frontend/campaign/submission/model";
import { profileapi } from "emaris-frontend/campaign/submission/profile";
import { eventType } from "emaris-frontend/event/constants";
import { productapi } from "emaris-frontend/product/api";
import { productSingular, productType } from "emaris-frontend/product/constants";
import { ProductDto } from "emaris-frontend/product/model";
import { requirementapi } from "emaris-frontend/requirement/api";
import { requirementType } from "emaris-frontend/requirement/constants";
import { RequirementDto } from "emaris-frontend/requirement/model";
import { EmarisState } from "emaris-frontend/state/model";
import moment from "moment-timezone";
import React from "react";
import { Trans } from "react-i18next";
import shortid from "shortid";
import { requirementSingular } from '../requirement/constants';
import { dashboardapi } from "./api";
import { CalendarRow, Group } from "./CalendarTable";

const labelMargin = 4

const marginLeft = { marginLeft: labelMargin }
const marginRight = { marginRight: labelMargin }
const margin = { ...marginLeft, ...marginRight}

export const dashboardcalendarapi = (s: EmarisState) => (campaign : Campaign) => {

    const t = intlapi(s).getT()
    const l = withMultilang(s)
    const products = productapi(s)
    const requirements = requirementapi(s)
    const tenants = tenantstateapi(s)
    const dashboard = dashboardapi(s).given(campaign)
    const events = eventinstapi(s)(campaign)
    const logged = userapi(s).logged

    const eventsTranslations = {
        'other': t('event.types.reminder_singular'),
        [requirementType] : t(requirementSingular),
        [productType] : t(productSingular),
        [campaignType] : t(campaignSingular)
    }

    const profileApi = profileapi(s)(campaign, {
        reqinstapi: requirementinstapi(s)(campaign), 
        prodinstapi: productinstapi(s)(campaign),
        reqapi: requirementapi(s),
        prodapi: productapi(s),
        tags: tagapi(s),
        users: userapi(s)
    })

    const profiles = { 
        
        [requirementType] : profileApi.profileOf(requirementType),
        [productType] : profileApi.profileOf(productType)
            
    }

    const self = {

        eventInstanceToCalendarRow : (e: EventInstance, allEvents: EventInstanceDto[], submission?: SubmissionStub, party?: PartyInstanceDto ): CalendarRow => {
            
            const target = (e.type === productType || e.type === requirementType) ? e.type === productType ? products.safeLookup(e.target) : requirements.safeLookup(e.target) : undefined
            const targetType = (e.type === productType || e.type === requirementType) ? e.type : undefined
            const date = events.absolute(e.date)
            
            return {
                id: `COL-${shortid()}`
                ,
                label: self.eventCalendarLabel(e, submission, party)
                ,
                date: date ? moment(date) : undefined!
                ,
                targetType
                ,
                type: eventsTranslations[e.type]
                ,
                target
                ,
                stringify: (f, i) => {
                    const eventString = events.stringify(e)
                    const targetString = i.targetType === requirementType ? requirements.stringify(i.target as RequirementDto) : i.targetType === productType ? products.stringify(i.target as ProductDto) : ''
                    return `${eventString} ${targetString}`.toLowerCase().includes(f.toLocaleLowerCase())
                }
                ,

                filter: (selections): boolean => {
                    // driven by 1) type menu and 2) asset menu, if they are about assets and those assets are selected

                    const typeSelected = selections[eventType]?.includes(e.type) 
                   
                    const aboutAsset = [requirementType,productType].includes(e.type) && e.target
                    const assetSelected =  selections[e.type]?.includes(e.target!)
            
                    return typeSelected && (!aboutAsset || assetSelected)
                }
            }
        }

        ,

        buildCalendarRows: (trails: TrailDto[], labelFor: 'all' | 'asset' | 'party') => {


            const submissionToCalendarRow = (trail: TrailDto, submission: SubmissionStub, type: 'submitted' | 'assessed'): CalendarRow => {

                const target = trail.key.assetType === requirementType ? requirements.safeLookup(trail.key.asset) : products.safeLookup(trail.key.asset)
                const party = tenants.safeLookup(trail.key.party)
                const partyname = l(party.name)
                const date = submission.lifecycle.lastSubmitted
                
                // const rowLabelType = type === 'submitted' ? 'submission' : 'assessment'

                // const resolveLabel = ():JSX.Element => {
                //     if (labelFor === 'all') return self.fullCalendarLabel(type, submission, trail, party)
                //     else if (labelFor === 'asset') return self.partyCalendarLabel(type, submission, trail, party)
                //     else return self.assetCalendarLabel(trail.key.assetType, type, target, submission, trail )
                // }

                // const label = resolveLabel()

                let label
                if (labelFor === 'all') label = self.fullCalendarLabel(type, submission, trail, party)
                else if (labelFor === 'asset') label = self.partyCalendarLabel(type, submission, trail, party)
                else label = self.assetCalendarLabel(trail.key.assetType, type, target, submission, trail )
    
                return {
                    id: `COL-${shortid()}`
                    ,
                    type
                    ,
                    date: date ? moment(date) : undefined!
                    ,
                    party
                    ,
                    targetType: trail.key.assetType === requirementType ? requirementType : productType
                    ,
                    target
                    ,
                    label
                    ,
                    stringify: (f) => labelFor === 'all' ?
                        `${l(target.name).inCurrent()} ${t("submission.status.submitted")} ${partyname.inCurrent()}`.toLocaleLowerCase().includes(f.toLocaleLowerCase()) :
                            labelFor === 'asset' ?
                                `${partyname.inCurrent()} ${t("submission.status.submitted")}`.toLocaleLowerCase().includes(f.toLocaleLowerCase()) :
                                `${l(target.name).inCurrent()} ${t("submission.status.submitted")}`.toLocaleLowerCase().includes(f.toLocaleLowerCase())
                    
                    ,
                    filter: (selections): boolean => {

                        const allAssets = [...selections[requirementType] ?? {}, ...selections[productType] ?? {}]
    
                        const partySelected = selections[tenantType]?.includes(trail.key.party)
                        const assetSelected =  allAssets.includes(trail.key.asset)
                        const typeSelected = selections['action']?.includes(type) 
                        

                        return partySelected && assetSelected && typeSelected
    
                    }
                }
            }

            const trailsMap = clock('computing trailMap', () => trails.map(trail => {
                const setDataFor = (s: SubmissionStub, type: 'submission' | 'assessment'): moment.Moment => {
                    return type === 'submission' ? moment(s.lifecycle.lastSubmitted) :
                    s.lifecycle.compliance?.lastAssessed === undefined ? moment(s.lifecycle.lastSubmitted) : moment(s.lifecycle.compliance.lastAssessed)
                }

                const submissions = trail.submissions.filter(s => s.lifecycle.lastSubmitted !== undefined).sort((a, b) => moment(a.lifecycle.lastSubmitted).isAfter(b.lifecycle.lastSubmitted) ? 1 : 0)
                const assessments = trail.submissions.filter(s => s.lifecycle.compliance?.lastAssessed).sort((a, b) => moment(a.lifecycle.compliance?.lastAssessed).isAfter(b.lifecycle.compliance?.lastAssessed) ? 1 : 0)
    
                const allSubmissions = submissions.map(submission => (submissionToCalendarRow(trail, submission, 'submitted'))) as CalendarRow[]
                const allAssessments = assessments.map(assessment => ({...submissionToCalendarRow(trail, assessment, 'assessed'), date: setDataFor(assessment, 'assessment')})) as CalendarRow[]
    
                return [...allAssessments, ...allSubmissions]
    
            }))

            return trailsMap.flat().filter(e => e.date !== undefined)
        }

        ,

        //Find distinct values in an array or array[object] with max 2 levels depth
        getDistinct : <T extends {}>(array: T[], prop: string, subprop: string | undefined, sort: (a: any, b: any) => number, filter?: (_: T) => boolean) => {

            const sorted = filter === undefined ? array.filter(a => a[prop] !== undefined).map(t => t[prop]).sort(sort) : array.filter(a => a[prop] !== undefined).filter(filter).map(t => t[prop]).sort(sort)
            return sorted.reduce((acc, curr) => !acc.includes(subprop === undefined ? curr! : curr[subprop]!) ? [...acc, subprop === undefined ? curr! : curr[subprop]!] : [...acc], [] as any[])

        }

        ,

        dateToInt : (year: number, month: number) => parseInt(`${year}${month}`)

        ,

        closestMonthFor : (groups: Group[]) => {

            if (!groups || groups.length === 0) return 0
        
            const currentDate = moment(new Date(), 'YYYY/MM/DD')
       
            const currentMonth = self.dateToInt(parseInt(currentDate.format('YYYY')), (parseInt(currentDate.format('M')) - 1))

            const closest = groups.filter(g => g.group.length > 0).reduce((prev, curr) => Math.abs(self.dateToInt(curr.key.year, curr.key.month) - currentMonth) < Math.abs(self.dateToInt(prev.key.year, prev.key.month) - currentMonth) ? curr : prev)
        
            const closestMonth = self.dateToInt(closest.key.year, closest.key.month)
        
            return groups.findIndex(group => self.dateToInt(group.key.year, group.key.month) === closestMonth)
        }

        ,

        closestGroupToNow : (grouped: (CalendarRow | {id: any, title: any})[]) => {
            
            const currentDate = moment(new Date(), 'YYYY/MM/DD')
            const currentYearMonth = parseInt(currentDate.format('YYYY')) + (parseInt(currentDate.format('M')) - 1)

            const closest = grouped.reduce(
                (prev, curr) => Object.keys(curr).includes('title') ? 
                    Math.abs((curr['title'].year + curr['title'].month) - currentYearMonth) < Math.abs((prev['title'].year + prev['title'].month) - currentYearMonth) ? 
                        curr : prev : 
                    prev)
            return closest['title'] ? closest['title'].year + closest['title'].month : undefined

        }

        ,

        eventCalendarLabel : (event: EventInstance, submission?:SubmissionStub, party?: PartyInstanceDto) => {

            const tenant = logged.isTenantUser() ? tenants.lookup(logged.tenant) : undefined

            const sourceEvent = events.lookup(event.target)
            let asset = requirements.lookup(event.target) || products.lookup(event.target)
            if (asset === undefined) {
                asset = sourceEvent ? ( requirements.lookup(sourceEvent!.target)  || products.lookup(sourceEvent!.target) ) : undefined
            }

            const assetType = (event.type === requirementType || event.type === productType) ? event.type : sourceEvent ? (sourceEvent.type === requirementType || sourceEvent.type === productType) ? sourceEvent.type : undefined : undefined

           
            const linkTo = asset ? 
                tenant ? 
                    `${dashboard.routeTo(campaign)}/${tenantType}/${tenant.id}/${assetType}/${asset.id}` 
                :
                    `${dashboard.routeTo(campaign)}/${assetType}/${asset.id}` 
                :
                undefined

            const assetInstance = assetType ? assetType === 'requirement' ? requirementinstapi(s)(campaign).lookupBySource(event.target) : productinstapi(s)(campaign).lookupBySource(event.target) : undefined

            const isEnabled = (assetInstance && party) ?
                profiles[assetType!].isForParty(party, assetInstance)
            : true

            const notApplicableclass = !isEnabled ? 'notapplicable' : ''

            return <div style={{ display: 'flex' }}>
                <EventInstanceLabel className={`event-type ${notApplicableclass}`} noDate instance={event} noLineage/>
                {
                    event.target !== campaign.id &&
                    <>
                        <Label style={{...margin, paddingTop: 1.4}} className={`label-main ${notApplicableclass}`} title={t("common.labels.for")} noIcon />
                        {events.given(event).labelOf(event.target, { noDate: true, noIcon: true, linkTo, className: notApplicableclass, noLineage: true })!}
                        {submission && <SubmissionLabel className={notApplicableclass} submission={submission} displayMode="state-only" />}
                    </>
                }
            </div>
        }

        ,

        fullCalendarLabel : (type: "submitted" | "assessed", submission: SubmissionStub, trail?: TrailDto, party?: TenantDto ) => {
            

            const resolvedTrail = trail ?? submissionapi(s)(campaign).lookupTrail(submission.trail)!
            
            const campaignRoute = dashboard.routeTo(campaign)
            const linkToParty = `${campaignRoute}/${tenantType}/${party?.id}`

            const assetType = resolvedTrail?.key.assetType ?? undefined

            const assetInstance = resolvedTrail ? resolvedTrail.key.assetType === 'requirement' ? requirementinstapi(s)(campaign).lookupBySource(resolvedTrail.key.asset) : productinstapi(s)(campaign).lookupBySource(resolvedTrail.key.asset) : undefined

            const linkToSubmission = submission && `${linkToParty}/${resolvedTrail.key.assetType}/${resolvedTrail.key.asset}/${submission.id}`

            const isEnabled = (assetInstance && party) ?
                profiles[assetType!].isForParty(party, assetInstance)
            : true


            const notApplicableclass = !isEnabled ? 'notapplicable' : ''
        
            const tenantLabel = (noIcon: boolean) => <TenantLabel className={`party-label ${notApplicableclass}`} tenant={party} linkTo={linkToParty} noIcon={noIcon} style={noIcon ? marginLeft : marginRight} linkTarget={undefined} />
            const assetLabel = (noIcon: boolean) => <SubmissionLabel className={notApplicableclass} mode='light' submission={submission} linkTo={linkToSubmission} displayMode='asset' noIcon={noIcon} style={noIcon ? margin : marginRight} />
        
            return <div style={{ display: 'flex' }}>
                {
                    type === 'submitted' ?
                        <Trans i18nKey="dashboard.calendar.submission_full">
                            {tenantLabel(false)}
                            <Label className="event-label" title={t("dashboard.labels.submitted.title")} tip={() => <UserLabel user={submission.lifecycle.lastModifiedBy} />} noLink noIcon style={marginLeft} />
                            {assetLabel(true)}
                        </Trans>
                        :
                        <Trans i18nKey="dashboard.calendar.assessed_full">
                            {tenantLabel(false)}
                            <Label className="event-label" title={t("dashboard.labels.assessed.title")} tip={() => <UserLabel user={submission.lifecycle.compliance?.lastAssessedBy} />} noLink noIcon style={margin} />
                            {assetLabel(true)}
                        </Trans>
                }
        
            </div>
        }

        ,

        assetCalendarLabel : (assetType: string, type: "submitted" | "assessed", asset: ProductDto | RequirementDto, submission: SubmissionStub, trail? : TrailDto ) => {
            const resolvedTrail = trail ?? submissionapi(s)(campaign).lookupTrail(submission.trail)!

            const linkToSubmission = dashboard.partyRouteToSubmissionWithTrail(submission, resolvedTrail)
        
            const requirements = requirementinstapi(s)(campaign)
            const products = productinstapi(s)(campaign)
        
            const linkToAsset = assetType === requirementType ? requirements.route(requirements.all().find(i => i.source === asset.id)) : products.route(products.all().find(i => i.source === asset.id))
            const assetLabel = (noIcon: boolean) => assetType === requirementType ?
                <RequirementInstanceLabel style={{ marginRight: 4 }} noLineage instance={requirements.all().find(i => i.source === asset.id)} noIcon={noIcon} linkTo={linkToAsset} noOptions />
                :
                <ProductInstanceLabel style={{ marginRight: 4 }} noLineage instance={products.all().find(i => i.source === asset.id)} noIcon={noIcon} linkTo={linkToAsset} noOptions />
        
            return <div style={{ display: 'flex' }}>{
                type === "submitted" ?
                    <Trans i18nKey="dashboard.calendar.submission_party" >
                        {assetLabel(false)}
                        <Label style={{ marginLeft: 4 }} className="asset-label" title={t("dashboard.labels.submitted.name")} tip={() => <UserLabel user={submission.lifecycle.lastModifiedBy} />} linkTo={linkToSubmission} noIcon />
                    </Trans>
                    :
                    <Trans i18nKey="dashboard.calendar.assessed_party">
                        {assetLabel(false)}
                        <Label style={{ marginLeft: 4 }} title={t("dashboard.labels.assessed.title")} tip={() => <UserLabel user={submission.lifecycle.compliance?.lastAssessedBy} />} linkTo={linkToSubmission} noIcon  />
                    </Trans>
            }
            </div>
        
        }

        ,

        partyCalendarLabel : (type: "submitted" | "assessed", submission: SubmissionStub, trail?: TrailDto,party?: TenantDto ) => {

            const linkToSubmission = dashboard.partyRouteToSubmission(submission)
            const linkToParty = `${dashboard.routeTo(campaign)}/${tenantType}/${party?.id}`

            const resolvedTrail = trail ?? submissionapi(s)(campaign).lookupTrail(submission.trail)
            const assetType = resolvedTrail?.key.assetType ?? undefined
            const assetInstance = resolvedTrail ? resolvedTrail.key.assetType === 'requirement' ? requirementinstapi(s)(campaign).lookupBySource(resolvedTrail.key.asset) : productinstapi(s)(campaign).lookupBySource(resolvedTrail.key.asset) : undefined

            const isEnabled = (assetInstance && party) ?
                profiles[assetType!].isForParty(party, assetInstance)
            : true

            const notApplicableclass = !isEnabled ? 'notapplicable' : ''
        
            const tenantLabel = <TenantLabel tenant={party} linkTo={linkToParty} className={notApplicableclass} style={{ marginRight: 4 }} />
        
            return <div style={{ display: 'flex' }}>{
                type === "submitted" ?
                    <Trans i18nKey="dashboard.calendar.submission_asset" >
                        {tenantLabel}
                        <Label className={`asset-label ${notApplicableclass}`} title={t("dashboard.labels.submitted.name")} tip={() => <UserLabel user={submission.lifecycle.lastModifiedBy} />} linkTo={linkToSubmission} noIcon style={{ marginLeft: 4 }} />
                    </Trans>
                    :
                    <Trans i18nKey="dashboard.calendar.assessed_asset">
                        {tenantLabel}
                        <Label className={notApplicableclass} title={t("dashboard.labels.assessed.title")} tip={() => <UserLabel user={submission.lifecycle.compliance?.lastAssessedBy} />} linkTo={linkToSubmission} noIcon style={{ marginLeft: 4 }} />
                    </Trans>
            }
            </div>
        
        }
        
    }

    return self
}

export type CalendarContextType = React.PropsWithChildren<{
    general: CalendarRow[] | undefined
    party: Record<string, CalendarRow[]> | undefined
    asset: Record<string, CalendarRow[]>| undefined
    regenerate: boolean
}>

type CalendarContextProviderType = {
    calendars: CalendarContextType
    setCalendars: (_: CalendarContextType) => void
}

const CalendarContextProvider = React.createContext<CalendarContextProviderType>(undefined!)

export const CalendarContext = (props: React.PropsWithChildren<{}>) => {

    const campaign = useCurrentCampaign()
    const eventInstances = useEventInstances(campaign)
    const events = eventInstances.all()
    const submissions = useSubmissions(campaign)
    const trails = submissions.allTrails()
    const partyInstances = usePartyInstances(campaign)
    const requirementInstances = useRequirementInstances(campaign)
    const productInstances = useProductInstances(campaign)
    const allPartyInstances = partyInstances.all()
    const allRequirementInstances = requirementInstances.all()
    const allProductInstances = productInstances.all()

    const deps = React.useMemo(() =>[events, trails, allPartyInstances, allRequirementInstances, allProductInstances] as const, [events, trails, allPartyInstances, allRequirementInstances, allProductInstances])

    const [calendars, setCalendars] = React.useState<CalendarContextType>(undefined!)

    //eslint-disable-next-line
    const setCalendarAndReset = React.useCallback((value: CalendarContextType) => setCalendars({...value, regenerate: false}), [deps])
    //eslint-disable-next-line
    const value = React.useMemo(() => ({ calendars, setCalendars: setCalendarAndReset }) , [calendars])

    React.useEffect(() => {
        if (calendars)
            setCalendars({...calendars, regenerate: true})
    //eslint-disable-next-line
    } , [deps])

    return <CalendarContextProvider.Provider value={value}>
        {props.children}
    </CalendarContextProvider.Provider>
}

export const useCalendarContext = () => React.useContext(CalendarContextProvider)
