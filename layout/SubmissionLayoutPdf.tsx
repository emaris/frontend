import { useCurrentLanguage } from 'apprise-frontend/intl/api'
import { PDFProps } from 'apprise-frontend/layout/components/model'
import { useLayout } from 'apprise-frontend/layout/LayoutProvider'
import { LayoutInstance } from 'apprise-frontend/layout/model'
import { usePdfLayout } from 'apprise-frontend/layout/pdf/hook'
import { BytestreamedContext } from 'apprise-frontend/stream/BytestreamedHelper'
import { stripHtmlTags } from 'apprise-frontend/utils/common'
import { CurrentCampaignContext, useCurrentCampaign } from 'emaris-frontend/campaign/hooks'
import { SubmissionReactContext, useSubmissionContext } from 'emaris-frontend/campaign/submission/hooks'
import { useProducts } from 'emaris-frontend/product/api'
import { productType } from 'emaris-frontend/product/constants'
import { useRequirements } from 'emaris-frontend/requirement/api'
import * as React from 'react'

export const useSubmissionDownload = (layout: LayoutInstance) => {

    const { document, downloadPdf, generatePdf } = usePdfLayout({ layout })
    const ctx = useSubmissionContext()
    const currentCampaign = useCurrentCampaign()

    const { party, submission } = ctx

    // const publicName = usePublicName()

    // const name = `${publicName}.pdf`

    const submissionDocument = (props: PDFProps) => <CurrentCampaignContext campaign={currentCampaign}>
        <SubmissionReactContext.Provider value={ctx}>
            <BytestreamedContext tenant={party.id} target={submission.id}  >
                {document?.(props)}
            </BytestreamedContext>
        </SubmissionReactContext.Provider>
    </CurrentCampaignContext>

    const downloadSubmissionPdf = (name: string) => downloadPdf({ fileName: name, document: submissionDocument })
    const generateSubmissionPdf = () => generatePdf({ document: submissionDocument })
    // const generateBytestreamForPdf = () => generateSubmissionPdf().then(doc => bytestreamForBlob(name, doc))

    return { downloadSubmissionPdf, document: submissionDocument, generateSubmissionPdf }

}


export const usePublicName = () => {
    const { layoutLanguage, ll, rr } = useLayout()
    const ctx = useSubmissionContext()
    const currentLang = useCurrentLanguage()

    const language = layoutLanguage ?? currentLang

    const isPublished = ctx.submission.lifecycle.state === 'published'

    const products = useProducts()
    const requirements = useRequirements()

    const campaign = ll(ctx.campaign.name)
    const title = ctx.asset.instanceType === productType ? ll(products.lookup(ctx.asset.source)?.name!) : ll(requirements.lookup(ctx.asset.source)?.name!)

    const referenceParamInLang = ctx.submission.lifecycle.reference ? ctx.submission.lifecycle.reference[language] ? ctx.submission.lifecycle.reference[language] : undefined : undefined

    const formatFile = (fileName: string): string => stripHtmlTags(fileName).substring(0, 254).replaceAll(',', '').replaceAll(' ', '_')

    const fileName = (referenceParamInLang && isPublished) ? rr(referenceParamInLang) : `${title}-${language}-${campaign}`

    return formatFile(`${fileName}.pdf`)

}