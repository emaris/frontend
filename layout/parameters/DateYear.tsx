import { Select } from "antd"
import { Label } from "apprise-frontend/components/Label"
import { Field } from "apprise-frontend/form/Field"
import { useReadonly } from "apprise-frontend/form/ReadonlyBox"
import { icns } from "apprise-frontend/icons"
import { layoutapi } from 'apprise-frontend/layout/api'
import { newIdFor } from "apprise-frontend/layout/components/model"
import { useLayoutState } from "apprise-frontend/layout/LayoutProvider"
import { UnknownParameterValueLabel } from "apprise-frontend/layout/parameters/List"
import { Parameter, ParameterSpec, partialParameterSpec, ValueBoxProps } from "apprise-frontend/layout/parameters/model"
import { layoutsettingsapi } from "apprise-frontend/layout/settings/api"
import { LayoutState } from "apprise-frontend/layout/state"
import { timeapi } from "apprise-frontend/time/api"
import { useTimePicker } from "apprise-frontend/time/TimePickerBox"
import { campaignmodelapi } from 'emaris-frontend/campaign/model'
import { pinnedDateIcon, recurringDateIcon, singleUseDateIcon } from "emaris-frontend/event/constants"
import { EmarisState } from "emaris-frontend/state/model"
import moment from "moment-timezone"
import { useTranslation } from "react-i18next"
import '../../campaign/event/styles.scss'

const yearFormat = "YYYY"

export type DateYear = {
    type: 'date' | 'year'
    date?: number
    branchType: 'recurring' | 'single' | 'pinned'
}

export const isDateYear = (dy: any|undefined) : dy is DateYear | undefined => dy ? Object.keys(dy).includes('type') && Object.keys(dy).includes('branchType')  : dy


export type DateYearParam = Parameter<DateYear>

export const defaultDateParameterValue = () => ({branchType: 'recurring' as const, date: undefined, type: 'year' as const})

export const dateyearspec: ParameterSpec<DateYear, DateYearParam> = {

    ...partialParameterSpec,

    id: 'dateyear',
    name: 'layout.parameters.dateyear.name',
    icon: icns.calendar,
    defaultValue: defaultDateParameterValue,

    ValueBox: props => <DateYearBox {...props} />,

    textValue: (p, s) => {
        
        const { layoutConfig } = layoutapi(s)
    
        const { campaign } = layoutConfig

        let timezone = campaign ? campaignmodelapi(stateOf(s)).timeZone(campaign) : undefined

        return  p.value ? formatDateYear(p.value, s, timezone ) : 'N/A'
    
    }

    ,

    // The label is with noMemo because if a decoration changes the label does not get updated.
    Label: (p) => {
        const s = useLayoutState()

        const { layoutConfig } = layoutapi(s)
    
        const { campaign } = layoutConfig

        let timezone = campaign ? campaignmodelapi(stateOf(s)).timeZone(campaign) : undefined

        return (p.parameter.value && p.parameter.value.date) ?
            <Label noMemo icon={icns.calendar} decorations={decorationFor(p.parameter.value)} title={formatDateYear(p.parameter.value, s, timezone)} />
            : <div style={{display: 'flex', flexDirection:'row'}}><UnknownParameterValueLabel />{decorationFor(p.parameter.value)}</div>
    },

    clone: (p, c) => {

        if (c) {
            const value = p.value.date || moment().unix()
            const offset = c.timeOffset ?? 0
    
            if (p.value.branchType === 'recurring') {
                const newDate = p.value.type === undefined || p.value.type === 'year' ?
                    value + offset
                    :
                    moment.unix(value).add(offset, 'years').unix()
                return { ...p, id: newIdFor(p.spec), value: { ...p.value, date: newDate } }
            } else if (p.value.branchType === 'single') {
                return undefined
            }
        }
        return { ...p, id: newIdFor(p.spec) }
    }


}

const RecurringDateLabel = () => {
    const { t } = useTranslation()
    return <Label style={{ paddingLeft: 10 }} icon={recurringDateIcon} title={t("campaign.date.recurring")} tip={t("campaign.date.recurring_tip")} />
}
const SingleUseDateLabel = () => {
    const { t } = useTranslation()
    return <Label style={{ paddingLeft: 10 }} icon={singleUseDateIcon} title={t("campaign.date.single_use")} tip={t("campaign.date.single_use_tip")} />
}
const PinnedDateLabel = () => {
    const { t } = useTranslation()
    return <Label style={{ paddingLeft: 10 }} icon={pinnedDateIcon} title={t("campaign.date.pinned")} tip={t("campaign.date.pinned_tip")} />
}


const DateYearBox = (props: ValueBoxProps<DateYear, DateYearParam>) => {
    const { parameter, onChange, readonly, ...rest } = useReadonly(props)

    const { t } = useTranslation()

    const { Picker, setOpen: changeOpenState } = useTimePicker()

    const type = parameter.value?.type ?? 'year'
    const value = parameter.value?.date ? type === 'year' ? moment().year(parameter.value.date) : moment.unix(parameter.value.date) : undefined
    const format = type ? type === 'year' ? yearFormat : 'l' : yearFormat
    const branchType = parameter.value?.branchType

    const hideIfSelected = (value: string) => ({ display: type !== value ? 'inherit' : 'none' })

    const hideBranchTypeIfSelected = (value: string) => {
        const visible = { display: 'inherit' }
        const hidden = { display: 'none' }

        if (parameter.value?.branchType === undefined && value === 'recurring') return hidden
        return parameter.value?.branchType !== value ? visible : hidden
    }

    const switchDate = (t: 'year' | 'date', value?: number) => {
        if (value) {
            if (t === 'year') return moment.unix(value).year()
            if (t === 'date') return moment().year(value).unix()
            return value
        }
        return undefined!
    }

    return <Field {...rest}>
        <div className="datebox"><div className="datebox-frame">
            <div className="datebox-absolute">
                <Select disabled={readonly} className="datebox-type" dropdownClassName="date-option" suffixIcon={icns.down} defaultValue={type || 'year'} onChange={t => onChange({ ...parameter.value, type: t, date: switchDate(t, parameter.value?.date) })} >
                    <Select.Option style={hideIfSelected('year')} className="datebox-option" value="year">{t('layout.parameters.dateyear.year')}</Select.Option>
                    <Select.Option style={hideIfSelected('date')} className="datebox-option" value="date">{t('layout.parameters.dateyear.date')}</Select.Option>
                </Select>
                <Picker
                    light
                    disabled={readonly}
                    format={format}
                    onChange={v => {
                        onChange(
                            { ...parameter.value, date: v ? type === 'year' ? v.year() : v.unix() : undefined, branchType: parameter.value?.branchType || 'recurring' }
                        )
                        changeOpenState(false)
                    }
                    }
                    value={value}
                    mode={type}
                    showTime={false} />
                {<Select style={{ marginLeft: 0 }} disabled={readonly} className="datebox-branch-type" dropdownClassName="date-option" suffixIcon={icns.down} defaultValue={branchType || 'recurring'} onChange={bt => onChange({ ...parameter.value, branchType: bt })} >
                    <Select.Option style={hideBranchTypeIfSelected('recurring')} className="datebox-option" value="recurring"><RecurringDateLabel /></Select.Option>
                    <Select.Option style={hideBranchTypeIfSelected('pinned')} className="datebox-option" value="pinned"><PinnedDateLabel /></Select.Option>
                    <Select.Option style={hideBranchTypeIfSelected('single')} className="datebox-option" value="single"><SingleUseDateLabel /></Select.Option>
                </Select>}
            </div></div>
        </div>
    </Field>
}

const formatDateYear = (d: DateYear, s: LayoutState, timezone?: string) => {

    const time = timeapi(stateOf(s))

    const language = layoutsettingsapi(s).resolveLanguage()
    

    return d.date ? d.date > 9999 ? time.format( timezone ? moment.unix(d.date).tz(timezone) : moment.unix(d.date), 'short', language) : d.date.toString() : ''
}

const decorationFor = (d: DateYear) => d.branchType === 'recurring' ? [<RecurringDateLabel key='recurring' />] : d.branchType === 'pinned' ? [<PinnedDateLabel key='pinned' />] : [<SingleUseDateLabel key='single' />]

const stateOf = (s: LayoutState) => s as any as EmarisState
