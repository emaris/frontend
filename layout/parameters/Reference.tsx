import { layoutapi } from 'apprise-frontend/layout/api';
import { useLayoutState } from 'apprise-frontend/layout/LayoutProvider';
import { ParameterSpec } from "apprise-frontend/layout/parameters/model";
import { TextParam, textspec } from "apprise-frontend/layout/parameters/Text";
import { MultilangDto } from "apprise-frontend/model/multilang";
import { predefinedProductReferenceId } from './constants';

// based on text param for now.
export type ReferenceParam = TextParam

// extends base spec with utlity methods for for predefined instances.
export const referencespec: ParameterSpec<MultilangDto, ReferenceParam> = {

    ...textspec,

    id: 'reference',
    name: 'layout.parameters.reference.name'

}


export const useReferenceParam = () => {

    const state = useLayoutState()
  
    const self = {

        param: () => layoutapi(state).lookupParameterById(predefinedProductReferenceId),
        value: () => self.param()?.value

    }

    return self
}


