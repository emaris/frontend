

import { tenantspec } from "apprise-frontend/layout/parameters/Tenant"
import { LiveParameter } from "./model"
import { SubmissionContext } from "emaris-frontend/campaign/submission/model"


export const partyspec : LiveParameter =  {
    
    
    ...tenantspec,


    valueFor: (ctx:SubmissionContext)=> ctx.party.source

}