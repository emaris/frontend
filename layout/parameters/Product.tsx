import { configapi } from "apprise-frontend/config/state"
import { intlapi } from "apprise-frontend/intl/api"
import { config } from "apprise-frontend/intl/model"
import { useLayout } from "apprise-frontend/layout/LayoutProvider"
import { Parameter, ParameterReference, partialParameterSpec } from "apprise-frontend/layout/parameters/model"
import { layoutsettingsapi } from "apprise-frontend/layout/settings/api"
import { LayoutState } from "apprise-frontend/layout/state"
import { useLocale } from "apprise-frontend/model/hooks"
import { ContextAwareSelectBox } from "apprise-frontend/system/ContextAwareSelectBox"
import { timeapi } from "apprise-frontend/time/api"
import { randomIn, recwalk } from "apprise-frontend/utils/common"
import { campaignmodelapi } from 'emaris-frontend/campaign/model'
import { productinstapi } from "emaris-frontend/campaign/product/api"
import { ProductInstance } from 'emaris-frontend/campaign/product/model'
import { SubmissionLayoutConfig } from "emaris-frontend/campaign/submission/Detail"
import { submissionapi } from 'emaris-frontend/campaign/submission/api'
import { Submission, SubmissionContext, Trail } from "emaris-frontend/campaign/submission/model"
import { ProductLabel } from "emaris-frontend/product/Label"
import { productapi, useProducts } from "emaris-frontend/product/api"
import { productIcon } from "emaris-frontend/product/constants"
import { Product, ProductDto } from 'emaris-frontend/product/model'
import { EmarisState } from "emaris-frontend/state/model"
import moment from 'moment-timezone'
import { AssetParameter } from "./Asset"
import { LiveParameter } from "./model"


export type ProductParameterValue = {

    id: string,

    liveData?: LiveData
}

export type LiveData = {

    product: Product

    // the instance associated with this product in this campaign, if any.
    instance: ProductInstance

    // a related submission of the instance in this campaign, if any.
    submission: Submission

    // the trail of the related submission, if any.
    trail: Trail

}


export type ProductParameter = Parameter<ProductParameterValue> & {

    fixedValue: boolean
}

export const productspec: LiveParameter<ProductParameterValue, ProductParameter> = {


    ...partialParameterSpec as any as LiveParameter<ProductParameterValue, ProductParameter>,

    id: 'product',
    name: 'layout.parameters.product.name',
    icon: productIcon

    ,

    valueFor: (ctx: SubmissionContext, parameter: Parameter, s: EmarisState) => {

        const product = productapi(s).safeLookup(parameter.value.id)

        const instance = productinstapi(s)(ctx.campaign).safeLookupBySource(parameter.value.id)

        return {

            id: parameter.value.id,

            liveData: {
                product,
                instance,
                submission: ctx.submission,
                trail: ctx.trail
            }

        }
    }

    ,

    textValue: (p, s, path) => {
        const settingsapi = layoutsettingsapi(s)

        const { ll } = settingsapi

        const { id, liveData } = ( p as AssetParameter).value

        // fetch source where it's guaranteed to exist both inside and outside campaigns (can't just use live data.)
       
        const product = productapi(stateOf(s)).lookup(id)!

        if (!liveData) {
           
            return recwalk(

                {
                    name: ll(product.name),
                    title: ll(product.description),
                    source: product.properties.source?.title ? ll(product.properties.source.title) : '',
                    complianceObservation: undefined, // so it appears as missing, vs. not-existing (red error).

                }
                
                
                , path)
        }

        const {resolveLanguage, layoutConfig} = settingsapi

        const language = resolveLanguage()

        const t = intlapi(s).getFixedT(language)

        const { instance, submission } = liveData
        
        const {dashboard, campaign, party } = layoutConfig as SubmissionLayoutConfig

        const deadlineOf = () => {
            const currentZone = campaignmodelapi(stateOf(s)).timeZone(campaign)!

            const deadline = layoutConfig.dashboard.summaries.dueDates[instance.source]

            return timeapi(
                stateOf(s)).format(moment(deadline).tz(currentZone) ?? t("submission.labels.not_assessed"), 
                'short', 
                language
            )
        }
        
        switch (path[0]) {

            case 'name': return recwalk( { name: ll(product.name)},path)
            case 'title': return recwalk( { title: product.description ? ll(product.description) : ll(product.name) },path)
            case 'source': return recwalk({ source: product.properties.source?.title ? ll(product.properties.source?.title) : ''},path)
            case 'deadline': return deadlineOf()
            case 'assessed': return submission?.lifecycle.compliance?.state ? 'true' : 'false'
            case 'submitted': return submission?.lifecycle.lastSubmitted && submission?.lifecycle.state !== 'missing' ? 'true' : 'false'
            case 'reference' : return recwalk({reference: ll(submission?.lifecycle.reference)}, path)
            
        }
        
        if (!campaign || !party) return undefined

       
        const submitted = submission?.lifecycle.lastSubmitted && submission?.lifecycle.state !== 'missing'

        switch (path[0]) {

            case 'submissionDate': return submitted ? timeapi(stateOf(s)).format(submission.lifecycle.lastSubmitted, 'long', language) : t("submission.status.missing")
       
        }
        const trailSummary = dashboard.summaries.trailSummaryMap[submission?.trail!]

        // If the submission is the latest we can get the compliance from trailsummary cache instead of recomputing because the trailsummary holds computations based on the current situtation
        const complianceProfile = () => submission ? 
            trailSummary.lastSubmitted?.id === submission.id ? trailSummary.compliance
            : submissionapi(stateOf(s))(campaign).complianceProfile(submission, instance) 
            : undefined

        switch (path[0]) {

            case 'complianceObservation' : return recwalk({
                
                complianceObservation: complianceProfile()?.complianceObservation  ??  (configapi(s).get().intl.languages ?? config.languages).reduce((acc, cur) => ({ ...acc, [cur]: 'N/A' }), {})
            
            }, path )

            case 'complianceRate': return recwalk({  complianceRate: ll(complianceProfile()?.name) ?? t("submission.labels.not_assessed")},path)
            case 'complianceCode' : return complianceProfile()?.code ?? t("submission.labels.not_assessed")

        }
        
        const computedTimeliness = trailSummary?.computedTimeliness

        // If the submission is the latest we can get the timeliness from trailsummary cache instead of recomputing because the trailsummary holds computations based on the current situtation
        const timelinessProfile = () => submission ? trailSummary.lastSubmitted?.id === submission.id ? 
            trailSummary.timeliness
        : submissionapi(stateOf(s))(campaign).timelinessProfile(submission, instance, undefined, computedTimeliness) : undefined

        switch (path[0]) {

            case 'timelinessRate': return recwalk( {timelinessRate: ll(timelinessProfile()?.tag?.name) ?? t("submission.labels.not_assessed")},path)
            case 'timelinessCode':  return timelinessProfile()?.tag?.code ?? t("submission.labels.not_assessed")
            case 'timeliness' : return t(`dashboard.labels.timeliness.${computedTimeliness}`)

        }
        

        const profile = { name: ll(product.name), title: ll(product.description) }

        return recwalk(profile, path)


    }
    ,

    randomValue: (s) => randomIn(productapi(stateOf(s)).all())?.id

    ,

    references: (p, s): ParameterReference[] => {

        const t = intlapi(s).getT()
        const name = { name: p.name }

        const languages = configapi(s).get().intl.languages || config.languages

        const complianceObservationReferences = languages.map(l => ({ id: `${p.id}.complianceObservation.${l}`, value: t('layout.parameters.product.references.complianceObservation', { name: p.name, lang: l }) }))

        return [
            { id: `${p.id}.name`, value: t('layout.parameters.product.references.name', name) },
            { id: `${p.id}.title`, value: t('layout.parameters.product.references.title', name) },
            { id: `${p.id}.deadline`, value: t('layout.parameters.product.references.deadline', name) },
            { id: `${p.id}.submissionDate`, value: t('layout.parameters.product.references.submissionDate', name) },
            { id: `${p.id}.assessed`, value: t('layout.parameters.product.references.assessed', name) },
            { id: `${p.id}.submitted`, value: t('layout.parameters.product.references.submitted', name) },
            { id: `${p.id}.complianceRate`, value: t('layout.parameters.product.references.complianceRate', name) },
            { id: `${p.id}.complianceCode`, value: t('layout.parameters.product.references.complianceCode', name) },
            { id: `${p.id}.timelinessRate`, value: t('layout.parameters.product.references.timelinessRate', name) },
            { id: `${p.id}.timelinessCode`, value: t('layout.parameters.product.references.timelinessCode', name) },
            { id: `${p.id}.timeliness`, value: t('layout.parameters.product.references.timeliness', name) },
            { id: `${p.id}.reference`, value: t('layout.parameters.product.references.reference', name) },
            ...complianceObservationReferences
        ]

    }

    ,

    Label: ({ parameter }) => parameter.value ? <ProductLabel product={parameter.value.id ? parameter.value.id : parameter.value} /> : null

    ,

    ValueBox: props => {

        const { l } = useLocale()

        const { domainContext } = useLayout().layoutConfig
        const products = useProducts()

        const { parameter, onChange, ...rest } = props

        const currentValue = parameter.value && products.lookup(parameter.value.id)

        return <ContextAwareSelectBox disabled={parameter.fixedValue} {...rest}
            clearable
            currentContext={domainContext}
            options={products.allSorted()}
            onChange={(product: ProductDto) => onChange({ id: product.id })}
            renderOption={p => <ProductLabel product={p} noLink />}
            lblTxt={p => l(p.name)}
            optionId={r => ({ id: r.id })}>

            {[currentValue]}

        </ContextAwareSelectBox>

    }

}



const stateOf = (s: LayoutState) => s as any as EmarisState
