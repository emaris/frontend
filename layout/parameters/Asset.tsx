import { configapi } from "apprise-frontend/config/state"
import { icns } from "apprise-frontend/icons"
import { intlapi } from "apprise-frontend/intl/api"
import { config } from "apprise-frontend/intl/model"
import { layoutapi } from "apprise-frontend/layout/api"
import { Component } from 'apprise-frontend/layout/components/model'
import { useLayout } from "apprise-frontend/layout/LayoutProvider"
import { parameterapi, ResolveMode } from "apprise-frontend/layout/parameters/api"
import { Parameter, ParameterReference, ParameterSpec, partialParameterSpec } from "apprise-frontend/layout/parameters/model"
import { layoutsettingsapi } from "apprise-frontend/layout/settings/api"
import { LayoutState } from "apprise-frontend/layout/state"
import { useLocale } from "apprise-frontend/model/hooks"
import { ContextAwareSelectBox } from "apprise-frontend/system/ContextAwareSelectBox"
import { timeapi } from "apprise-frontend/time/api"
import { randomIn, recwalk } from "apprise-frontend/utils/common"
import { campaignmodelapi } from "emaris-frontend/campaign/model"
import { productinstapi } from "emaris-frontend/campaign/product/api"
import { ProductInstance } from "emaris-frontend/campaign/product/model"
import { requirementinstapi } from "emaris-frontend/campaign/requirement/api"
import { RequirementInstance } from 'emaris-frontend/campaign/requirement/model'
import { submissionapi } from "emaris-frontend/campaign/submission/api"
import { SubmissionLayoutConfig } from "emaris-frontend/campaign/submission/Detail"
import { Submission, SubmissionContext, SubmissionWithTrail, Trail } from 'emaris-frontend/campaign/submission/model'
import { productapi, useProducts } from "emaris-frontend/product/api"
import { ProductLabel } from "emaris-frontend/product/Label"
import { Product, ProductDto } from "emaris-frontend/product/model"
import { requirementapi, useRequirements } from "emaris-frontend/requirement/api"
import { requirementType } from "emaris-frontend/requirement/constants"
import { RequirementLabel } from "emaris-frontend/requirement/Label"
import { Requirement, RequirementDto } from 'emaris-frontend/requirement/model'
import { EmarisState } from "emaris-frontend/state/model"
import moment from "moment-timezone"
import { isLiveParameter, LiveParameter } from "./model"


export type AssetParameterValue = {

    id: string
    liveData?: AssetLiveData
    type: string

}

export type AssetLiveData = {

    // the asset.
    asset: Requirement | Product

    // the instance associated with this asset in this campaign.
    instance: RequirementInstance | ProductInstance

    // a related submission of the instance in this campaign, if any (inside reports, req sections, there may be no related submissions).
    submission?: Submission

    // the trail of the related submission.
    trail: Trail

}

export type AssetParameter = Parameter<AssetParameterValue> & {

    // in bridge mode, references and resolves any other non-live custom parameter.
    bridgeMode: boolean
    fixedValue: boolean
    components: Component[]
    data: Record<string, any>
}


// helpers
const flatten = (idprefix: string, nameprefix: string, data: any): ParameterReference[] =>

    typeof data === 'object' ?
        Object.keys(data).flatMap(k => flatten(`${idprefix}.${k}`, `${nameprefix}.${k}`, data[k]))
        :

        [{ id: idprefix, value: nameprefix }]    // basecase



const otherCustomParameters = (asset: Requirement | Product, p: AssetParameter, s: LayoutState) => {

    const { parameterSpecOf } = layoutapi(s)

    return asset.properties.layout.parameters

        .filter(p => !isLiveParameter(parameterSpecOf(p)))  // excludes live params: specially handled
        .filter(p => parameterSpecOf(p).id !== assetspec.id) // excludes other requirement parameters, would be cyclic.


}


export const assetspec: LiveParameter<AssetParameterValue, AssetParameter> = {


    ...partialParameterSpec as any as ParameterSpec<AssetParameterValue, AssetParameter>,

    id: 'asset',
    name: 'layout.parameters.asset.name',
    icon: icns.asset

    ,

    valueFor: (ctx: SubmissionContext, parameter: AssetParameter, s: EmarisState) => {

        const { safeLookup: requirementSafeLookup } = requirementapi(s)
        const { safeLookup: productSafeLookup } = productapi(s)

        const asset = parameter.value.type === requirementType ? requirementSafeLookup(parameter.value.id) : productSafeLookup(parameter.value.id)

        // lookup the requirement this parameter is configured with.
        const instance = parameter.value.type === requirementType ?
            requirementinstapi(s)(ctx.campaign).safeLookupBySource(parameter.value.id)
            :
            productinstapi(s)(ctx.campaign).safeLookupBySource(parameter.value.id)

        // if we're rendering a requirement, take live data. 
        // if we're rendering a report, seek a match among related submissions.
        const trailAndSubmission = ctx.asset.instanceType === requirementType ? { submission: ctx.submission, trail: ctx.trail } :

            ctx.relatedSubmissions.find(sub => sub.trail.key.asset === instance?.source) ?? {} as SubmissionWithTrail



        return {

            id: parameter.value?.id,

            liveData: {

                asset,
                instance,
                ...trailAndSubmission
            }
            ,
            type: parameter.value.type
        }
    }

    ,

    mode: (path: string[], defaultMode: ResolveMode) => defaultMode === 'live' && path[0] === 'data' ? 'livedata' : defaultMode

    ,

    textValue: (p, s, path) => {

        const settingsapi = layoutsettingsapi(s)

        const { ll } = settingsapi

        const { id, liveData, type } = ( p as AssetParameter).value

        // fetch source where it's guaranteed to exist both inside and outside campaigns (can't just use live data.)
       

        if (!liveData) {
            const asset = type === requirementType ? requirementapi(stateOf(s)).lookup(id)! : productapi(stateOf(s)).lookup(id)! // worst case scenario: parameter doesn't resolve.
            
            return recwalk(

                {
                    name: ll(asset.name),
                    title: ll(asset.description),
                    source: asset.properties.source?.title ? ll(asset.properties.source.title) : '',
                    complianceObservation: undefined, // so it appears as missing, vs. not-existing (red error).
                    data: p.data
                }
                
                
                , path)
        }

        const {parameterSpecOf} = parameterapi(s)

        const {resolveLanguage, layoutConfig} = settingsapi

        const language = resolveLanguage()

        const t = intlapi(s).getFixedT(language)

        const { instance, submission } = liveData
        
        const {dashboard, campaign, party } = layoutConfig as SubmissionLayoutConfig

        const deadlineOf = () => {
            const currentZone = campaignmodelapi(stateOf(s)).timeZone(campaign)!

            const deadline = layoutConfig.dashboard.summaries.dueDates[instance.source]

            return timeapi(
                stateOf(s)).format(moment(deadline).tz(currentZone) ?? t("submission.labels.not_assessed"), 
                'short', 
                language
            )
        }
        
        switch (path[0]) {

            case 'name': return recwalk( { name: ll(liveData.asset.name)},path)
            case 'title': return recwalk( { title: liveData.asset.description ? ll(liveData.asset.description) : ll(liveData.asset.name) },path)
            case 'source': return recwalk({ source: liveData.asset.properties.source?.title ? ll(liveData.asset.properties.source?.title) : ''},path)
            case 'deadline': return deadlineOf()
            case 'assessed': return submission?.lifecycle.compliance?.state ? 'true' : 'false'
            case 'submitted': return submission?.lifecycle.lastSubmitted && submission?.lifecycle.state !== 'missing' ? 'true' : 'false'
            
        }
        
        if (!campaign || !party) return undefined

       
        const submitted = submission?.lifecycle.lastSubmitted && submission?.lifecycle.state !== 'missing'

        switch (path[0]) {

            case 'submissionDate': return submitted ? timeapi(stateOf(s)).format(submission.lifecycle.lastSubmitted, 'long', language) : t("submission.status.missing")
       
        }

        const trailSummary = dashboard.summaries.trailSummaryMap[submission?.trail!]

        // If the submission is the latest we can get the compliance from trailsummary cache instead of recomputing because the trailsummary holds computations based on the current situtation
        const complianceProfile = () => submission ? 
            trailSummary.lastSubmitted?.id === submission.id ? trailSummary.compliance
            : submissionapi(stateOf(s))(campaign).complianceProfile(submission, instance) 
            : undefined

        switch (path[0]) {

            case 'complianceObservation' : return recwalk({
                
                complianceObservation: complianceProfile()?.complianceObservation  ??  (configapi(s).get().intl.languages ?? config.languages).reduce((acc, cur) => ({ ...acc, [cur]: 'N/A' }), {})
            
            }, path )

            case 'complianceRate': return recwalk({  complianceRate: ll(complianceProfile()?.name) ?? t("submission.labels.not_assessed")},path)
            case 'complianceCode' : return complianceProfile()?.code ?? t("submission.labels.not_assessed")

        }
        
        const computedTimeliness = trailSummary?.computedTimeliness

        // If the submission is the latest we can get the timeliness from trailsummary cache instead of recomputing because the trailsummary holds computations based on the current situtation
        const timelinessProfile = () => submission ? trailSummary.lastSubmitted?.id === submission.id ? 
            trailSummary.timeliness
        : submissionapi(stateOf(s))(campaign).timelinessProfile(submission, instance, undefined, computedTimeliness) : undefined

        switch (path[0]) {

            case 'timelinessRate': return recwalk( {timelinessRate: ll(timelinessProfile()?.tag?.name) ?? t("submission.labels.not_assessed")},path)
            case 'timelinessCode':  return timelinessProfile()?.tag?.code ?? t("submission.labels.not_assessed")
            case 'timeliness' : return t(`dashboard.labels.timeliness.${computedTimeliness}`)

        }
        
       
        // prepares to resolve all parameters other than 
        const customs = p.bridgeMode ? otherCustomParameters(liveData.asset, p, s).reduce((acc, op) => {

            // overlays instance overrides 
            const opWithOverrides = { ...op, value: p.value.liveData?.instance.properties.parameterOverlay?.[op.name]?.value ?? op.value }

            return {

                ...acc,
                [`${op.id}`]: parameterSpecOf(op).textValue(opWithOverrides, s, path)

            }

        }, {}) : {}

        
        const profile = {


            ...customs,
            data: p.data
        }

        return recwalk(profile, path)

    }

    ,

    randomValue: (s) => randomIn(requirementapi(stateOf(s)).all())?.id

    ,

    references: (p, s): ParameterReference[] => {

        const t = intlapi(s).getT()

        const { parameterSpecOf } = layoutapi(s)

        const asset = p.value.type === requirementType ? requirementapi(stateOf(s)).safeLookup(p.value.id) : productapi(stateOf(s)).safeLookup(p.value.id)
        const name = p.name || asset.name.en?.replace(/\s/g, '-').toLowerCase()

        // 'bridges' to other custom, non-live params.
        const customParamReferencesPrefixed = p.bridgeMode ?

            otherCustomParameters(asset, p, s).flatMap(p => parameterSpecOf(p).references(p, s)).map(cp => ({ id: `${p.id}.${cp.id}`, value: `${name}.${cp.value}` }))

            :

            []

        const languages = configapi(s).get().intl.languages || config.languages

        const complianceObservationReferences = languages.map(l => ({ id: `${p.id}.complianceObservation.${l}`, value: t('layout.parameters.asset.references.complianceObservation', { name, lang: l }) }))

        const references = [
            { id: `${p.id}.name`, value: t('layout.parameters.asset.references.name', { name }) },
            { id: `${p.id}.title`, value: t('layout.parameters.asset.references.title', { name }) },
            { id: `${p.id}.source`, value: t('layout.parameters.asset.references.source', { name }) },
            { id: `${p.id}.deadline`, value: t('layout.parameters.asset.references.deadline', { name }) },
            { id: `${p.id}.submissionDate`, value: t('layout.parameters.asset.references.submissionDate', { name }) },
            { id: `${p.id}.submitted`, value: t('layout.parameters.asset.references.submitted', { name }) },
            { id: `${p.id}.assessed`, value: t('layout.parameters.asset.references.assessed', { name }) },
            { id: `${p.id}.complianceRate`, value: t('layout.parameters.asset.references.complianceRate', { name }) },
            { id: `${p.id}.complianceCode`, value: t('layout.parameters.asset.references.complianceCode', { name }) },
            { id: `${p.id}.timelinessRate`, value: t('layout.parameters.asset.references.timelinessRate', { name }) },
            { id: `${p.id}.timelinessCode`, value: t('layout.parameters.asset.references.timelinessCode', { name }) },
            { id: `${p.id}.timeliness`, value: t('layout.parameters.asset.references.timeliness', { name }) },

            ...complianceObservationReferences,

            ...customParamReferencesPrefixed,

            // data references

            ...(p.data ?

                flatten(`${p.id}.data`, t('layout.parameters.asset.references.data', { name }), p.data)


                : [])
        ]

        return references

    }

    ,

    Label: ({ parameter }) => parameter.value ? <RequirementLabel requirement={parameter.value.id ? parameter.value.id : parameter.value} /> : null

    ,

    ValueBox: props => {

        const { l } = useLocale()
        const { domainContext } = useLayout().layoutConfig
        const requirements = useRequirements()
        const products = useProducts()

        const { parameter, onChange, ...rest } = props

        const isRequirement = !!(requirements.lookup(parameter.value.id))

        const currentValue = parameter.value ?

            parameter.value.type === requirementType ? requirements.lookup(parameter.value.id) : products.lookup(parameter.value.id)
            : undefined

        return <ContextAwareSelectBox disabled={parameter.fixedValue} {...rest}
            clearable
            currentContext={domainContext}
            options={[...requirements.allSorted(), ...products.allSorted()]}
            onChange={(asset: RequirementDto | ProductDto) => onChange({ id: asset.id, type: parameter.value.type })}
            renderOption={asset => isRequirement ? <RequirementLabel requirement={asset} noLink noDecorations /> : <ProductLabel product={asset} noLink noDecorations />}
            lblTxt={r => l(r.name)}
            optionId={asset => ({ id: asset.id })}>

            {[currentValue]}

        </ContextAwareSelectBox>

    }

}


const stateOf = (s: LayoutState) => s as any as EmarisState
