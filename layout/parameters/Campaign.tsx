import { VSelectBox } from "apprise-frontend/form/VSelectBox"
import { intlapi } from "apprise-frontend/intl/api"
import { layoutapi } from "apprise-frontend/layout/api"
import { ParameterReference, partialParameterSpec } from "apprise-frontend/layout/parameters/model"
import { useLocale } from 'apprise-frontend/model/hooks'
import { randomIn, recwalk } from "apprise-frontend/utils/common"
import { campaignapi, useCampaigns } from "emaris-frontend/campaign/api"
import { campaignIcon, campaignType } from "emaris-frontend/campaign/constants"
import { CampaignLabel } from "emaris-frontend/campaign/Label"
import { EmarisState } from "emaris-frontend/state/model"
import * as React from "react"
import { LiveParameter } from "./model"
import { CampaignDto } from "emaris-frontend/campaign/model"


const stateOf = (s:any) => s as EmarisState

export const campaignspec : LiveParameter<string> = {


    ...partialParameterSpec,

    id:campaignType,
    name: 'layout.parameters.campaign.name',
    icon: campaignIcon


    ,


    valueFor: ctx => ctx.campaign.id
    
    ,

    textValue: (p,s,path) => { 
        
        const {ll} = layoutapi(s)
        
        const campaign = campaignapi(stateOf(s)).lookup(p.value)!  // worst case scenario: parameter doesn't resolve.

        const profile = { name: ll(campaign.name)}

        return recwalk(profile,path)
        
    
    }
    ,

    randomValue: (s) => {
        return (randomIn(campaignapi(stateOf(s)).all()) as CampaignDto)?.id

    }

    ,

    references: (p,s) : ParameterReference[] => {

        const t = intlapi(s).getT()
        
        return [
                {id:`${p.id}.name`,value:t('layout.parameters.campaign.references.name',{name:p.name})}
               ]

    }

    ,

    Label: ({parameter}) =>  parameter.value ? <CampaignLabel noLiveView campaign={parameter.value} /> : null

    ,

    ValueBox: props => {

        const {l} = useLocale()
        const campaigns = useCampaigns()

        const {parameter,onChange, ...rest} = props

        const currentValue = campaigns.lookup(parameter.value)

        return   <VSelectBox 
                    {...rest}
                    clearable
                    lblTxt={c=>l(c.name)}
                    options={campaigns.allSorted()}  
                    onChange={(v:any) => onChange(v?.id )}
                    renderOption={c=><CampaignLabel noLink noLiveView campaign={c} />} 
                    optionId={c=>c.id}>
                                
            {[currentValue]}

        </VSelectBox>
                
    }

}

