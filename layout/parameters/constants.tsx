import { intlapi } from "apprise-frontend/intl/api"
import { newParameterFrom, Parameter } from "apprise-frontend/layout/parameters/model"
import { RequirementParameter, RequirementParameterValue, requirementspec } from "emaris-frontend/layout/parameters/Requirement"
import { EmarisState } from "emaris-frontend/state/model"
import { campaignspec } from "./Campaign"
import { dateyearspec, defaultDateParameterValue } from "./DateYear"
import { partyspec } from "./Party"
import { ProductParameter, productspec } from "./Product"
import { ReferenceParam, referencespec } from "./Reference"
import { numspec } from 'apprise-frontend/layout/parameters/Number'
import { defaultBaseline } from "apprise-frontend/layout/style/Text"


export const predefinedPartyId = "party"
export const predefinedCampaignId = "campaign"

export const predefinedParameterIds = [predefinedPartyId,predefinedCampaignId]

export const predefinedParameters : (_: EmarisState) => Parameter[] = s=>{
    
    const t = intlapi(s).getT()

    return [ 
            
        {...newParameterFrom(partyspec),
            name: t('layout.parameters.tenant.predefined_no_translation'), 
            id:predefinedPartyId,
            fixed:true,
            required:true,
            flag:true } ,

        {...newParameterFrom(campaignspec),
            id: predefinedCampaignId,
            name: t('layout.parameters.campaign.predefined_no_translation'), 
            fixed:true,
            required:true } ,

        {...newParameterFrom(dateyearspec),
            id: predefinedRequirementReportingYearParameterId,
            name: t('layout.parameters.reported_on.predefined_no_translation'), 
            fixed:true,
            required:true,
            value: defaultDateParameterValue() } ,

        {...newParameterFrom(numspec),
            id: predefinedFontSizeBaseLineParameterId,
            name: predefinedFontSizeBaseLineParameterId, 
            fixed:false,
            required:false,
            value: defaultBaseline
        }
    ]

}



export const predefinedFontSizeBaseLineParameterId = "layout-font-baseline"
export const predefinedRequirementParameterId = "requirement"
export const predefinedRequirementReportingYearParameterId = "requirement-reporting-year"
export const predefinedRequirementParameterIds = [predefinedRequirementParameterId, predefinedRequirementReportingYearParameterId]

export const predefinedRequirementParameters : (_: EmarisState) => Parameter[] = s=>{
    
    const t = intlapi(s).getT()

    const thisRequirementParam = {...newParameterFrom(requirementspec),
        
        id: predefinedRequirementParameterId,
        name: t('layout.parameters.requirement.predefined_no_translation'), 
        fixed:true,
        required:true,
        fixedValue:true,
        value: {} as RequirementParameterValue 

    } as RequirementParameter


    // const thisReportingYearParam =
    //     {...newParameterFrom(dateyearspec),
    //         id: predefinedRequirementReportingYearParameterId,
    //         name: t('layout.parameters.reported_on.predefined_no_translation'), 
    //         fixed:true,
    //         required:true }

    return [ thisRequirementParam ]

}


export const predefinedProductParameterId = "product"
export const predefinedProductReferenceId = "ref-template"
export const predefinedProductParameterIds = [predefinedProductParameterId, predefinedProductReferenceId]

const defaultReferenceValue = '<p><span class="mention" contenteditable="false" data-index="3" data-denotation-char="@" data-content-editable="false" data-id="product.name" data-value="report.name">@report.name</span>-<span class="mention" contenteditable="false" data-index="0" data-denotation-char="@" data-content-editable="false" data-id="campaign.name" data-value="campaign.name">@campaign.name</span>-<span class="mention" contenteditable="false" data-index="1" data-denotation-char="@" data-content-editable="false" data-id="party.name" data-value="party.name">@party.name</span>'

export const predefinedProductParameters : (_: EmarisState) => Parameter[] = s => {
    
    const t = intlapi(s).getT()

    const thisProductParameter:ProductParameter = {...newParameterFrom(productspec),
        id: predefinedProductParameterId,
        name: t('layout.parameters.product.predefined_no_translation'), 
        fixed:true,
        required:true,
        fixedValue:true,
        value: { id: undefined!, liveData: undefined}
    
    }

    const thisProductReferenceSpecParameter:ReferenceParam = {...newParameterFrom(referencespec),
        id: predefinedProductReferenceId,
        name: t('layout.parameters.reference_format.predefined_no_translation'), 
        fixed:true,
        required:true,
        value: {en:defaultReferenceValue, fr:defaultReferenceValue} 
    
    } 

    return [ thisProductParameter, thisProductReferenceSpecParameter ]

}