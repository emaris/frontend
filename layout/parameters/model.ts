import { Parameter, ParameterSpec } from "apprise-frontend/layout/parameters/model";
import { SubmissionContext } from 'emaris-frontend/campaign/submission/model';
import { EmarisState } from "emaris-frontend/state/model";


// a layut parameter that takes specific values in a campaign.
export type LiveParameter <V=any, P extends Parameter=Parameter<V>> = ParameterSpec<V,P> & {


    valueFor: (_:SubmissionContext, p:P, s:EmarisState) => V


}

export const isLiveParameter = (spec:ParameterSpec) : spec is LiveParameter => !!(spec as LiveParameter)?.valueFor