
import { Empty, Tooltip } from 'antd'
import { CardlistBox } from 'apprise-frontend/components/CardlistBox'
import { TextBox } from 'apprise-frontend/form/TextBox'
import { icns } from 'apprise-frontend/icons'
import { DetailProps } from 'apprise-frontend/layout/components/ComponentDetail'
import { ComponentDraggable, ComponentDroppable } from 'apprise-frontend/layout/components/ComponentDnD'
import { ComponentPreview } from 'apprise-frontend/layout/components/ComponentPreview'
import { specOf } from 'apprise-frontend/layout/components/helper'
import { useRowSectionContext } from 'apprise-frontend/layout/components/hooks'
import { Component, ComponentSpec, Container, isContainer, nameOf, newComponentFrom, PreviewProps, PrintProps, RenderProps } from 'apprise-frontend/layout/components/model'
import { LayoutScrollSubscriber } from 'apprise-frontend/layout/components/scroll'
import { useComponents, useLayout, useLayoutState, useRows } from 'apprise-frontend/layout/LayoutProvider'
import { ParameterContext } from 'apprise-frontend/layout/parameters/hooks'
import { newParameterFrom } from 'apprise-frontend/layout/parameters/model'
import StyledView from 'apprise-frontend/layout/pdf/StyledView'
import { layoutRegistry } from 'apprise-frontend/layout/registry'
import { layoutsettingsapi } from 'apprise-frontend/layout/settings/api'
import { alignmentspec } from 'apprise-frontend/layout/style/Alignment'
import { backgroundspec } from 'apprise-frontend/layout/style/Background'
import { borderspec } from 'apprise-frontend/layout/style/Border'
import { heightspec } from 'apprise-frontend/layout/style/Height'
import { StyleProps } from 'apprise-frontend/layout/style/model'
import { spacingspec } from 'apprise-frontend/layout/style/Spacing'
import { StyledBox } from 'apprise-frontend/layout/style/StyledBox'
import { widthspec } from 'apprise-frontend/layout/style/Width'
import { useLocale } from 'apprise-frontend/model/hooks'
import { ContextAwareSelectBox } from 'apprise-frontend/system/ContextAwareSelectBox'
import { shortid } from 'apprise-frontend/utils/common'
import { Validation } from 'apprise-frontend/utils/validation'
import { useCurrentCampaign } from 'emaris-frontend/campaign/hooks'
import { useProductInstances } from 'emaris-frontend/campaign/product/api'
import { useRequirementInstances } from 'emaris-frontend/campaign/requirement/api'
import { submissionIcon, submissionType } from 'emaris-frontend/campaign/submission/constants'
import { SubmissionReactContext } from 'emaris-frontend/campaign/submission/hooks'
import { useCurrentParty, useDashboard } from 'emaris-frontend/dashboard/hooks'
import { useProducts } from 'emaris-frontend/product/api'
import { productIcon, productType } from 'emaris-frontend/product/constants'
import { ProductLabel } from 'emaris-frontend/product/Label'
import { Product } from 'emaris-frontend/product/model'
import { useRequirements } from 'emaris-frontend/requirement/api'
import { requirementIcon, requirementType } from 'emaris-frontend/requirement/constants'
import { RequirementLabel } from 'emaris-frontend/requirement/Label'
import { Requirement } from 'emaris-frontend/requirement/model'
import * as React from 'react'
import { useTranslation } from 'react-i18next'
import { AssetLiveData, AssetParameter, assetspec } from '../parameters/Asset'
import { isLiveParameter } from '../parameters/model'
import { NamedRequirement } from './RequirementSection'
import './styles.scss'


export type NamedAsset = {

    id: string
    asset: string
    alias: string
    type: 'product' | 'requirement'

}

export const isNamedAsset = (asset: NamedAsset | NamedRequirement | undefined): asset is NamedAsset => {
    if (asset === undefined) return false
    return (asset as NamedAsset).asset !== undefined
}

export const sectionAssetOf = (nasset: NamedAsset | NamedRequirement | undefined): string => isNamedAsset(nasset) ? (nasset?.asset ?? nasset?.id)! : (nasset?.requirement ?? nasset?.id)!
export const sectionAssetFor = (nasset: NamedAsset | undefined) => nasset?.type

export type AssetSection = Container & StyleProps & {


    assets: NamedAsset[]

}


export const assetsectionspec: ComponentSpec<AssetSection> = specOf({


    id: 'assetsection',
    icon: icns.asset,
    name: 'layout.components.asset.name',

    styles: () => ({

        [backgroundspec.id]: {},
        [widthspec.id]: {},
        [heightspec.id]: {},
        [borderspec.id]: {},
        [alignmentspec.id]: {},
        [spacingspec.id]: {}

    })

    ,

    Title: ({ component }: { component: AssetSection }) => {

        const { t } = useTranslation()
        const { l } = useLocale()
        const { safeLookup: requirementSafeLookup } = useRequirements()
        const { safeLookup: productSafeLookup } = useProducts()

        const assets = component.assets

        const firstAsset = sectionAssetOf(assets ? assets[0] : undefined)

        const asset = sectionAssetFor(assets ? assets[0] : undefined) === requirementType ? requirementSafeLookup(firstAsset) : productSafeLookup(firstAsset)

        const assetType = assets ? assets[0]?.type : undefined

        let title

        if (asset) {
            title = l(asset.name)
            if (component.assets?.length > 1)
                title = t("layout.components.asset.title_and_others", { name: title, count: assets.length - 1 })
        }

        return (assetType && title) ? assetType === requirementType ?
            <RequirementLabel noMemo requirement={firstAsset} title={title} noDecorations noLink /> :
            <ProductLabel noMemo product={firstAsset} title={title} noDecorations noLink />
            : t(assetsectionspec.name)

    }

    ,

    generate: (): AssetSection => ({ ...newComponentFrom(assetsectionspec), assets: [], style: {}, children: [] })

    ,

    Preview: ({ component, path }) =>

        <ComponentDroppable path={path}>
            <ComponentDraggable path={path} component={component}>
                <ComponentPreview path={path} component={component}>
                    <Preview path={path} component={component} />
                </ComponentPreview>
            </ComponentDraggable>
        </ComponentDroppable>
    ,

    Detail,
    Render,
    Print,
    PrintPreview
})



export function Preview(props: PreviewProps<AssetSection>) {

    const { extend, componentSpecOf, allRows, isInstance } = useLayout()

    const { t } = useTranslation()

    const { component, path } = props

    const nextPosition = component.children.length

    const { parameters } = useSectionParameters(component)
    const rowSectionContext = useRowSectionContext()

    const assets = component.assets

    const renderChildren = assets?.length > 0 || component.children?.length > 0

    const allRowsInRowSection = rowSectionContext ? allRows(rowSectionContext).get() : []
    const idx = allRowsInRowSection.findIndex(row => component.children.some(innerrow => innerrow.id === row.id))
    const top = (idx === -1 ? 0 : idx) * -1

    return renderChildren ?

        <ParameterContext.Provider value={parameters}>
            <div className='section'>
                <StyledBox component={component} style={{ top }} >

                    {
                        component.children.map((child, position) => {

                            const { Preview } = componentSpecOf(child)

                            return <Preview key={position} component={child} path={extend(path).at(position)} />

                        })
                    }

                </StyledBox>

                {!isInstance() && <ComponentDroppable key={nextPosition} className="component-placeholder" path={extend(path).at(nextPosition)} />}

            </div>

        </ParameterContext.Provider>


        :

        <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description={t('common.labels.select_one_or_more', { plural: t("asset.plural") })} />

}


function Render(props: RenderProps<AssetSection>) {

        const { t } = useTranslation()

        const { componentSpecOf } = useComponents()
        const { allRows } = useRows()

        const { component, className = '', style } = props

        const { missing, parameters } = useLiveSectionParameters(component)
        const campaign = useCurrentCampaign()
        const requirements = useRequirementInstances(campaign)
        const products = useProductInstances(campaign)
        const { party } = useCurrentParty()
        const rowSectionContext = useRowSectionContext()

        const ctx = React.useContext(SubmissionReactContext)
        const dashboard = useDashboard()

        const warningLabelStyle = { color: 'orange' }
        const missingLabelStyle = { color: 'lightcoral' }
        const notInAudienceLabelStyle = { color: 'gray' }

        const isForParty = (asset: AssetParameter): boolean => {
            const type = asset.value.type
            const instance = asset.value.liveData?.instance
            return (party && instance) ? type === requirementType ? requirements.isForParty(party, instance) :
                type === productType ? products.isForParty(party, instance) : true : true

        }

        const allRowsInRowSection = rowSectionContext ? allRows(rowSectionContext).get() : []
        const idx = allRowsInRowSection.findIndex(row => component.children.some(innerrow => innerrow.id === row.id))
        const top = (idx === -1 ? 0 : idx) * -1

        const hasWarnings = parameters.filter(isForParty).map(p => p.value.liveData?.submission?.lifecycle.state === 'submitted').some(p => p === false)
        const hasMissing = parameters.filter(isForParty).map(p => p.value.liveData?.submission?.lifecycle.state === 'missing').some(p => p === true)
        const isApplicable = parameters.map(isForParty).some(p => p === true)

        const linksToAssetsSubmissions = () =>  parameters.map(p => {

            // this is always live
            const { asset, submission } = p.value.liveData!

            const type = p.value.type

            const baseroute = dashboard.partyRouteToSubmissionWith(type, asset.id, ctx.party.source)

            const linkTo = submission ? `${baseroute}/${submission.id}` : baseroute

            const assessedAsMissing = p.value.liveData?.submission?.lifecycle.state === 'missing'

            const missingTarget = missing.includes(p.id)

            const inAudience = isForParty(p)

            const titleStyle = !inAudience ? notInAudienceLabelStyle : p.value.liveData?.submission?.lifecycle.state === 'submitted' ? undefined : (missingTarget || assessedAsMissing) ? missingLabelStyle : warningLabelStyle

            const tip = !inAudience ? () => t("submission.labels.not_applicable_badge") : missingTarget ? () => t("layout.components.asset.missing_target_warning") : assessedAsMissing ? () => t("layout.components.asset.assessed_missing_target") : undefined

            return type === requirementType ? <RequirementLabel key={p.id}
                linkTarget={submissionType}
                titleStyle={titleStyle}
                icon={p.value.liveData?.submission?.lifecycle.state === 'submitted' ? submissionIcon : !inAudience ? { ...requirementIcon, props: { ...requirementIcon.props, style: { ...requirementIcon.props.style ?? {}, ...notInAudienceLabelStyle } } } : icns.unknown}
                requirement={asset.id}
                tip={tip}
                noLink={!inAudience || missingTarget}
                linkTo={linkTo} />
                :
                <ProductLabel key={p.id}
                    linkTarget={submissionType}
                    titleStyle={titleStyle}
                    icon={p.value.liveData?.submission?.lifecycle.state === 'submitted' ? submissionIcon : !inAudience ? { ...productIcon, props: { ...productIcon.props, style: { ...productIcon.props.style ?? {}, ...notInAudienceLabelStyle } } } : icns.unknown}
                    product={asset.id}
                    tip={tip}
                    noLink={!inAudience || missingTarget}
                    linkTo={linkTo} />
        })

        const containerRef = React.useRef<HTMLDivElement>(null);

        const wrapperIcon = hasWarnings ? icns.edit : requirementIcon

        const warningClass = !isApplicable ? 'section-notapplicable-submission' : hasWarnings ? 'section-missing-submission' : ''
        const missingClass = missing.length > 0 || hasMissing? 'section-missing-target' : ''

        const filterStyles = top < 1 ? ['paddingBottom', 'paddingTop'] : []

        // heuristic: asset sections are often used to wrap single rows, 
        // in this case we render them eagerly because rows are already lazy (why? because likely to occur in multiple numbers).
        const renderEagerly = false
        return <ParameterContext.Provider value={parameters}>
            <StyledBox component={component} style={{ top }} filterStyle={filterStyles}>

                <div style={{ ...style, top }} className={`requirement-section-contents ${className} ${missingClass} ${warningClass}`} ref={containerRef} >

                    <span className="section-requirement-placeholder" >

                        <Tooltip
                            overlayStyle={{ minWidth: 'fit-content' }}
                            placement="left"
                            title={linksToAssetsSubmissions}
                            getPopupContainer={() => containerRef.current!}
                            autoAdjustOverflow={false}

                        >{wrapperIcon}</Tooltip>

                    </span>

                    {component.children.map(child => {

                        const { Render } = componentSpecOf(child)

                        return renderEagerly ?

                            <Render component={child} key={child.id} />

                            :

                            <LayoutScrollSubscriber key={child.id} component={child}>
                                <Render component={child} />
                            </LayoutScrollSubscriber>
                    })
                    }

                </div>
            </StyledBox>
        </ParameterContext.Provider>
}

Render.displayName = 'AssetSectionRender'


function Detail(props: DetailProps<AssetSection>) {

    const { domainContext } = useLayout().layoutConfig

    const source = useLayout().getState()['for'] ?? '-------'

    const { l } = useLocale()
    const { t } = useTranslation()
    const { allSorted: allRequirementSorted, fullFetch: reqFullFetch, lookup } = useRequirements()
    const { allSorted: allProductSorted, fullFetch: prodFullFetch } = useProducts()

    const { component, onChange } = props

    const assets = component.assets

    const { targets } = useSectionParameters(component)

    const isEmpty = index => !assets[index]?.alias || assets[index].alias.length === 0
    const isDuplicate = index => !isEmpty(index) && assets.find((r, j) => j !== index && assets[index]?.alias === r?.alias)
    const validation = index => {

        const filled = !!sectionAssetOf(assets[index])
        const empty = filled && isEmpty(index)
        const duplicate = filled && isDuplicate(index)

        return {

            status: empty ? 'warning' : duplicate ? 'error' : 'success',
            msg: empty ? t("layout.components.asset.empty_alias_warning") :
                duplicate ? t("layout.components.asset.duplicate_alias_error") :
                    t("layout.components.asset.full_alias")

        } as Validation

    }

    type AssetMerge = (Requirement | Product) & { type: string }

    const options = [...allRequirementSorted().map(r => ({ ...r, type: requirementType })), ...allProductSorted().map(p => ({ ...p, type: productType }))].filter(asset => !assets.find(a => sectionAssetOf(a) === asset.id)).filter(asset => asset.id !== source)


    const isRequirement = (asset) => typeof asset === 'string' ? lookup(asset) !== undefined ? true : false : asset.type === requirementType
    const idOf = (asset) => typeof asset === 'string' ? asset : asset.id

    const element = (index: number) => {

        const placeholder = targets[index]?.name.en?.replace(/\s/g, '-').toLowerCase() ?? t("layout.components.asset.alias_placeholder")

        return <div key={index} className="requirement-section-element">
            <div>
                <ContextAwareSelectBox placeholder={t("layout.components.asset.select_placeholder")} light

                    currentContext={domainContext}
                    options={options}

                    onChange={(asset: AssetMerge) => asset.type === requirementType ?

                        reqFullFetch(asset as Requirement).then(fetched => onChange({
                            ...component,
                            assets: fetched ?
                                assets.map((req, i) => i === index ? { ...req, asset: fetched.id, type: requirementType } : req)
                                : assets.filter((_, i) => i !== index)
                        }))

                        : prodFullFetch(asset as Product).then(fetched => onChange({
                            ...component,
                            assets: fetched ? assets.map((prod, i) => i === index ? { ...prod, asset: fetched.id, type: productType } : prod) : assets.filter((_, i) => i !== index)
                        }))}

                    renderOption={(asset: AssetMerge) => isRequirement(asset) ? <RequirementLabel requirement={idOf(asset)} noLink /> : <ProductLabel product={idOf(asset)} noLink />}

                    lblTxt={asset => l(asset.name)}
                    optionId={asset => ({ id: asset.id })}>

                    {targets[index] ? [targets[index]?.id] : undefined}

                </ContextAwareSelectBox>
            </div>
            <TextBox validation={validation(index)} placeholder={placeholder} disabled={!sectionAssetOf(assets[index])}
                onChange={alias => onChange({
                    ...component,
                    assets: assets.map((r, i) => i === index ? { ...assets[index], alias } : r)
                })}>
                {assets[index]?.alias}
            </TextBox>
        </div>
    }



    const addIf = options.length > 0
    const onAdd = () => onChange({ ...component, assets: [...assets, { id: shortid(), asset: null!, alias: undefined!, type: undefined! }] })

    const onRemove = index => onChange({ ...component, assets: component.assets.filter((_, i) => i !== index) })


    return <>
        <CardlistBox id={`asset-${component.id}`} singleLabel={t("asset.singular")} addIf={addIf} onAdd={onAdd} onRemove={onRemove} >
            {assets.map((_, i) => <div key={i}>{element(i)}</div>)}
        </CardlistBox>
    </>

}


const makeParameterFor = (id: string, type: string, asset: Requirement | Product, alias: string): AssetParameter => ({

    ...newParameterFrom(assetspec),
    bridgeMode: true,
    fixedValue: true,
    id,
    name: alias ?? asset.name.en?.replace(/\s/g, '-').toLowerCase(),
    components: [],
    data: undefined!,
    value: { id: asset.id, type }

})

const flatten = (c: Component): Component[] => isContainer(c) ? c.children.flatMap(flatten) : [c]





/* 
    synthesises parameters from the requirements in scope, 
    including metadata exported by _named_ components in the requirements.

    returns the requirements and the synthesised parameters.

*/
export const useSectionParameters = (section: AssetSection) => {

    const requirements = useRequirements()
    const products = useProducts()
    const componentReqs = section.assets
    const state = useLayoutState()

    return React.useMemo(() => {

        // resolves the requirements in this section and pairs them with their aliases.
        const targets = section.assets?.filter(t => t.id !== undefined && t.asset !== null).map(t => {
            const asset = t.type === requirementType ? requirements.safeLookup(sectionAssetOf(t)) : products.safeLookup(sectionAssetOf(t))
            return { id: t.id, asset, alias: t.alias, type: t.type }
        })

        // creates parameters for each requirement adding the data exported by their named components.
        const parameters = targets?.map(({ id, asset, alias, type }) => {

            const param = makeParameterFor(id, type, asset, alias)

            // collects all the named components with their specifications specifications.
            const components = flatten(asset.properties.layout.components).filter(r => !!r.shortname)

            // collects data from each component and indexes it by component name.
            const data = components.reduce((acc, c) => ({ ...acc, [nameOf(c)]: layoutRegistry().lookupComponent(c.spec).staticDataOf(c, state) }), {})

            return { ...param, components, data }
        })

        return { targets: targets?.map(t => t.asset), parameters }


    }

        //eslint-disable-next-line
        , [componentReqs])


}


/* 

    synthesises parameters from the requirements in scope, including any data exported by their components.
    
    returns the requirements AND the synthesised parameters.

*/
export const useLiveSectionParameters = (component: AssetSection) => {

    const campaign = useCurrentCampaign()
    const requirements = useRequirements()
    const products = useProducts()
    
    const reqinsts = useRequirementInstances(campaign).all()
    const prodinst = useProductInstances(campaign).all()

    const instances = React.useMemo(() => [...reqinsts, ...prodinst], [reqinsts, prodinst])
    
    const ctx = React.useContext(SubmissionReactContext) 
    
    const state = useLayoutState()
    
    const language = layoutsettingsapi(state).resolveLanguage()
    const componentAssets = component.assets
    
    return React.useMemo(() => {

        // resolves the requirements in this section and pairs them with their aliases.
        const targets =  component.assets.filter(t => sectionAssetOf(t) !== undefined).map(t => ({
            id: t.id,
            asset: t.type === requirementType ? requirements.safeLookup(sectionAssetOf(t)) : products.safeLookup(sectionAssetOf(t)),
            alias: t.alias,
            type: t.type,
            missing: !instances.some(i => i.source === t.asset)
        }))

        // creates parameters for each requirement adding to the data the current state of their named components.
        const parameters: AssetParameter[] = targets.map(({ id, asset, alias, type }) => {

            const param = makeParameterFor(id, type, asset, alias)

            param.value.liveData = { asset } as AssetLiveData

            const value = assetspec.valueFor(ctx, param, state as any)

            // collects all the named components with their specifications specifications.
            const components = flatten(asset.properties.layout.components).filter(r => !!r.shortname)

            // all the parameters of the target requirement, so they can be passed to components to resolve the data.
            // live parameters from the requirement need to be initialised to this context.
            const assetparameters = asset.properties.layout.parameters.map(p => {

                const spec = layoutRegistry().lookupParameter(p.spec)

                const liveParameterValue = isLiveParameter(spec) ? spec.valueFor({ ...ctx, submission: value.liveData?.submission! }, p, state as any) : p.value

                const resolvedvalue = value.liveData?.instance?.properties?.parameterOverlay?.[p.name]?.value ?? liveParameterValue

                const initialised = { ...p, value: resolvedvalue }

                return initialised

            })

            // asks each component to derive data from their state in the current submission, and indexes it by component name.
            const data = {}

            components.reduce((acc, c) => {

                acc[nameOf(c)] = value.liveData?.submission ?

                    layoutRegistry().lookupComponent(c.spec).dynamicDataOf(c,
                        {
                            data: value.liveData.submission.content.data[c.id],
                            resources: value.liveData.submission.content.resources[c.id]
                        },

                        assetparameters,
                        state) : undefined

                return acc

            }, data)

            return { ...param, value, components, data }
        })


        return { targets: targets.map(t => t.asset), missing: targets.filter(t => t.missing).map(t => t.id), parameters }


    }

        //eslint-disable-next-line
        , [componentAssets, language])
}

function Print(props: PrintProps<AssetSection>) {

    // const { componentSpecOf, allRows } = useLayout()

    const { componentSpecOf } = useLayout()

    const { component, wrap = true, View } = props
    // const rowSectionContext = useRowSectionContext()

    const { parameters } = useLiveSectionParameters(component)

    // const allRowsInRowSection = rowSectionContext ? allRows(rowSectionContext).get() : []
    // const idx = allRowsInRowSection.findIndex(row => component.children.some(innerrow => innerrow.id === row.id))
    // const top = (idx === -1 ? 0 : idx) * -1

    const marginTop = -1

    return <ParameterContext.Provider value={parameters}>
        <StyledView {...props} component={component} style={{ marginTop }}>
            <View style={{ marginTop }} wrap={wrap}>
                {
                    component.children.map((child, position) => {

                        const { Print } = componentSpecOf(child)

                        return <Print {...props} key={position} component={child} />

                    })
                }
            </View>
        </StyledView>
    </ParameterContext.Provider>

}


function PrintPreview(props: PrintProps<AssetSection>) {
    // const { componentSpecOf, allRows } = useLayout()
    const { componentSpecOf } = useLayout()

    const { component, View } = props
    // const rowSectionContext = useRowSectionContext()

    const { parameters } = useSectionParameters(component)

    // const allRowsInRowSection = rowSectionContext ? allRows(rowSectionContext).get() : []
    // const idx = allRowsInRowSection.findIndex(row => component.children.some(innerrow => innerrow.id === row.id))
    // const top = (idx === -1 ? 0 : idx) * -1

    return <ParameterContext.Provider value={parameters}>
        <StyledView {...props} component={component} style={{ marginTop: -1 }}>
            <View style={{ marginTop: -1 }} >
                {
                    component.children.map((child, position) => {

                        const { PrintPreview } = componentSpecOf(child)

                        return <PrintPreview  {...props} key={position} component={child} />

                    })
                }
            </View>
        </StyledView>
    </ParameterContext.Provider>

}