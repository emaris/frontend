import { Tooltip } from "antd"
import { HtmlSnippet } from "apprise-frontend/components/HtmlSnippet"
import { SelectBox } from "apprise-frontend/form/SelectBox"
import { Switch } from "apprise-frontend/form/Switch"
import { icns } from "apprise-frontend/icons"
import { intlapi } from "apprise-frontend/intl/api"
import { noDataChange } from 'apprise-frontend/layout/components/api'
import { DetailProps } from 'apprise-frontend/layout/components/ComponentDetail'
import { ComponentDraggable, ComponentDroppable } from 'apprise-frontend/layout/components/ComponentDnD'
import { ComponentPreview } from 'apprise-frontend/layout/components/ComponentPreview'
import { FieldProps, partialField } from 'apprise-frontend/layout/components/FieldDetail'
import { useFieldHelper } from 'apprise-frontend/layout/components/FieldHelper'
import { specOf } from 'apprise-frontend/layout/components/helper'
import { Component, newComponentFrom, PreviewProps, PrintProps, RenderProps } from 'apprise-frontend/layout/components/model'
import { useLayout } from 'apprise-frontend/layout/LayoutProvider'
import { ParametricMultiTextBox } from 'apprise-frontend/layout/parameters/ParametricBoxes'
import StyledView from 'apprise-frontend/layout/pdf/StyledView'
import { usePdfStyles } from 'apprise-frontend/layout/pdf/stylehook'
import { layoutsettingsapi } from "apprise-frontend/layout/settings/api"
import { alignmentspec } from 'apprise-frontend/layout/style/Alignment'
import { borderspec } from 'apprise-frontend/layout/style/Border'
import { heightspec } from 'apprise-frontend/layout/style/Height'
import { StyleProps } from 'apprise-frontend/layout/style/model'
import { spacingspec } from 'apprise-frontend/layout/style/Spacing'
import { StyledBox } from 'apprise-frontend/layout/style/StyledBox'
import { widthspec } from 'apprise-frontend/layout/style/Width'
import { MultilangDto } from "apprise-frontend/model/multilang"
import { ReadOnlyContext } from "apprise-frontend/scaffold/ReadOnly"
import { BaseState } from "apprise-frontend/state/model"
import { useBytestreams } from "apprise-frontend/stream/api"
import { FileTypeInfo, fileTypeInfo } from "apprise-frontend/stream/constants"
import { FileBox, FileBoxProps } from "apprise-frontend/stream/FileBox"
import { Bytestream } from 'apprise-frontend/stream/model'
import { timeapi } from "apprise-frontend/time/api"
import { getHostname } from "apprise-frontend/utils/common"
import { useSubmissionContext } from 'emaris-frontend/campaign/submission/hooks'
import moment from "moment"
import * as React from "react"
import { useTranslation } from "react-i18next"

/*
The SubmissionFileDrop is an extension of the file drop to keep track of the uploaded files within the submissions
When a new submissions is created users can delete files uploaded on previous submissions.
This component keeps track of them and associate uploaded files to their submissions.
*/

export type SubmissionFileDrop = Component & FieldProps & StyleProps & {

    placeholder?: MultilangDto | undefined
    // required: boolean
    mode: FileBoxProps['mode']
    allowedMimeTypes: FileTypeInfo[]
}



export type UploadTimestamp = {
    timestamp: number
    submission: string
}

export const submissionfiledropspec = specOf({

    id: 'submissionfiledrop',
    icon: icns.file,
    name: 'layout.components.filedrop.name',


    styles: () => ({

        [widthspec.id]: {},
        [heightspec.id]: {},
        [borderspec.id]: {},
        [alignmentspec.id]: {},
        [spacingspec.id]: {}

    })

    ,

    generate: (): SubmissionFileDrop => ({
        ...newComponentFrom(submissionfiledropspec),
        ...partialField(),
        style: {},
        // required: false,
        mode: "multi",
        allowedMimeTypes: []
    })

/*     ,

    useValidation: (input: SubmissionFileDrop) => {

        const { t } = useTranslation()

        const { ll, r, resourcesFor, isInstance } = useLayout()

        const statusmsg = input.msg ? r(ll(input.msg)) : undefined

        const resources = resourcesFor(input) ?? []

        return isInstance() && input.required ? check(resources).with(empty(t)).nowOr(statusmsg) : { msg: statusmsg }

    }
 */
    ,

    staticDataOf: (_, s: BaseState) => ({ submitted: undefined!, submissionDate: undefined!, files: undefined! })

    ,

    dynamicDataOf: (_, componentstate, __, state) => {

        // const layout = layoutapi(state)

        const hostname = getHostname()
        // const t = state.language ? intlapi(state).getFixedT(state.language) : intlapi(state).getT()
        const language = layoutsettingsapi(state).resolveLanguage()
        const t = intlapi(state).getFixedT(language)

        const uploads = componentstate.data as UploadTimestamp[]
        const uploadNoEmpty = componentstate.resources?.length

        const firstUpload = uploadNoEmpty ? uploads?.[0] : undefined

        const submissionDate = firstUpload ? timeapi(state).format(moment(firstUpload.timestamp), 'long', language) : undefined
        const submitted = (!!componentstate.data && componentstate.resources?.length > 0) ? t("common.labels.yes") : t("common.labels.no")

        const files = componentstate.resources?.length > 0 ? componentstate.resources.map(r => `<a href="${hostname}/domain/stream/${r.id}">${r.name}</a>`).join("&nbsp;&nbsp;") : undefined

        return { submitted, submissionDate, files }

    }

    ,

    Render

    ,

    Detail

    ,

    Preview

    ,

    Print

})


function Preview(props: PreviewProps<SubmissionFileDrop>) {

    const { component, path } = props

    const layout = useLayout()

    const bytestreams = useBytestreams()
    const resources = layout.resourcesFor(component) ?? []
    const descriptors = layout.descriptorsFor(component) ?? []

    const onDrop = files => layout.addResourcesFor(component, files)
    const onRemove = (stream: Bytestream) => layout.removeResourcesFor(component, [stream])
    const onRender = (stream: Bytestream) => {

        const match = descriptors.find(d => d.resource === stream.id)

        return <a download href={match ? URL.createObjectURL(match.file) : bytestreams.linkOf(stream.id)}>{stream.name}</a>
    }

    return <ComponentDroppable path={path}>
        <ComponentDraggable path={path} component={component}>
            <ComponentPreview path={path} component={component} >
                <FileDropRender {...props} resources={resources} onDrop={onDrop} onRender={onRender} onRemove={onRemove} />
            </ComponentPreview>
        </ComponentDraggable>
    </ComponentDroppable>


}


function Render(props: RenderProps<SubmissionFileDrop>) {

    const { component } = props

    const layout = useLayout()
    const { submission } = useSubmissionContext()

    const bytestreams = useBytestreams()
    const resources = layout.resourcesFor(component) ?? []
    const descriptors = layout.descriptorsFor(component) ?? []

    // the current value in state is a list of timestamps.
    const timestamps: UploadTimestamp[] = layout.dataFor(component) ?? []

    // note: if submission hasn't been saved, id null still. doesn't change the logic in practice.
    const isOwned = (ts: UploadTimestamp) => ts.submission === submission.id
    const isOwnedResource = (r: Bytestream) => r.ref === submission.id

    // the timestamp that corresponds to this submission (should be the last inserted).
    // if this submission hasn't been saved, the timestamp will be unassociated.
    const currentTimestamp = timestamps.find(isOwned)

    // the timestamp we add next if the upload changes.
    // if this submission hasn't been saved, the id is null and the timestamp will be unassociated.
    const nextTimestamp: UploadTimestamp = { timestamp: Date.now(), submission: submission.id }

    //console.log("data",timestamps,"resources",resources)

    // adds new resources, and timestamps the upload for this submission if it hasn't already.
    const onDrop = (files: File[]) => {

        const newtimestamps = currentTimestamp ? noDataChange : [...timestamps, nextTimestamp]

        layout.addResourcesAndDataFor(component, layout.resourcesAndDescriptorsFor(files), newtimestamps)
    }

    // removes a resource, and if there are no other resources owned by this subission,
    // it removes the timestamp on the entire upload too..
    const onRemove = (stream: Bytestream) => {

        const owned = resources.filter(r => isOwnedResource(r) && r.id !== stream.id)

        let data

        // some owned left, and no current timestamp, add it now.
        if (owned.length > 0 && !currentTimestamp)
            data = [...timestamps, nextTimestamp]

        // no uploads left, remove existing timestamp.
        else if (owned.length === 0 && currentTimestamp)
            data = timestamps.filter(t => !isOwned(t))

        layout.removeResourcesAndDataFor(component, [stream], data)

    }

    const onRender = (stream: Bytestream) => {

        const match = descriptors.find(d => d.resource === stream.id)

        const className = stream.ref !== submission.id ? 'link-faded' : ''

        return <span className={className}><a download href={match ? URL.createObjectURL(match.file) : bytestreams.linkOf(stream.id)}>{stream.name}</a></span>

    }

    return <FileDropRender {...props} resources={resources} onDrop={onDrop} onRender={onRender} onRemove={onRemove} />

}

type FullProps = RenderProps<SubmissionFileDrop> & {

    resources: Bytestream[]
    onRemove: (_: Bytestream) => any
    onRender: (_: Bytestream) => any
    onDrop: (_: File[]) => any

}


function FileDropRender(props: FullProps) {

    const { component, resources, onRemove, onRender, onDrop } = props

    const layout = useLayout()

    const language = layoutsettingsapi(layout.getState()).resolveLanguage()

    const { ll, r } = layout

    const { readonly, readOnlyMode, enabled } = useFieldHelper(component)

    const { mode, placeholder, label } = component

    const dragMsg = placeholder && r(ll(placeholder))
    const lbl = label && <HtmlSnippet snippet={r(ll(label))} />

    const statusmsg = component.msg ? r(ll(component.msg)) : undefined
    const validation = {msg: statusmsg}

    // const validation = submissionfiledropspec.useValidation!(component)

    const readonlyContext = React.useContext(ReadOnlyContext)

    return <StyledBox component={component}>
        <FileBox disabled={!enabled} label={lbl} 
            validation={validation} 
            mode={mode}
            resources={resources}
            language={language}
            onRemove={onRemove}
            showJustFiles={enabled ? false : readonlyContext || (readonly && readOnlyMode === 'content')}
            render={onRender}
            dragMsg={dragMsg}
            onDrop={onDrop}
            allowedMimeTypes={component.allowedMimeTypes} />
    </StyledBox>

}

function Detail(props: DetailProps<SubmissionFileDrop>) {

    const { t } = useTranslation()

    const { component, onChange } = props

    const { mode, /* required, */ placeholder } = component

    return <>

        {/* <Switch label={t("layout.components.filedrop.required.name")} onChange={v => onChange({ ...component, required: v })}
            validation={{ msg: t("layout.components.filedrop.required.msg") }}>
            {!!required}
        </Switch> */}


        <Switch label={t("layout.components.filedrop.multi.name")} onChange={v => onChange({ ...component, mode: v ? "multi" : 'single' })}
            validation={{ msg: t("layout.components.filedrop.multi.msg") }}>
            {mode === 'multi'}
        </Switch>

        <ParametricMultiTextBox label={t("layout.components.fields.placeholder.name")} id={`uploadbox-placeholder-${component.id}`} onChange={v => onChange({ ...component, placeholder: v })}
            validation={{ msg: t("layout.components.fields.placeholder.msg") }}>
            {placeholder}
        </ParametricMultiTextBox>

        <SelectBox
            label={t("layout.components.filedrop.type.name")}
            getlbl={m => <FileTypeLabel name={t(m.name)} tooltip={t(m.description)} />}
            selectedKey={component.allowedMimeTypes && component.allowedMimeTypes.map(m => m.name)}
            getkey={m => m.name}
            onChange={a => onChange({ ...component, allowedMimeTypes: a ? a.map(m => fileTypeInfo.filter(ft => ft.name === m)[0]) : [] })}
            validation={{ msg: t("layout.components.filedrop.type.msg") }}>
            {fileTypeInfo}
        </SelectBox>
    </>
}

const FileTypeLabel = (props: { name: string, tooltip: string }) => {
    const { name, tooltip } = props

    return <Tooltip title={tooltip}><span>{name}</span></Tooltip>
}


function Print(props: PrintProps<SubmissionFileDrop>) {

    const { t } = useTranslation()

    const { component, wrap = true, View, Link, Text, StyleSheet } = props

    const layout = useLayout()

    const bytestreams = useBytestreams()
    const resources = layout.resourcesFor(component) ?? []

    const fileStyle = StyleSheet.create(usePdfStyles(component, 'fileDrop'))
    const noFileStyle = StyleSheet.create(usePdfStyles(component, 'fileDropMissing'))

    return <StyledView {...props} component={component}>

        <View style={fileStyle} wrap={wrap}>

            {
                resources.length === 0 ? <Text style={noFileStyle}>{t("layout.components.filedrop.no_files")}</Text> :
                    resources.map((stream, i) => {
                        const hostname = getHostname()
                        const link = `${hostname}${bytestreams.linkOf(stream.id)}`

                        return <Link key={i} src={link}>{stream.name}</Link>
                    })
            }

        </View>
    </StyledView>

}



