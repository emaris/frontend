import { askReloadOnPushChange, askReloadOnPushRemove } from 'apprise-frontend/push/constants';
import { Subscription } from 'apprise-frontend/push/model';
import { PushEventSlot } from 'apprise-frontend/push/module';
import { tenantRoute, tenantSingular, tenantType } from 'apprise-frontend/tenant/constants';
import { Tenant } from 'apprise-frontend/tenant/model';
import { tenantstateapi } from 'apprise-frontend/tenant/state';
import { userapi } from 'apprise-frontend/user/api';
import { Campaign } from 'emaris-frontend/campaign/model';
import { dashboardapi } from 'emaris-frontend/dashboard/api';
import { dashboardRoute } from 'emaris-frontend/dashboard/constants';
import { productSingular } from 'emaris-frontend/product/constants';
import { requirementSingular, requirementType } from 'emaris-frontend/requirement/constants';
import { EmarisState } from 'emaris-frontend/state/model';
import { unstable_batchedUpdates } from 'react-dom';
import shortid from 'shortid';
import { campaignRoute, campaignSingular } from './constants';
import { CampaignInstance } from './model';
import { userpreferencesapi } from './preferences/api';
import { campaignstateapi } from './state';
import { Trail } from './submission/model';

export type CampaignChangeEvent = {

    campaign: Campaign
    type: 'add' | 'remove' | 'change'

    instances: CampaignInstance[]
    instanceType: string

    trails: Trail[]
}



export type TenantChangeEvent = {

    tenant: Tenant
    type: 'add' | 'remove' | 'change'

}


export const pushCampaignSlot: PushEventSlot<EmarisState, any> = {

    onSubcribe: () => {

        console.log("subscribing for campaign and tenant changes...")

        return [

            {

                topics: ['campaign']

                ,

                onEvent: (event: CampaignChangeEvent, state: EmarisState, history, location) => {

                    const { campaign, type, instances } = event

                    console.log(`received ${type} event for campaign ${campaign.name.en}...`, { event })

                    if (instances.length > 0)
                        return onInstanceChange(event, state, history, location)

                    return onCampaignEvent(event, state, history, location)
                }

                ,



            },

            {

                topics: [tenantType]

                ,

                onEvent: (event: TenantChangeEvent, s: EmarisState, history, location) => {

                    const { tenant, type } = event

                    const currentUrl = `${location.pathname}${location.search}`

                    console.log(`received ${type} event for tenant ${tenant.name.en}...`, { event })

                    const tenants = tenantstateapi(s)
                    const campaigns = campaignstateapi(s)

                    const detailOrDashBordOrTrailOnScreen = currentUrl.includes(tenant.id)
                    const detailOnScreen = detailOrDashBordOrTrailOnScreen && !currentUrl.includes(dashboardRoute)

                    let campaignArchived = false

                    if (detailOrDashBordOrTrailOnScreen) {
                        const campaignId = location.pathname.split("/")[2]
                        const campaign = campaigns.lookup(campaignId)
                        campaignArchived = campaignArchived || campaign?.lifecycle.state === 'archived'
                    }

                    switch (type) {

                        case 'add': {

                            tenants.setAll([...tenants.all(), tenant])

                            break
                        }


                        case 'change': {

                            const change = () => tenants.setAll(tenants.all().map(c => c.id === tenant.id ? tenant : c))
                            const changeAndRefresh = () => { change(); history.push(currentUrl, shortid()) } // uses location state to send down a signal for remount.

                            // note: we don't expect abrupt changes in trails, so we don't warn there.
                            if (detailOnScreen && !campaignArchived)
                                askReloadOnPushChange(s, { singular: tenantSingular, onCancel: change, onOk: changeAndRefresh })

                            else change()

                            break
                        }


                        case 'remove': {

                            // when designing, we go back to list. in dshboard we go back to summary current campaign.
                            const fallbackRoute = detailOnScreen ? tenantRoute : dashboardRoute

                            const remove = () => tenants.setAll(tenants.all().filter(r => r.id !== tenant.id))
                            const leaveAndRemove = () => { history.push(fallbackRoute); remove() }

                            if (detailOrDashBordOrTrailOnScreen && !campaignArchived)
                                askReloadOnPushRemove(s, { singular: tenantSingular, onOk: leaveAndRemove })

                            else remove()

                            break
                        }

                    }

                }

            }



        ]
    }
}

const onInstanceChange: Subscription<EmarisState, CampaignChangeEvent>['onEvent'] = (event: CampaignChangeEvent, s: EmarisState, history, location) => {

    const { campaign, type, instances } = event

    const pathname = location.pathname
    const currentUrl = `${pathname}${location.search}`

    const campaigns = campaignstateapi(s)
    const dashboard = dashboardapi(s)

    const users = userapi(s)
    const userpreferences = userpreferencesapi(s)

    const logged = users.logged

    const campaignDashboardRoute = dashboard.routeTo(campaign)

    let singular: string

    switch (instances[0].instanceType) {

        case tenantType: singular = tenantSingular; break;
        case requirementType: singular = requirementSingular; break;
        default: singular = productSingular; break;
    }

    const detailOnScreen = currentUrl.includes(campaigns.routeTo(campaign)) && instances.some(i => currentUrl.includes(i.id))
    const dashboardOrTrailOnScreen = currentUrl.includes(dashboard.routeTo(campaign)) && instances.some(i => currentUrl.includes(i.source))

    switch (type) {

        case 'change': {


            // we shouldn't add a campaign until the tenant's party is in it.
            if (logged.isTenantUser() && event.instanceType === tenantType) {

                // buffer the updates required to add the campaign and load its data in the background.
                // then flushes the buffer for a single re-render.
                const updates: (() => void)[] = []

                const tenantInstance = event.instances.find(i => i.source === logged.tenant)

                if (tenantInstance) {

                    updates.push(() => campaigns.setAll([...campaigns.all(), campaign]))

                    campaigns.fetchManyQuietly([campaign], false, updates).then(() =>

                        unstable_batchedUpdates(() => updates.forEach(f => f()))

                    )

                    return

                }

            }

            const change = () => campaigns.livePush(event)
            const changeAndRefresh = () => { change(); history.push(currentUrl, shortid()) }

            // change applies to instances.
            if (detailOnScreen)
                askReloadOnPushChange(s, { singular, onOk: changeAndRefresh })

            else change()

            break
        }

        case 'remove': {

            // in design, just close the instance drawer (remove query and keep pathname) 
            // in dashboard, go back to campaign dashboard.
            const fallbackRoute = detailOnScreen ? pathname : campaignDashboardRoute

            const removeWholeCampaign = logged.isTenantUser() && event.instanceType === tenantType

            const remove = () => campaigns.livePush(event)

            const leaveAndRemove = () => {

                if (removeWholeCampaign) {

                    userpreferences.setLastVisitedCampaign(logged, undefined!)
                        .then(() => campaigns.setAll(campaigns.all().filter(c => c.id !== campaign.id)))
                        .then(() => history.push(dashboardRoute))

                }
                else {

                    history.push(fallbackRoute)

                    remove()
                }
            }

            // remove applies to instances.
            if (detailOnScreen || dashboardOrTrailOnScreen)
                askReloadOnPushRemove(s, { singular, onOk: leaveAndRemove })

            else remove()

            break
        }

    }

}


const onCampaignEvent: Subscription<EmarisState, CampaignChangeEvent>['onEvent'] = (event: CampaignChangeEvent, s: EmarisState, history, location) => {

    const { campaign, type } = event

    const currentUrl = `${location.pathname}${location.search}`

    const campaigns = campaignstateapi(s)
    const dashboard = dashboardapi(s)

    const users = userapi(s)
    const userpreferences = userpreferencesapi(s)

    const detailOnScreen = currentUrl.includes(campaigns.routeTo(campaign))
    const dashboardOnScreen = currentUrl.includes(dashboard.routeTo(campaign))


    switch (type) {

        case 'add': {

            // buffer the updates required to add the campaign and load its data in the background.
            // then flushes the buffer for a single re-render.
            const updates: (() => void)[] = []

            const logged = users.logged

            // we shouldn't add a campaign until the tenant's party is in it.
            if (logged.isTenantUser())
                return

            updates.push(() => campaigns.setAll([...campaigns.all(), campaign]))

            campaigns.fetchManyQuietly([campaign], false, updates).then(() =>

                unstable_batchedUpdates(() => updates.forEach(f => f()))

            )

            break
        }

        case 'change': {

            const change = () => campaigns.setAll(campaigns.all().map(c => c.id === campaign.id ? campaign : c))
            const changeAndRefresh = () => { change(); history.push(currentUrl, shortid()) } // uses location state to send down a signal for remount.

            if (detailOnScreen)
                askReloadOnPushChange(s, { singular: campaignSingular, onCancel: change, onOk: changeAndRefresh })

            else change()

            break
        }

        case 'remove': {

            const fallbackRoute = detailOnScreen ? campaignRoute : dashboardRoute

            const remove = () => {

                const logged = users.logged

                if (dashboard.defaultCampaign()?.id === campaign.id)
                    userpreferences.setLastVisitedCampaign(logged, undefined!)

                campaigns.setAll(campaigns.all().filter(c => c.id !== campaign.id))


            }
            const leaveAndRemove = () => { history.push(fallbackRoute); remove() }

            if (detailOnScreen || dashboardOnScreen)
                askReloadOnPushRemove(s, { singular: campaignSingular, onOk: leaveAndRemove })

            else remove()

            break

        }

    }

}
