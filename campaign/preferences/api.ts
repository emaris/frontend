import { Tenant } from "apprise-frontend/tenant/model"
import { tenantstateapi } from "apprise-frontend/tenant/state"
import { userapi } from "apprise-frontend/user/api"
import { User } from "apprise-frontend/user/model"
import { summaryRoute } from "emaris-frontend/dashboard/ViewRouter"
import { EmarisState } from "emaris-frontend/state/model"
import { ApproveCycle } from "../model"
import { CalendarView, CampaignTenantPreferences, DashboardView, DateLocation, UserPreferences } from "./model"



export const tenantpreferencesapi = (s:EmarisState) => {   

    const prefs = (t:Tenant) =>  t.preferences as any as CampaignTenantPreferences
    const tenantFrom = (id:string) => tenantstateapi(s).safeLookup(id)

    const self = {


        location: (t:Tenant) => prefs(t).location

        ,


        locationFor: (id:string) => self.location(tenantFrom(id))

        ,

        setLocation: (t:Tenant,value:string) => prefs(t).location = value

        ,

        dateLocation: (t:Tenant) => prefs(t).dateLocation || 'home'

        ,

        dateLocationFor: (id:string) => self.dateLocation(tenantFrom(id))

        ,

        setDateLocation: (t:Tenant,value:DateLocation) => {

            if (value==='home' || !value)
             delete prefs(t).dateLocation
    
            else  prefs(t).dateLocation = value

        }

        ,

        approveCycle: (t:Tenant) : ApproveCycle =>  prefs(t).approveCycle ?? 'none'

        ,

        approveCycleFor: (id:string) : ApproveCycle =>  self.approveCycle(tenantFrom(id))

        ,

        setApproveCycle: (t:Tenant,value:ApproveCycle|undefined) => {
            
            if (!value)
                delete prefs(t).approveCycle
            
            else  prefs(t).approveCycle = value
        
        }
    }

    return self;
}



export const userpreferencesapi = (s:EmarisState) => {   


    const prefs = (u:User) =>  (u.details.preferences as UserPreferences)
    const logged = () => userapi(s).logged


    const self = {


        location: (u:User) => prefs(u).location ??  self.defaultLocation(u)

        ,

        defaultLocation: (u:User) => u ? tenantpreferencesapi(s).locationFor(u.tenant) : undefined

        ,

        setLocation: (u:User,value:string) => {

            if (!value || value===self.defaultLocation(u) ) 
             delete prefs(u).location
    
            else  prefs(u).location = value

        }

        ,

        dateLocation: (u:User) => prefs(u).dateLocation ?? self.defaultDateLocation(u)

        ,

        defaultDateLocation: (u:User) => u ?  tenantpreferencesapi(s).dateLocationFor(u.tenant) : undefined 

        ,

        setDateLocation: (u:User,value:DateLocation) => {

            if (!value || value===self.defaultDateLocation(u) ) 
             delete prefs(u).dateLocation
    
            else  prefs(u).dateLocation = value

        }
        
        ,

        dashboardView: (u:User = logged()) : DashboardView => prefs(u).defaultDashboardView ?? self.dashboardViewDefault(),


        dashboardViewDefault: () : DashboardView => summaryRoute

        ,


        setDashboardView : (u:User,value:DashboardView|undefined) => {

            if (!value || value===self.dashboardViewDefault())
                delete prefs(u).defaultDashboardView
    
            else  prefs(u).defaultDashboardView = value

        }

        ,

        lastVisitedCampaign: (u:User=logged()) => prefs(u).lastVisitedCampaign


        ,

        setLastVisitedCampaign : (u:User,value:string) => Promise.resolve(prefs(u).lastVisitedCampaign = value).then(self.updateLoggedUserProfile)

        ,

        calendarView: (u:User=logged()) : CalendarView => prefs(u).defaultCalendarView ?? self.calendarViewDefault()

        , 

        calendarViewDefault: (): CalendarView => 'month'

        ,

        setDefaultCalendarView: (u:User, value:CalendarView|undefined) => Promise.resolve(prefs(u).defaultCalendarView = value).then(self.updateLoggedUserProfile)

        ,

        updateLoggedUserProfile: () => userapi(s).updateUserProfileQuietly(prefs(logged()))

    }

    return self;


}