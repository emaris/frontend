import { Radio } from 'antd'
import { Paragraph } from 'apprise-frontend/components/Typography'
import { FormState } from 'apprise-frontend/form/hooks'
import { RadioBox } from 'apprise-frontend/form/RadioBox'
import { SelectBox } from 'apprise-frontend/form/SelectBox'
import { Tenant } from 'apprise-frontend/tenant/model'
import { ZonePickerBox } from 'apprise-frontend/time/TimeZonePicker'
import * as React from 'react'
import { useTranslation } from 'react-i18next'
import { useCampaigns } from '../api'
import { approveCycleOptions } from '../model'
import { dateLocationOptions } from './model'






// ui

export const TenantPreferencesForm = (formstate: FormState<Tenant>) => {

    const { t } = useTranslation()

    const { edited, change } = formstate

    const campaigns = useCampaigns()

    const report = campaigns.tenantPreferences().validate(formstate)

    const prefs = campaigns.tenantPreferences()


    return <React.Fragment>

        <ZonePickerBox label={t("campaign.fields.location.name")} validation={report.home} 
            includeLocal={false} 
            value={prefs.location(edited)}
            onChange={change(prefs.setLocation)} />

        <SelectBox label={t("campaign.fields.preferred_zone.name")}  validation={report.timezone}
            getlbl={l => t(dateLocationOptions[l])}
            selectedKey={prefs.dateLocation(edited)}
            onChange={change(prefs.setDateLocation)}  >
            {Object.keys(dateLocationOptions)}
        </SelectBox>


        <RadioBox   label={t("campaign.fields.approval.name")}
                    validation={{
                        msg:t("campaign.fields.approval.msg"),
                        help:<React.Fragment>
                                <Paragraph>{t("campaign.fields.approval.help1")}</Paragraph>
                                <Paragraph spaced >{t("campaign.fields.approval.help2_tenants")}</Paragraph>
                             </React.Fragment>}}
                    value={ prefs.approveCycle(edited)}
                    onChange={change( (t,v)=>prefs.setApproveCycle(t,v==='none' ? undefined : v))}>
            
            {
                Object.keys(approveCycleOptions).map(o=>
            
                <Radio key={o} value={o}>{t(approveCycleOptions[o])}.</Radio>)
            
            }
        </RadioBox>

    </React.Fragment>
}

