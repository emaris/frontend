import { FormState } from 'apprise-frontend/form/hooks'
import { SelectBox } from 'apprise-frontend/form/SelectBox'
import { tenantType } from 'apprise-frontend/tenant/constants'
import { ZonePickerBox } from 'apprise-frontend/time/TimeZonePicker'
import { useUsers } from 'apprise-frontend/user/api'
import { User } from 'apprise-frontend/user/model'
import * as React from 'react'
import { useTranslation } from 'react-i18next'
import { useCampaigns } from '../api'
import { calendarOptions, dashboardOptions, dateLocationOptions } from './model'
import { Label } from 'apprise-frontend/components/Label'
import { dashboardIcon } from 'emaris-frontend/dashboard/constants'
import { AiOutlineCalendar, AiOutlineUnorderedList } from 'react-icons/ai'
import { Icon } from 'antd'




export const UserPreferencesForm = (formstate: FormState<User>) => {

    const { t } = useTranslation()

    const { edited, change } = formstate

    const users = useUsers()
    
    const campaigns = useCampaigns()

    const report = campaigns.userPreferences().validate(formstate)

    const prefs = campaigns.userPreferences()

    const managesManyParties = users.logged.managesMultipleTenants()

    const isApplicable = (view:string) => managesManyParties || view!==tenantType
    
    return <React.Fragment>

        <ZonePickerBox label={t("campaign.fields.location.name")} validation={report.home}
            includeLocal={false} 
            value={prefs.location(edited)}
            onChange={change(prefs.setLocation)} />

        <SelectBox label={t("campaign.fields.preferred_zone.name")}
            getlbl={l => t(dateLocationOptions[l])}
            validation={report.timezone}
            selectedKey={prefs.dateLocation(edited)}
            onChange={change(prefs.setDateLocation)}  >
            {
                Object.keys(dateLocationOptions)
            }

        </SelectBox>


        <SelectBox label={t("campaign.fields.default_dashboard_view.name")}
                   lblText={l=>t(dashboardOptions[l])}
                   getlbl={l => <Label icon={dashboardIcon} title={t(dashboardOptions[l])} />}
                   validation={report.defaultDashboardView}
                   selectedKey={prefs.dashboardView(edited)}
                   onChange={change(prefs.setDashboardView)}  >
                    {
                     Object.keys(dashboardOptions).filter(isApplicable)
                    }
        </SelectBox>

        <SelectBox label={t("campaign.fields.default_calendar_view.name")}
                   lblText={l=>t(calendarOptions[l])}
                   getlbl={l => <Label icon={ l === 'month' ? <Icon component={AiOutlineCalendar} /> : <Icon component={AiOutlineUnorderedList} /> } title={t(calendarOptions[l])} />}
                   validation={report.defaultCalendarView}
                   selectedKey={prefs.calendarView(edited)}
                   onChange={change(prefs.setDefaultCalendarView)}  >
                    {
                     Object.keys(calendarOptions).filter(isApplicable)
                    }
        </SelectBox>


    </React.Fragment>

}