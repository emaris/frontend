import { Paragraph } from 'apprise-frontend/components/Typography'
import { FormState } from 'apprise-frontend/form/hooks'
import { intlapi } from 'apprise-frontend/intl/api'
import { Tenant } from 'apprise-frontend/tenant/model'
import { User } from 'apprise-frontend/user/model'
import { check, checkIt, notdefined } from 'apprise-frontend/utils/validation'
import { EmarisState } from 'emaris-frontend/state/model'
import * as React from 'react'


export const  preferencesvalidationapi = (s:EmarisState) => ({


    validateUserPreferences : (_:FormState<User>) => {

        const t = intlapi(s).getT()

        return {

            home: checkIt().nowOr(t("campaign.fields.location.msg"),t("campaign.fields.location.help")),

            timezone: checkIt().nowOr(t("campaign.fields.preferred_zone.msg"),
                                                 <>
                                                    <Paragraph>{t("campaign.fields.preferred_zone.help1")}</Paragraph>
                                                    <Paragraph spaced>{t("campaign.fields.preferred_zone.help2")}</Paragraph>
                                                  </>),

            defaultDashboardView: checkIt().nowOr(t("campaign.fields.default_dashboard_view.msg"),
                                                  <>
                                                    <Paragraph>{t("campaign.fields.default_dashboard_view.help1")}</Paragraph>
                                                    <Paragraph spaced>{t("campaign.fields.default_dashboard_view.help2")}</Paragraph>
                                                  </>),

            defaultCalendarView: checkIt().nowOr(t("campaign.fields.default_calendar_view.msg"),
                                                <>
                                                <Paragraph>{t("campaign.fields.default_calendar_view.help1")}</Paragraph>
                                                <Paragraph spaced>{t("campaign.fields.default_calendar_view.help2")}</Paragraph>
                                                </>),

        }
    }
    
    ,


    validateTenantPreferences : (_:FormState<Tenant>) => {

        const t = intlapi(s).getT()

        return {
            home: check(_.edited.preferences.location).with(notdefined(t)).nowOr(t("campaign.fields.location.msg"),t("campaign.fields.location.help")),
            timezone: checkIt().nowOr(t("campaign.fields.preferred_zone.msg"),
                                                            <>
                                                                <Paragraph>{t("campaign.fields.preferred_zone.help1")}</Paragraph>
                                                                <Paragraph spaced>{t("campaign.fields.preferred_zone.help2")}</Paragraph>
                                                            </>),

                                                            
            approvalCycle: checkIt().nowOr(t("campaign.fields.approval.msg"),
                                                        <>
                                                            <Paragraph>{t("campaign.fields.approval.help1")}</Paragraph>
                                                            <Paragraph spaced>{t("campaign.fields.approval.help2_tenants")}</Paragraph>
                                                        </>)
        }
    }
})