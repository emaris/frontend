import { Placeholder } from "apprise-frontend/components/Placeholder"
import { mailType } from "apprise-frontend/mail/constants"
import { MailSlot } from "apprise-frontend/mail/module"
import { Module } from "apprise-frontend/module"
import { pushEventType } from 'apprise-frontend/push/constants'
import { settingsType } from "apprise-frontend/settings/constants"
import { SettingsSlot } from "apprise-frontend/settings/module"
import { BaseState } from "apprise-frontend/state/model"
import { tagType } from "apprise-frontend/tag/constants"
import { TagSlot } from "apprise-frontend/tag/module"
import { noTenant, tenantType } from "apprise-frontend/tenant/constants"
import { TenantSlot } from "apprise-frontend/tenant/module"
import { userapi } from "apprise-frontend/user/api"
import { userType } from "apprise-frontend/user/constants"
import { ComponentProps, UserSlot } from "apprise-frontend/user/module"
import { deepequals } from 'apprise-frontend/utils/common'
import { messageType } from "apprise-messages/constants"
import { Message, Topic } from "apprise-messages/model"
import { MessageSlot } from "apprise-messages/module"
import { RouteResolver, TopicInfo, TopicResolver } from "apprise-messages/registry"
import { dashboardapi } from "emaris-frontend/dashboard/api"
import { eventType } from "emaris-frontend/event/constants"
import { EventSlot } from "emaris-frontend/event/module"
import { productType } from "emaris-frontend/product/constants"
import { requirementType } from "emaris-frontend/requirement/constants"
import { EmarisState } from "emaris-frontend/state/model"
import moment from "moment-timezone"
import * as React from "react"
import { campaignapi } from "./api"
import { CampaignPermissions } from "./CampaignPermissions"
import { anyPartyTopic, campaignIcon, campaignPlural, campaignSingular, campaignType, complianceIcon, compliancePlural, complianceScaleCategory, complianceSingular, complianceType, defaultRelativeDate, defaultStatisticalHorizon, dueDateMailTopic, dueDateReminderMailTopic, subAssessedMailTopic, subchangeMailTopic, subPendingApprovalMailTopic, subRequestForChangeMailTopic, subSharedMailTopic, subSubmittedMailTopic, timelinessIcon, timelinessPlural, timelinessSingular, timelinessType } from "./constants"
import { CampaignLoader } from "./Loader"
import { messagerouteapi } from "./messageroute"
import { CampaignAppSettings } from "./model"
import { partyinstapi } from "./party/api"
import { PartyInstanceLabel } from "./party/Label"
import { TenantPreferencesForm } from "./preferences/TenantPreferences"
import { UserPreferencesForm } from "./preferences/UserPreferences"
import { productinstapi } from "./product/api"
import { ProductInstanceLabel } from "./product/Label"
import { ProductInstance } from "./product/model"
import { pushCampaignSlot } from "./pushevents"
import { requirementinstapi } from "./requirement/api"
import { RequirementInstanceLabel } from "./requirement/Label"
import { RequirementInstance } from "./requirement/model"
import { ApplicationSettings, ApplicationSettingsFields } from "./Settings"
import "./styles.scss"
import { submissionType } from "./submission/constants"
import { submissionstateapi } from './submission/state'
import { campaignvalidationapi } from "./validation"


export const campaignmodule: Module = {

    icon: campaignIcon,
    type: campaignType,

    dependencies: () => [tenantType, requirementType, productType, eventType],

    nameSingular: campaignSingular,
    namePlural: campaignPlural,

    [tagType]: {

        enable: true


    } as TagSlot,

    [eventType]: {

        enable: true

    } as EventSlot

    ,

    [pushEventType] : pushCampaignSlot

    ,

    [settingsType]: {

        validate: (s: EmarisState, settings: ApplicationSettings) => campaignvalidationapi(s).validateApplicationSettings(settings as any),

        defaults: (): CampaignAppSettings => ({

            [campaignType]: {
                approveCycle: 'all',
                adminCanEdit: false,
                liveGuard: undefined,
                suspendOnEnd: true,
                suspendSubmissions: false,
                adminCanSubmit: false,
                partyCanSeeNotApplicable: false,
                statisticalHorizon: defaultStatisticalHorizon,
                complianceScale: complianceScaleCategory,
                defaultRelativeDate: defaultRelativeDate,
                timeZone: moment.tz.guess()

            }
        }),

        component: ApplicationSettingsFields

    } as SettingsSlot<ApplicationSettings, EmarisState>

    ,

    [userType]: {

        // include only if ...
        renderIf: (s: ComponentProps & BaseState) => {

            // tags cannot be handled by tenant users
            const someTenantSubject = s?.subjectRange?.some(s => s.tenant !== noTenant)

            // ...1) logged user can manage tags himself, hence delegate to others.
            return !someTenantSubject

        },

        permissionComponent: props => <CampaignLoader placeholder={Placeholder.list}>
            <CampaignPermissions {...props} />
        </CampaignLoader>

        ,

        preferencesComponent: UserPreferencesForm,

        preferencesValidation: (formstate, state) => campaignapi(state as EmarisState).userPreferences().validate(formstate),

     
    } as UserSlot

    ,

    [mailType]: {
        topics: [
            {key: 'campaign.mail.topic.due', value: dueDateMailTopic},
            {key: 'campaign.mail.topic.due_reminder', value: dueDateReminderMailTopic},
            {key: 'campaign.mail.topic.submission', value: subchangeMailTopic},
            {key: 'campaign.mail.topic.submission_pendingapproval', value: subPendingApprovalMailTopic},
            {key: 'campaign.mail.topic.submission_requestchanges', value: subRequestForChangeMailTopic},
            {key: 'campaign.mail.topic.submission_submitted', value: subSubmittedMailTopic},
            {key: 'campaign.mail.topic.submission_assessed', value: subAssessedMailTopic},
            // {key: 'campaign.mail.topic.submission_published', value: subPublishedMailTopic},
            {key: 'campaign.mail.topic.submission_shared', value: subSharedMailTopic}
        ]
    } as MailSlot

    ,

    [tenantType]: {

        preferencesComponent: TenantPreferencesForm,

        preferencesValidation: (formstate, state) => campaignapi(state as EmarisState).tenantPreferences().validate(formstate)



    } as TenantSlot

    ,

    [pushEventType]: pushCampaignSlot

    ,

    [messageType]: {

        resolvers: (): TopicResolver<EmarisState>[] => [

            {
                type: tenantType,
                resolve: (topic: Topic, state: EmarisState) => {

                    if (deepequals(topic,anyPartyTopic))
                        return undefined

                    const [cid, tid] = topic.name.split(":")

                    const campaigns = campaignapi(state)
                    const campaign = campaigns.lookup(cid);

                    if (!campaign)
                        return {label:topic.name} as TopicInfo

                    const party = partyinstapi(state)(campaign).safeLookupBySource(tid)

                 

                    const route = `${dashboardapi(state).given(campaign).routeToParty(party)}`
                    const routeToMessages = `${route}/${messageType}`

                    return {

                        label: <PartyInstanceLabel linkTo={route} linkTarget={submissionType} instance={party} className='topic-icon' noDecorations noOptions />,
                        tip: <PartyInstanceLabel instance={party} noLink noIcon noOptions noDecorations />,
                        link: routeToMessages
                    }
                }
            },



            {
                type: requirementType,
                resolve: (topic: Topic, state: EmarisState) => {

                    const [cid, id] = topic.name.split(":")

                    const campaigns = campaignapi(state)
                    const campaign = campaigns.lookup(cid);

                    if (!campaign)
                        return {label:topic.name} as TopicInfo

                    const asset = requirementinstapi(state)(campaign).safeLookupBySource(id)

                    const { logged } = userapi(state)

                    if (logged.managesMultipleTenants()) {

                        const route = `${dashboardapi(state).given(campaign).routeToAsset(asset)}`
                        const routeToMessages = `${route}/${messageType}`

                        return {

                            label: <RequirementInstanceLabel linkTo={route} instance={asset as RequirementInstance} className='topic-icon' noDecorations noOptions />,
                            tip: <RequirementInstanceLabel instance={asset as RequirementInstance} noLink noIcon noOptions />,
                            link: routeToMessages
                        }

                    }

                    else {

                        const submissions = submissionstateapi(state)(campaign);

                        const trail = submissions.lookupTrailKey({ campaign: cid, party: logged.tenant, asset: id, assetType: requirementType })

                        // const submission = submissions.lookupTrail(tid)?.submissions.find(s=>s.id===sid)
                        const submission = submissions.latestIn(trail?.id)

                        if (!submission)
                            return {

                                label: <RequirementInstanceLabel instance={asset as RequirementInstance} className='topic-icon' noLink noDecorations noOptions />,

                            }


                        const linkTo = dashboardapi(state).given(campaign).partyRouteToSubmission(submission)
                        const linkToMessage = `${linkTo}?tab=${messageType}`

                        return {

                            label: <RequirementInstanceLabel linkTo={linkTo} instance={asset as RequirementInstance} className='topic-icon' noDecorations noOptions />,
                            tip: <RequirementInstanceLabel instance={asset as RequirementInstance} noLink noIcon noOptions />,
                            link: linkToMessage
                        }

                    }
                }
            },

            {
                type: productType,
                resolve: (topic: Topic, state: EmarisState) => {

                    const [cid, id] = topic.name.split(":")
                    
                    const campaigns = campaignapi(state)
                    const campaign = campaigns.lookup(cid);

                    if (!campaign)
                        return {label:topic.name} as TopicInfo

                    const asset = productinstapi(state)(campaign).safeLookupBySource(id)

                    const { logged } = userapi(state)

                    if (logged.managesMultipleTenants()) {

                        const route = `${dashboardapi(state).given(campaign).routeToAsset(asset)}`
                        const routeToMessages = `${route}/${messageType}`

                        return {

                            label: <ProductInstanceLabel linkTo={route} instance={asset as ProductInstance} className='topic-icon' noDecorations noOptions />,
                            tip: <ProductInstanceLabel instance={asset as ProductInstance} noLink noIcon noOptions />,
                            link: routeToMessages
                        }

                    }

                    // resolves to latest submission of corresponding trail
                    else {

                        const submissions = submissionstateapi(state)(campaign);

                        const trail = submissions.lookupTrailKey({ campaign: cid, party: logged.tenant, asset: id, assetType: productType })

                        // const submission = submissions.lookupTrail(tid)?.submissions.find(s=>s.id===sid)
                        const submission = submissions.latestIn(trail?.id)

                        if (!submission)
                            return {

                                label: <ProductInstanceLabel instance={asset as ProductInstance} className='topic-icon' noLink noDecorations noOptions />,

                            }


                        const linkTo = dashboardapi(state).given(campaign).partyRouteToSubmission(submission)
                        const linkToMessage = `${linkTo}?tab=${messageType}`

                        return {

                            label: <ProductInstanceLabel linkTo={linkTo} instance={asset as ProductInstance} className='topic-icon' noDecorations noOptions />,
                            tip: <ProductInstanceLabel instance={asset as ProductInstance} noLink noIcon noOptions />,
                            link: linkToMessage
                        }

                    }


                }
            }
        ],

        routes: (): RouteResolver<EmarisState> =>
        ({
            routeFor: (message: Message, state: EmarisState) => {

                const messageroute = messagerouteapi(state)(message)

                if (messageroute.topicsTypes().includes(submissionType))
                    return messageroute.submissionMessageRoute()

                if (messageroute.topicsTypes().includes(productType) || messageroute.topicsTypes().includes(requirementType))
                    return messageroute.assetMessageRoute()

                if (messageroute.topicsTypes().includes(tenantType))
                    return messageroute.partyMessageRoute()


                if (messageroute.topicsTypes().includes(campaignType))
                    return messageroute.campaignMessageRoute()

                return undefined
            }
        })

    } as MessageSlot


}


export const compliancemodule: Module = {

    icon: complianceIcon,
    type: complianceType,

    nameSingular: complianceSingular,
    namePlural: compliancePlural,

    [tagType]: {

        enable: true

    } as TagSlot
}

export const timelinessmodule: Module = {

    icon: timelinessIcon,
    type: timelinessType,

    nameSingular: timelinessSingular,
    namePlural: timelinessPlural,

    [tagType]: {

        enable: true

    } as TagSlot
}