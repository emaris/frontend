
import { useAsyncRender } from "apprise-frontend/utils/hooks"
import * as React from "react"
import { useCampaigns } from "./api"

type Props = React.PropsWithChildren<{
  placeholder?: React.ReactNode
}>


export const CampaignLoader = (props: Props) => {

  const { areReady, fetchAll } = useCampaigns()

  const [render] = useAsyncRender({
    when: areReady(),
    task: fetchAll,
    content: props.children,
    placeholder: props.placeholder

  })

  return render

}


export const CampaignPreloader = () => {

  const campaigns = useCampaigns()

  const preloaded = campaigns.arePreloaded()

  React.useEffect(() => {

    if (!preloaded)
    campaigns.preloadRunningCampaigns()

    // eslint-disable-next-line
  }, [preloaded])

  return null

}
