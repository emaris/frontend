import { useEmarisState } from "emaris-frontend/state/hooks";
import { EmarisState } from "../state/model";
import { campaignActions } from './actions';
import { campaignmodelapi } from "./model";
import { tenantpreferencesapi, userpreferencesapi } from "./preferences/api";
import { preferencesvalidationapi } from "./preferences/validation";
import { campaignstateapi } from "./state";
import { campaignvalidationapi } from "./validation";


export const useCampaigns = () => campaignapi(useEmarisState())

export const campaignapi = (s:EmarisState) => ({

    ...campaignstateapi(s),
    ...campaignvalidationapi(s),
    ...campaignmodelapi(s),

    actions: campaignActions,


    tenantPreferences: () => ({...tenantpreferencesapi(s),validate:preferencesvalidationapi(s).validateTenantPreferences}),
    userPreferences: () => ({...userpreferencesapi(s),validate:preferencesvalidationapi(s).validateUserPreferences})

    


})

export const campaigns = {

    breadcrumbResolver: (id:string,s: EmarisState) => campaignapi(s).lookup(id)?.name
}

