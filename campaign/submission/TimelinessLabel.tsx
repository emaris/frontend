import { Label, LabelProps, UnknownLabel } from "apprise-frontend/components/Label"
import { TagLabel } from "apprise-frontend/tag/Label"
import { useDashboard, useTrail } from "emaris-frontend/dashboard/hooks"
import { eventIcon } from "emaris-frontend/event/constants"
import { useTranslation } from "react-i18next"
import { AssetInstance } from "../model"
import { PartyInstance } from "../party/model"
import { TrailKey } from "./model"

export type Props = LabelProps & {

    asset?: AssetInstance
    party? : PartyInstance
    trailKey?: TrailKey

}


export const TimelinessLabel = (props: Props) => {
    const {asset, party, trailKey, ...rest} = props

    const {t} = useTranslation()

    const { summaries } = useDashboard()
    const { keyWithAsset, keyWithParty } = useTrail()

    const key = trailKey ? trailKey : party ? keyWithParty(party) : asset ? keyWithAsset(asset) : undefined

    if (!key) return <UnknownLabel />

    const summary = summaries.trailSummaryOf(key)
    const timeliness = summary.timeliness


    const tag = timeliness ? timeliness.tag : timeliness

    if (!tag) return <Label mode='tag' className="timeliness-notdue" icon={eventIcon} title={t("dashboard.labels.timeliness.notdue")} />

    const className = timeliness ? timeliness.timelinessClass : ''

    
    return <TagLabel className={className} tag={tag} {...rest} icon={eventIcon} noDecorations  />
}