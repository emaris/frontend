
import { Form } from "apprise-frontend/form/Form"
import { FormState } from "apprise-frontend/form/hooks"
import { pagespec } from "apprise-frontend/layout/components/Page"
import { LayoutScrollSubscriber } from 'apprise-frontend/layout/components/scroll'
import { useLayout } from "apprise-frontend/layout/LayoutProvider"
import { LayoutInstance } from "apprise-frontend/layout/model"
import { LazyPdfViewer } from 'apprise-frontend/layout/pdf/LazyViewer'
import { useCurrentAsset } from "emaris-frontend/dashboard/hooks"
import { useSubmissionDownload } from "emaris-frontend/layout/SubmissionLayoutPdf"
import * as React from "react"
import { Submission } from "./model"
import "./styles.scss"






export type Props = {

    state: FormState<Submission>

}



export const SubmissionForms = (props: Props) => {

    return <div className='submission-layout'>

        <Pages {...props} />

    </div>


}

export const Pages = (props: Props) => {

    const layout = useLayout()

    const { state } = props

    const { Render } = pagespec

    const asset = useCurrentAsset()

    return <div className={`submission-pages ${asset.type}`}>
        <Form state={state} >
            {layout.allPages().map(p =>
                <div className="page-card" key={p.id}>
                    <LayoutScrollSubscriber key={p.id} component={p}>
                        <Render style={{ display: "flex", flexDirection: "column" }} component={p} />
                    </LayoutScrollSubscriber>
                </div>)}
        </Form>
    </div>
}

export const SubmissionPDFForms = (props: Props & { layout: LayoutInstance }) => {

    const { document } = useSubmissionDownload(props.layout)

    return <LazyPdfViewer style={{ height: "100%" }}>{document}</LazyPdfViewer>

}