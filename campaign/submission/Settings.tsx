
import { Radio } from "antd"
import { FormState } from "apprise-frontend/form/hooks"
import { RadioBox } from "apprise-frontend/form/RadioBox"
import { SettingsForm } from "apprise-frontend/settings/SettingsForm"
import { useUsers } from "apprise-frontend/user/api"
import { through } from "apprise-frontend/utils/common"
import { Validation } from "apprise-frontend/utils/validation"
import * as React from "react"
import { useTranslation } from "react-i18next"
import { approveCycleOptionDefaults } from "../model"
import { useSubmissions } from './api'
import { useSubmissionContext } from "./hooks"
import { Trail, TrailSettings } from "./model"


type Props = {

    state: FormState<Trail>
    onSave: (_:Trail)=>void
    report: Partial<Record<keyof TrailSettings ,Validation>>
}

export const TrailSettingsFields = (props:Props) => {

    const {t} = useTranslation()

    const {state,onSave,report} = props

    const users = useUsers()

    const ctx = useSubmissionContext()
    const submissions = useSubmissions(ctx.campaign)

    const {edited,change,reset} = state

    const isCoordinator = users.logged.hasNoTenant()

    return  <SettingsForm state={state} onSave={()=>submissions.saveTrail(edited).then(through(saved=>reset(saved,false))).then(onSave)}>

                <RadioBox label={t("submission.fields.approval.name")} validation={report.approveCycle}
                    value={ submissions.approveCycle(edited,true) }
                    onChange={change( (t,v) => {  submissions.setApproveCycle(t,v)})} >
                        <Radio value={false}>{t("submission.fields.approval.options.not_required")}.</Radio>
                        <Radio value={true}>{t("submission.fields.approval.options.required")}.</Radio>
                        <Radio value={undefined}>
                            {t("submission.fields.approval.options.default",{value:t(approveCycleOptionDefaults[submissions.approveCycleDefault(edited) ? 'all' : 'none' ])})}.
                        </Radio>
                </RadioBox>

                {isCoordinator && 

                    <RadioBox label={t("submission.fields.admin_can_edit.name")} validation={report.adminCanEdit}
                        value={ edited.properties.adminCanEdit ?? 'default'  }
                        onChange={change((t, v) => submissions.setCanEdit(t, v==='default' ? undefined : v)) }>

                        <Radio value={true}>{t("submission.fields.admin_can_edit.options.can")}.</Radio>
                        <Radio value={false}>{t("submission.fields.admin_can_edit.options.cannot")}.</Radio>
                        <Radio value={'default'}>{t("submission.fields.admin_can_edit.options.default",
                                        {value:t(submissions.canEditDefault(edited)?
                                                "submission.fields.admin_can_edit.options.can_short"
                                                : "submission.fields.admin_can_edit.options.cannot_short" )})}.</Radio>
                    </RadioBox>
                }

                {isCoordinator && 

                    <RadioBox label={t("submission.fields.admin_can_submit.name")} validation={report.adminCanEdit}
                        value={ edited.properties.adminCanSubmit ?? 'default'  }
                        onChange={change((t, v) => submissions.setCanSubmit(t, v==='default' ? undefined : v)) }>

                        <Radio value={true}>{t("submission.fields.admin_can_submit.options.can")}.</Radio>
                        <Radio value={false}>{t("submission.fields.admin_can_submit.options.cannot")}.</Radio>
                        <Radio value={'default'}>{t("submission.fields.admin_can_submit.options.default",
                                        {value:t(submissions.canSubmitDefault(edited)?
                                                "submission.fields.admin_can_submit.options.can_short"
                                                : "submission.fields.admin_can_submit.options.cannot_short" )})}.</Radio>
                    </RadioBox>
                }

           </SettingsForm>
}