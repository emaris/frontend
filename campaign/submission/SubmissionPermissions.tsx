
import { useLocale } from 'apprise-frontend/model/hooks'
import { noTenant } from 'apprise-frontend/tenant/constants'
import { UserPermissions } from 'apprise-frontend/user/UserPermissionTable'
import { useProducts } from 'emaris-frontend/product/api'
import { productPlural, productSingular } from 'emaris-frontend/product/constants'
import { ProductLabel } from 'emaris-frontend/product/Label'
import { Product } from 'emaris-frontend/product/model'
import { useRequirements } from 'emaris-frontend/requirement/api'
import { requirementPlural, requirementSingular } from 'emaris-frontend/requirement/constants'
import { RequirementLabel } from 'emaris-frontend/requirement/Label'
import { Requirement } from 'emaris-frontend/requirement/model'
import { ChangesProps } from 'apprise-frontend/iam/permission'
import { ResourceProps, StateProps, SubjectProps } from 'apprise-frontend/iam/PermissionTable'
import { User } from 'apprise-frontend/user/model'
import * as React from 'react'
import { useTranslation } from 'react-i18next'
import { productSubmissionActions, requirementSubmissionActions } from './actions'
import { productSubmissionType, requirementSubmissionType } from './constants'

type RequirementPermissionsProps = ChangesProps & Partial<ResourceProps<Requirement>> & Partial<SubjectProps<User>> & Partial<StateProps<User,Requirement>> & {
    edited? : User
}


//  wraps UserPermissions for Requirements, so as to inject appropriate defaults.

export const RequirementSubmissionPermissions =  (props:RequirementPermissionsProps) => {

    const {t} = useTranslation()
    const {l} = useLocale()

    const requirements = useRequirements()


    return  <UserPermissions {...props}
                id="requirement-submission-permissions" 
                subjectRange={props.subjectRange?.filter(s=>s.tenant!==noTenant)}
                resourceSingular={props.resourceSingular || t(requirementSingular)}
                resourcePlural={props.resourcePlural || t(requirementPlural)}
                renderResource={props.renderResource || ((t:Requirement) => <RequirementLabel requirement={t} /> )}
                resourceText={t=>l(t.name)}
                resourceId={props.resourceId || ((r:Requirement) => r.id) }
                renderResourceOption={props.renderResourceOption || ((r:Requirement) => <RequirementLabel noLink requirement={r} />)} 
                resourceRange={props.resourceRange || requirements.all() } 
                resourceType={requirementSubmissionType}

                actions={ Object.values(requirementSubmissionActions)} />
                
 }


 type ProductPermissionsProps = ChangesProps & Partial<ResourceProps<Product>> & Partial<SubjectProps<User>> & Partial<StateProps<User,Product>> & {
    edited? : User
}


 export const ProductSubmissionPermissions =   (props:ProductPermissionsProps) => {

    const {t} = useTranslation()
    const {l} = useLocale()
    const products = useProducts()

    return  <UserPermissions {...props}
                id="product-submission-permissions" 
                subjectRange={props.subjectRange?.filter(s=>s.tenant!==noTenant)}
                resourceSingular={props.resourceSingular || t(productSingular)}
                resourcePlural={props.resourcePlural || t(productPlural)}
                resourceText={t=>l(t.name)}
                renderResource={props.renderResource || ((t:Product) => <ProductLabel product={t} /> )}
                resourceId={props.resourceId || ((r:Product) => r.id) }
                renderResourceOption={props.renderResourceOption || ((r:Product) => <ProductLabel noLink product={r} />)} 
                resourceRange={props.resourceRange || products.all() } 
                resourceType={productSubmissionType}

                actions={Object.values(productSubmissionActions)} />
                
}