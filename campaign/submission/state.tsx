
import { intlapi } from "apprise-frontend/intl/api";
import { Language } from "apprise-frontend/intl/model";
import { Component, isContainer } from 'apprise-frontend/layout/components/model';
import { mailapi } from "apprise-frontend/mail/api";
import { MultilangDto } from 'apprise-frontend/model/multilang';
import { bytestreamapi } from "apprise-frontend/stream/api";
import { Bytestream, bytestreamForBlob } from 'apprise-frontend/stream/model';
import { systemapi } from "apprise-frontend/system/api";
import { tagapi } from 'apprise-frontend/tag/api';
import { Tag } from 'apprise-frontend/tag/model';
import { noTenant } from 'apprise-frontend/tenant/constants';
import { timeapi } from 'apprise-frontend/time/api';
import { userapi } from 'apprise-frontend/user/api';
import { clock, deepequals, index, indexMap, through } from "apprise-frontend/utils/common";
import { notify, showAndThrow, showError } from "apprise-frontend/utils/feedback";
import { messageapi } from 'apprise-messages/api';
import { Message, MessageContent } from 'apprise-messages/model';
import { AssetSection, assetsectionspec, sectionAssetOf } from "emaris-frontend/layout/components/AssetSection";
import { reqsectionspec, RequirementSection } from 'emaris-frontend/layout/components/RequirementSection';
import { submissionfiledropspec, UploadTimestamp } from 'emaris-frontend/layout/components/SubmissionFileDrop';
import { productapi } from 'emaris-frontend/product/api';
import { prodImportanceCategory, productType } from 'emaris-frontend/product/constants';
import { reqImportanceCategory, requirementType } from 'emaris-frontend/requirement/constants';
import { EmarisState } from "emaris-frontend/state/model";
import moment from 'moment';
import { subAssessedMailTopic, subAssessementRevokedMailTopic, subPendingApprovalMailTopic, subRequestForChangeMailTopic, subSubmittedMailTopic } from '../constants';
import { mailtemplateapi } from '../mail/api';
import { Campaign, CampaignInstance, campaignmodelapi } from "../model";
import { partyinstapi } from "../party/api";
import { PartyInstance } from "../party/model";
import { submissioncallapi } from "./calls";
import { ComplianceScale, ManagementData, nextSubmission, Submission, SubmissionDto, submissionmodelapi, SubmissionStub, SubmissionWithTrail, TimelinessScale, Trail, TrailDto, TrailKey } from "./model";
import { submissionModule } from "./module";
import { profileapi } from './profile';


export type SubmissionState = {

    submissions: {

        all: Trail[]
        compliance: ComplianceCache
        timeliness: TimelinessCache
        map: Record<string, Trail>

    }

}

export type ComplianceCache = {

    scale: ComplianceScale | undefined
}
    & Record<string, {
        weights: Tag[]
    }>

export type TimelinessCache = {

    scale: TimelinessScale | undefined
}


export const initialSubmissions = () => ({

    all: undefined!,
    compliance: {
        [requirementType]: {
            weights: []

        },
        [productType]: {
            weights: []

        }
    }
})


export const submissionstateapi = (s: EmarisState) => (c: Campaign) => {

    const t = intlapi(s).getT()

    const system = () => systemapi(s)
    const campaigns = campaignmodelapi(s)

    const [singular, plural] = [t(submissionModule.nameSingular), t(submissionModule.namePlural)].map(n => n.toLowerCase())

    const resolve = (trail: Trail | string) => typeof trail === 'string' ? self.lookupTrail(trail) : trail

    const self = {


        livePush: (trails: Trail[], type: 'change' | 'remove') => {


            let current = s.campaigns.instances[c.id]?.submissions?.all ?? []

            trails.forEach(trail => {

                switch (type) {

                    case "change":

                        console.log("received unexpected change event on trails: this should have a different per-submission channel")
                        break;

                    case "remove": current = current.filter(i => i.id !== trail.id); break

                }

            })

            self.setAllTrails(current)

        }

                
        ,

        setAllTrails: (trails: Trail[], props?: { noOverwrite: boolean }) => s.changeWith(s => {

            const campaign = s.campaigns.instances[c.id] ?? {}
            const submissions = campaign.submissions ?? initialSubmissions()

            if (submissions.all && props?.noOverwrite)
                return

            submissions.all = trails

            submissions.map = indexMap(trails).by(i => i.id)
            
            submissions.compliance.scale = submissionmodelapi(s)(c).complianceScale()
            submissions.compliance[requirementType].weights = tagapi(s).allTagsOfCategory(reqImportanceCategory)
            submissions.compliance[productType].weights = tagapi(s).allTagsOfCategory(prodImportanceCategory)
            submissions.timeliness = {scale: submissionmodelapi(s)(c).timelinessScale()}


            s.campaigns.instances[c.id] = { ...campaign, submissions }
        })

        ,

        complianceCache: () => s.campaigns.instances[c.id]?.submissions?.compliance

        ,

        timelinessCache: () => s.campaigns.instances[c.id]?.submissions?.timeliness

        ,

        replaceTrail: (trail: TrailDto) => self.setAllTrails(

            (self.allTrails() ?? []).some(t => t.id === trail.id) ?
                self.allTrails().map(t => t.id === trail.id ? trail : t)
                : [...self.allTrails(), trail]
        )

        ,

        allTrails: () => s.campaigns.instances[c.id]?.submissions?.all

        ,

        allActiveTrails: () => {
            const currentParties = partyinstapi(s)(c).all().map(p => p.source)
            return self.allTrails().filter(t => currentParties.some(p => p === t.key.party))
        }

        ,

        allTrailsForParty: (party: PartyInstance) => self.allTrails().filter(t => t.key.party === party.source)

        ,

        allTrailsForAsset: (asset: CampaignInstance) => self.allTrails().filter(t => t.key.asset === asset.source)

        ,

        allTrailsByAsset: (type: string): { [_: string]: Trail[] } => index(self.allActiveTrails().filter(t => t.key.assetType === type)).by(t => t.key.asset)

        ,

        allTrailsByParty: (): { [_: string]: Trail[] } => index(self.allTrails()).by(t => t.key.party)

        ,

        allSubmissions: (trail: string | Trail, comparator = submissionmodelapi(s)(c).dateComparatorDesc) => [...resolve(trail)?.submissions ?? []].sort(comparator)

        ,

        areTrailsReady: () => !!self.allTrails()

        ,

        previous: (submission: SubmissionStub) => {

            const all = self.allSubmissions(submission.trail)

            return all[all.findIndex(s => s.id === submission.id) + 1]

        }

        ,

        next: (submission: SubmissionStub) => {

            const all = self.allSubmissions(submission.trail)

            return all[all.findIndex(s => s.id === submission.id) - 1]

        }

        ,

        hasBeenManaged: (submission: Submission) => self.isManagedState(self.next(submission))

        ,

        // either a draft or the latest submission, or a managed/published versions of it
        isLatest: (submission: SubmissionStub) => !submission.lifecycle.lastSubmitted || self.latestIn(submission.trail)?.id === submission.id

        ,

        // latest CPC submission
        latestSubmission: (trail: string | Trail) => self.allSubmissions(trail).find(s => s.lifecycle.state === 'submitted')

        ,

        firstSubmission: (trail: string | Trail) => self.allSubmissions(trail).reverse().find(s => s.lifecycle.state === 'submitted')


        ,

        isLastSubmitted: (submission: SubmissionStub) => {

            return self.latestSubmission(submission.trail)?.id === submission.id

        }

        ,

        previousSubmission: (submission: Submission) => {

            const all = self.allSubmissions(submission.trail)
            const thisIndex = all.findIndex(s => s.id === submission.id)

            // includes the submission itself, if it matches.
            return all.slice(thisIndex).find(s => s.lifecycle.state === 'submitted')
        }

        ,

        previousReference: (trail: string | Trail, submission: Submission) => self.allSubmissions(trail).find(s => s.id !== submission.id && s.lifecycle.reference)?.lifecycle.reference




        ,

        isLastSubmittedOrAbout: (submission: SubmissionStub) => {

            if (!self.isSubmittedorAbout(submission))
                return false;

            const submissions = self.allSubmissions(submission.trail)
            const submitted = submissions.findIndex(s => s.lifecycle.state === 'submitted')
            const thisSubmission = submissions.findIndex(s => s.id === submission.id)

            return thisSubmission <= submitted

        }

        ,

        isManagedState: (submission?: SubmissionStub) => submission?.lifecycle.state === 'managed' || submission?.lifecycle.state === 'published'

        ,

        isSubmittedorAbout: (submission: SubmissionStub) => {

            return !!submission.lifecycle.lastSubmitted
        }

        ,

        isSupersededOrAbout: (submission: Submission) => {

            const submissions = self.allSubmissions(submission.trail)
            const submitted = submissions.findIndex(s => s.lifecycle.state === 'submitted')
            const thisSubmission = submissions.findIndex(s => s.id === submission.id)

            return submitted >= 0 && submitted <= thisSubmission

        }

        ,

        latestIn: (trail: string | Trail | undefined) => trail ? self.allSubmissions(trail)[0] : undefined


        ,


        lastSubmittedBefore: (trail: string | Trail | undefined, date: string | undefined) => {

            if (!trail)
                return undefined

            if (date)

                //  submitted before
                return self.allSubmissions(trail).find(s => moment(s.lifecycle.lastSubmitted).isSameOrBefore(date))

                    ??

                    // what if we don't have anything submitted before or at all ? 
                    self.latestIn(trail)


            return self.latestIn(trail)

        }

        ,


        fetchAllTrails: (forceRefresh = false) =>

            self.areTrailsReady() && !forceRefresh ? Promise.resolve(self.allTrails())

                :

                system().toggleBusy(`trails.fetchAll`, t("common.feedback.load", { plural }))

                    .then(_ => console.log(`fetching trails...`))
                    .then(_ => submissioncallapi(s)(c).fetchAllTrails())
                    .then(through($ => self.setAllTrails($, { noOverwrite: !forceRefresh })))


                    .catch(e => showAndThrow(e, t("common.calls.fetch_all_error", { plural })))
                    .finally(() => system().toggleBusy(`trails.fetchAll`))


        ,

        // lookupTrailKey: (key: TrailKey) => self.allTrails().find(t => deepequals(key, t.key))
        lookupTrailKey: (key: TrailKey) => {
            let trail
            const trails = self.allTrails()
            for (let i = 0 ; i < trails.length; i++) {
                if (deepequals(trails[i].key, key)) {
                    trail = trails[i]
                    break
                }
            }
            return trail
        }

        ,

        lookupTrail: (id: string) => id ? s.campaigns.instances[c.id]?.submissions?.map[id] : undefined

        ,


        fetchOneSubmission: (id: string): Promise<Submission> =>

            system().toggleBusy(`submissions.fetchOne`, t("common.feedback.load", { singular }))

                //.then( _ =>console.log(`fetching submission ${id}...`))
                .then(_ => submissioncallapi(s)(c).fetchOneSubmission(id))


                .catch(e => showAndThrow(e, t("common.calls.fetch_one_error", { singular })))
                .finally(() => system().toggleBusy(`submissions.fetchOne`))

        ,

        fetchSubmissionsRelatedTo: (submission: SubmissionStub, trail: Trail): Promise<SubmissionWithTrail[]> =>

            clock("fetching submissions related to submission of " + trail.key.asset, () =>

                system().toggleBusy(`submissions.fetchRelated`, t("dashboard.labels.load_related"))

                    .then(_ => trail.key.assetType === productType ?

                        self.fetchSubmissionsRelatedToProduct(submission, trail)
                        :

                        Promise.resolve([])
                    )

                    .catch(e => {
                        // show the error, but allow progress with unresolvable references.
                        showError(e, t("dashboard.labels.load_related_error", { plural }));
                        return []
                    })
                    .finally(() => system().toggleBusy(`submissions.fetchRelated`))

            )

        ,


        // returns the full requirement submissions whose data may be referenced from within this product.
        fetchSubmissionsRelatedToProduct: (submission: SubmissionStub, trail: Trail): Promise<SubmissionWithTrail[]> => {
            // find all requirements referenced from this product.
            const flattencontainers = (c: Component): Component[] => isContainer(c) ? [c, ...c.children.flatMap(flattencontainers)] : []
            const product = productapi(s).safeLookup(trail.key.asset)
            const flattenComponent = flattencontainers(product.properties.layout.components)
            const referenceAssets = [
                ...flattenComponent.filter(c => c.spec === reqsectionspec.id).flatMap(c => (c as RequirementSection).requirements),
                ...flattenComponent.filter(c => c.spec === assetsectionspec.id).flatMap(c => (c as AssetSection).assets)
            ]

            // identify related submissions.
            const related = self.allTrails()

                .filter(t => referenceAssets.find(r => sectionAssetOf(r) === t.key.asset && t.key.party === trail.key.party))

                // old, broader logic: requirements about same party.
                //.filter(t => t.key.party === trail.key.party && t.key.assetType === requirementType)

                // connect to the latest submission before this report was submitted or, if the report is not submitted, to date.
                .map(t => self.allSubmissions(t).find(s => (s.lifecycle.state === 'submitted' || s.lifecycle.state === 'missing') && s.lifecycle.lastSubmitted && moment(s.lifecycle.lastSubmitted).isSameOrBefore(submission.lifecycle.lastSubmitted ?? undefined))!)
                .filter(s => s !== undefined)

            return related.length > 0 ? submissioncallapi(s)(c).fetchManySubmissions(related).then(submissions => submissions.map(s => ({ submission: s, trail: self.lookupTrail(s.trail)! }))) : Promise.resolve([])

        }
        ,

        assessSubmission: async (submission: Submission, trail: Trail) => {

            const compliance = submissionmodelapi(s)(c).complianceProfile(submission)?.name!

            const saved = await self.saveSubmission(submission, trail)

            const changeTemplate = mailtemplateapi(s).submissionChange(saved, trail)

            changeTemplate.parameters.status = 'assessed'

            const messageContent = { keys: ["submission.message.assessed"], parameters: { compliance } }

            self.postMessage(trail, submission, messageContent)

            self.scheduleMail(() => mailapi(s).sendTo([noTenant, trail.key.party]).content(changeTemplate).onTopic(subAssessedMailTopic))

            return saved

        }

        ,

        revokeAssessment: async (submission: Submission, trail: Trail) => {

            const saved = await self.saveSubmission(submission, trail)

            const changeTemplate = mailtemplateapi(s).submissionChange(saved, trail)

            const messageContent = { keys: ["submission.message.revoked"], parameters: {} }

            self.postMessage(trail, submission, messageContent)

            changeTemplate.parameters.status = 'revokedassessment'

            self.scheduleMail(() => mailapi(s).sendTo([noTenant, trail.key.party]).content(changeTemplate).onTopic(subAssessementRevokedMailTopic))

            return saved


        }

        ,

        saveSubmission: (submission: Submission, trail: Trail) =>

            submission.lifecycle?.created ? self.updateSubmission(submission) : self.addSubmission(submission, trail)


        ,


        changeDate: async (submission: Submission, trail: Trail) => {

            const savedSubmission = await self.updateSubmission(submission)

            const parameters: Record<string, MultilangDto> = { date: timeapi(s).formatAsMultilang(submission.lifecycle.lastSubmitted, 'short') }

            const messageContent = { keys: [`submission.message.datechange`], parameters }

            self.postMessage(trail, savedSubmission, messageContent)

            return savedSubmission

        }



        ,


        addSubmission: (submission: Submission, trail: Trail) => {

            return system().toggleBusy(`submissions.save`, t("common.feedback.save_changes"))
                .then(_ => submissioncallapi(s)(c).addSubmission(submission, trail.key))

                // process the pair: intern the trail and return submission.
                .then(([trail, submission]) => {

                    self.replaceTrail(trail)
                    return submission

                })


                .then(through(() => notify(t('common.feedback.saved'))))

                .catch(e => showAndThrow(e, t("common.calls.save_one_error", { singular })))
                .finally(() => system().toggleBusy(`submissions.save`))

        }

        ,

        updateSubmission: (submission: Submission) => {

            return system().toggleBusy(`submissions.save`, t("common.feedback.save_changes"))
                .then(_ => submissioncallapi(s)(c).updateSubmission(submission)
                    .then(updated =>
                        submissioncallapi(s)(c).fetchOneTrail(submission.trail)
                            .then(trail => [trail, updated] as [TrailDto, SubmissionDto])

                    ))

                // process the pair: intern the trail and return submission.
                .then(([trail, submission]) => {

                    self.replaceTrail(trail)
                    return submission

                })


                .then(through(() => notify(t('common.feedback.saved'))))

                .catch(e => showAndThrow(e, t("common.calls.save_one_error", { singular })))
                .finally(() => system().toggleBusy(`submissions.save`))

        }

        ,

        saveTrail: (trail: Trail) =>

            system().toggleBusy(`trail.save`, t("common.feedback.save_changes"))

                .then(() => submissioncallapi(s)(c).saveOneTrail(trail))
                .then(through(self.replaceTrail))
                .then(through(() => notify(t('common.feedback.saved'))))

                .catch(e => showAndThrow(e, t("common.calls.save_one_error", { singular })))
                .finally(() => system().toggleBusy(`trail.save`))

        ,

        removeSubmission: (submission: SubmissionStub) =>

            system().toggleBusy(`submissions.remove`, t("common.feedback.save_changes"))
                .then(_ => submissioncallapi(s)(c).removeSubmission(submission.id))
                .then(_ => submissioncallapi(s)(c).fetchOneTrail(submission.trail))      // may now be empty.
                .then(self.replaceTrail)

                .catch(e => showAndThrow(e, t("common.calls.remove_one_error", { singular })))
                .finally(() => system().toggleBusy(`submissions.remove`))


        ,

        postMessage: (trail: Trail, submission: Submission, content: MessageContent) => {

            const messages = messageapi(s)

            const { topics, flow } = submissionmodelapi(s)(c).topicsAndFlow(trail, submission)

            const message = { scope: c.id, topics, recipient: flow.recipient, readBy: [noTenant, trail.key.party], content } as Message

            if (campaigns.isMuted(c)) return message

            return messages.postOne(message)

        }

        ,

        // mailapi(s).sendTo([noTenant, trail.key.party]).content(changeTemplate).onTopic(subAssessedMailTopic)

        scheduleMail: (sender: () => void) => {
            if (!campaigns.isMuted(c)) sender()
        }

        ,

        // updates the timestamp on all uploads.
        timestamp: (submission: Submission, trail: Trail) => {

            const now = timeapi(s).current()

            const flatten = (c: Component): Component[] => isContainer(c) ? c.children.flatMap(flatten) : [c]

            const layout = profileapi(s)(c).profileOf(trail.key.assetType).layoutOf(trail.key.asset)

            // upload components.
            const uploads = flatten(layout.components).filter(c => c.spec === submissionfiledropspec.id)

            // timestamps uploads.
            uploads.flatMap(c => (submission.content.data[c.id] ?? []) as UploadTimestamp[])
                .filter(ts => ts.submission === submission.id)
                .forEach(ts => {
                    ts.timestamp = now.valueOf()
                })

            return submission;

        }


        ,

        submit: async (submission: Submission, trail: Trail) => {

            const approveCycle = submissionmodelapi(s)(c).approveCycle(trail)

            const state = submission.lifecycle.state === 'draft' ? (approveCycle ? 'pending' : 'submitted') : 'submitted'

            let saved: Submission = { ...submission, lifecycle: { ...submission.lifecycle, state } }

            if (state === 'submitted')
                saved = self.timestamp(saved, trail)

            const savedSubmission = await self.saveSubmission(saved, trail)

            const changeTemplate = mailtemplateapi(s).submissionChange(savedSubmission, trail)

            const messageState = state === 'submitted' && submission.lifecycle.lastSubmitted ? 'managed_submitted' : state
            const parameters: Record<string, MultilangDto> = messageState === 'managed_submitted' ? { date: timeapi(s).formatAsMultilang(submission.lifecycle.lastSubmitted, 'short') } : {}

            const messageContent = { keys: [`submission.message.${messageState}`], parameters }

            self.postMessage(trail, savedSubmission, messageContent)

            const mailTopic = state === 'pending' ? subPendingApprovalMailTopic : subSubmittedMailTopic
            const targets = state === 'pending' ? [trail.key.party] : [noTenant, trail.key.party]
            self.scheduleMail(() => mailapi(s).sendTo(targets).content(changeTemplate).onTopic(mailTopic))

            return savedSubmission

        }

        ,


        reject: async (submission: Submission, trail: Trail) => {

            const saved = {

                ...submission,
                lifecycle: {
                    ...submission.lifecycle,
                    state: 'draft'
                }

            } as Submission

            const savedSubmission = await self.saveSubmission(saved, trail)
            const changeTemplate = mailtemplateapi(s).submissionChange(savedSubmission, trail)

            changeTemplate.parameters.status = 'requestforchange'

            const messageContent = { keys: ["submission.message.rejected"], parameters: {} }

            self.postMessage(trail, saved, messageContent)

            self.scheduleMail(() => mailapi(s).sendTo(trail.key.party).content(changeTemplate).onTopic(subRequestForChangeMailTopic))

            return savedSubmission


        },

        manageSubmission: (submission: Submission, trail: Trail, data: ManagementData) => {

            const { reference } = data

            const managed: Submission = {

                ...nextSubmission(submission)

                ,

                lifecycle: {
                    ...submission.lifecycle,
                    state: 'managed' as const,
                    reference

                }
            }

            return self.addSubmission(managed, trail)

        }

        ,

        publishSubmission: (submission: Submission, trail: Trail) => {

            const now = timeapi(s).current()
            const { logged } = userapi(s)

            const published = {

                ...nextSubmission(submission)

                ,

                lifecycle: {

                    ...submission.lifecycle,

                    state: 'published' as const,
                    lastPublished: now.format(),
                    lastPublishedBy: logged.username
                },
            }

            return self.addSubmission(published, trail)

        }

        ,

        shareSubmission: async (submission: Submission, trail: Trail, blob: Blob, name: string, lang: Language) => {

            const bs = bytestreamapi(s)

            const share = async () => {

                const publication = submission.lifecycle.publication ?? {}
                const publicationInLang = publication[lang] !== undefined ? publication[lang]!.trim() === "" ? undefined : publication[lang] : undefined
                const bytestream = (publicationInLang ? { ...bytestreamForBlob(name, blob), id: publicationInLang, ref: submission.id } : { ...bytestreamForBlob(name, blob), ref: submission.id }) as Bytestream
                const streams = await bs.upload([[{ id: bytestream.id, stream: bytestream }, blob]], !!publicationInLang)

                if (streams.length > 0) {
                    const stream = streams[0]
                    const newSubmission = { ...submission, lifecycle: { ...submission.lifecycle, publication: { ...publication, [lang]: stream.id } } }
                    self.saveSubmission(newSubmission, trail)
                    return newSubmission
                }

                return submission

            }

            try {

                await system().toggleBusy(`submissions.share`, t("submission.labels.share_wait"))

                const shared = await share()

                const changeTemplate = mailtemplateapi(s).submissionChange(shared, trail)

                changeTemplate.parameters.status = 'shared'

                const messageContent = { keys: ["submission.message.shared"], parameters: {} }

                self.postMessage(trail, shared, messageContent)

                self.scheduleMail(() => mailapi(s).sendTo([noTenant, trail.key.party]).content(changeTemplate).onTopic(subRequestForChangeMailTopic))

                system().toggleBusy(`submissions.share`)

                return shared

            }
            catch (e) {
                return showAndThrow(e as Error, t("common.calls.share_error"))
            }


        }

    }

    return self;
}