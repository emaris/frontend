

import { useEmarisState } from 'emaris-frontend/state/hooks'
import * as React from "react"
import { useCampaigns } from '../api'
import { usePartyInstances } from '../party/api'
import { useSubmissions } from "./api"
import { Submission, SubmissionContext, Trail } from "./model"
import { profileapi } from './profile'



// propagates a submission and its context.
export const SubmissionReactContext = React.createContext<SubmissionContext>(undefined!)


// custom hook: return the curent submission api and the current submission.
export const useSubmissionContext = () => {

    const context = React.useContext(SubmissionReactContext) 
    
    const submissions = useSubmissions(context.campaign)
   
    return submissions.inContext(context)

}

// like above, but for a non-contextual submission versions. 
export const useContextFactory = (trail:Trail) => {

    const state = useEmarisState()
    const campaign = useCampaigns().safeLookup(trail.key.campaign)

    const P = profileapi(state)(campaign).profileOf(trail.key.assetType)
    const asset = P.instance(trail.key.asset)
    const party = usePartyInstances(campaign).safeLookup(trail.key.party)

    const submissions = useSubmissions(campaign)
    
    return { contextOf: (submission:Submission) => {

        const context : SubmissionContext = {submission,trail,campaign,party,asset,relatedSubmissions:[]}
        return submissions.inContext(context)
    
    }}

}