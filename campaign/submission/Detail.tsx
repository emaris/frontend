
import { Badge, Tooltip } from "antd"
import { buildinfo } from 'apprise-frontend/buildinfo'
import { Button } from "apprise-frontend/components/Button"
import { IdProperty } from "apprise-frontend/components/IdProperty"
import { Label } from 'apprise-frontend/components/Label'
import { AdvancedRouteGuard } from "apprise-frontend/components/RouteGuard"
import { useRememberScroll } from 'apprise-frontend/components/hooks'
import { FormState, useFormState } from "apprise-frontend/form/hooks"
import { icns } from "apprise-frontend/icons"
import { LanguageBox } from 'apprise-frontend/intl/LanguageBox'
import { useCurrentLanguage } from "apprise-frontend/intl/api"
import { defaultLanguage } from "apprise-frontend/intl/model"
import { LayoutProvider, useLayout } from "apprise-frontend/layout/LayoutProvider"
import { Layout, LayoutConfig, LayoutInstance } from "apprise-frontend/layout/model"
import { newParameterFrom } from "apprise-frontend/layout/parameters/model"
import { ThemeLoader } from "apprise-frontend/layout/themes/Loader"
import { useLocale } from "apprise-frontend/model/hooks"
import { PushGuard } from 'apprise-frontend/push/PushGuard'
import { RemoteChangeGuard } from 'apprise-frontend/push/RemoteChangeGuard'
import { PushSubscription } from 'apprise-frontend/push/model'
import { Page } from "apprise-frontend/scaffold/Page"
import { Subtitle, Titlebar } from "apprise-frontend/scaffold/PageHeader"
import { Sidebar } from "apprise-frontend/scaffold/Sidebar"
import { Tab } from "apprise-frontend/scaffold/Tab"
import { Topbar } from "apprise-frontend/scaffold/Topbar"
import { useSettingsDrawer } from "apprise-frontend/settings/hooks"
import { BytestreamedContext } from 'apprise-frontend/stream/BytestreamedHelper'
import { useBytestreams } from "apprise-frontend/stream/api"
import { TagLabel, TagList } from 'apprise-frontend/tag/Label'
import { TimeZoneContext } from "apprise-frontend/time/hooks"
import { useUsers } from "apprise-frontend/user/api"
import { useUtils } from "apprise-frontend/utils/api"
import { through } from "apprise-frontend/utils/common"
import { paramsInQuery, parentIn } from "apprise-frontend/utils/routes"
import { FlowViewer, useFlowCount } from "apprise-messages/VirtualFlow"
import { messageIcon, messagePlural, messageType } from "apprise-messages/constants"
import "apprise-messages/styles.scss"
import { useCurrentAsset, useCurrentParty, useDashboard } from "emaris-frontend/dashboard/hooks"
import { usePublicName, useSubmissionDownload } from "emaris-frontend/layout/SubmissionLayoutPdf"
import { NamedAsset, assetsectionspec, isNamedAsset, sectionAssetOf } from "emaris-frontend/layout/components/AssetSection"
import { NamedRequirement, reqsectionspec, sectionRequirementOf } from "emaris-frontend/layout/components/RequirementSection"
import { AssetParameter, assetspec } from "emaris-frontend/layout/parameters/Asset"
import { useProducts } from "emaris-frontend/product/api"
import { productType } from "emaris-frontend/product/constants"
import { Product } from "emaris-frontend/product/model"
import { NotAssessableLabel } from 'emaris-frontend/requirement/Label'
import { useRequirements } from "emaris-frontend/requirement/api"
import { requirementType } from "emaris-frontend/requirement/constants"
import { Requirement } from "emaris-frontend/requirement/model"
import { useEmarisState } from "emaris-frontend/state/hooks"
import * as React from "react"
import { useTranslation } from "react-i18next"
import { useHistory, useLocation } from "react-router-dom"
import { ArchiveLabel, MutedLabel, SuspendedLabel } from '../Label'
import { useCampaigns } from "../api"
import { useCurrentCampaign } from "../hooks"
import { AssetInstance, Campaign } from "../model"
import { PartyInstance } from "../party/model"
import { useProductInstances } from "../product/api"
import { ProductInstance } from "../product/model"
import { useRequirementInstances } from "../requirement/api"
import { AssessmentFields, AssessmentViewer, useAssessmentDrawer, useAssessmentViewerDrawer, useMissingAssessmentDrawer } from "./Assessment"
import { DateChangeForm, useDateChangeDrawer } from './DateChange'
import { SubmissionForms } from "./Forms"
import { SubmissionHistory } from "./History"
import { SubmissionLabel } from "./Label"
import { SubmissionLifecycle } from "./Lifecycle"
import { ManagedSubmission, useManagedSubmissionDrawer } from './ManagedSubmission'
import { EditableFields, ManagementFields, useEditManagementDrawer, useManagementDrawer } from './Management'
import { TrailSettingsFields } from "./Settings"
import { useSubmissions } from "./api"
import { dateChangeIcon, latestSubmission, manageIcon, rejectIcon, submissionIcon, submissionSingular, submissionType } from "./constants"
import { SubmissionReactContext, useSubmissionContext } from "./hooks"
import { ManagementData, Submission, SubmissionContext, SubmissionWithTrail, Trail, newMissingSubmission, nextSubmission } from "./model"
import { TrailEvent } from './pushevents'
import "./styles.scss"



type ContextProps = {

    trail: Trail

    campaign: Campaign
    asset: AssetInstance
    party: PartyInstance


}

type Props = ContextProps & {

    submission: Submission,
    relatedSubmissions: SubmissionWithTrail[]
    onReload: () => void
}

type Accent = 'current' | 'superseded' | 'missing' | 'na' | 'other'

// puts the submission in form and pushes it down below a context of per-submission data and utilities. 
export const SubmissionDetail = (props: Props) => {

    const { campaign, trail, submission, asset, relatedSubmissions, onReload } = props

    const { t } = useTranslation()

    const submissions = useSubmissions(campaign)

    const initialSubmission = () => {
        if (!submissions.isManagedState(submission)) return submission
        return {...submission, lifecycle : 
            {
                ...submission.lifecycle, 
                compliance: submissions.previousSubmission(submission)!.lifecycle.compliance
            }
        }
    }

    const formstate = useFormState(initialSubmission())

    const { edited, change } = formstate

    const source = submissions.assetSourceFrom(trail)

    const submissionContext: SubmissionContext = { ...props, submission: edited, relatedSubmissions }

    const profile = submissions.profileOf(asset.instanceType);

    const explicitParameters = profile.layoutParameters(asset)

    const { initializeParams } = submissions.inContext(submissionContext)

    // renounce to (little) optimisation here: parameters may fish deep into model and asset, so we can easily run state.
    // eslint-disable-next-line
    //const parameters = React.useMemo(() => initializeParams(explicitParameters), [asset, allAssets, edited.lifecycle.compliance])
    const parameters = initializeParams(explicitParameters)

    const layout: LayoutInstance = {

        ...source.properties.layout,
        parameters,
        state: edited.content,
        documents: source.documents

    }

    const isNew = edited.lifecycle?.created === undefined

    const onChange = change((sub, layout) => { sub.content = layout.state })

    const changeSubscription: PushSubscription<TrailEvent> = {

        topics: [`${trail.key.party}.${submissionType}.${trail.key.asset}`],
        filter: e => campaign.id === e.trail.key.campaign

    }

    return <PushGuard>
        <RemoteChangeGuard singular={t(submissionSingular)} name={isNew ? `new submission` : `submission ${submission.id}`}
            subscriptions={[changeSubscription]}
            //askConsent={dirty}
            onReload={onReload}>

            <SubmissionReactContext.Provider value={submissionContext}  >
                <SubmissionLayoutProvider layout={layout} onChange={onChange}>
                    <InnerDetail state={formstate} layout={layout} reload={onReload} />
                </SubmissionLayoutProvider>
            </SubmissionReactContext.Provider>

        </RemoteChangeGuard>
    </PushGuard>

}

export type SubmissionLayoutConfig = LayoutConfig & {
    dashboard: ReturnType<typeof useDashboard>
}


// takes the layout and builds a context to work with it.
const SubmissionLayoutProvider = (props: React.PropsWithChildren<{

    layout: LayoutInstance
    onChange: (_: Layout) => void

}>) => {

    const { logged } = useUsers()

    const { layout, onChange } = props

    const ctx = useSubmissionContext()

    const dashboard = useDashboard()


    const { campaign, party, asset, submission } = ctx

    // coordinators can unlock readonly components when submission is in a certain state.
    // note: this won't take effect unless the components themselves pass this setting to fields.
    // at the time of writing, only InputBox does this.
    const canUnlock = logged.hasNoTenant() && ctx.isManaged()

    const layoutConfig: SubmissionLayoutConfig = {

        type: asset.instanceType,
        mode: 'live',
        canUnlock,

        campaign,
        party,
        submission

        ,

        dashboard
    }

    return <BytestreamedContext tenant={party.id} target={submission.id}  >
        <LayoutProvider for={`${campaign.id}-${asset.instanceType}-${asset.id}-${submission.id}`} layoutConfig={layoutConfig} layout={layout} onChange={onChange}>
            <ThemeLoader>
                {props.children}
            </ThemeLoader>
        </LayoutProvider>
    </BytestreamedContext>


}



const InnerDetail = (props: { state: FormState<Submission>, layout: LayoutInstance, reload: () => any }) => {

    const { languageSelector, primaryEditBtn, secondaryEditBtns, assessmentMissingBtn, commonBtns, Titlebar, AssessmentForm, AssessmentViewer, ManagementDialog, EditManagementDialog, DateChangeDialog, ManagedSubmissionDialog, openDateChange, Settings } = useDetailComponents(props)

    const { t } = useTranslation()
    const { search } = useLocation()

    const ctx = useSubmissionContext()
    const submissions = useSubmissions(ctx.campaign)
    const { asset } = useCurrentAsset()
    const { party } = useCurrentParty()

    const currentCampaign = useCurrentCampaign()

    const campaigns = useCampaigns()

    const { tab = 'edit' } = paramsInQuery(search)

    const { state } = props

    const { edited, dirty, softReset, initial } = state

    const assetType = asset?.instanceType

    const profiles = {
        [requirementType]: submissions.profileOf(requirementType),
        [productType]: submissions.profileOf(productType),
    }

    const isApplicable = (assetType && party && asset) && (assetType === requirementType ? profiles[requirementType].isForParty(party, asset) : profiles[productType].isForParty(party, asset))

    const { editManagementParam } = useEditManagementDrawer()
    const { datechangeParam } = useDateChangeDrawer()

    const canMessage = submissions.profileOf(ctx.asset.instanceType).canMessage(ctx.asset)

    const pageref = React.useRef<HTMLDivElement>(undefined!)

    //  note: if we navigate from 'latest' to 'latest' (eg from product to linked requirement), browser (or router) forces scroll restoration, seemingly because parametric root is the same.
    // react-ruter v5 shouldn-t be resonsible, but setting window.history.scrollRestoration = 'manual' doesn't stop this either).
    // this fights it explicitly.
    React.useEffect(() => {

        window.scrollTo(0, 0)

    }, [])

    // we do want to remember the scroll when saving first draft though.
    useRememberScroll("new-detail", {
        id: edited.id,          // this keeps the state specific to current draft even if we move to other instance of the same parametric route.
        when: !ctx.isSaved()
    })

    const isEdit = tab === 'edit'

    const readonly = !(ctx.canEdit() && ctx.areSubmissionsAllowed())

    const { flow } = submissions.topicsAndFlow(ctx.trail, edited)

    const flowId = ctx.submission.id

    const unreadMessages = useFlowCount(flowId, flow)
    const unreadBadge = <Badge dot={unreadMessages > 0} className='unread-message-badge'>{t(messagePlural)}</Badge>

    const accent: Accent = edited.lifecycle.state === 'missing' ? 'missing' : !isApplicable ? 'na' : submissions.isLastSubmittedOrAbout(ctx.submission) ? 'current' : submissions.isSupersededOrAbout(ctx.submission) ? 'superseded' : 'other'

    const badge = accent === 'superseded' ? t('submission.labels.old_version_badge') : accent === 'missing' ? t("submission.status.missing") : accent === 'current' ? t('submission.labels.current_version_badge') : accent === 'na' ? t('submission.labels.not_applicable_badge') : undefined

    const accentBadge = (!isApplicable || (isEdit && submissions.isSubmittedorAbout(edited) && accent !== 'other')) && <Label className={`accent-badge accent-${accent}`} mode='tag' icon={submissionIcon} title={badge} />

    return <TimeZoneContext.Provider value={currentCampaign.properties.timeZone}>
        <Page ref={pageref} className={`submission-${tab} accent-${accent}`} readOnly={readonly}>

            <Sidebar>

                {isEdit && primaryEditBtn}
                {isEdit && secondaryEditBtns}

                <br />

                {ctx.canAssessMissing() && <>{assessmentMissingBtn}<br /></>}

                {commonBtns}

                <br />

                <IdProperty id={edited.id} />

                <br />

                <SubmissionLifecycle submission={edited} onDateChange={ctx.canChangeDate() ? openDateChange : undefined} />

            </Sidebar>


            <Topbar>

                {Titlebar}

                <Tab default id='edit' icon={icns.edit} name={t("submission.labels.edit")} />
                <Tab id='history' disabled={dirty} icon={icns.history} name={t("submission.labels.history")} />
                <Tab id={messageType} disabled={dirty} icon={messageIcon} name={unreadMessages > 0 ? <Tooltip title={t('message.unread_count', { count: unreadMessages })}>{unreadBadge}</Tooltip> : unreadBadge} />
                {/* <Tab default id='pdf' icon={icns.edit} name={"PDF"} /> */}

                {accentBadge}

                {isEdit && React.cloneElement(languageSelector, { style: { marginRight: 10 } })}

                {/* WARN!!! ALL BUTTONS ARE ENABLED ONLY IF THE SUBMISSION IS APPLICABLE FOR THE PARTY */}
                {isEdit && React.cloneElement(primaryEditBtn, { enabled: (primaryEditBtn.props.enabled ?? true) && isApplicable })}
                {isEdit && secondaryEditBtns.map(btn => React.cloneElement(btn, { enabled: (btn.props.enabled ?? true) && isApplicable }))}
                {isEdit && ctx.canAssessMissing() ? [React.cloneElement(assessmentMissingBtn, { enabled: (assessmentMissingBtn.props.enabled ?? true) && isApplicable })] : []}
                {isEdit && commonBtns.map(btn => React.cloneElement(btn, { enabled: (btn.props.enabled ?? true) && isApplicable }))}

            </Topbar>


            {tab === `edit` ? <SubmissionForms state={state} />
                // : tab === `pdf` ? <SubmissionPDFForms state={state} layout={props.layout} />
                : tab === 'history' ? <SubmissionHistory onReload={props.reload} />
                    : <FlowViewer id={flowId} flow={flow} multiTenant readOnly={!canMessage || campaigns.isArchived(ctx.campaign) || campaigns.isMuted(ctx.campaign)} />}


            {Settings}
            {AssessmentForm}
            {AssessmentViewer}
            {ManagementDialog}
            {EditManagementDialog}
            {DateChangeDialog}
            {ManagedSubmissionDialog}

            <AdvancedRouteGuard when={dirty} ignoreQueryParams={[editManagementParam, datechangeParam]} onOk={() => softReset(initial)} />


        </Page>
    </TimeZoneContext.Provider>
}

const makeAssetParameterFor = (id: string, type: string, asset: Requirement | Product, alias: string): AssetParameter => ({

    ...newParameterFrom(assetspec),
    bridgeMode: true,
    fixedValue: true,
    id,
    name: alias ?? asset?.name.en?.replace(/\s/g, '-').toLowerCase(),
    components: [],
    data: undefined!,
    value: { id: asset?.id, type }

})


const useDetailComponents = (props: { state: FormState<Submission>, layout: LayoutInstance, reload: () => any }) => {

    const { t } = useTranslation()
    const { l } = useLocale()
    const history = useHistory()

    const { logged } = useUsers()
    const currentLang = useCurrentLanguage()
    const { ask } = useUtils()

    const ctx = useSubmissionContext()
    const submissions = useSubmissions(ctx.campaign)
    const layout = useLayout()
    const appstate = useEmarisState()

    const layoutLang = layout.layoutLanguage ?? currentLang

    const canAssess = ctx.canAssess() || ctx.canAssessMissing()

    const campaigns = useCampaigns()
    const currentCampaign = useCurrentCampaign()

    const isArchivedCampaign = campaigns.isArchived(currentCampaign)

    //const layoutReport = layout.validate()

    // this has to go...
    const requirements = useRequirements()
    const products = useProducts()

    const requirementsinstance = useRequirementInstances(currentCampaign)
    const productinstance = useProductInstances(currentCampaign)
    const { party } = useCurrentParty()

    const assessEmpty = () => ask({
        title: t('submission.consent.assess_missing.title'),
        content: t('submission.consent.assess_missing.msg'),
        okText: t('submission.consent.assess_missing.confirm')
    }).thenRun(() => submissions.saveSubmission(newMissingSubmission(edited), ctx.trail)
        .then(through(() => { if (ctx.isSaved()) layout.removeDescriptors() }))
        .then(saved => { if (ctx.isSaved()) softReset(saved) })
        .then(_ => openAssessment())
    )

    const { SettingsDrawer, settingsButton, closeSettings } = useSettingsDrawer()
    const { ComplianceDrawer, assessBtn, closeCompliance } = useAssessmentDrawer()
    const { assessMissingBtn, openAssessment } = useMissingAssessmentDrawer({ onClick: assessEmpty })
    const { ComplianceViewerDrawer, complianceViewerButton } = useAssessmentViewerDrawer()
    const { ManagementDrawer, openManagement, closeManagement } = useManagementDrawer()
    const { DateChangeDrawer, openDateChange, closeDataChange } = useDateChangeDrawer()
    const { EditManagementDrawer, openEditManagement } = useEditManagementDrawer()
    const { ManagedSubmissionDrawer, openManagedSubmission, closeManagedSubmission } = useManagedSubmissionDrawer()

    const { state, reload } = props

    const { edited, dirty, reset, softReset, initial, change } = state

    const isNew = edited.lifecycle?.created === undefined

    //  tasks

    const save = () => layout.uploadResources()
        .then(() => submissions.saveSubmission(edited, ctx.trail))
        // detail is remounted on first save
        .then(through(() => { if (ctx.isSaved()) layout.removeDescriptors() }))
        .then(saved => { if (ctx.isSaved()) softReset(saved) })

    const changeDate = (date: string) => {

        const modified = { ...edited, lifecycle: { ...edited.lifecycle, lastSubmitted: date } }

        return submissions.changeDate(modified, ctx.trail)
            .then(softReset)
            .then(closeDataChange)
            .then(() => history.push(routeTo(modified.id)))

    }

    const saveCompliance = () => submissions.assessSubmission(edited, ctx.trail).then(softReset).then(closeCompliance)
    const revokeCompliance = () => ask({
        title: t('submission.consent.revoke.title'),
        content: t('submission.consent.revoke.msg'),
        okText: t('submission.consent.revoke.confirm')
    })
        .thenRun(() => submissions.revokeAssessment(
            {
                ...edited, lifecycle: { ...edited.lifecycle, compliance: undefined }
            }, ctx.trail)
            .then(softReset)
            .then(closeCompliance)
        )

    const routeTo = (id: string) => `${parentIn(history.location.pathname)}/${id}`

    const revert = () => reset(initial)
    const remove = () => ask({

        title: t('submission.consent.remove.title'),
        content: t('submission.consent.remove.msg'),
        okText: t('submission.consent.remove.confirm')

    }).thenRun(() => submissions.removeSubmission(edited).then(reload).then(() => history.push(routeTo(latestSubmission))))


    const clone = () => ask({

        title: t('submission.consent.clone.title'),
        content: t('submission.consent.clone.msg'),
        okText: t('submission.consent.clone.confirm'),

    })
        .if(!submissions.isLastSubmittedOrAbout(edited))
        .thenRun(() => {

            Promise.resolve(

                // the "base" for the clone is the last submitted version at or before this one, so might need fetching.
                ctx.isManagedState() ?

                    submissions.fetchOneSubmission(submissions.previousSubmission(edited)!.id) // we know a previous submission must exist.

                    :
                    edited
            )
                .then(nextSubmission)
                .then(clone => submissions.saveSubmission(clone, ctx.trail).then(sub => history.push(routeTo(sub.id))))

        })


    const [managementData, managementDataSet] = React.useState<ManagementData>(undefined!);

    const manage = (data: ManagementData) => { closeManagement(); managementDataSet(data) }

    React.useEffect(() => {

        if (managementData)
            submissions.manageSubmission(edited, ctx.trail, managementData).then(s => history.push(routeTo(s.id)))


        //eslint-disable-next-line
    }, [managementData])

    const namedAssetToInstance = (namedAsset: NamedAsset): AssetInstance | ProductInstance | undefined => {
        const { id, asset, alias, type } = namedAsset
        const resolved = type === requirementType ? requirements.lookup(asset) : products.lookup(asset)
        const param = makeAssetParameterFor(id, type, resolved!, alias)
        const value = assetspec.valueFor(ctx, param, appstate)
        return value.liveData?.instance
    }

    const namedRequirementToInstance = (namedRequirement: NamedRequirement): AssetInstance | ProductInstance | undefined => {
        const { id, alias, requirement } = namedRequirement
        const resolved = requirements.lookup(requirement)
        const param = makeAssetParameterFor(id, requirementType, resolved!, alias)
        const value = assetspec.valueFor(ctx, param, appstate)
        return value.liveData?.instance
    }

    const isForParty = (asset: NamedAsset | NamedRequirement): boolean => {
        const instance = isNamedAsset(asset) ? namedAssetToInstance(asset) : namedRequirementToInstance(asset)
        const type = isNamedAsset(asset) ? asset.type : requirementType
        return (party && instance) ? type === requirementType ? requirementsinstance.isForParty(party, instance) : productinstance.isForParty(party, instance) : true
    }



    const submit = () => {

        // requirement references that have been resolved into requirement submissions.
        const relatedRequirements = ctx.relatedSubmissions.flatMap(s => s.trail.key.asset)

        const unresolved = [
            ...layout.allOf(reqsectionspec).flatMap(c => c.requirements).filter(isForParty).filter(r => sectionRequirementOf(r) && !relatedRequirements.includes(sectionRequirementOf(r)!)),
            ...layout.allOf(assetsectionspec).flatMap(c => c.assets).filter(isForParty).filter(r => sectionAssetOf(r) && !relatedRequirements.includes(sectionAssetOf(r)!))
        ]

        // const errors = layoutReport.errors()


        const confirm = ask({

            title: <span style={{ color: 'red' }}>{t("submission.actions.products.warns.no_req_submissions.title")}</span>,
            content: <div>{t("submission.actions.products.warns.no_req_submissions.contents")} <br /> {
                unresolved.map((r, i) => <span key={i}><b>{l(requirements.safeLookup(sectionAssetOf(r)).name)}</b><br /></span>)}
            </div>

        }).if(unresolved.length > 0)
            // .thenAsk(
            //     {
            //         title: <span style={{ color: 'red' }}>{t('submission.consent.errors.title')}</span>,
            //         content: t('submission.consent.errors.msg', { errors }),
            //         okText: t('submission.consent.errors.confirm'),
            //     }
            // ).if(errors > 0)

        if (ctx.canSubmitManaged() && (!submissions.approveCycle(ctx.trail) || ctx.isPending()))
            confirm.thenRun(openManagedSubmission)

        else
            confirm.thenAsk({
                title: t('submission.consent.pending.title'),
                content: t('submission.consent.pending.msg'),
                okText: t('submission.consent.pending.confirm'),

            }).if(ctx.isDraft() && !!submissions.approveCycle(ctx.trail))
                .thenAsk({
                    title: t('submission.consent.submit.title'),
                    content: t('submission.consent.submit.msg'),
                    okText: t('submission.consent.submit.confirm'),
                }).if((ctx.isDraft() && !submissions.approveCycle(ctx.trail)) || ctx.isPending())

                .thenRun(() => submissions.submit(edited, ctx.trail).then(softReset))

    }

    const submitManaged = (date: string) => submissions.submit({ ...edited, lifecycle: { ...edited.lifecycle, lastSubmitted: date } }, ctx.trail)
        .then(softReset)
        .then(closeManagedSubmission)

    const reject = () => ask({

        title: t('submission.consent.reject.title'),
        content: t('submission.consent.reject.msg'),
        okText: t('submission.consent.reject.confirm')

    }).thenRun(() => submissions.reject(edited, ctx.trail).then(softReset))


    const { downloadSubmissionPdf, generateSubmissionPdf } = useSubmissionDownload(props.layout)


    const publish = () =>

        ask({

            title: t('submission.consent.publish.title'),
            content: t('submission.consent.publish.msg'),
            okText: t('submission.consent.publish.confirm')

        })
            .thenRun(() => submissions.publishSubmission(ctx.submission, ctx.trail).then(s => history.push(routeTo(s.id))))

    const publicName = usePublicName()

    const share = () => ask({

        title: t('submission.consent.share.title'),
        content: t('submission.consent.share.msg'),
        okText: t('submission.consent.share.confirm')

    }).thenRun(() =>

        generateSubmissionPdf().then(blob => blob && submissions.shareSubmission(edited, ctx.trail, blob, publicName, layoutLang).then(softReset)).then(_ => downloadSubmissionPdf(publicName))

    )


    //  buttons
    const downloadBtn = <Button icn={icns.download} key="download" enabledOnReadOnly={ctx.areSubmissionsAllowed()} onClick={() => downloadSubmissionPdf(publicName)}>{t("submission.labels.download")}</Button>



    const submitTitle = submissions.approveCycle(ctx.trail) ?
        (ctx.isDraft() ? "submission.labels.submit_for_approval"
            : ctx.isPending() ? "submission.labels.approve_and_submit" : undefined)
        : undefined

    const submitBtn = <Button key='submit' icn={icns.upload} enabledOnReadOnly={ctx.canSubmit()} onClick={submit}>
        {t(submitTitle ?? "submission.labels.submit")}
    </Button>

    const saveBtn = <Button key='save' enabledOnReadOnly={layout.layoutConfig.canUnlock} icn={icns.save} enabled={(isNew || dirty) && ctx.areSubmissionsAllowed()} onClick={save}>
        {t("common.buttons.save")}
    </Button>

    const viewAssessmentBtn = React.cloneElement(complianceViewerButton, { key: 'view-assessment' })

    const assessmentBtn = React.cloneElement(assessBtn, { key: 'assess', enabledOnReadOnly: ctx.canAssess() })
    const assessmentMissingBtn = React.cloneElement(assessMissingBtn, { key: 'assess-missing', enabledOnReadOnly: ctx.canAssessMissing() })

    const addNewBtn = <Button key='add' enabledOnReadOnly={ctx.areSubmissionsAllowed()} enabled={ctx.canCreate() && ctx.isVersionable()} icn={icns.add} onClick={clone}>
        {t("common.buttons.new_one", { singular: t(submissionSingular) })}
    </Button>

    const rejectBtn = <Button key='reject' icn={rejectIcon} enabledOnReadOnly={ctx.canReject()} onClick={reject}>
        {t("submission.labels.reject")}
    </Button>

    const revertBtn = <Button key='revert' enabledOnReadOnly={ctx.areSubmissionsAllowed()} icn={icns.revert} enabled={dirty} onClick={revert}>
        {t("common.buttons.revert")
        }</Button>

    const removeBtn = <Button key='remove' type="danger" icn={icns.remove} enabledOnReadOnly={buildinfo.development || ctx.canRemove()} onClick={remove}>
        {`${t("common.buttons.remove")} ${(buildinfo.development && !ctx.canRemove()) ? '(Dev)' : ''}`}
    </Button>

    const manageBtn = <Button key="manage" icn={manageIcon} onClick={openManagement} enabledOnReadOnly={ctx.canManage()}>
        {t("submission.labels.manage")}
    </Button>


    const editManagementBtn = <Button key="editMgmt" icn={manageIcon} onClick={openEditManagement} enabledOnReadOnly={ctx.isManaged()}>
        {t("submission.labels.manage_edit")}
    </Button>


    const publishBtn = <Button key="publish" icn={icns.publish} onClick={publish} enabledOnReadOnly={ctx.canPublish()}>
        {t("submission.labels.publish")}
    </Button>

    const shareBtn = <Button key="share" icn={icns.share} onClick={share} enabledOnReadOnly={ctx.canShare()}>
        {t("submission.labels.share")}
    </Button>

    const previousBtn = <Button key='previous' enabledOnReadOnly icn={icns.left} onClick={() => history.push(routeTo(submissions.previous(edited)?.id))} disabled={!submissions.previous(edited)}>
        {t("submission.labels.previous_rev")}
    </Button>

    const nextBtn = <Button key='next' enabledOnReadOnly icn={icns.right} onClick={() => history.push(routeTo(submissions.next(edited)?.id))} disabled={!submissions.next(edited)}>
        {t("submission.labels.next_rev")}
    </Button>

    const changeDateBtn = <Button key='changedate' enabledOnReadOnly={!isArchivedCampaign} icn={dateChangeIcon} onClick={openDateChange}>
        {t("submission.datechange.open_lbl")}
    </Button>

    const primary =

        ctx.isDraft() ?

            dirty || !ctx.isSaved() ? saveBtn : submitBtn

            : ctx.isPending() ? ctx.canSubmit() ? submitBtn : addNewBtn

                : ctx.isSubmitted() ?

                    ctx.isAssessed() ?

                        (ctx.canAssess() || ctx.canAssessMissing()) ? ctx.canManage() ? manageBtn : assessmentBtn

                            : viewAssessmentBtn

                        : (ctx.canAssess() || ctx.canAssessMissing()) ? assessmentBtn

                            : ctx.canManage() ? manageBtn : addNewBtn


                    : ctx.isManaged() ? ctx.canPublish() ?

                        dirty ? saveBtn : publishBtn

                        : viewAssessmentBtn

                        : ctx.canShare() ? shareBtn

                            : ctx.isMissing() ? ctx.canAssess() ? assessmentBtn : addNewBtn

                                : downloadBtn       // this should never happen, if we've covered all cases above.



    let primaryEditBtn = React.cloneElement(primary, { type: 'primary' })

    let secondaryEditBtns = [] as JSX.Element[]

    if (ctx.canAssess())
        secondaryEditBtns.push(assessmentBtn)

    if (ctx.canViewAssessment())
        secondaryEditBtns.push(viewAssessmentBtn)

    if ((ctx.isSubmitted() || ctx.isManagedState() || ctx.isMissing()) && ctx.canCreate())
        secondaryEditBtns.push(addNewBtn)

    if (ctx.isDraft() || (ctx.isManaged() && ctx.canPublish()))
        secondaryEditBtns.push(revertBtn)

    if (ctx.isPending() && ctx.canReject())
        secondaryEditBtns.push(rejectBtn)

    //remove drafts as long as they've been saved. in development, remove also non-draft. */
    if (ctx.isSaved() && (buildinfo.development || ctx.isDraft()))
        secondaryEditBtns.push(removeBtn)

    if (ctx.canShare())
        secondaryEditBtns.push(shareBtn)

    if (ctx.canChangeDate())
        secondaryEditBtns.push(changeDateBtn)

    secondaryEditBtns.push(downloadBtn)
    
    if (ctx.canEditManagementData())
        secondaryEditBtns.push(<br key='sep' />, editManagementBtn)


    secondaryEditBtns = secondaryEditBtns.filter(btn => btn !== primary)

    const commonBtns = [
        React.cloneElement(settingsButton, { key: 'settings', enabledOnReadOnly: true }),
        previousBtn,
        nextBtn


    ]



    //  -------  title


    const asset = submissions.assetFrom(ctx.trail)

    const stream = useBytestreams()

    const assetSource = submissions.assetSourceFrom(ctx.trail)
    const partyName = l(submissions.partySourceFrom(ctx.trail).name)
    const assetName = l(assetSource.name)

    const assetTitle = l(assetSource.description) ?? assetName
    const assetSourceName = l(assetSource.properties?.source?.title)

    const title = logged.managesMultipleTenants() ? `${partyName}: ${assetTitle}` : assetTitle

    const complianceTag = ctx.isAssessable() ?
        (ctx.isSubmitted() || ctx.isAssessed()) && ctx.complianceScale() && <SubmissionLabel displayMode='compliance' submission={edited} />

        :

        <NotAssessableLabel />

    const submittedTag = <>{ctx.isSaved() && <SubmissionLabel displayMode='state-only' tipMode='author' submission={edited} />}{ctx.isSubmitted() && <SubmissionLabel mode='tag' locale={layoutLang} submission={edited} />}</>

    const getPublicationPill = (submission: Submission) => {
        const language = layout.layoutLanguage ?? defaultLanguage
        const sharedBytestream = submission.lifecycle.publication?.[language] ?? submission.lifecycle.publication?.[defaultLanguage]
        return sharedBytestream ? <a download href={stream.linkOf(sharedBytestream)}>
            <TagLabel tag={t("dashboard.labels.shared_documents")} icon={icns.share} noTip className={'share-tag'} />
        </a> : <></>
    }
    const publicationPill = <>{edited.lifecycle.publication && getPublicationPill(edited)}</>

    const pills = (edited.lifecycle.state === 'published' || edited.lifecycle.state === 'managed') ?
        <>{complianceTag}{submittedTag}{publicationPill}</> :
        <>{submittedTag}{complianceTag}</>

    const Title = <Titlebar title={title}>
        <Subtitle className='submission-subtitle'>
            {assetSourceName &&
                <div>
                    <span className='subtitle-label'>{t("common.labels.source")}:</span>&nbsp;{assetSourceName}
                </div>
            }
        </Subtitle>
        <ArchiveLabel campaign={ctx.campaign} />
        <SuspendedLabel campaign={ctx.campaign} />
        <MutedLabel campaign={ctx.campaign} />
        {submissions.profileOf(asset.instanceType).label(asset, { mode: 'tag', deadlineOnly: true, deadlineMode: 'default', format: 'numbers', accentScheme: "weeks", locale: layoutLang })}
        {pills}
        <TagList taglist={asset.tags}></TagList>
    </Titlebar>

    const languageSelector = <LanguageBox className='layout-language-selector' light enabledOnReadOnly onChange={l => layout.changeLayoutLanguage(l)} selectedKey={layout.resolveLanguage()} />

    const AssessmentForm = ctx.complianceScale() &&

        <ComplianceDrawer enabledOnReadOnly={!isArchivedCampaign} readonly={!canAssess}>
            <AssessmentFields state={state} scale={ctx.complianceScale()!} timeliness={ctx.timelinessScale()!} onSave={saveCompliance} onRevokeCompliance={revokeCompliance} />
        </ComplianceDrawer>

    const AssessmentViewerDrawer = ctx.isAssessed() &&

        <ComplianceViewerDrawer enabledOnReadOnly={!isArchivedCampaign}>
            <AssessmentViewer submission={edited} />
        </ComplianceViewerDrawer>


    const ManagementDialog = ctx.canManage() &&

        <ManagementDrawer enabledOnReadOnly={!isArchivedCampaign}>
            <ManagementFields onManage={manage} />
        </ManagementDrawer>

    const EditManagementDialog = ctx.isManaged() &&

        <EditManagementDrawer enabledOnReadOnly={!isArchivedCampaign}>
            <EditableFields data={{ reference: edited.lifecycle.reference }} onChange={change((t, data) => t.lifecycle = { ...t.lifecycle, ...data })} />
        </EditManagementDrawer>

    const DateChangeDialog = ctx.canChangeDate() &&

        <DateChangeDrawer enabledOnReadOnly={!isArchivedCampaign} >
            <DateChangeForm submission={edited} onChange={changeDate} />
        </DateChangeDrawer>


    const ManagedSubmissionDialog = ctx.canSubmitManaged() &&

        <ManagedSubmissionDrawer enabledOnReadOnly={!isArchivedCampaign}>
            <ManagedSubmission submission={edited} onSubmit={submitManaged} />
        </ManagedSubmissionDrawer>

    const report = submissions.validateSubmission(edited)

    const trailstate = useFormState(ctx.trail)

    const Settings = <SettingsDrawer enabledOnReadOnly={!isArchivedCampaign} readonly={!ctx.canChangeSettings() || isArchivedCampaign} warnOnClose={trailstate.dirty}>
        <TrailSettingsFields state={trailstate} onSave={closeSettings} report={report} />
    </SettingsDrawer>


    return { languageSelector, primaryEditBtn, secondaryEditBtns, assessmentBtn, assessmentMissingBtn, commonBtns, Titlebar: Title, Settings, AssessmentViewer: AssessmentViewerDrawer, ManagementDialog, DateChangeDialog, openDateChange, closeDataChange, EditManagementDialog, ManagedSubmissionDialog, AssessmentForm }


}

