import { Placeholder } from 'apprise-frontend/components/Placeholder';
import { iamType } from 'apprise-frontend/iam/constants';
import { IamSlot } from 'apprise-frontend/iam/module';
import { intlapi } from "apprise-frontend/intl/api";
import { Module, moduleRegistry } from "apprise-frontend/module";
import { pushEventType } from 'apprise-frontend/push/constants';
import { noTenant, tenantType } from 'apprise-frontend/tenant/constants';
import { TenantSlot } from 'apprise-frontend/tenant/module';
import { userType } from 'apprise-frontend/user/constants';
import { UserSlot } from 'apprise-frontend/user/module';
import { messageType } from "apprise-messages/constants";
import { Topic } from "apprise-messages/model";
import { MessageSlot } from "apprise-messages/module";
import { TopicResolver } from "apprise-messages/registry";
import { dashboardapi } from "emaris-frontend/dashboard/api";
import { ProductLoader } from 'emaris-frontend/product/Loader';
import { RequirementLoader } from 'emaris-frontend/requirement/Loader';
import { EmarisState } from "emaris-frontend/state/model";
import * as React from 'react';
import { campaignapi } from "../api";
import { productSubmissionActions, requirementSubmissionActions } from './actions';
import { productSubmissionPlural, productSubmissionSingular, productSubmissionType, requirementSubmissionPlural, requirementSubmissionSingular, requirementSubmissionType, submissionIcon, submissionPlural, submissionSingular, submissionType } from "./constants";
import { SubmissionLabel } from "./Label";
import { submissionChangeEvent } from './pushevents';
import { submissionstateapi } from "./state";
import { ProductSubmissionPermissions, RequirementSubmissionPermissions } from './SubmissionPermissions';

export const submissionRequirementModule: Module = {

    icon: submissionIcon,
    type: requirementSubmissionType,

    nameSingular: requirementSubmissionSingular,
    namePlural: requirementSubmissionPlural
    ,

    [userType] : {

        renderIf: ({subjectRange}) => subjectRange?.some(s=>s.tenant!==noTenant),
        permissionComponent: props => <RequirementLoader placeholder={Placeholder.list}>
                                            <RequirementSubmissionPermissions {...props} />
                                      </RequirementLoader>

    } as UserSlot

    ,

    [iamType]: {
        actions: requirementSubmissionActions,
        roles: {}
     
     } as IamSlot

     ,

     [pushEventType] : submissionChangeEvent


    ,

    [tenantType]: {

        tenantResource:true


    } as TenantSlot
}


export const productSubmissionModule: Module = {

    icon: submissionIcon,
    type: productSubmissionType,

    nameSingular: productSubmissionSingular,
    namePlural: productSubmissionPlural

    ,

    [userType] : {

        renderIf: ({subjectRange}) => subjectRange?.some(s=>s.tenant!==noTenant) ,
        permissionComponent: props =>  <ProductLoader placeholder={Placeholder.list}>
                                            <ProductSubmissionPermissions {...props} />
                                      </ProductLoader>
    } as UserSlot

    ,

    [tenantType]: {

        tenantResource:true


    } as TenantSlot

    ,

    [iamType]: {
        actions: productSubmissionActions,
        roles: {}
     
     } as IamSlot
}

export const submissionModule: Module = {

    icon: submissionIcon,
    type: submissionType,


    nameSingular: submissionSingular,
    namePlural: submissionPlural

    ,


    onInit: () => {


        console.log(`init ${requirementSubmissionType} module`)
        console.log(`init ${productSubmissionType} module`)

        moduleRegistry.register(submissionRequirementModule,productSubmissionModule)


    }

    ,

    
    // registers pre-defined modules
    [messageType]: {

        
        resolvers: (): TopicResolver<EmarisState>[] => [

            {
                type: submissionType,
                resolve: (topic: Topic, state: EmarisState) => {

                    const t = intlapi(state).getT()

                    const [cid,tid] = topic.name.split(":")
                    const campaigns = campaignapi(state)
                    const campaign = campaigns.lookup(cid);
                
                    if (!campaign)
                        return undefined
                
                    const submissions = submissionstateapi(state)(campaign);
                                        
                    // const submission = submissions.lookupTrail(tid)?.submissions.find(s=>s.id===sid)
                    const submission = submissions.latestIn(tid)
                
                    if (!submission)
                        return undefined
                
                                    
                    const linkTo = dashboardapi(state).given(campaign).partyRouteToSubmission(submission)
                    const linkToMessage = `${linkTo}?tab=${messageType}`

                    return {

                        label: <SubmissionLabel noTip className='topic-icon' linkTo={linkTo} displayMode='submission' submission={submission} />,
                        tip: <SubmissionLabel displayMode='submission' title={t(submissionSingular).toLowerCase()} submission={submission} noLink noIcon noOptions />,
                        link: linkToMessage

                    }

                }
            }

        ]

    } as MessageSlot
}
