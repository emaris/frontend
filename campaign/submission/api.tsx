import { EmarisState } from "emaris-frontend/state/model"
import { Campaign } from "../model"
import { submissionmodelapi, submissioncontextapi, SubmissionContext } from "./model"
import { profileapi } from "./profile"
import { submissionstateapi } from "./state"
import { submissionvalidationapi } from "./validation"
import { submissionstatisticsapi } from "./statistics/api"
import { useEmarisState } from "emaris-frontend/state/hooks"

export const useSubmissions = (c:Campaign) => submissionapi(useEmarisState())(c)

export const submissionapi = (s:EmarisState) => (c:Campaign) => ({


    ...submissionstateapi(s)(c),
    ...submissionmodelapi(s)(c),
    ...submissionvalidationapi(s),
    ...profileapi(s)(c),
    ...submissionstatisticsapi(s)(c),


    inContext: (ctx:SubmissionContext) => submissioncontextapi(s)(ctx)
    
})