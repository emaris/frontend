import { UnknownLabel } from "apprise-frontend/components/Label";
import { specialise } from 'apprise-frontend/iam/model';
import { Layout } from 'apprise-frontend/layout/model';
import { tagapi } from "apprise-frontend/tag/api";
import { TenantDto } from "apprise-frontend/tenant/model";
import { userapi } from 'apprise-frontend/user/api';
import { productActions } from 'emaris-frontend/product/actions';
import { productapi } from "emaris-frontend/product/api";
import { prodImportanceCategory, productType } from "emaris-frontend/product/constants";
import { ProductDto } from "emaris-frontend/product/model";
import { productmodule } from "emaris-frontend/product/module";
import { requirementActions } from 'emaris-frontend/requirement/actions';
import { requirementapi } from "emaris-frontend/requirement/api";
import { reqImportanceCategory, requirementType } from "emaris-frontend/requirement/constants";
import { RequirementDto } from "emaris-frontend/requirement/model";
import { requirementmodule } from "emaris-frontend/requirement/module";
import { EmarisState } from "emaris-frontend/state/model";
import { campaignapi } from "../api";
import { InstanceLabelProps } from "../InstanceLabel";
import { AssetInstance, Campaign, CampaignInstance } from "../model";
import { PartyInstance } from "../party/model";
import { productinstapi } from "../product/api";
import { ProductInstanceLabel } from "../product/Label";
import { ProductInstance, productinstmodelapi } from "../product/model";
import { requirementinstapi } from "../requirement/api";
import { RequirementInstanceLabel } from "../requirement/Label";
import { RequirementInstance, requirementinstmodelapi } from "../requirement/model";
import { productSubmissionActions, requirementSubmissionActions } from './actions';
import { submissionType } from './constants';


export const profileapi = (s: EmarisState) => (c: Campaign, deps : {
    reqinstapi : ReturnType<ReturnType<typeof requirementinstapi>>,
    prodinstapi : ReturnType<ReturnType<typeof productinstapi>>,
    reqapi : ReturnType<typeof requirementapi>,
    prodapi : ReturnType<typeof productapi>,
    tags : ReturnType<typeof tagapi>,
    users : ReturnType<typeof userapi>
} = {} as any ) => {

    const reqinstapi = () => deps.reqinstapi ?? requirementinstapi(s)(c)
    const prodinstapi = () => deps.prodinstapi ?? productinstapi(s)(c)
    const reqapi = () => deps.reqapi ?? requirementapi(s)
    const prodapi = () => deps.prodapi ?? productapi(s)
    const tags = () => deps.tags ?? tagapi(s)
    const users = () => deps.users ?? userapi(s)

    const self = {

        profileOf: (type: string | undefined) => {

            switch (type) {

                case requirementType: return {

                    module: () => requirementmodule,
                    allSorted: () => reqinstapi().allSorted(),
                    allSources: () => reqapi().all(),
                    sourceOf: (source: string) => reqapi().safeLookup(source),
                    layoutOf: (source: string) => reqapi().safeLookup(source).properties.layout,
                    layoutParameters: (ci: CampaignInstance) => reqapi().safeLookup(ci.source).properties.layout.parameters,
                    allForParty: (party: PartyInstance) => reqinstapi().allForParty(party),
                    isForParty: (party: PartyInstance | TenantDto, ci: CampaignInstance) => reqinstapi().isForParty(party, ci),
                    stringify: (ci: CampaignInstance) => reqinstapi().stringify(ci as RequirementInstance),
                    stringifySource: (source: string) => reqapi().stringify(reqapi().safeLookup(source)),
                    nameOf: (ci: CampaignInstance) => reqinstapi().nameOf(ci as ProductInstance),
                    instance: (source: string) => reqinstapi().safeLookupBySource(source),
                    source: (source: string) => reqapi().safeLookup(source),
                    deadlineOf: (ci: CampaignInstance) => reqinstapi().deadlineOf(ci as RequirementInstance),
                    dueDateOf: (ci: CampaignInstance) => reqinstapi().deadlineDateOf(ci as RequirementInstance),
                    allDueDates: reqinstapi().allDueDatesByRequirementId,
                    label: (ci: CampaignInstance, props?: InstanceLabelProps & { [key: string]: any }) => <RequirementInstanceLabel tipTitle instance={ci as RequirementInstance} linkTarget={submissionType} {...props} />,
                    route: (ci: CampaignInstance) => reqinstapi().route(ci as RequirementInstance),
                    allWeights: () => tags().allTagsOfCategory(reqImportanceCategory),

                    isTemplateManager: (ci: CampaignInstance) => {

                        const { logged } = users()

                        return logged.can(specialise(requirementActions.manage, ci.source))
                    }

                    ,

                    isEditor: (ci: CampaignInstance) => {

                        const { logged } = users()

                        return logged.can(specialise(requirementSubmissionActions.edit, ci.source))
                    }

                    ,


                    isAssessor: (ci: CampaignInstance) => {

                        const { logged } = users()

                        return logged.can(specialise(requirementActions.assess, ci.source))
                    }

                    ,

                    isInUserProfile: (ci: AssetInstance) => {

                        const { logged } = users()

                        return requirementinstmodelapi(s)(c).matches(ci as RequirementInstance, logged)

                    }

                    ,

                    canMessage: (ci: AssetInstance) => {

                        const { logged } = users()

                        return logged.can(specialise(requirementActions.edit, ci.source)) || logged.can(specialise(requirementActions.assess, ci.source)) || logged.can(specialise(requirementSubmissionActions.message, ci.source)) || logged.hasNoTenant()
                    }


                }

                case productType: return {

                    module: () => productmodule,
                    allSorted: () => prodinstapi().allSorted(),
                    allSources: () => prodapi().all(),
                    sourceOf: (source: string) => prodapi().safeLookup(source),
                    layoutOf: (source: string) => prodapi().safeLookup(source).properties.layout,
                    layoutParameters: (ci: CampaignInstance) => prodapi().safeLookup(ci.source).properties.layout.parameters,
                    allForParty: (party: PartyInstance) => prodinstapi().allForParty(party),
                    isForParty: (party: PartyInstance | TenantDto, ci: CampaignInstance) => prodinstapi().isForParty(party, ci),
                    stringify: (ci: CampaignInstance) => prodinstapi().stringify(ci as ProductInstance),
                    stringifySource: (source: string) => prodapi().stringify(prodapi().safeLookup(source)),
                    nameOf: (ci: CampaignInstance) => prodinstapi().nameOf(ci as ProductInstance),
                    instance: (source: string) => prodinstapi().safeLookupBySource(source),
                    source: (source: string) => prodapi().safeLookup(source),
                    label: (ci: CampaignInstance, props?: InstanceLabelProps & { [key: string]: any }) => <ProductInstanceLabel tipTitle instance={ci as ProductInstance} {...props} />,
                    dueDateOf: (ci: CampaignInstance) => prodinstapi().deadlineDateOf(ci as RequirementInstance),
                    deadlineOf: (ci: CampaignInstance) => prodinstapi().deadlineOf(ci as ProductInstance),
                    allDueDates: prodinstapi().allDueDatesByProductId,
                    route: (ci: CampaignInstance) => prodinstapi().route(ci as ProductInstance),
                    allWeights: () => tags().allTagsOfCategory(prodImportanceCategory)

                    ,


                    isTemplateManager: (ci: CampaignInstance) => {

                        const { logged } = users()

                        return logged.can(specialise(productActions.manage, ci.source))
                    }
                    ,

                    isEditor: (ci: CampaignInstance) => {

                        const { logged } = users()

                        return logged.can(specialise(productSubmissionActions.edit, ci.source))
                    }

                    ,

                    isAssessor: (ci: CampaignInstance) => {

                        const { logged } = users()

                        return logged.can(specialise(productActions.assess, ci.source))
                    }

                    ,


                    isInUserProfile: (ci: AssetInstance) => {

                        const { logged } = users()

                        return productinstmodelapi(s)(c).matches(ci as ProductInstance, logged)

                    }

                    ,

                    canMessage: (ci: AssetInstance) => {

                        const { logged } = users()

                        return logged.can(specialise(productActions.edit, ci.source)) || logged.can(specialise(productActions.assess, ci.source)) || logged.can(specialise(productSubmissionActions.message, ci.source)) || logged.hasNoTenant()

                    }

                }

                default: return {

                    module: () => requirementmodule,
                    allSorted: () => [],
                    stringify: () => '',
                    stringifySource: (_: string) => '',
                    nameOf: () => '',
                    allSources: () => [],
                    sourceOf: (_: string) => { },
                    layoutOf: () => undefined! as Layout,
                    layoutParameters: (ci: CampaignInstance) => [],
                    allForParty: (_: PartyInstance) => [],
                    isForParty: (_: PartyInstance | TenantDto, __: CampaignInstance) => false,
                    instance: (_: string) => undefined as unknown as AssetInstance,
                    source: (_: string) => undefined as unknown as RequirementDto | ProductDto,
                    label: (ci: CampaignInstance, props?: InstanceLabelProps & { [key: string]: any }) => <UnknownLabel tip={() => ci.id} {...props} />,
                    deadlineOf: (ci: CampaignInstance) => undefined,
                    dueDateOf: (ci: CampaignInstance) => undefined,
                    allDueDates: () => [],
                    route: (_: CampaignInstance) => campaignapi(s).routeTo(c),
                    getWeight: (tag: string) => tagapi(s).lookupTag(tag).properties.value,
                    allWeights: () => [],

                    isTemplateManager: () => false,
                    isEditor: () => false,
                    isAssessor: () => false,
                    isInUserProfile: () => false,
                    canMessage: () => false
                }
            }
        }


    }

    return self
}