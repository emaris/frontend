import { Icon } from "antd"
import { Button } from "apprise-frontend/components/Button"
import { DrawerProps, RoutableDrawerProps, useRoutableDrawer } from "apprise-frontend/components/Drawer"
import { HtmlSnippet } from "apprise-frontend/components/HtmlSnippet"
import { Form } from "apprise-frontend/form/Form"
import { FormState } from "apprise-frontend/form/hooks"
import { MultiBox } from "apprise-frontend/form/MultiBox"
import { RichBox } from "apprise-frontend/form/RichBox"
import { icns } from "apprise-frontend/icons"
import { useLocale } from 'apprise-frontend/model/hooks'
import { Topbar } from "apprise-frontend/scaffold/Topbar"
import { TagCategory } from "apprise-frontend/tag/model"
import { TagBoxset } from "apprise-frontend/tag/TagBoxset"
import { useTime } from 'apprise-frontend/time/api'
import { defaultTimeFormat } from 'apprise-frontend/time/constants'
import { useUsers } from "apprise-frontend/user/api"
import { useDashboard, useTrail } from "emaris-frontend/dashboard/hooks"
import moment from "moment-timezone"
import * as React from "react"
import { useTranslation } from "react-i18next"
import { RiDeleteBack2Line } from 'react-icons/ri'
import { complianceIcon, complianceType, timelinessType } from "../constants"
import { useCurrentCampaign } from "../hooks"
import { useSubmissions } from './api'
import { useSubmissionContext } from './hooks'
import { Submission, SubmissionDto } from "./model"


type Props = {
    onSave: () => void
    onRevokeCompliance?: () => void
    scale: TagCategory
    timeliness: TagCategory
    state: FormState<Submission>
}

export const AssessmentFields = (props: Props) => {

    const { t } = useTranslation()
    const dashboard = useDashboard()
    const campaign = useCurrentCampaign()
    const submissions = useSubmissions(campaign)


    const { onSave, onRevokeCompliance = () => { }, state, scale, timeliness } = props
    // const { onSave, onRevokeCompliance = () => { }, state, scale } = props

    const users = useUsers()
    const { initial, dirty, reset, change, edited } = state
    const trails = useTrail()

    const trailKey = trails.submissions.allTrails().find(t => t.id === edited.trail)?.key

    const summary = trailKey ? dashboard.summaries.trailSummaryOf(trailKey) : undefined

    const defaultTimeliness = summary ? submissions.defaultTimeliness(summary.computedTimeliness) : undefined

    const revertBtn = <Button icn={icns.revert} enabled={dirty} onClick={() => reset(initial, true)}>{t("common.buttons.revert")}</Button>
    
    const saveBtn = <Button
        type="primary"
        icn={icns.save}
        enabled={dirty && !!edited.lifecycle.compliance?.state}
        onClick={onSave}
    >
        {t("common.buttons.save")}
    </Button>

    const revokeBtn = <Button
        icn={<Icon component={RiDeleteBack2Line} />}
        type="danger"
        onClick={
            onRevokeCompliance
        }>{t("common.labels.revoke")}</Button>

    return <React.Fragment>

        <Topbar autoGroupButtons={false}>

            {saveBtn}
            {revertBtn}
            {initial.lifecycle.compliance?.state && initial.lifecycle.state !== 'missing' && revokeBtn}

        </Topbar>

        <Form className="settings-form" state={state} sidebar>

            <TagBoxset edited={edited.lifecycle.compliance?.state ? [edited.lifecycle.compliance.state] : []}
                type={complianceType}
                categories={[scale]}
                onChange={change((t, v) => {

                    const now = moment().format()

                    t.lifecycle.compliance = { ...t.lifecycle.compliance!, state: typeof v === 'string' ? v : v[0]!, lastAssessed: now, lastAssessedBy: users.logged.username }

                })}
            />

            <TagBoxset edited={edited.lifecycle.compliance?.timeliness ? [edited.lifecycle.compliance.timeliness] : defaultTimeliness ? [defaultTimeliness.id] : []}
                type={timelinessType}
                categories={[timeliness]}
                onChange={change((t, v) => t.lifecycle.compliance = { ...t.lifecycle.compliance!, timeliness: typeof v === 'string' ? v : v[0]!})}
            />

            <RichBox
                // id="compliance_msg"
                label={t("submission.fields.compliance_message.name")}
                height="mid"
                onChange={change((t, v) => t.lifecycle.compliance = { ...t.lifecycle.compliance!, message: v })}
                validation={{ msg: t("submission.fields.compliance_message.msg") }}
            >{edited.lifecycle.compliance?.message}</RichBox>

            <MultiBox
                rte
                id="compliance_official_observation"
                label={t("submission.fields.compliance_official_observation.name")}
                onChange={change((t, v) => t.lifecycle.compliance = { ...t.lifecycle.compliance!, officialObservation: v })}
                validation={{ msg: t("submission.fields.compliance_official_observation.msg") }}
            >{edited.lifecycle.compliance?.officialObservation}</MultiBox>

        </Form>
    </React.Fragment>

}

export const AssessmentViewer = (props: { submission: SubmissionDto }) => {

    const { t } = useTranslation()
    const { l } = useLocale()
    const time = useTime()

    const { submission } = props

    const ctx = useSubmissionContext()
    const submissions = useSubmissions(ctx.campaign)
    const profile = submissions.trailSummary(ctx.trail.key)

    const { name, complianceClass } = submissions.complianceProfile(submission)!
    const { name:timeliness, timelinessClass } = profile.timeliness!
    
    const msg = submission.lifecycle.compliance?.message
    const officialObservation = submission.lifecycle.compliance?.officialObservation

    return <div style={{ marginTop: 10, padding: 30 }} >

        <div className="assessment-label">
            <div className="label-name">{t("submission.assessment.level")}</div>
            <div style={{ marginTop: -5 }} className={`assessment-level compliance ${complianceClass}`}>{l(name)}</div>
        </div>

        <div className="assessment-label">
            <div className="label-name">{t("submission.assessment.timeliness")}</div>
            <div style={{ marginTop: -5 }} className={`timeliness-level timeliness ${timelinessClass}`}>{l(timeliness)}</div>
        </div>

        <div className="assessment-label">
            <div className="label-name">{t("submission.assessment.date")}</div>
            <div>{moment(submission.lifecycle.compliance?.lastAssessed).format(t(defaultTimeFormat))} ({moment(submission.lifecycle.compliance?.lastAssessed).from(time.current())})</div>
        </div>

        <div className="assessment-label">
            <div className="label-name">{t("submission.assessment.comment")}</div>
            <div className="assessment-comment">
            {

                msg ?

                    <HtmlSnippet snippet={msg} />


                    :

                    t("submission.assessment.no_comment")

            }
            </div>
        </div>

        <div className="assessment-label">
            <div className="label-name">{t("submission.assessment.official_observation")}</div>
            <div className="assessment-comment">
            {

                officialObservation ?

                    <HtmlSnippet snippet={l(officialObservation)} />


                    :

                    t("submission.assessment.no_official_observation")

            }
            </div>
        </div>

    </div>

}




export const useAssessmentDrawer = (opts: Partial<RoutableDrawerProps> = {}) => {

    const { t } = useTranslation()

    const { id = 'assessment', title = t("submission.assessment.title"), icon = complianceIcon } = opts

    const { Drawer, open, close, visible, route } = useRoutableDrawer({ id, title, icon })

    const DrawerProxy = (props: Partial<DrawerProps>) => <Drawer width={700} icon={icon} title={title} {...props}>{props.children}</Drawer>

    //eslint-disable-next-line
    const StableProxy = React.useCallback(DrawerProxy, [visible])

    const assessBtn = <Button enabledOnReadOnly icn={icon} linkTo={route()}>{t("submission.assessment.link")}</Button> as JSX.Element
    
    return { ComplianceDrawer: StableProxy, openAssessment: open, complianceRoute: route, closeCompliance: close, visibleCompliance: visible, assessBtn }

}

export const useMissingAssessmentDrawer = (opts: Partial<RoutableDrawerProps & {onClick: () => void}> = {}) => {
    
    const { t } = useTranslation()

    const { ComplianceDrawer, openAssessment, complianceRoute, closeCompliance, visibleCompliance, assessBtn } = useAssessmentDrawer(opts)

    const assessMissingBtn = React.cloneElement(assessBtn, { onClick: opts.onClick, linkTo: undefined, children: t("submission.assessment.assess_missing") })

    return { ComplianceDrawer, openAssessment, complianceRoute, closeCompliance, visibleCompliance, assessMissingBtn }
}

export const useAssessmentViewerDrawer = (opts: Partial<RoutableDrawerProps & { title: string }> = {}) => {

    const { t } = useTranslation()

    const { id = 'assessmentviewer', title = t("submission.assessment.viewer_title"), icon = complianceIcon } = opts

    const { Drawer, open, close, visible, route } = useRoutableDrawer({ id, title, icon })

    //eslint-disable-next-line
    const DrawerProxy = (props: Partial<DrawerProps>) => <Drawer width={700} icon={icon} title={title} {...props}>{props.children}</Drawer>

    //eslint-disable-next-line
    const StableProxy = React.useCallback(DrawerProxy, [visible])

    const complianceViewerButton = <Button enabledOnReadOnly type="primary" icn={icon} linkTo={route()}>{t("submission.assessment.viewer_link")}</Button>

    return {
        ComplianceViewerDrawer: StableProxy,
        openAssessmentViewer: open,
        complianceViewerRoute: route,
        closeComplianceViewer: close,
        visibleComplianceViewer: visible,
        complianceViewerButton
    }
}