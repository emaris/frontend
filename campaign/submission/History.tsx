import { Button } from "apprise-frontend/components/Button"
import { Column, VirtualTable } from "apprise-frontend/components/VirtualTable"
import { icns } from "apprise-frontend/icons"
import { useTime } from "apprise-frontend/time/api"
import { useUsers } from "apprise-frontend/user/api"
import { UserLabel } from "apprise-frontend/user/Label"
import { useFeedback } from 'apprise-frontend/utils/feedback'
import { parentIn } from "apprise-frontend/utils/routes"
import { productType } from "emaris-frontend/product/constants"
import moment from "moment-timezone"
import * as React from "react"
import { useTranslation } from "react-i18next"
import { useLocation } from 'react-router'
import { useHistory } from "react-router-dom"
import { useSubmissions } from './api'
import { latestSubmission, submissionSingular } from "./constants"
import { useContextFactory, useSubmissionContext } from './hooks'
import { SubmissionLabel } from "./Label"
import { Submission, SubmissionStub } from "./model"
 


export const SubmissionHistory = (props:{
    onReload:()=>any

}) => {

    
    const {onReload} = props

    const { t } = useTranslation()
    const { search } = useLocation()
    const history = useHistory()

    const ctx = useSubmissionContext()
    const submissions = useSubmissions(ctx.campaign)
    const ctxfactory = useContextFactory(ctx.trail)

    const users = useUsers()
    const time = useTime()
    const { ask } = useFeedback()

    const now = time.current()

    const routeTo = (id: string) => `${parentIn(history.location.pathname)}/${id}`

    // make a copy not to re-order the original data. 
    const data = submissions.allSubmissions(ctx.trail)

    const openbtn = (s: SubmissionStub) => <Button enabledOnReadOnly key={'opem'} icn={icns.open} linkTo={routeTo(s.id)}>{t("common.buttons.open")}</Button>


    const remove = (s: SubmissionStub) => {

        const nextRoute = ctx.submission.id !== s.id ? ctx.submission.id : submissions.previous(s)?.id ?? submissions.next(s)?.id ?? latestSubmission


        return () => ask({

            title: t('submission.consent.remove.title'),
            content: t('submission.consent.remove.msg'),
            okText: t('submission.consent.remove.confirm')

        }).thenRun(() => submissions.removeSubmission(s).then(onReload).then(() => history.push(`${routeTo(nextRoute)}${search}`)))

    }

    const removebtn = (s: SubmissionStub) => <Button enabledOnReadOnly key={'remove'} enabled={ctxfactory.contextOf(s as Submission).canRemove()} icn={icns.remove} onClick={remove(s)}>{t("common.buttons.remove")}</Button>

    // the most relevant submission is the first, but for parties we exclude the managed ones.
    const mostRelevant = submissions.allSubmissions(ctx.trail).find(s => s.lifecycle.state === 'draft' ||  (users.logged.isTenantUser() ? submissions.isLastSubmitted(s) : submissions.isLastSubmittedOrAbout(s) ))

   
    const RowRenderer = (props) => {
        const {submission, ...rest} = props
        const classes = `${rest.className} ${submission === mostRelevant ? '' : 'accent-grey'}`
        return <div {...rest} className={classes} />
    }

    return <VirtualTable<SubmissionStub> data={data} rowKey='id' selectable={false} filtered={false}
        onDoubleClick={s => history.push(routeTo(s.id))}
        sortBy={[['date', 'desc']]}
        rowClassName={s => s.rowData.id === ctx.submission.id ? 'current-row' : ''}
        actions={a => [openbtn(a), removebtn(a)]} 
        customProperties={({rowData}) => ({submission: rowData})}
        customRowRenderer={RowRenderer}
        >

        <Column<SubmissionStub> flexGrow={1} title={t(submissionSingular)} dataKey="date"
            comparator={submissions.dateComparator}
            cellRenderer={({ rowData: s }) => <SubmissionLabel noDateAccent linkTo={routeTo(s.id)} submission={s}  />} />

        <Column<SubmissionStub> title={t("submission.labels.changed_by")} dataKey="changedBy"
            dataGetter={s => users.lookup(s.lifecycle.lastModifiedBy)?.username}
            cellRenderer={({ rowData: s }) => <UserLabel tip={s.lifecycle.lastModified ? () => moment(s.lifecycle.lastModified).from(now) : undefined} user={s.lifecycle.lastModifiedBy} />} />



        <Column<SubmissionStub> title={t("submission.labels.assessed_by")} dataKey="assessedBy"
            dataGetter={s => users.lookup(s.lifecycle.compliance?.lastAssessedBy)?.username}
            cellRenderer={({ rowData: s }) => s.lifecycle.state !== 'managed' && <UserLabel tip={s.lifecycle.compliance?.lastAssessed ? (() => moment(s.lifecycle.compliance?.lastAssessed).from(now)) : undefined} user={s.lifecycle.compliance?.lastAssessedBy} />} />

        {
        ctx.asset.instanceType === productType &&
            <Column<SubmissionStub> title={t("submission.labels.published_by")} dataKey="publishedBy"
                dataGetter={s => users.lookup(s.lifecycle.lastPublishedBy)?.username}
                cellRenderer={({ rowData: s }) => <UserLabel tip={s.lifecycle.lastPublished ? (() => moment(s.lifecycle.lastPublished).from(now)) : undefined} user={s.lifecycle.lastPublishedBy} />} />
        }

        <Column<SubmissionStub> title={t("submission.labels.compliance_state")} dataKey="assessment"
            dataGetter={s => users.lookup(s.lifecycle.compliance?.state)}
            cellRenderer={({ rowData: s }) => s.lifecycle.compliance?.state && (s.lifecycle.state === 'submitted' || s.lifecycle.state === 'missing') && <SubmissionLabel displayMode='compliance-rating' submission={s} />} />


    </VirtualTable>
}