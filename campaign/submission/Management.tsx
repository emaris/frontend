
import { Icon } from 'antd'
import { Button } from 'apprise-frontend/components/Button'
import { DrawerProps, RoutableDrawerProps, useRoutableDrawer } from 'apprise-frontend/components/Drawer'
import { RouteGuard } from 'apprise-frontend/components/RouteGuard'
import { Paragraph } from 'apprise-frontend/components/Typography'
import { Form } from 'apprise-frontend/form/Form'
import { useFormState } from 'apprise-frontend/form/hooks'
import { useLayout } from 'apprise-frontend/layout/LayoutProvider'
import { ParametricMultiTextBox } from 'apprise-frontend/layout/parameters/ParametricBoxes'
import { stripMultiHtmlTags } from 'apprise-frontend/utils/common'
import { useReferenceParam } from 'emaris-frontend/layout/parameters/Reference'
import * as React from 'react'
import { useTranslation } from 'react-i18next'
import { RiGroup2Fill } from 'react-icons/ri'
import { useSubmissions } from './api'
import { manageIcon } from './constants'
import { useSubmissionContext } from './hooks'
import { ManagementData } from './model'



export type Props = {

    onManage: (_: ManagementData) => any
}





export const ManagementFields = (props: Props) => {

    const { t } = useTranslation()
    const { onManage } = props

    const resolve = useResolveManagementData()
    const [finished,finishedSet] = React.useState(false)

    const ref = useReferenceParam()

    const {edited,dirty,set,reset} = useFormState<ManagementData>(resolve({ reference: ref.value()}))

    React.useEffect(()=>{

        if (finished)
            onManage(resolve(edited))
    })

       
    return <div className="submission-dialog-panel">

        <Paragraph className="submission-dialog-intro">{t("submission.management.introduction")}</Paragraph>

        <div className="submission-dialog-section" style={{ background: 'inherit' }}>

            <Icon style={{ color: 'cadetblue' }} className='submission-dialog-icon' component={RiGroup2Fill} />

            <Paragraph className='submission-dialog-explainer'>{t("submission.management.description")}</Paragraph>
            <Paragraph className='submission-dialog-explainer'>{t("submission.management.description2")}</Paragraph>

            <Button style={{ marginTop: 30 }} type="primary" onClick={() =>{ reset(edited,false); finishedSet(true)}} >
                {t("submission.management.confirm_btn")}
            </Button>

        </div>

        <EditableFields data={edited} onChange={set} />

        <RouteGuard when={dirty} />

    </div>
}


export const useResolveManagementData = () => {

    const { multir } = useLayout()

    return (data: ManagementData) => {

        const { reference } = data

        if (!reference)
            return data

        return { reference: stripMultiHtmlTags(multir(reference)) }

    }

}



export type EditableFieldProps = {

    data: ManagementData,
    onChange: (_: ManagementData) => any
}

// used to first gather management data, and to edit it afterwards
export const EditableFields = (props: EditableFieldProps) => {

    const { t } = useTranslation()
    const { data, onChange } = props

    const ctx = useSubmissionContext()
    const submissions = useSubmissions(ctx.campaign)

    const last = submissions.previousReference(ctx.trail,ctx.submission)

    const { ll } = useLayout()

    const resolve = useResolveManagementData()

    return <div className="submission-dialog-panel">

        <div className="submission-dialog-section">

            <div className='submission-dialog-section-title'>{t("submission.reference.title")}</div>

            <Paragraph className='submission-dialog-explainer'>{t('submission.reference.explainer')}</Paragraph>
            
            <Paragraph className='submission-dialog-explainer' style={{ paddingTop: 10 }}>{
                last ? <div>
                        <div>{t('submission.reference.last_reference')}</div>
                        <div style={{paddingTop: 2, fontStyle:'normal', fontWeight:400}}>{ll(last)}</div>
                        </div> 
                        : 
                        t('submission.reference.first_reference')}
            </Paragraph>

            <Form>
                <ParametricMultiTextBox id='reference' noToolbar style={{ minWidth: 350 }} onChange={reference=>onChange(resolve({...data,reference}))}>
                    {data.reference}
                </ParametricMultiTextBox>
            </Form>

        </div>
    </div>
}



export const useManagementDrawer = (opts: Partial<RoutableDrawerProps> = {}) => {

    const { t } = useTranslation()

    const { id = 'management', title = t("submission.management.title"), icon = manageIcon } = opts

    const { Drawer, open, close, visible, route, param } = useRoutableDrawer({ id, title, icon })

    const DrawerProxy = (props: Partial<DrawerProps>) => <Drawer width={500} icon={icon} title={title} {...props} />

    //eslint-disable-next-line
    const StableProxy = React.useCallback(DrawerProxy, [visible])


    return { ManagementDrawer: StableProxy, openManagement: open, managementRoute: route, closeManagement: close, managementParam:  param }

}

export const useEditManagementDrawer = (opts: Partial<RoutableDrawerProps> = {}) => {

    const { t } = useTranslation()

    const { id = 'edit-management', title = t("submission.management.title"), icon = manageIcon } = opts

    const { Drawer, open, close, visible, route, param } = useRoutableDrawer({ id, title, icon })

    const DrawerProxy = (props: Partial<DrawerProps>) => <Drawer width={500} icon={icon} title={title} {...props} />

    //eslint-disable-next-line
    const StableProxy = React.useCallback(DrawerProxy, [visible])


    return { EditManagementDrawer: StableProxy, openEditManagement: open, editManagementRoute: route, closeEditManagement: close, editManagementParam: param  }

}