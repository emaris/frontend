import { PushEventSlot } from 'apprise-frontend/push/module';
import { userapi } from 'apprise-frontend/user/api';
import { EmarisState } from 'emaris-frontend/state/model';
import { campaignapi } from '../api';
import { submissionapi } from './api';
import { submissionType } from './constants';
import { Trail } from './model';
import { debounce } from 'lodash'

export type  TrailEvent = {

   trail:Trail
}

const statcompDebounceDelayInSeconds = 180

export const submissionChangeEvent  : PushEventSlot<EmarisState,TrailEvent> = {



     onSubcribe:  (state) => {

        console.log("subscribing for submission changes")

        const { logged } = userapi(state)

        return [{

            topics: logged.hasNoTenant() ? [`*.${submissionType}.>`] : [`${logged.tenant}.${submissionType}.>`]
            
            ,
            
            onEvent: debounce( (event, state) => {

               console.log("received external change about trail" ,event.trail)

                const campaign = campaignapi(state).safeLookup(event.trail.key.campaign)

                submissionapi(state)(campaign).replaceTrail(event.trail)
             
             }, statcompDebounceDelayInSeconds * 1000, {leading:true})

        }]
     }

}