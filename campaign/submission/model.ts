

// trail of submissions for a source by party in a campaign.

import { LayoutInstanceState } from "apprise-frontend/layout/model"
import { Parameter } from 'apprise-frontend/layout/parameters/model'
import { layoutRegistry } from "apprise-frontend/layout/registry"
import { Lifecycle } from "apprise-frontend/model/lifecycle"
import { MultilangDto } from "apprise-frontend/model/multilang"
import { tagapi } from "apprise-frontend/tag/api"
import { Tag } from "apprise-frontend/tag/model"
import { tenantapi } from "apprise-frontend/tenant/api"
import { noTenant } from 'apprise-frontend/tenant/constants'
import { tenantstateapi } from 'apprise-frontend/tenant/state'
import { userapi } from "apprise-frontend/user/api"
import { compareDates } from "apprise-frontend/utils/common"
import { Flow, Topic } from "apprise-messages/model"
import { isLiveParameter } from "emaris-frontend/layout/parameters/model"
import { productapi } from "emaris-frontend/product/api"
import { requirementapi } from "emaris-frontend/requirement/api"
import { requirementType } from 'emaris-frontend/requirement/constants'
import { EmarisState } from "emaris-frontend/state/model"
import { partition } from "lodash"
import shortid from "shortid"
import { productType } from "../../product/constants"
import { campaignapi } from "../api"
import { anyPartyTopic } from "../constants"
import { AssetInstance, Campaign, campaignmodelapi } from "../model"
import { partyinstapi } from "../party/api"
import { PartyInstance, partyinstmodelapi } from "../party/model"
import { partyinststateapi } from "../party/state"
import { productinstapi } from '../product/api'
import { requirementinstapi } from '../requirement/api'
import { submissionapi } from "./api"
import { submissionType } from "./constants"
import { profileapi } from "./profile"
import { ComplianceCache, submissionstateapi, TimelinessCache } from "./state"



export type TrailKey = {

    campaign: string
    party: string
    asset: string
    assetType: string
}

export type TrailDto = {

    // opaque id for references.
    id: string

    // business key for queries.
    key: TrailKey

    submissions: SubmissionStub[]

    properties: Partial<TrailSettings>

}

export const newTrail = (key: TrailKey): Trail => ({ id: newTrailId(), key, submissions: [], properties: {} })

export type TrailSettings = {

    approveCycle: boolean
    adminCanEdit: boolean
    adminCanSubmit: boolean
}

//  may diverge in the future
export type Trail = TrailDto


export const newTrailId = () => `TR-${shortid()}`


// 'cheap' part of the model.
export type SubmissionStub = {

    id: string
    trail: string

    lifecycle: SubmissionLifecycle
}

export type SubmissionLifecycle = Lifecycle<SubmissionStatus> & {

    lastSubmitted?: string

    lastApprovedBy?: string

    lastPublished: string

    lastPublishedBy?: string

    compliance?: ComplianceState

    reference?: MultilangDto

    publication?: MultilangDto

}


export type ManagementData = Partial<{

    reference: MultilangDto

}>

export type PublicationData = {

    revision: string

}


export type SubmissionWithTrail = {

    submission: Submission
    trail: Trail
}

export type ComplianceState = {
    state: string | undefined
    timeliness: string | undefined
    message: string | undefined
    officialObservation: MultilangDto | undefined
    lastAssessed: string | undefined
    lastAssessedBy: string | undefined
}

export type ComplianceProfile = {

    name: MultilangDto
    rate: number
    allNames: MultilangDto[]
    complianceClass: string
    complianceObservation: MultilangDto | undefined
    weight?: number
    tag?: Tag
    code?: string

}

export type TimelinessProfile = {

    name: MultilangDto
    allNames: MultilangDto[]
    tag?: Tag
    code?: string
    timelinessClass: string

}

export type ComplianceScale = {

    names: MultilangDto[],
    min: number,
    max: number,
    tags: Tag[]

}

export type TimelinessScale = ComplianceScale

export type ComplianceProfiles = (Tag & { complianceClass: string })[]
export type TimelinessProfiles = (Tag & { timelinessClass: string })[]

export type SubmissionStatus = "draft" | "pending" | "submitted" | "managed" | "published" | "missing"


export const managedStates: SubmissionStatus[] = ["managed", "published"]
export const submissionStates: SubmissionStatus[] = ["draft", "pending", "submitted", "missing", ...managedStates]

export type SubmissionDto = SubmissionStub & {

    content: LayoutInstanceState

}

export type Submission = SubmissionDto

export const newSubmissionId = () => `SUB-${shortid()}`

export const newSubmission = (trail?: string) => ({ id: newSubmissionId(), trail: trail, lifecycle: { state: 'draft', created: undefined }, content: { data: {}, resources: {} } }) as Submission

export const newMissingSubmission = (submission: Submission): Submission => ({ ...submission, lifecycle: { ...submission.lifecycle, state: 'missing' } })

export const nextSubmission = (submission: Submission) => ({

    ...newSubmission(submission.trail),

    content: {
        data: submission.content.data,
        resources: submission.content.resources
    }
})




// bundles a submission and the context obtained from its trail.
export type SubmissionContext = {

    submission: Submission
    relatedSubmissions: SubmissionWithTrail[]

    trail: Trail

    campaign: Campaign
    party: PartyInstance
    asset: AssetInstance

}


export const submissionmodelapi = (s: EmarisState) => (c: Campaign) => {

    const profile = () => (trail: Trail) => profileapi(s)(c).profileOf(trail.key.assetType)
    const partyinstances = partyinststateapi(s)(c)

    const self = {


        isManagedState: (submission: SubmissionStub) => submission.lifecycle.state && managedStates.includes(submission.lifecycle.state)

        ,

        partyFrom: (trail: Trail) => partyinstapi(s)(c).safeLookupBySource(trail.key.party)

        ,

        assetFrom: (trail: Trail) => profile()(trail).instance(trail.key.asset)

        ,

        assetSourceFrom: (trail: Trail) => profile()(trail).source(trail.key.asset)

        ,

        partySourceFrom: (trail: Trail) => tenantapi(s).safeLookup(trail.key.party)

        ,

        approveCycle: (trail: Trail, noDelegation?: boolean) => trail.properties?.approveCycle ?? (noDelegation ? undefined : self.approveCycleDefault(trail))

        ,

        // lookup party first, then campaign, then tenant.
        approveCycleDefault: (trail: Trail) => {

            const setting = partyinstmodelapi(s)(c).approveCycle(self.partyFrom(trail)!)

            return setting === 'all' || setting === trail.key.assetType


        }
        ,

        setApproveCycle: (trail: Trail, value: boolean | undefined) => {

            if (value === undefined)
                delete trail.properties.approveCycle
            else trail.properties.approveCycle = value

        }

        ,

        canEdit: (trail: Trail, noDelegation?: boolean) => trail.properties.adminCanEdit ?? partyinstances.lookupBySource(trail.key.party)?.properties.settings.adminCanEdit ?? (noDelegation ? undefined : self.canEditDefault(trail))

        ,

        canEditDefault: (trail: Trail) => partyinstmodelapi(s)(c).canEditFor(trail.key.party)

        ,


        setCanEdit: (trail: Trail, value: boolean | undefined) => {

            if (value === undefined)
                delete trail.properties.adminCanEdit

            else trail.properties.adminCanEdit = value

        }


        ,

        canSubmit: (trail: Trail, noDelegation?: boolean) => trail.properties.adminCanSubmit ?? partyinstances.lookupBySource(trail.key.party)?.properties.settings.adminCanSubmit ?? (noDelegation ? undefined : self.canSubmitDefault(trail))

        ,

        canSubmitDefault: (trail: Trail) => partyinstmodelapi(s)(c).canSubmitFor(trail.key.party)

        ,

        setCanSubmit: (trail: Trail, value: boolean | undefined) => {

            if (value === undefined)
                delete trail.properties.adminCanSubmit
            else trail.properties.adminCanSubmit = value

        }

        ,

        allComplianceProfiles: (): ComplianceProfiles => {

            const complianceScale = campaignmodelapi(s).currentComplianceScale(c)

            if (!complianceScale)
                return []

            const orderedtags = tagapi(s).allTagsOfCategory(complianceScale.id).sort((t1, t2) => t1.properties.value - t2.properties.value)
            const orderedValues = orderedtags.map(t => t.properties.value)

            const floor = orderedValues.shift()
            const ceiling = orderedValues.pop()
            const range = ceiling - floor

            return orderedtags.map(tag => {
                const normalized = (tag.properties.value ?? floor) - floor
                const rate = normalized * 100 / range
                const complianceClassNumber = Math.floor(rate / 33 + 1)

                return { ...tag, complianceClass: `compliance-${complianceClassNumber}` }
            })
        }

        ,

        allTimelinessProfiles: (): TimelinessProfiles => {

            const timelinessScale = campaignmodelapi(s).currentTimelinessScale(c)

            if (!timelinessScale)
                return []

            const orderedtags = tagapi(s).allTagsOfCategory(timelinessScale.id).sort((t1, t2) => t1.properties.value - t2.properties.value)

            return orderedtags.map(tag => {
                const value = tag.properties.value
                const timelinessClass = !isNaN(value) ? value < 0 ? 'timeliness-late' : 'timeliness-ontime' : 'timeliness-plain'
                return { ...tag, timelinessClass }
            })
        }

        ,


        complianceProfile: (submission: SubmissionStub, instance?: AssetInstance, complianceCache?: ComplianceCache): ComplianceProfile | undefined => {

            const cache = complianceCache ?? submissionstateapi(s)(c).complianceCache()

            const asset = instance ?? (() => {

                const trail = submissionapi(s)(c).allTrails().find(t => t.id === submission.trail)
                return profileapi(s)(c).profileOf(trail?.key.assetType).instance(trail?.key.asset!)

            })()

            if (!asset)
                return undefined

            const scale = cache.scale

            if (!scale)
                return undefined

            const { tags, min, max, names } = scale

            const weights = cache[asset.instanceType].weights

            if (!submission.lifecycle.compliance)
                return undefined

            const state = submission.lifecycle.compliance.state
            const complianceObservation = submission.lifecycle.compliance.officialObservation

            if (!state)
                return undefined

            const tag = tags.find(t => t.id === state)

            if (!tag)
                return undefined

            const value = tag.properties.value

            let rate, complianceClass

            if (!isNaN(value)) {

                const normalized = value - min
                rate = normalized * 100 / (max - min)
                const group = Math.floor(rate / 33.1 + 1)
                complianceClass = `compliance-${group}`
            }

            const weight = weights.find(tag => asset.tags.includes(tag.id))?.properties.value || 1

            const profile = { allNames: names, tag, name: tag.name, code: tag.code, weight, rate, complianceClass, complianceObservation }

            return profile
        }

        ,


        timelinessProfile: (submission: SubmissionStub, instance?: AssetInstance, timelinessCache?: TimelinessCache, computedTimeliness?: 'ontime' | 'late' | 'notdue'): TimelinessProfile | undefined => {

            const cache = timelinessCache ?? submissionstateapi(s)(c).timelinessCache()

            const asset = instance ?? (() => {

                const trail = submissionapi(s)(c).allTrails().find(t => t.id === submission.trail)
                return profileapi(s)(c).profileOf(trail?.key.assetType).instance(trail?.key.asset!)

            })()

            if (!asset)
                return undefined

            const scale = cache.scale

            if (!scale)
                return undefined

            const { tags, names } = scale

            const state = submission.lifecycle.compliance?.timeliness ?? self.defaultTimeliness(computedTimeliness, cache.scale?.tags)?.id

            if (!state)
                return undefined

            const tag = tags.find(t => t.id === state)

            if (!tag)
                return undefined

            const value = tag.properties.value

            const timelinessClass = !isNaN(value) ? value < 0 ? 'timeliness-late' : 'timeliness-ontime' : 'timeliness-plain'

            const profile = { allNames: names, tag, name: tag.name, code: tag.code, timelinessClass }

            return profile
        }

        ,

        complianceScale: () => {


            const complianceScale = campaignmodelapi(s).currentComplianceScale(c)

            if (!complianceScale)
                return undefined

            const tags = tagapi(s).allTagsOfCategory(complianceScale.id)
            const sorted = tags.filter(t => !isNaN(t.properties.value)).sort((t1, t2) => t1.properties.value - t2.properties.value)
            const values = sorted.map(t => t.properties.value)

            return {
                names: sorted.map(v => v.name),
                min: values.shift(),
                max: values.pop(),
                tags
            }


        }
        
        ,

        timelinessScale: (cache?: Tag[]) => {


            const timelinessScale = campaignmodelapi(s).currentTimelinessScale(c)

            if (!timelinessScale)
                return undefined

            const tags = cache ?? tagapi(s).allTagsOfCategory(timelinessScale.id)
            const sorted = tags.filter(t => !isNaN(t.properties.value)).sort((t1, t2) => t1.properties.value - t2.properties.value)
            const values = sorted.map(t => t.properties.value)

            return {
                names: sorted.map(v => v.name),
                min: values.shift(),
                max: values.pop(),
                tags
            }


        }

        ,

        defaultTimeliness: (timeliness: string | undefined, cache?: Tag[]): Tag | undefined => {
            if (timeliness === undefined) return undefined

            const timelinessScale = self.timelinessScale(cache)
                
            if (!timelinessScale)
                return undefined

            let scaleValue
            if (timeliness === "ontime") scaleValue = timelinessScale?.max
            if (timeliness === "late") scaleValue = timelinessScale?.min

            if (scaleValue === undefined)
                return undefined

            return timelinessScale.tags.find(t => t.properties.value === scaleValue)
        }

        ,

        complianceProfileOld: (submission: SubmissionStub): ComplianceProfile | undefined => {

            const complianceScale = campaignmodelapi(s).currentComplianceScale(c)

            if (!complianceScale)
                return undefined

            const state = submission.lifecycle.compliance?.state

            if (!state)
                return undefined


            const tag = tagapi(s).lookupTag(state)

            // Partition the tags in complianceScaleCategory between ones with value and ones with no value
            // the ones with value will be used for computation
            // the ones with no value will be used to check if the current selected compliance should not be calculated (e.g. not applicable)
            const [withNoValueTags, withValueTags] = partition(tagapi(s).allTagsOfCategory(complianceScale.id), t => isNaN(t.properties.value))

            const orderedtags = withValueTags.sort((t1, t2) => t1.properties.value - t2.properties.value)
            const allNames = orderedtags.map(v => v.name)

            //weight
            const trail = submissionapi(s)(c).allTrails().find(t => t.id === submission.trail)
            const currentprofile = profile()(trail!)
            const weightTags = currentprofile.allWeights()
            const asset = currentprofile.source(trail?.key.asset!)
            const weightTag = weightTags.filter(tag => asset.tags.includes(tag.id))[0] || ""
            const weight = weightTag.properties.value

            // rate
            const orderedValues = orderedtags.map(t => t.properties.value)
            const floor = orderedValues.shift()
            const ceiling = orderedValues.pop()
            const range = ceiling - floor
            const normalized = (tag.properties.value ?? floor) - floor
            const rate = normalized * 100 / range
            const complianceClassNumber = Math.round(rate / 33 + 1)

            // If we don't need a rate because the current state is with no value (e.g. not applicable)
            return withNoValueTags.find(t => t.id === state) ?
                {
                    name: tag.name,
                    rate: undefined!,
                    allNames,
                    complianceClass: '',
                    complianceObservation: submission.lifecycle.compliance?.officialObservation,
                    weight,
                    tag
                }
                :
                {
                    name: tag.name,
                    rate,
                    allNames,
                    complianceClass: `compliance-${complianceClassNumber}`,
                    complianceObservation: submission.lifecycle.compliance?.officialObservation,
                    weight,
                    tag
                }
        }
        ,

        // most recent last.
        dateComparator: (o1: SubmissionStub, o2: SubmissionStub) => {

            const isMissing = (o1: SubmissionStub, o2: SubmissionStub) => o1.lifecycle.state === 'missing' || o2.lifecycle.state === 'missing'

            const comparison = isMissing(o1, o2) ? 0 : compareDates(o1.lifecycle.lastSubmitted, o2.lifecycle.lastSubmitted, false)

            return comparison === 0 ? self.stateComparator(o1, o2) : comparison

        }


        ,

        // compare by state in workflow order, missing state always down, and by latest 
        stateComparator: (o1: SubmissionStub, o2: SubmissionStub) => {

            const comparison = o1.lifecycle.state === 'missing' ? -1 : o2.lifecycle.state === 'missing' ? 1 : (o1.lifecycle.state ? submissionStates.indexOf(o1.lifecycle.state!) : 0) - (o2.lifecycle.state ? submissionStates.indexOf(o2.lifecycle.state) : 0)

            return comparison === 0 ? compareDates(o1.lifecycle.lastModified, o2.lifecycle.lastModified) : comparison
        }

        ,

        // most recent first, including drafts.
        dateComparatorDesc: (o1: SubmissionStub, o2: SubmissionStub) => self.dateComparator(o2, o1)

        ,


        // topics: (sub:SubmissionStub) : Topic[] => [{ type: submissionType, name:`${c.id}:${sub.trailId}:${sub.id}` }]

        topics: (sub: SubmissionStub): Topic[] => [{ type: submissionType, name: `${c.id}:${sub.trail}` }]


        ,

        topicsAndFlow: (trail: Trail, sub: Submission) => {

            const campaignTopics = campaignapi(s).topics(c)
            const party = self.partyFrom(trail)
            const partytopics = partyinstapi(s)(c).topics(party)
            const asset = self.assetFrom(trail)
            const assetTopics = asset.instanceType === requirementType ? requirementinstapi(s)(c).topics(asset) : productinstapi(s)(c).topics(asset)
            const submissionTopics = self.topics(sub)

            const trailTopics = [...campaignTopics, ...partytopics, ...assetTopics]
            const topics = [...trailTopics, ...submissionTopics]

            const profile = profileapi(s)(c).profileOf(asset.instanceType)

            const partySource = tenantstateapi(s).safeLookup(party.source)
            const assetSource = profile.source(asset.source)

            
            // as long as we don't have a first submission we work without submission topics as identifiers will change when a first draft is submitted.
            // we use instead trail topics and post/reply with those.
            // when we have a first submission, we shift to use submission topics as trail identifiers are now stable.
            // trail topics now cannot be posted or replied with, and the match on them becames strict in principle.

            const submissionExists = trail.submissions.length > 0

            const flow: Flow = {
                scope: c.id,
                recipient: party.source,

                mailProfile: () => ({

                    targets: [noTenant,party.source],
                    parameters:{
                        campaign: c.name,
                        campaignId: c.id,
                        tenant: partySource.name,
                        tenantId: partySource.id,
                        asset: assetSource.name,
                        assetId: assetSource.id,
                        assetType: asset.instanceType,
                        assetTitle: assetSource.description
                    }
                })
                ,
                channels: [

                    {
                        name: "main",
                        channel: {
                            read: topics,
                            noPost: !submissionExists,
                            noReply: !submissionExists
                        }
                    }
                    ,

                    {
                        name: "trail",
                        channel: {
                            read: trailTopics,
                            mode: submissionExists ? 'strict' : 'lax',
                            noPost: submissionExists,
                            noReply: submissionExists
                        }
                    }

                    ,
                    {
                        name: "broadcast",
                        channel: {
                            read: [...campaignTopics, anyPartyTopic, ...assetTopics],
                            mode: 'strict',
                            noPost: true,
                            write: trailTopics
                        }
                    }

                ]
            }

            return { topics: submissionExists ? topics : trailTopics, flow }
        }
    }


    return self
}



export const submissioncontextapi = (s: EmarisState) => (ctx: SubmissionContext) => {


    const { submission, trail, campaign, party, asset } = ctx

    const model = submissionmodelapi(s)(campaign)
    const state = submissionstateapi(s)(campaign)
    const campaigns = campaignapi(s)


    const { logged } = userapi(s)

    const P = profileapi(s)(campaign).profileOf(asset.instanceType)

    const isTemplateManager = () => P.isTemplateManager(asset)
    const isEditor = () => isTemplateManager() || P.isEditor(asset)
    const isValidator = () => isTemplateManager()
    const isAssessor = () => (isTemplateManager() || P.isAssessor(asset)) && logged.hasNoTenant()
    const isManager = () => isTemplateManager() && logged.hasNoTenant()
    const isPublisher = () => isTemplateManager() && logged.hasNoTenant()
    const isPartyManager = () => logged.isManagerOf(party.source)

    const isEditingAllowed = () => logged.isTenantUser() || model.canEdit(trail)
    const isSubmittingAllowed = () => logged.isTenantUser() || model.canSubmit(trail)

    const isUserProfileValid = () => P.isInUserProfile(asset)

    const isCampaignSuspended = () => logged.isTenantUser() && campaigns.isCampaignSuspended(campaign)
    const isCampaignArchived = () => campaigns.isArchived(campaign)


    const inAudience = () => {
        const audienceCheck = asset.audience ? tagapi(s).expression(asset.audience).matches(party) : true
        const audienceListCheck = asset.audienceList ? !asset.audienceList.excludes.includes(party.source) || asset.audienceList.includes.includes(party.source) : true
        return audienceCheck && audienceListCheck
    }



    const self = {

        ...ctx

        ,

        isSaved: () => !!submission.lifecycle.created,
        isDraft: () => submission.lifecycle.state === 'draft',
        isPending: () => submission.lifecycle.state === 'pending',
        isSubmitted: () => submission.lifecycle.state === 'submitted',
        isManaged: () => submission.lifecycle.state === 'managed',
        isPublished: () => submission.lifecycle.state === 'published',
        isManagedState: () => self.isManaged() || self.isPublished(),
        isMissing: () => submission.lifecycle.state === 'missing',
        isEditable: () => logged.hasNoTenant() || (asset.properties.editable ?? true),
        isVersionable: () => logged.hasNoTenant() || (asset.properties.versionable ?? true),
        isAssessable: () => asset.properties.assessed ?? true,
        isAssessed: () => !!submission.lifecycle.compliance?.state,
        isAssessor: () => isAssessor

        ,

        getAssetSource: (asset: AssetInstance) => asset.instanceType === requirementType ? requirementapi(s).safeLookup(asset.source) : productapi(s).safeLookup(asset.source)

        ,

        complianceScale: () => campaignapi(s).currentComplianceScale(campaign)

        ,

        timelinessScale: () => campaignapi(s).currentTimelinessScale(campaign)

        ,

        canChangeSettings: isPartyManager

        ,

        // can edit drafts if the user and its tenant meets some criteria based on permissions, profiles, and audience.
        canEdit: () => self.isDraft() && self.isEditable() && isEditingAllowed() && (isPartyManager() || (isEditor() && isUserProfileValid())) && inAudience()


        ,


        canSubmit: () => !self.isSubmitted() && isSubmittingAllowed() && isValidator() && self.areSubmissionsAllowed()

        ,


        canSubmitManaged: () => isManager() && self.canSubmit()

        ,

        canReject: () => self.isPending() && isSubmittingAllowed() && isValidator() && self.areSubmissionsAllowed()


        ,

        canCreate: () => isEditor() && isEditingAllowed() && state.latestIn(trail.id)?.lifecycle.state !== 'draft' && state.latestIn(trail.id)?.lifecycle.state !== 'pending'

        ,

        // can remove drafts, submitted under admin privileges, published, or managed without published, conditionally to privileges and settings.
        canRemove: () => (self.isDraft() && isEditingAllowed() && isValidator()) || self.isPublished() 
        
            || 
        
            (logged.isAdmin() && (self.isSubmitted() || self.isMissing()) && !state.allSubmissions(trail).some(s => s.lifecycle.lastSubmitted === submission.lifecycle.lastSubmitted && s.lifecycle.state === 'managed')) 
            
            ||

            (self.isManaged() && !state.allSubmissions(trail).some(s => s.lifecycle.lastSubmitted === submission.lifecycle.lastSubmitted && s.lifecycle.state === 'published'))

        ,

        // can be assessed if it has been submitted and it's either the latest submission or the latest is still in progresss. plus privileges.
        canAssess: () => self.isAssessable() && self.complianceScale() && (self.isSubmitted() || self.isMissing()) && (state.isLatest(submission) || state.latestIn(trail)?.lifecycle.state !== 'submitted') && isAssessor() && !isCampaignArchived()

        ,

        // can assess as missing only as the first submission (not necessarily the first revision or the last revision)/
        canAssessMissing: () => self.isAssessable() && self.complianceScale() && isAssessor() && !state.latestSubmission(trail) && !isCampaignArchived()

        ,

        canViewAssessment: () => self.isAssessed() && (logged.isTenantUser() || self.isManaged() || self.isPublished())

        ,

        areSubmissionsAllowed: () => !isCampaignSuspended() && !isCampaignArchived()

        , 

        // a submitted product can be managed, given appropriate rights and provided it's the latest or already managed.
        canManage: () => trail.key.assetType === productType && self.isSubmitted() && isManager() //&& state.isLatest(submission)

            // only one managed revision.
            && !state.allSubmissions(trail).some(s => s.lifecycle.state === 'managed' && s.lifecycle.lastSubmitted === submission.lifecycle.lastSubmitted)

            && self.areSubmissionsAllowed()

        ,

        canEditManagementData: () => self.isManaged() && isManager() /* && state.isLastSubmittedOrAbout(submission) */

        ,

        // a managed product can be published, given appropriate rights and provided it hasn't been superseded.
        canPublish: () => trail.key.assetType === productType && self.isManaged() && isPublisher() /* && state.isLastSubmittedOrAbout(submission) */ && self.areSubmissionsAllowed()

        ,

        // a managed product can be published, given appropriate rights and provided it hasn't been superseded.
        canShare: () => self.isPublished() && isPublisher() && self.areSubmissionsAllowed()

        ,

        canChangeDate: () => isManager() && (self.isSubmitted() || self.isMissing()) && !state.hasBeenManaged(submission)

        ,

        initializeParams: (params: Parameter[]) => {

            // computes value for live parameters
            const parameters = params.map(param => {

                const spec = layoutRegistry(ctx.asset.instanceType).lookupParameter(param.spec)

                return isLiveParameter(spec) ? { ...param, value: spec.valueFor(ctx, param, s) } : param

            })

            var overlay = asset?.properties.parameterOverlay ?? {}

            overlay = { ...overlay, ...party?.properties.parameterOverlays.requirements[ctx.asset.source] ?? {} }

            // applies per-party overrides.
            return parameters.map(p => ({ ...p, value: overlay[p.name] ? overlay[p.name].value : p.value }))
        }

    }


    return self;
}

