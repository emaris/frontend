
import { Label, LabelProps } from "apprise-frontend/components/Label"
import { icns } from "apprise-frontend/icons"
import { useIntl } from 'apprise-frontend/intl/api'
import { useLocale } from "apprise-frontend/model/hooks"
import { TagList } from "apprise-frontend/tag/Label"
import { useTenants } from "apprise-frontend/tenant/api"
import { TimeLabel } from "apprise-frontend/time/Label"
import { useTime } from "apprise-frontend/time/api"
import { UserLabel } from "apprise-frontend/user/Label"
import { eventIcon, missingIcon } from "emaris-frontend/event/constants"
import { NotAssessableLabel } from 'emaris-frontend/requirement/Label'
import { requirementType } from "emaris-frontend/requirement/constants"
import moment from "moment-timezone"
import * as React from "react"
import { useTranslation } from "react-i18next"
import { complianceIcon } from "../constants"
import { useCurrentCampaign } from "../hooks"
import { PartyInstanceLabel } from "../party/Label"
import { usePartyInstances } from "../party/api"
import { useProductInstances } from "../product/api"
import { useRequirementInstances } from "../requirement/api"
import { useSubmissions } from "./api"
import { manageIcon, submissionIcon, submissionSingular } from "./constants"
import { SubmissionStub } from "./model"


type Props = LabelProps & {

    submission: SubmissionStub

    tipMode?: 'party' | 'asset' | 'author'
    displayMode?: 'submission' | 'party' | 'asset' | 'asset-tags' | 'date' | 'date-only' | 'compliance' | 'compliance-rating' | 'state-only'

    noDateAccent?: boolean

    noPseudoNames?: boolean
}

// reprents a submission in terms of its status or its submission/editing date.

export const SubmissionLabel = (props: Props) => {

    const { t } = useTranslation()
    const { l } = useLocale()

    const { currentLanguage } = useIntl()

    const time = useTime()
    const tenants = useTenants()

    const campaign = useCurrentCampaign()
    const campaignTimeZone = campaign.properties.timeZone ? { 'original': campaign.properties.timeZone } : undefined

    const parties = usePartyInstances(campaign)
    const requirement = useRequirementInstances(campaign)
    const product = useProductInstances(campaign)

    const { submission, tip, tipMode, displayMode = 'date', mode, className = '', noPseudoNames, noDateAccent = false, ...rest } = props

    const submissions = useSubmissions(campaign)

    return React.useMemo(() => {


        const state = submission.lifecycle.state
        const classNames = `submission-label ${displayMode} ${state} ${className}`

        const now = time.current()

        const submissionDate = submission.lifecycle.lastSubmitted

        // if submitted, then submission icon (date view) or event icon (status view). otherwise, icon matches state.
        const stateAwareIcon = submissionDate ?

            state === 'missing' ? missingIcon :
                state === 'managed' ? manageIcon
                    : state === 'published' ? icns.publish
                        : displayMode === 'state-only' ? submissionIcon : eventIcon

            : state === 'draft' ? icns.edit
                : state === 'pending' ? icns.pause
                    : state === 'published' ? icns.publish

                        : submissionIcon

        if (displayMode === 'submission')

            return <Label className={classNames} icon={submissionIcon} title={t(submissionSingular)} {...rest} />


        if (displayMode === 'state-only') {

            const sotip = tip ?? (() =>


                tipMode === 'author' ? <UserLabel user={submission.lifecycle.state === 'published' ? submission.lifecycle.lastPublishedBy : submission.lifecycle.lastApprovedBy ?? submission.lifecycle.lastModifiedBy} />

                    :

                    submissionDate ? t("submission.labels.submitted_since", { since: moment(submissionDate).from(now) })

                        :

                        t("submission.labels.last_edited", { since: moment(submission.lifecycle.lastModified ?? now).from(now) })

            )

            const sotitle = t(`submission.status.${state}`)

            return <Label className={classNames} tip={sotip} icon={stateAwareIcon} title={sotitle} mode={mode ?? 'tag'} {...rest} />

        }

        const trail = submissions.lookupTrail(submission.trail)

        if (displayMode === 'compliance' || displayMode === 'compliance-rating') {

            const { name: judgement, complianceClass/* , allNames, tag */ } = submissions.complianceProfile(submission) ?? {}
            //console.log(submission, submissions.lookupTrail(submission.trail), requirement.all())
            // const trail = submissions.lookupTrail(submission.trail)
            const instance = trail?.key.assetType === requirementType ? requirement.lookupBySource(trail.key.asset) : product.lookupBySource(trail?.key.asset)

            const canBeAssessed = instance?.properties.assessed ?? true

            // const index = allNames?.findIndex(n => n.en === judgement?.en)

            const cotip = (judgement && submission.lifecycle.compliance?.lastAssessed) ? () => t('submission.labels.last_assessed_since', { since: moment(submission.lifecycle.compliance?.lastAssessed).from(now) }) : undefined
            const cotitle = l(judgement) ?? t("submission.labels.not_assessed")

            return canBeAssessed ?

                <Label className={`${classNames} ${complianceClass ?? ''}`} tip={cotip} icon={complianceIcon} title={cotitle} mode={mode ?? 'tag'} {...rest} />

                :

                <NotAssessableLabel />

            // retracted:  doesn't cover scales where multiple levels have same values (eg unweighed schemes).
            //         :
            //         // compliance-rating
            //         <Tooltip title={l(tag?.name)}>
            //             {/* <Rate> seems to override mouse event listeners for its own tooltips? So we interject a <div>. */}
            //             <span>
            //                 <Rate className={`${classNames} ${complianceClass ?? ''}`} disabled
            //                     count={allNames?.length ?? 0}
            //                     character={icns.dot}
            //                     defaultValue={(index ?? 0) + 1} />
            //             </span>
            //         </Tooltip>
            // 

        }

        if (displayMode === 'asset-tags') {
            return <React.Fragment>{trail && <TagList taglist={submissions.assetSourceFrom(trail).tags}></TagList>}</React.Fragment>
        }

        let computedtip = tip;

        if (trail && !computedtip) {

            switch (tipMode) {

                case 'asset': {

                    const profile = submissions.profileOf(trail.key.assetType)
                    computedtip = () => profile.label(profile.instance(trail.key.asset), { linkTo: props.linkTo })
                    break
                }

                case 'party': {
                    const party = parties.safeLookupBySource(trail.key.party)
                    computedtip = () => <PartyInstanceLabel linkTo={props.linkTo} instance={party} />
                    break
                }

                case 'author':
                    computedtip = () => <UserLabel user={submission.lifecycle.lastApprovedBy ?? submission.lifecycle.lastModifiedBy} />


            }

        }

        if (!computedtip) {

            switch (state) {

                case 'published': computedtip = () => <TimeLabel className={classNames} displayMode="relative" accentFrame="days" accentMode="record" noGreyAccent={noDateAccent} icon={stateAwareIcon} value={moment(submission.lifecycle.lastPublished)} mode={mode} timezones={campaignTimeZone} {...rest} />
                    break
                case 'managed': computedtip = () => <TimeLabel className={classNames} displayMode="relative" accentFrame="days" accentMode="record" noGreyAccent={noDateAccent} icon={stateAwareIcon} value={moment(submission.lifecycle.lastModified)} mode={mode} timezones={campaignTimeZone} {...rest} />
                    break
                case 'submitted': computedtip = () => <TimeLabel className={classNames} displayMode="relative" accentFrame="days" accentMode="record" noGreyAccent={noDateAccent} icon={stateAwareIcon} value={moment(submission.lifecycle.lastModified)} mode={mode} timezones={campaignTimeZone} {...rest} />
            }
        }

        const getTimeLabel = (submission: SubmissionStub, rest: any) => {

            const value = submissionDate ?? moment(submission.lifecycle.lastModified)

            return <TimeLabel className={classNames} format='numbers' accentFrame="days" accentMode="record" noGreyAccent={noDateAccent} icon={stateAwareIcon} value={value} mode={mode} timezones={campaignTimeZone} {...rest} />
        }

        if (displayMode === 'date-only' && (submissionDate || noPseudoNames)) {

            return getTimeLabel(submission, rest)
        }


        if (displayMode === 'date' && (!submissions.isManagedState(submission) || noPseudoNames) && (submissionDate || noPseudoNames)) {

            return getTimeLabel(submission, rest)
        }

        let title

        if (trail) {

            switch (displayMode) {

                case 'asset': {

                    const profile = submissions.profileOf(trail.key.assetType)
                    title = l(profile.source(trail.key.asset).name)
                    break;
                }
                case 'party': {

                    title = l(tenants.safeLookup(trail.key.party).name)
                    break;
                }

                default: {

                    title = submission.lifecycle.reference ?

                        <div className='submission-title'>{t(`submission.status.${state}`)} <span className={state}>({t("submission.reference.label_short")}{l(submission.lifecycle.reference)})</span></div>
                        :

                        t(`submission.status.${state}`)
                }
            }

        }

        return <Label className={classNames} tip={computedtip} icon={stateAwareIcon} title={title} mode={mode} {...rest} />


        // eslint-disable-next-line
    }, [submission, currentLanguage])

}