

import { intlapi } from "apprise-frontend/intl/api";
import { withReport } from "apprise-frontend/utils/validation";
import { EmarisState } from "emaris-frontend/state/model";
import { SubmissionDto } from "./model";
import * as React from "react"
import { Paragraph } from "apprise-frontend/components/Typography";


export const submissionvalidationapi = (s:EmarisState) => ({


    validateSubmission: (_:SubmissionDto) => {

        const t = intlapi(s).getT()
    
        return withReport({
            
                approveCycle: {msg:t("submission.fields.approval.msg"),help:

                            <React.Fragment>
                                <Paragraph>{t("submission.fields.approval.help1")}</Paragraph>
                                <Paragraph spaced>{t("submission.fields.approval.help2")}</Paragraph>

                            </React.Fragment>
            
               }

               ,

               adminCanEdit : {

                msg:t("submission.fields.admin_can_edit.msg"),
                help:<React.Fragment>
                        <Paragraph>{t("submission.fields.admin_can_edit.help1")}</Paragraph>
                        <Paragraph spaced >{t("submission.fields.admin_can_edit.help2")}</Paragraph>
                    </React.Fragment>
                }

                ,

                adminCanSubmit : {

                    msg:t("submission.fields.admin_can_submit.msg"),
                    help:<React.Fragment>
                            <Paragraph>{t("submission.fields.admin_can_submit.help1")}</Paragraph>
                            <Paragraph spaced >{t("campaign.fields.admin_can_submit.help2")}</Paragraph>
                        </React.Fragment>
                }


        })

    }

      
})
