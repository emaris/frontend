import { PartyInstance } from 'emaris-frontend/campaign/party/model'
import { productinstapi } from 'emaris-frontend/campaign/product/api'
import { requirementinstapi } from 'emaris-frontend/campaign/requirement/api'
import { productType } from 'emaris-frontend/product/constants'
import { requirementType } from 'emaris-frontend/requirement/constants'
import { EmarisState } from "emaris-frontend/state/model"
import { Campaign, CampaignInstance } from "../../model"
import { partyinstapi } from "../../party/api"
import { aggregateMany, AggregateSummaries } from './aggregateMany'
import { aggregate, AggregateSummary } from './aggregateOne'
import { TrailSummary } from './trail'



export type AssetSummary = AggregateSummary & {

    dueParties: PartyInstance[]
    dueDate: string | undefined
}

// collects and aggreagtes reporting, assessment, and compliance statistics about parties.
export type AllAssetsSummary = AggregateSummaries<AssetSummary>


// collects and aggregates reporting, assessment, and compliance statistics about and across assets.
export const allAssetsSummary = (s: EmarisState) => (c: Campaign) =>

    (assets: CampaignInstance[], trailSummaries: Record<string, TrailSummary[]>, dueDates: Record<string, string>): AllAssetsSummary => {

        const summaries = aggregateMany<AssetSummary>()

        const parties = partyinstapi(s)(c)

        const isForParty = {

            [requirementType]: requirementinstapi(s)(c).isForParty,
            [productType]: productinstapi(s)(c).isForParty,
        }

        const partiesForLogged =  parties.allForStats()
        const partiesForLoggedId = partiesForLogged.map(p => p.source)

        assets.forEach(asset => {

            const dueDate = dueDates[asset.source]

            const dueParties = partiesForLogged.filter(p => isForParty[asset.instanceType](p, asset))

            // computes summaries of all trails of party submission for this asset wrt to its due date.
            const filteredAssetSummaries =  (trailSummaries[asset.source] ?? []).filter(ts => partiesForLoggedId.includes(ts.trail.key.party))

            // computes common statistics for this asset. 
            const summary: AssetSummary = {

                ...aggregate(s)(filteredAssetSummaries, dueParties.length),

                dueParties,
                dueDate
            }

            summaries.add(asset.source, summary)


        })

        return summaries.done()
    }



