import { productType } from 'emaris-frontend/product/constants'
import { requirementType } from 'emaris-frontend/requirement/constants'
import { EmarisState } from "emaris-frontend/state/model"
import moment from "moment-timezone"
import { Campaign } from "../../model"
import { submissionapi } from "../api"
import { ComplianceProfile, newTrail, submissionmodelapi, SubmissionStub, TimelinessProfile, Trail, TrailKey } from "../model"
import { profileapi } from '../profile'
import { ComplianceCache, submissionstateapi, TimelinessCache } from '../state'

//  trail and data derived from it for caching purposes.

export type TrailSummary = {

    trail: Trail                                    // the trail this is a summary of (access convenience).

    revisions: number                               // revision count (access convenience).

    lastRevision: SubmissionStub | undefined        // most recently created (undefined => empty trail).

    lastSubmitted: SubmissionStub | undefined       // most recently submitted, if any.

    lastUpdate: string | undefined                  // timestamp most recent event: edit, approval/rejection, submission and assessment (undefined => empty trail)

    dueDate: string | undefined                     // not all assets have a deadline, at present or ever.  

    computedTimeliness:
    'ontime' |                                  // first submitted before due date, or no due date at all.
    'late' |                                    // due date is in the past, submission or not.
    'notdue'                                    // no submission, but due date is in the future.

    timeliness: TimelinessProfile | undefined

    missing: boolean                                // submitted as placeholder for a party submission.
    assessable: boolean                             // one reason why the compliance profile may be undefined.

    compliance: ComplianceProfile | undefined  // the profile of the most recent submission, if one exists and it has been judged. undefined otherwise.


}


//  most often called in a loop of N at some level of aggregation, so its performance is significant.
//  takes due date so that they can be computed once for the entire loop, 

export type TrailSummaryProps = {

    now: Date,
    trail: Trail,
    due?: string,
}

export const trailSummary = (s: EmarisState) => (c: Campaign) => (trailKey: TrailKey, due?: string, complianceCache?: ComplianceCache, timelinessCache?: TimelinessCache): TrailSummary => {

    const submissions = submissionapi(s)(c)

    const trail = submissions.lookupTrailKey(trailKey) ?? newTrail(trailKey)        // use the new trail if trail hasn't been saved in state yet.

    return trailSummaryNoDeps(s)(c)({

        now: new Date(),
        trail,
        due

    })

}


export const trailSummaryNoDeps = (s: EmarisState) => (c: Campaign) => {

    const { dateComparatorDesc, complianceProfile, timelinessProfile } = submissionmodelapi(s)(c)
    const state = submissionstateapi(s)(c)
    const {profileOf} = profileapi(s)(c)
   
    
    const complianceCache = state.complianceCache()
    const timelinessCache = state.timelinessCache()

    const profiles = {

        [requirementType]: profileOf(requirementType),
        [productType]: profileOf(productType)
    }

    return (props: TrailSummaryProps): TrailSummary => {

        const {  now, trail, due } = props

        const profile = profiles[trail.key.assetType]

        const asset = profile.instance(trail?.key.asset!)

        const dueDate = due ?? profile.dueDateOf(asset)

        const parsedDueDate = dueDate ? new Date(dueDate) : dueDate

        const all = [...trail.submissions ?? []].sort(dateComparatorDesc)            // ordered by most the recent submissionDate, if any (drafts first).

       
        const lastSubmitted = all.find(s => s.lifecycle.state === 'submitted' || s.lifecycle.state === 'missing')

        const missing = lastSubmitted?.lifecycle.state === 'missing'

        // 'ontime' is based on the first submission that anticipates the due date (not necessarily lastSubmitted).
        // 'late' if not such submission exists and the deadline has passed.
        // 'notdue' if it's not on time but can still be.
        let computedTimeliness: 'ontime' | 'late' | 'notdue'

        // As the reverse method modifies the original array in place we slice it before, to clone the original array (slice is the faster way to clone an array)
        const firstSubmissionDate = all.slice().reverse().find(s => s.lifecycle.state === 'submitted' || s.lifecycle.state)?.lifecycle.lastSubmitted

        if (firstSubmissionDate)
            computedTimeliness = parsedDueDate && new Date(firstSubmissionDate) > parsedDueDate ? 'late' : 'ontime'

        else // no submission

            computedTimeliness = parsedDueDate && now > parsedDueDate ? 'late' : 'notdue'

        const lastRevision = all[0]

        const changes = [

            lastRevision?.lifecycle.lastModified,
            lastRevision?.lifecycle.lastSubmitted,
            lastRevision?.lifecycle.compliance?.lastAssessed,
            lastSubmitted?.lifecycle.compliance?.lastAssessed
        ]
            .filter(d => !!d).map(d => moment(new Date(d!)))

        const lastUpdate = changes.length > 0 ? moment.max(changes).format() : undefined!

        const compliance = lastSubmitted ? complianceProfile(lastSubmitted, asset, complianceCache) : undefined
        const timeliness = lastSubmitted ? timelinessProfile(lastSubmitted, asset, timelinessCache, computedTimeliness) : undefined

        const assessable = asset.properties.assessed ?? true

        return { trail, lastRevision, lastSubmitted, lastUpdate, dueDate, compliance, timeliness, computedTimeliness, missing, assessable, revisions: missing ? all.length -1 : all.length }

    }
}
