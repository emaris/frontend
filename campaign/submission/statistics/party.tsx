

import { AssetInstance, Campaign, campaignmodelapi } from 'emaris-frontend/campaign/model'
import { productType } from 'emaris-frontend/product/constants'
import { requirementType } from "emaris-frontend/requirement/constants"
import { EmarisState } from "emaris-frontend/state/model"
import { partition } from "lodash"
import moment from "moment-timezone"
import { PartyInstance } from "../../party/model"
import { productinstapi } from "../../product/api"
import { requirementinstapi } from "../../requirement/api"
import { TrailSummary } from '../statistics/trail'
import { aggregateMany, AggregateSummaries } from './aggregateMany'
import { aggregate, AggregateSummary } from './aggregateOne'


// statistics over a party have specific aspects:
//
//  1. we need to split derived data and counts across asset types.
//  2. we may want to measure them in real-time, not necessarily at the end of the campaign
//     (how's the performance today, how are they doing for the upcoming submissions). so we need split counts at a given horizon.
//

export type PartySummary = AggregateSummary & {

    dueAssets: AssetInstance[]                                         // expected at this point.
    dueRequirements: AssetInstance[]
    dueProducts: AssetInstance[]

    next: PartySummary

}



// collects and aggregates reporting, assessment, and compliance statistics about and across all parties.
export type AllPartiesSummary = AggregateSummaries<PartySummary>


//  computes stats from a collection of parties, starting from their trails.
//  the stats are with respect to a real-time horizon, by default that set on the campaign.

export const allPartiesSummary = (s: EmarisState) => (c: Campaign) => (summaryMap: [PartyInstance, TrailSummary[]][], horizon?: moment.Moment): AllPartiesSummary => {

    const summaries = aggregateMany<PartySummary>()
    const excludeList = c.properties.statExcludeList ?? []
    const requirements = requirementinstapi(s)(c)
    const products = productinstapi(s)(c)
    const campaigns = campaignmodelapi(s)

    const currentHorizon = horizon ?? campaigns.currentHorizon(c)

    // we need to know what how many assets of which type are due at the horizon
    // for this we need due dates, but we can't just look at those inside summaries, as a parties might have not submitted for all assets yet.
    // so we recompute them here. but then we end up including assets that are not due for some parties.
    // so we need to filter those out as we iterate across parties.
    const dueDates = { ...requirements.allDueDatesByRequirementId(), ...products.allDueDatesByProductId() }

    const assetsInCampaign = [...requirements.all(), ...products.all()]

    const [assetsDueAtHorizon, assetsDueBeyondHorizon] = partition(assetsInCampaign, a => !dueDates[a.source] || currentHorizon.isAfter(new Date(dueDates[a.source])))

    const isForParty = {

        [requirementType]: requirements.isForParty,
        [productType]: products.isForParty

    }
    const assetFor = (party: PartyInstance) => (a: AssetInstance) => isForParty[a.instanceType](party, a)


    summaryMap.forEach(([party, trailSummaries]) => {

        // filter sunmmaries by those that are currently applicable to party.
        // or a submisison made before a restriction came into force will break statistics.
        const dueAssetsMap = {} as Record<string, boolean>
        const assetsDueAtHorizonForParty = [] as AssetInstance[]
        const requirementsDueAtHorizon = [] as AssetInstance[]
        const productsDueAtHorizon = [] as AssetInstance[]

        assetsDueAtHorizon.forEach(asset => {

            if (assetFor(party)(asset)) {
                assetsDueAtHorizonForParty.push(asset)
                dueAssetsMap[asset.source] = true
                if (asset.instanceType === requirementType)
                    requirementsDueAtHorizon.push(asset)
                else
                    productsDueAtHorizon.push(asset)

            }

        })

        const requirementsDueBeyondHorizon = [] as AssetInstance[]
        const productsDueBeyondtHorizon = [] as AssetInstance[]

        assetsDueBeyondHorizon.forEach(asset => {

            if (assetFor(party)(asset)) {

                if (asset.instanceType === requirementType)
                    requirementsDueBeyondHorizon.push(asset)
                else
                    productsDueBeyondtHorizon.push(asset)
            }
        })

        const applicableTrailSummaries = trailSummaries.filter(summary => dueAssetsMap[summary.trail.key.asset])

        // counts requirement, product, and total expectations for this party.
        summaries.get().dueSubmissions += assetsDueAtHorizonForParty.length

        const assetsDueBeyondHorizonForParty = assetsDueBeyondHorizon.filter(assetFor(party))

        const [dueTrailSummaries, nextTrailSummaries] = partition(applicableTrailSummaries, s => !s.dueDate || currentHorizon.isAfter(new Date(s.dueDate)))

        //console.log({party:party.source,dueTrailSummaries,assetsDueAtHorizonForParty,applicableTrailSummaries, dueAssetsMap})

        const dueSummary = aggregate(s)(dueTrailSummaries, assetsDueAtHorizonForParty.length) as PartySummary
        const nextSummary = aggregate(s)(nextTrailSummaries, assetsDueBeyondHorizonForParty.length) as PartySummary

        const summary: PartySummary = {

            ...dueSummary,

            dueAssets: assetsDueAtHorizonForParty,
            dueRequirements: requirementsDueAtHorizon,
            dueProducts: productsDueAtHorizon,

            next: {

                ...nextSummary,

                dueAssets: assetsDueBeyondHorizon,
                dueRequirements: requirementsDueBeyondHorizon,
                dueProducts: productsDueBeyondtHorizon

            }
        }



        if (excludeList.includes(party.source))
            summaries.get()[party.source] = summary
        else
            summaries.add(party.source, summary)


    })

    return summaries.done()

}


