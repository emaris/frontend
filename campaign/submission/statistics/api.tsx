import { EmarisState } from "emaris-frontend/state/model"
import { Campaign } from "../../model"
import { partyinstapi } from "../../party/api"
import { PartyInstanceDto } from "../../party/model"
import { productinstapi } from "../../product/api"
import { requirementinstapi } from "../../requirement/api"
import { allAssetsSummary } from './asset'
import { allPartiesSummary } from './party'
import { trailSummary, TrailSummary, trailSummaryNoDeps } from './trail'




export const submissionstatisticsapi = (s: EmarisState) => (c: Campaign) => {

    const self = {

        trailSummary: trailSummary(s)(c),


        trailSummaryNoDeps: trailSummaryNoDeps(s)(c),


        // the trail summaries required to calculate statistics across all parties.
        // includes all parties in the campaign, whether they have some trails or not.
        // intended for caching, it can be passed to allPartiesSummary below.
        partiesWithTrailSummaries: (trailSummaries: Record<string, TrailSummary[]>) => {

            // pairs each party with the summaries of its trails.
            return partyinstapi(s)(c).all().map(party => [party, trailSummaries[party.source] ?? []] as [PartyInstanceDto, TrailSummary[]])

        }

        ,

        // partiesWithTrailSummaries: (trailSummaries: Record<string, TrailSummary[]>) => {

        //     const requirements = requirementinstapi(s)(c)
        //     const products = productinstapi(s)(c)

        //     const summaries = (party: PartyInstance) => {

        //         return (trailSummaries[party.source] ?? []).filter(summary =>{

        //             const trail = summary.trail
        //             const instanceType = trail.key.assetType
        //             const api = instanceType === requirementType ? requirements : products

        //             return api.isForParty(party, api.safeLookupBySource(trail.key.asset))
        //         })


        //     }

        //     // pairs each party with the summaries of its trails.
        //     return partyinstapi(s)(c).all().map(party => [party, summaries(party)] as [PartyInstanceDto, TrailSummary[]])

        // }

        //,

        // calculate stats across all parties based on a pre-computed list of trail summaries.
        allPartiesSummary: allPartiesSummary(s)(c)


        ,

        //  builds summaries for all requirements by iterating and aggregating over trail summaries.
        //  pre-computes as much as possible.

        allRequirementsSummary: (trailSummaries: Record<string, TrailSummary[]>, dueDates: Record<string, string>) => {

            const assets = requirementinstapi(s)(c)

            return allAssetsSummary(s)(c)(assets.all(), trailSummaries, dueDates)

        }

        ,

        allProductsSummary: (trailSummaries: Record<string, TrailSummary[]>, dueDates: Record<string, string>) => {

            const assets = productinstapi(s)(c)

            return allAssetsSummary(s)(c)(assets.all(), trailSummaries, dueDates)

        }


    }

    return self
}