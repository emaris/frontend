
import { Button } from 'apprise-frontend/components/Button'
import { DrawerProps, RoutableDrawerProps, useRoutableDrawer } from 'apprise-frontend/components/Drawer'
import { RouteGuard } from 'apprise-frontend/components/RouteGuard'
import { Paragraph } from 'apprise-frontend/components/Typography'
import { Form } from 'apprise-frontend/form/Form'
import { useFormState } from 'apprise-frontend/form/hooks'
import { useTimePicker } from 'apprise-frontend/time/TimePickerBox'
import { eventIcon } from 'emaris-frontend/event/constants'
import moment from 'moment-timezone'
import * as React from 'react'
import { useTranslation } from 'react-i18next'
import { useCurrentCampaign } from '../hooks'
import { useSubmissions } from './api'
import { dateChangeIcon } from './constants'
import { SubmissionStub } from './model'



export type Props = {

    submission: SubmissionStub
    onChange: (_: string) => Promise<void>

}


export const useDateChangeDrawer = (opts: Partial<RoutableDrawerProps> = {}) => {

    const { t } = useTranslation()

    const { id = 'datechange', title = t("submission.datechange.title"), icon = eventIcon } = opts

    const { Drawer, open, close, visible, route, param } = useRoutableDrawer({ id, title, icon })

    const DrawerProxy = (props: Partial<DrawerProps>) => <Drawer width={500} icon={icon} title={title} {...props} />

    //eslint-disable-next-line
    const StableProxy = React.useCallback(DrawerProxy, [visible])


    return { DateChangeDrawer: StableProxy, openDateChange: open, datechangeRoute: route, closeDataChange: close, datechangeParam: param }

}


export const DateChangeForm = (props: Props) => {

    const { t } = useTranslation()
    const { submission, onChange } = props

    const initialDate = submission.lifecycle.lastSubmitted

    const { edited, dirty, change, softReset } = useFormState({ date: initialDate })

    const changeDate = () => {

        onChange(edited.date!).then(() => softReset({ date: initialDate }))
    }

    const { timepicker, keepsHistoryLine } = useDateChangePicker({

        submission,
        date: edited.date,
        onChange: change((m, v) => m.date = v)

    })

    return <div className="submission-dialog-panel">

        <Paragraph className="submission-dialog-intro">{t("submission.datechange.introduction")}</Paragraph>

        <div className="submission-dialog-section" style={{ background: 'inherit' }}>

            {React.cloneElement(dateChangeIcon, { className: 'submission-dialog-icon' })}

            <Paragraph className='submission-dialog-explainer'>{t("submission.datechange.explainer")}</Paragraph>

            <Form style={{ marginTop: 30 }}>
                {timepicker}
            </Form>

            <Button style={{ marginTop: 30 }} enabled={dirty} type={keepsHistoryLine ? 'primary' : 'danger'} onClick={changeDate} >
                {t("submission.datechange.confirm_btn")}
            </Button>
        </div>

        <RouteGuard when={dirty} />

    </div>
}


type DateChangePickerProps = {

    submission: SubmissionStub
    onChange: (_: string) => void
    date: string | undefined
    enabledOnReadOnly?: boolean
}

export const useDateChangePicker = (props: DateChangePickerProps) => {

    const { t } = useTranslation()
    const { submission, date, enabledOnReadOnly=false, onChange } = props

    const { Picker: TimePicker } = useTimePicker()

    const submissions = useSubmissions(useCurrentCampaign())

    const prev = submissions.previous(submission)
    const next = submissions.next(submission)

    const afterPrevious = (!prev?.lifecycle.lastSubmitted || moment(date).isAfter(moment(prev.lifecycle.lastSubmitted)))
    const beforeNext = (!next?.lifecycle.lastSubmitted || moment(date).isBefore(moment(next.lifecycle.lastSubmitted)))

    const keepsHistoryLine = afterPrevious && beforeNext
    const now = moment.now()

    const timepicker = <>

        <TimePicker label={t('submission.datechange.label')} style={{ minWidth: 250 }} showToday allowClear={false}
            onChange={date => onChange(date.format())}
            disabledDate={date => date?.isAfter(now) ?? true}
            enabledOnReadOnly={enabledOnReadOnly}
            showTime
            value={date} />

        {keepsHistoryLine ||

            <Paragraph className="submission-dialog-warning">
                {t(!afterPrevious ? 'submission.datechange.previous_warning' : 'submission.datechange.next_warning')}
            </Paragraph>
        }

    </>

    return { timepicker, keepsHistoryLine }
}