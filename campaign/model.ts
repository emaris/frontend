import { buildinfo } from 'apprise-frontend/buildinfo'
import { intlapi } from 'apprise-frontend/intl/api'
import { ParameterRef } from 'apprise-frontend/layout/parameters/model'
import { Lifecycle, newLifecycle } from "apprise-frontend/model/lifecycle"
import { compareMultilang, l, MultilangDto, newMultiLang } from "apprise-frontend/model/multilang"
import { settingsapi } from "apprise-frontend/settings/api"
import { defaultContext } from "apprise-frontend/system/constants"
import { tagapi } from "apprise-frontend/tag/api"
import { newTagged, TagExpression, Tagged } from "apprise-frontend/tag/model"
import { TenantAudience } from 'apprise-frontend/tenant/AudienceList'
import { timeapi } from "apprise-frontend/time/api"
import { compareDates, deepclone, shortid } from "apprise-frontend/utils/common"
import { Topic } from "apprise-messages/model"
import { productType } from "emaris-frontend/product/constants"
import { requirementType } from "emaris-frontend/requirement/constants"
import moment from "moment-timezone"
import { EmarisState } from "../state/model"
import { campaignapi } from "./api"
import { campaignType, defaultRelativeDate, defaultTimelinessScale } from "./constants"
import { eventinstapi } from "./event/api"
import { RelativeDate } from "./event/model"
import { campaignstateapi } from "./state"
import { campaignvalidationapi } from "./validation"



export type Lineage = {

    lineage?: InstanceRef
}



export type InstanceRef = {
    source: string,
    campaign: string
}



export const isInstanceRef = (ci: any): ci is InstanceRef => ci && ci.campaign && ci.source && !(ci as CampaignInstance).instanceType

export type CampaignDto = Tagged & {

    id: string,

    guarded?: boolean
    predefined?: boolean

    lineage?: string,

    name: MultilangDto,
    description: MultilangDto,

    // note: we use events to track the key states (open/closed), state goes unused for now.
    lifecycle: Lifecycle<CampaignState>

    tags: string[],

    properties: CampaignProperties

    summary?: Summary

}

type CampaignState = 'active' | 'archived'




export type Campaign = CampaignDto & {

    loaded: boolean
}

export type CampaignProperties = Partial<CampaignSettings> & {

    note?: Record<string, string>
    defaultRelativeDate?: RelativeDate
    muted?: boolean
    statExcludeList?: string[]

}

export type CampaignSettings = {

    complianceScale?: string
    timelinessScale?: string
    approveCycle: ApproveCycle
    liveGuard?: boolean
    statisticalHorizon: number
    adminCanEdit: boolean
    adminCanSubmit: boolean
    partyCanSeeNotApplicable: boolean
    defaultRelativeDate: RelativeDate
    timeZone: string
    suspendOnEnd: boolean
    suspendSubmissions: boolean
}

export type CampaignAppSettings = Record<typeof campaignType, CampaignSettings>


export type ApproveCycle = typeof requirementType | typeof productType | 'all' | 'none'

export const approveCycleOptions = {
    none: "campaign.fields.approval.options.none",
    all: "campaign.fields.approval.options.all",
    [requirementType]: "campaign.fields.approval.options.requirement",
    [productType]: "campaign.fields.approval.options.product"
}

export const approveCycleOptionDefaults = {
    none: "campaign.fields.approval.options.none_short",
    all: "campaign.fields.approval.options.all_short",
    [requirementType]: "campaign.fields.approval.options.requirement_short",
    [productType]: "campaign.fields.approval.options.product_short"
}


export type Summary = {

    startDate: string | undefined
    endDate: string | undefined
}

export type CampaignInstance = Tagged & Lineage & {

    id: string
    instanceType: string
    campaign: string
    source: string
    audience?: TagExpression
    audienceList?: TenantAudience
    userProfile?: TagExpression

    properties: InstanceProperties

}

// export type Overlay = { [name:string] : Parameter }
export type Overlay = Record<string, ParameterRef>

export type AssetInstanceProperties = InstanceProperties & {

    parameterOverlay?: Overlay
    editable?: boolean
    versionable?: boolean
    assessed?: boolean
}

export type AssetInstance = CampaignInstance & {

    properties: AssetInstanceProperties
}


export type InstanceProperties = {

    note?: Record<string, string | undefined>

}


export const newCampaign: (s: EmarisState) => Campaign = s => {

    const campaignSettings = settingsapi(s).get<CampaignAppSettings>().campaign

    return {


        ...newTagged,
        id: undefined!,
        lifecycle: newLifecycle('active'),


        name: newMultiLang(),
        description: newMultiLang(),

        loaded: false,
        tags: [defaultContext],

        archive: false,

        properties: {
            ...campaignSettings,
            silent: false
        }

    }
}

export const noCampaign = (s: EmarisState) => ({ ...newCampaign(s), id: 'unknown', lifecycle: {} }) as Campaign
export const noCampaignInstance = (type: string, campaign: string) => ({ id: undefined!, instanceType: type, notified: false, source: 'unknown', campaign, tags: [], properties: { defaultRelativeDate: defaultRelativeDate, editable: undefined, versionable: undefined, assessed: undefined } }) as CampaignInstance

export const newCampaignId = () => `C-${shortid()}`

export const unresolvedCampaignId = "unknown"

export const unresolvedCampaign: (id: string | undefined) => Campaign = id => ({
    ...newTagged,
    id: unresolvedCampaignId,
    name: { en: `Unknown campaign ${id}` },
    description: { en: `Unknown campaign ${id}` },
    lifecycle: newLifecycle('active'),
    loaded: false,
    archive: false,
    properties: {}
})


export const campaignmodelapi = (s: EmarisState) => {

    const self = {

        stringify: (c: Campaign) => `${l(s)(c.name)} ${l(s)(c.description) ?? ''} ${l(s)(campaignstateapi(s).lookup(c.lineage)?.name)}`

        ,

        status: (campaign?: Campaign, at?: moment.Moment) => {

            const time = at ?? timeapi(s).current()

            const start = campaign ? self.startDate(campaign) : undefined
            const end = campaign ? self.endDate(campaign) : undefined

            const started = moment(start).isBefore(time)
            const ended = moment(end).isBefore(time)

            const t = intlapi(s).getT()

            const label = started ? t(ended ? "campaign.lifecycle.closed_state" : "campaign.lifecycle.open_state") : t("campaign.lifecycle.design_state")

            return { label, start, end, planned: !started, started, ended, running: started && !ended }


        }

        ,

        startDate: (c: Campaign) => {

            const api = eventinstapi(s)(c)

            return api.startDate() ?? c.summary?.startDate    // uses loaded data or summary from backend if data not loaded yet.

        }
        ,

        isStarted: (c: Campaign) => {

            const now = timeapi(s).current()

            const startDate = self.startDate(c)

            return !!startDate && now.isAfter(startDate)
        }

        ,

        endDate: (c: Campaign) => {

            const api = eventinstapi(s)(c)

            return api.endDate() ?? c.summary?.endDate      // uses loaded data or summary from backend if data not loaded yet.

        }

        ,

        isRunning: (c: Campaign) => {

            const now = timeapi(s).current()
            const start = self.startDate(c)
            const end = self.endDate(c)

            return !!start && moment(start).isBefore(now) && (!end || moment(end).isAfter(now))

        }

        ,

        isEnded: (c: Campaign) => {
            const endDate = self.endDate(c)
            return endDate ? moment(endDate).isBefore(timeapi(s).current()) : false
        }

        ,

        intern: (dto: CampaignDto): Campaign => ({ ...dto, loaded: false })

        ,

        extern: ({ loaded, ...rest }: Campaign) => rest

        ,

        clone: (c: Campaign): Campaign => {
            const cloned = deepclone(c)

            return { ...cloned, id: undefined!, lineage: c.lineage, lifecycle: newLifecycle(), summary: undefined, properties: {...cloned.properties, muted: true} }
        }

        ,

        branch: (c: Campaign): Campaign => ({ ...self.clone(c), lineage: c.id })

        ,

        comparator: (o1: Campaign, o2: Campaign) => compareMultilang(s)(o1.name, o2.name)

        ,

        startedComparator: (o1: Campaign, o2: Campaign) => {

            const started1 = self.startDate(o1)
            const started2 = self.startDate(o2)

            return started1 ? (started2 ? compareDates(started1, started2) : -1) : started2 ? 1 : compareDates(started1, started2)
        }

        ,

        openComparator: (o1: Campaign, o2: Campaign) => {

            const open1 = self.isRunning(o1)
            const open2 = self.isRunning(o2)

            // TODO: based on some temporal notion, to clarify.

            return open1 ? (open2 ? self.comparator(o1, o2) : -1) : open2 ? 1 : self.comparator(o1, o2)

        }
        ,

        validationErrors: (c: Campaign) => {

            const validation = campaignvalidationapi(s)

            return campaignapi(s).isLoaded(c.id) ?
                validation.validateParties(c).errors()
                + validation.validateProducts(c).errors()
                + validation.validateRequirements(c).errors()
                + validation.validateEvents(c).errors()
                : 0
        }
        ,

        liveGuard: () => settingsapi(s).get<CampaignAppSettings>().campaign.liveGuard ?? !buildinfo

        ,


        approveCycle: (c: Campaign) => c.properties.approveCycle

        ,

        approveCycleFor: (id: string) => self.approveCycle(campaignstateapi(s).safeLookup(id))

        ,

        setApprovalCycle: (c: Campaign, value: ApproveCycle | undefined) => {

            if (!value)
                delete c.properties.approveCycle

            else
                c.properties.approveCycle = value

        }

        ,


        complianceScale: (c: Campaign) => c.properties.complianceScale

        ,

        setComplianceScale: (c: Campaign, value: string | undefined) => {

            if (!value){
                delete c.properties.complianceScale
                self.setTimelinessScale(c, undefined)
            }

            else
                c.properties.complianceScale = value

        }

        ,

        timelinessScale: (c: Campaign) => self.currentTimelinessScale(c).id

        ,

        setTimelinessScale: (c: Campaign, value: string | undefined) => {

            if (!value)
                delete c.properties.timelinessScale

            else
                c.properties.timelinessScale = value

        }

        ,

        // resolves tag ref and faces all clients except the config editor.
        currentComplianceScale: (c: Campaign) => c.properties.complianceScale ? tagapi(s).lookupCategory(c.properties.complianceScale) : undefined

        ,

        currentTimelinessScale: (c: Campaign) => c.properties.timelinessScale ? tagapi(s).lookupCategory(c.properties.timelinessScale) : tagapi(s).lookupCategory(defaultTimelinessScale)

        ,

        canEdit: (c: Campaign) => !!c.properties.adminCanEdit

        ,

        canEditFor: (id: string) => self.canEdit(campaignstateapi(s).safeLookup(id))

        ,

        statisticalHorizon: (c: Campaign) => c.properties.statisticalHorizon

        ,

        setStatisticalHorizon: (c: Campaign, value: number | undefined) => {

            if (!value)
                delete c.properties.statisticalHorizon

            else
                c.properties.statisticalHorizon = value

        }

        ,

        isSuspendOnEnd: (c: Campaign) => c.properties.suspendOnEnd ?? settingsapi(s).get<CampaignAppSettings>().campaign.suspendOnEnd ?? true

        ,

        setSuspendOnEnd: (c: Campaign, value: boolean) => c.properties.suspendOnEnd = value

        ,

        isMuted: (c: Campaign) => c.properties.muted


        ,

        setMuted: (c: Campaign, value: boolean) => c.properties.muted = value

        ,

        isCampaignMuted: (c: Campaign) => c.properties.muted


        ,

        isSuspendSubmissions: (c: Campaign) => c.properties.suspendSubmissions

        ,



        setSuspendSubmissions: (c: Campaign, value: boolean) => c.properties.suspendSubmissions = value

        ,

        isCampaignSuspended: (c: Campaign) => self.isSuspendSubmissions(c) || (self.isEnded(c) && self.isSuspendOnEnd(c))

        ,

        isArchived: (c: Campaign) => c.lifecycle.state === 'archived' ?? false

        ,

        setArchived: (c: Campaign) => c.lifecycle.state = 'archived'

        ,

        canArchive: (c: Campaign) => !self.isArchived(c) && self.isCampaignSuspended(c)

        ,

        timeZone: (c: Campaign) => c.properties.timeZone

        ,

        setTimeZone: (c: Campaign, value: string | undefined) => {

            if (!value)
                delete c.properties.timeZone

            else
                c.properties.timeZone = value

        }
        ,

        defaultRelativeDate: (c: Campaign) => c.properties.defaultRelativeDate

        ,

        setDefaultRelativeDate: (c: Campaign, value: RelativeDate | undefined) => {
            if (!value)
                delete c.properties.defaultRelativeDate

            else
                c.properties.defaultRelativeDate = value
        }

        ,

        currentHorizon: (c: Campaign) => timeapi(s).current().add(self.statisticalHorizon(c), 'days')

        ,


        setCanEdit: (c: Campaign, value: boolean) => {

            if (!value)
                delete c.properties.adminCanEdit

            else
                c.properties.adminCanEdit = true

        }

        ,

        canSubmit: (c: Campaign) => !!c.properties.adminCanSubmit

        ,

        canSubmitFor: (id: string) => self.canSubmit(campaignstateapi(s).safeLookup(id))

        ,


        setCanSubmit: (c: Campaign, value: boolean) => {

            if (!value)
                delete c.properties.adminCanSubmit

            else
                c.properties.adminCanSubmit = true

        }

        ,

        setPartyCanSeeNotApplicable: (c: Campaign, value: boolean) => {

            if (!value)
                delete c.properties.partyCanSeeNotApplicable

            else
                c.properties.partyCanSeeNotApplicable = true

        }

        ,

        canPartySeeNotApplicable: (c: Campaign | string) => typeof c === 'string' ? campaignstateapi(s).safeLookup(c).properties.partyCanSeeNotApplicable : !!c.properties.partyCanSeeNotApplicable

        ,

        topics: (c: Campaign): Topic[] => [{ type: campaignType, name: c.id }]

    }

    return self

}