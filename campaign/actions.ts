
import { any } from "apprise-frontend/iam/model"
import { campaignIcon, campaignType } from "./constants"

const baseAction = { icon:campaignIcon, type: campaignType, resource :any }

export const campaignActions = {  
    
    manage:  {...baseAction, labels:["manage"], shortName:"campaign.actions.manage.short", name:"campaign.actions.manage.name", description: "campaign.actions.manage.desc"}
}