import { State } from "#app"
import { intlapi } from "apprise-frontend/intl/api"
import { defaultLanguage } from "apprise-frontend/intl/model"
import { systemapi } from "apprise-frontend/system/api"
import { tenantType } from 'apprise-frontend/tenant/constants'
import { Tenant } from 'apprise-frontend/tenant/model'
import { TenantState } from 'apprise-frontend/tenant/state'
import { utilapi } from "apprise-frontend/utils/api"
import { indexMap, through } from "apprise-frontend/utils/common"
import { notify, showAndThrow, showError } from "apprise-frontend/utils/feedback"
import { DashboardState, initialDashboard } from "emaris-frontend/dashboard/state"
import { eventcalls } from 'emaris-frontend/event/calls'
import { eventType } from 'emaris-frontend/event/constants'
import { Event } from 'emaris-frontend/event/model'
import { EventState, eventstateapi } from 'emaris-frontend/event/state'
import { productcalls } from 'emaris-frontend/product/calls'
import { productType } from 'emaris-frontend/product/constants'
import { Product } from 'emaris-frontend/product/model'
import { ProductState, productstateapi } from 'emaris-frontend/product/state'
import { requirementcalls } from 'emaris-frontend/requirement/calls'
import { requirementType } from 'emaris-frontend/requirement/constants'
import { Requirement } from 'emaris-frontend/requirement/model'
import { RequirementState, reqstateapi } from 'emaris-frontend/requirement/state'
import { unstable_batchedUpdates } from 'react-dom'
import { campaignapi } from "./api"
import { CampaignArchive, campaigncalls } from "./calls"
import { campaignPlural, campaignRoute, campaignSingular, campaignType } from "./constants"
import { eventinstapi } from "./event/api"
import { eventinstcallapi } from './event/calls'
import { EventInstance } from './event/model'
import { EventInstanceState, eventinststateapi } from "./event/state"
import { Campaign, campaignmodelapi, newCampaign, newCampaignId, noCampaign, unresolvedCampaign } from "./model"
import { partyinstapi } from "./party/api"
import { partyinstcallapi } from './party/calls'
import { PartyInstance } from './party/model'
import { PartyInstanceState, partyinststateapi } from "./party/state"
import { productinstapi } from "./product/api"
import { productinstcallapi } from './product/calls'
import { ProductInstanceState, productinststateapi } from "./product/state"
import { CampaignChangeEvent } from './pushevents'
import { requirementinstapi } from "./requirement/api"
import { requirementinstcallapi } from './requirement/calls'
import { RequirementInstanceState, requirementinststateapi } from "./requirement/state"
import { submissionapi } from "./submission/api"
import { submissioncallapi } from './submission/calls'
import { SubmissionState, submissionstateapi } from "./submission/state"

export type CampaignNext = {

    source: Campaign
    type: 'new' | 'branch' | 'clone'
    lineage?: Campaign
    timeOffset?: number
}

export type CampaignState = {

    campaigns: {

        all: Campaign[]
        map: Record<string, Campaign>
        next?: CampaignNext
        preloaded: boolean,
        instances: {

            [key: string]: PartyInstanceState & RequirementInstanceState & ProductInstanceState & EventInstanceState & SubmissionState & { archive: ArchiveState }
        }

    } & DashboardState
}

export type ArchiveState = RequirementState & ProductState & EventState & TenantState


export const initialCampaign: CampaignState = {

    campaigns: {

        all: undefined!,
        map: undefined!,
        preloaded: false,
        instances: {},

        ...initialDashboard

    }


}

export type PruneInstanceCheck = {
    predicate: (c: Campaign) => boolean
}


export const campaignstateapi = (s: State) => {


    const system = () => systemapi(s)
    const call = () => campaigncalls(s)

    const t = intlapi(s).getT()
    const model = () => campaignmodelapi(s)

    const type = campaignType
    const [singular, plural] = [t(campaignSingular), t(campaignPlural)].map(n => n.toLowerCase())



    const self = {

        areReady: () => !!s.campaigns.all

        ,

        arePreloaded: () => !!s.campaigns.preloaded

        ,

        all: () => s.campaigns.all

        ,

        allInstances: () => s.campaigns.instances

        ,

        isLoaded: (campaign: string) => !!s.products.all && !!s.requirements.all && !!s.events.all && !!s.campaigns.instances[campaign]?.events && !!s.campaigns.instances[campaign]?.parties && !!s.campaigns.instances[campaign]?.requirements && !!s.campaigns.instances[campaign]?.products && !!s.campaigns.instances[campaign]?.submissions

        ,

        allWith: (type: string, instanceOf: string) =>

            Promise.all([
                self.fetchAll(),
                call().fetchWith({ type: type, id: instanceOf })
            ])
                .then(([campaigns, matches]) => matches.map(m => campaigns.find(c => c.id === m) || unresolvedCampaign(m)))
        ,

        allSorted: () => {

            return [...self.all() ?? []].sort(model().comparator)

        }

        ,

        allStarted: () => {

            return [...self.all() ?? []].filter(model().isStarted).sort(model().startedComparator)

        }

        ,

        defaultCampaign: () => self.lookup(campaignapi(s).userPreferences().lastVisitedCampaign()) ??
            self.allSorted().filter(c => campaignmodelapi(s).startDate(c))[0]
            ?? self.allSorted()[0]

        ,

        lookup: (id: string | undefined) => id ? s.campaigns.map?.[id] : undefined

        ,

        safeLookup: (id: string | undefined) => self.lookup(id) ?? noCampaign(s)
        ,

        lookupByName: (name: string | undefined) => self.all()?.find(u => u.name[defaultLanguage] === name)

        ,

        livePush: (event: CampaignChangeEvent) => {

            const { campaign, instanceType, instances, type, trails } = event

            unstable_batchedUpdates(() => {

                switch (instanceType) {

                    case requirementType: requirementinststateapi(s)(campaign).livePush(instances, type === 'remove' ? type : 'change'); break;
                    case productType: productinststateapi(s)(campaign).livePush(instances, type === 'remove' ? type : 'change'); break;
                    case tenantType: partyinststateapi(s)(campaign).livePush(instances as PartyInstance[], type === 'remove' ? type : 'change'); break;
                    case eventType: eventinststateapi(s)(campaign).livePush(instances as EventInstance[], type === 'remove' ? type : 'change'); break;
                }

                submissionstateapi(s)(campaign).livePush(trails, type === 'remove' ? type : 'change');

            })
        }

        ,

        setAll: (campaigns: Campaign[], props?: { noOverwrite: boolean }) => s.changeWith(s => {

            if (s.campaigns.all && props?.noOverwrite)
                return

            s.campaigns.all = campaigns;
            s.campaigns.map = indexMap(campaigns).by(c => c.id)

        })

        ,

        setArchive: (campaign: Campaign, archive: CampaignArchive) => {

            const map = archive.reduce((acc, e) => {

                switch (e.type) {

                    case tenantType: acc.tenants = [...acc.tenants ?? [], e.details]; break
                    case requirementType: acc.requirements = [...acc.requirements ?? [], e.details]; break
                    case productType: acc.products = [...acc.products ?? [], e.details]; break
                    case eventType: acc.events = [...acc.events ?? [], e.details]; break
                }

                return acc

            },  {
                    requirements: [],
                    products: [],
                    tenants: [],
                    events: []
                
                } as { requirements: Requirement[], products: Product[], tenants: Tenant[], events: Event[] })


            s.changeWith(s => {

                s.campaigns.instances[campaign.id] = {

                    ...s.campaigns.instances[campaign.id] ?? {}, archive: {

                        requirements: {

                            all: map.requirements,
                            map: indexMap(map.requirements).by(r => r.id)
                        }
                        ,
                        products: {

                            all: map.products,
                            map: indexMap(map.products).by(p => p.id)
                        }
                        ,
                        tenants: {

                            all: map.tenants,
                            map: indexMap(map.tenants).by(p => p.id)

                        }
                        ,
                        events: {

                            all: map.events

                        }

                    } as ArchiveState
                }

            })

        }

        ,


        fetchAll: (forceRefresh = false): Promise<Campaign[]> =>


            self.areReady() && !forceRefresh ? Promise.resolve(self.all())

                :

                system().toggleBusy(`${type}.fetchAll`, t("common.feedback.load", { plural }))

                    .then(_ => console.log(`fetching campaigns...`))
                    .then(_ => call().fetchAll())
                    .then(through($ => self.setAll($, { noOverwrite: true })))


                    .catch(e => showAndThrow(e, t("common.calls.fetch_all_error", { plural })))
                    .finally(() => system().toggleBusy(`${type}.fetchAll`))

        ,

        // featches all campaigns to preload the data of those that are running.
        preloadRunningCampaigns: () => {

            const updates: (() => void)[] = []

            const {isRunning} = model()
           
            call().fetchAll()
                .then(through($ => updates.push(() => self.setAll($, { noOverwrite: true }))))
                .then(campaigns => campaigns.filter(isRunning))
                .then(campaigns => self.fetchManyQuietly(campaigns, true, updates))
                .finally(() => {

                    unstable_batchedUpdates(() => {

                        updates.forEach(f => f())

                        s.changeWith(s => s.campaigns.preloaded = true)
                    })


                })



        }
        ,

        // loads one ore more campaigns in the background, i.e. without slowing the calls required to render a first view.
        // accumulates all state updates triggered by loading, so that they can be flushed at once and trigger a single render.
        // if the client presents itw own buffer of updates, it add those that and leaves the flushing to the client.
        fetchManyQuietly: (campaigns: Campaign[], noOverwrite: boolean = false, clientudpates?: (() => void)[]) => {


            const updates: (() => void)[] = clientudpates ?? []

            // 1 fetch all campaigns and templates in parallel.
            // 2. for eah running campaign, load instances/trails in parallel and stores in client state (if it hasn't already in concurrent)
            // 3. mark end of preload for who wants to sync with it.

            // note; we'd better parallelise requests across all campaigns, not just inside single campaigns.
            // but in http/1 this would exceed the limits of browsers, to the effect that calls required for rendering (high-priority) woul be scheduled after.
            // instead we want to preload in the background and never increase the wait to render the first page.
            // we can increase parallelism significantly if we move to htto/2.

            return Promise.all([

                requirementcalls(s).fetchAll().then($ => updates.push(() => reqstateapi(s).setAll($, { noOverwrite }))),
                productcalls(s).fetchAll().then($ => updates.push(() => productstateapi(s).setAll($, { noOverwrite }))),
                eventcalls(s).fetchAll().then($ => updates.push(() => eventstateapi(s).setAll($, { noOverwrite }))),

                campaigns.reduce((acc, next) => acc.then(() =>

                    Promise.resolve(console.log(`loading campaign ${next.name.en}(${next.id})`))
                        .then(() => Promise.all([

                            partyinstcallapi(s)(next).fetchAll().then($ => updates.push(() => partyinststateapi(s)(next).setAll($, { noOverwrite }))),
                            requirementinstcallapi(s)(next).fetchAll().then($ => updates.push(() => requirementinststateapi(s)(next).setAll($, { noOverwrite }))),

                            productinstcallapi(s)(next).fetchAll().then($ => updates.push(() => productinststateapi(s)(next).setAll($, { noOverwrite }))),
                            eventinstcallapi(s)(next).fetchAll().then($ => updates.push(() => eventinststateapi(s)(next).setAll($, { noOverwrite }))),
                            submissioncallapi(s)(next).fetchAllTrails().then($ => updates.push(() => submissionstateapi(s)(next).setAllTrails($, { noOverwrite })))

                        ]))

                ), Promise.resolve() as Promise<any>)

            ])

                .finally(() => {

                    if (!clientudpates)

                        unstable_batchedUpdates(() => {

                            updates.forEach(f => f())

                        })


                })

        }

        ,

        fetchOne: (campaign: Campaign, forceRefresh?: boolean): Promise<Campaign> => {

            const archived = campaign.lifecycle.state === 'archived'
            const t0 = performance.now()

            return system().toggleBusy(`${type}.fetchOne`, archived ? t("campaign.feedback.load_archive") : t( "common.feedback.load_one", { singular }))

                .then(_ => call().fetchOne(campaign.id))
                .then(({ campaign, archive }) => {

                    if (archived)
                        self.setArchive(campaign, archive)

                    return campaign

                })
                .then(through(fetched =>

                    // if no instance is mocked, then we can afford a more parallel approach.
                    Promise.all([partyinstapi(s)(fetched).fetchAll(forceRefresh),
                    requirementinstapi(s)(fetched).fetchAll(forceRefresh),
                    productinstapi(s)(fetched).fetchAll(forceRefresh),
                    eventinstapi(s)(fetched).fetchAll(forceRefresh),
                    submissionapi(s)(fetched).fetchAllTrails(forceRefresh)])

                ))

                .then(through(fetched => { if (!fetched) throw Error(`Invalid resource lookup for ${campaign.id}, returned undefined.`) }))
                .then(fetched => {
                    const time = performance.now() - t0
                    console.log(`loading campaign took ${time}`)
                    return { ...fetched, loaded: true }
                })   // interning campaign
                .then(through(fetched => self.setAll(self.all().map(u => u.id === campaign.id ? fetched : u))))

                .catch(e => showAndThrow(e, t("common.calls.fetch_one_error", { singular })))
                .finally(() => system().toggleBusy(`${type}.fetchOne`))


        }


        ,

        routeTo: (campaign: Campaign) => campaign ? `${campaignRoute}/${campaign.id}` : undefined!


        ,

        next: (): CampaignNext => s.campaigns.next ?? { source: newCampaign(s), type: 'new' }

        ,

        resetNext: () => self.setNext(undefined)

        ,

        setNext: (context: CampaignNext | undefined) => s.changeWith(s => s.campaigns.next = context)

        ,


        save: (campaign: Campaign, context: CampaignNext): Promise<Campaign> => {


            const updateCampaign = () => call().update(campaign)
                    .then(({ campaign, archive }) => {

                        if (campaign.lifecycle.state==='archived')
                            self.setArchive(campaign, archive)

                        return campaign

                    })
                    .then(saved => ({ ...saved, loaded: true }))
                    .then(through(saved => self.setAll(self.all().map(c => c.id === campaign.id ? saved : c))))

            
            
            const addCampaign = () => call().add({ ...campaign, id: newCampaignId() })
                .then(through(saved => self.setAll([saved, ...self.all()])))
                .then(through(saved => context.type === 'new' ?

                    eventinstapi(s)(saved).addManagedForCampaign()
                    :

                    self.cloneInstances(saved, context)))




            return system().toggleBusy(`${type}.save`, t("common.feedback.save_changes"))


                .then(() => campaign.id ? updateCampaign() : addCampaign())

                .then(through(() => notify(t('common.feedback.saved'))))
                .catch(e => showAndThrow(e, t("common.calls.save_one_error", { singular })))
                .finally(() => system().toggleBusy(`${type}.save`))
        }


        ,



        cloneInstances: (to: Campaign, context: CampaignNext) => {

            const { lineage } = context

            const from = lineage!

            console.log(`cloning instances from ${from.name[defaultLanguage]} into ${to.name[defaultLanguage]}`)

            const parties = partyinstapi(s)
            const requirements = requirementinstapi(s)
            const products = productinstapi(s)
            const events = eventinstapi(s)

            return system().toggleBusy(`clone.campaign.${to.id}`, t("campaign.feedback.clone_resources"))

                .then(_ => Promise.all([

                    parties(from).fetchAll().then(all => parties(to).cloneAll(all, context)),
                    requirements(from).fetchAll().then(all => requirements(to).cloneAll(all, context)),
                    products(from).fetchAll().then(all => products(to).cloneAll(all, context)),
                    events(from).fetchAll().then(all => events(to).cloneAll(all, context))



                ]))
                .catch(e => showError(e, { title: t("campaign.feedback.clone_error", { singular }) }))
                .finally(() => system().toggleBusy(`clone.campaign.${to.id}`))


        }

        ,

        // removeFromStateByInstanceSource : (source: string) => {
        //     const toRemove = self.allByInstanceOf(source)

        //     self.setAll(self.all().filter(campaign=> toRemove.some(rem=>rem.id===campaign.id)))
        // }

        // ,

        remove: (id: string, onConfirm: () => void, challenge?: boolean) =>

            utilapi(s).askConsent({

                title: t('common.consent.remove_one_title', { singular }),
                content: t('common.consent.remove_one_msg', { singular }),
                okText: t('common.consent.remove_one_confirm', { singular }),

                okChallenge: challenge ? t('common.consent.remove_challenge', { singular }) : undefined,

                onOk: () => {


                    system().toggleBusy(`${type}.removeOne`, t("common.feedback.save_changes"))

                        .then(_ => call().delete(id))
                        .then(_ => self.all().filter(u => id !== u.id))
                        .then(self.setAll)
                        .then(_ => notify(t('common.feedback.saved')))
                        .then(_ => onConfirm && onConfirm())

                        .catch(e => showAndThrow(e, t("common.calls.remove_one_error", { singular })))
                        .finally(() => system().toggleBusy(`${type}.removeOne`))


                }
            }),

        removeAll: (list: string[], onConfirm?: () => void) =>

            utilapi(s).askConsent({


                title: t('common.consent.remove_many_title', { count: list.length, plural }),
                content: t('common.consent.remove_many_msg', { plural }),
                okText: t('common.consent.remove_many_confirm', { count: list.length, plural }),

                onOk: () => {


                    system().toggleBusy(`${type}.removeAll`, t("common.feedback.save_changes"))

                        .then(_ => list.forEach(id => call().delete(id)))
                        .then(_ => self.all().filter(u => !list.includes(u.id)))
                        .then(self.setAll)
                        .then(_ => notify(t('common.feedback.saved')))
                        .then(_ => onConfirm && onConfirm())

                        .catch(e => showAndThrow(e, t("common.calls.remove_one_error", { singular })))
                        .finally(() => system().toggleBusy(`${type}.removeAll`))


                }
            })

    }



    return self;

}