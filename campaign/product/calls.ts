import { callapi } from "apprise-frontend/call/call";
import { domainService } from "../../constants";
import { EmarisState } from "../../state/model";
import { Campaign } from "../model";
import { ProductInstance } from "./model";




export const productinstcallapi = (s:EmarisState) => (c: Campaign) => {

    const products = `/productinstance`
    const {at} = callapi(s)


   return {

        fetchAll : () : Promise<ProductInstance[]>  =>  at(`${products}/search`,domainService).post({campaign:c.id})

        , 

        fetchOne : (id:string) : Promise<ProductInstance>  =>  at(`${products}/${id}?byref=true`,domainService).get()

        ,
        
        addAll : (instances:ProductInstance[]) : Promise<ProductInstance[]>  =>  at(products,domainService).post(instances)

        ,

        updateOne: (instance:ProductInstance) : Promise<void>  => at(`${products}/${instance.id}`,domainService).put(instance)

        ,
                    
        removeOne: (instance:ProductInstance) : Promise<void>  =>  at(`${products}/${instance.id}`,domainService).delete()
        
        ,
        
        removeMany: (instances:ProductInstance[]) : Promise<void>  => at(`${products}/remove`,domainService).post(instances.map(i=>i.id))
 
    }
}
