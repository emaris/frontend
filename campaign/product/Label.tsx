import { Tooltip } from "antd"
import { Label, sameLabel, UnknownLabel } from "apprise-frontend/components/Label"
import { configapi, useConfig } from "apprise-frontend/config/state"
import { icns } from "apprise-frontend/icons"
import { TimeLabel } from "apprise-frontend/time/Label"
import { dashboardapi, useDashboards } from "emaris-frontend/dashboard/api"
import { CampaignMode, useCampaignMode } from "emaris-frontend/dashboard/hooks"
import { productRoute } from "emaris-frontend/product/constants"
import React from "react"
import { useTranslation } from "react-i18next"
import { Link } from "react-router-dom"
import { ProductLabel } from "../../product/Label"
import { campaignapi, useCampaigns } from "../api"
import { campaignSingular, campaignType, runningIcon } from "../constants"
import { eventinstapi, useAllEventInstances } from "../event/api"
import { InstanceLabel, InstanceLabelProps } from "../InstanceLabel"
import { CampaignLabel } from "../Label"
import { InstanceRef, isInstanceRef } from "../model"
import { submissionType } from "../submission/constants"
import { productinstapi, useAllProductsInstances } from "./api"
import { ProductInstance } from "./model"

type Props = InstanceLabelProps & {

    instance: ProductInstance | InstanceRef | undefined
    link?: boolean

    deadlineOnly?: boolean
    showTitle?: boolean
    tipTitle?: boolean
}

export const ProductInstanceLabel = (props: Props) => {

    const config = useConfig()
    const campaigns = useCampaigns()
    const dashboard = useDashboards()
    const campaignMode = useCampaignMode()
    const allProducts = useAllProductsInstances()
    const allEvents = useAllEventInstances()

    return <MemoLabel {...props} config={config} campaigns={campaigns} dashboard={dashboard} campaignMode={campaignMode} allProducts={allProducts} allEvents={allEvents} />

}

const emptySpace = <div style={{width: 13}} />

const MemoLabel = React.memo(function PureTenantLabel(props: Props & {

    config: ReturnType<typeof configapi>
    campaigns: ReturnType<typeof campaignapi>
    dashboard: ReturnType<typeof dashboardapi>
    campaignMode: CampaignMode
    allProducts: ReturnType<typeof productinstapi>
    allEvents: ReturnType<typeof eventinstapi>

}) {

    const { instance, linkTo, linkTarget, options = [], showCampaign, deadlineOnly, showTitle=false, tipTitle=false,
        config, campaigns, dashboard, campaignMode, allEvents, allProducts, ...rest } = props

    const { t } = useTranslation()

    const campaign = campaigns.lookup(instance?.campaign)

    if (!campaign)
        return <UnknownLabel title={t("common.labels.unknown_one", { singular: t(campaignSingular).toLowerCase() })} />


    const campaignTimeZone = campaign.properties.timeZone ? {'original' : campaign.properties.timeZone} : undefined
    
    const products = allProducts(campaign)

    var productinstance = instance as ProductInstance;

    if (isInstanceRef(instance))

        if (campaigns.isLoaded(instance.campaign))
            productinstance = products.lookupLineage(instance)!
        else
            return <CampaignLabel  loadTrigger campaign={instance.campaign} />

    if (!productinstance)
        return <UnknownLabel {...props} />
    
    const events = allEvents(campaign)

    if (deadlineOnly) {

        const deadline = products.deadlineOf(productinstance)

        if (deadline) {
            const date = events.absolute(deadline.date)
            return date ? <TimeLabel accentFrame='weeks' format='numbers' render={props.mode === 'tag' ? date => t('campaign.date.due', { date }) : undefined} value={date} timezones={campaignTimeZone} {...rest} />
                    : <Label {...props} noIcon noLink title={t("common.labels.unknown_due_date")} disabled />
        }
        else return <Label {...props} noIcon noLink title={t("common.labels.no_due_date")} disabled />

    }
    // console.log(productType, submissionType, linkTarget, linkTo)

    if (campaignMode === 'dashboard' && config.get().routedTypes?.includes(campaignType))
        options.push(<Tooltip title={t("campaign.labels.campaign_view.design")}><Link to={() => products.route(productinstance)}>{icns.edit}</Link></Tooltip>)
    else 
        options.push(emptySpace)

    if (campaignMode === 'design' && config.get().routedTypes?.includes(submissionType))
        options.push(<Tooltip title={t("campaign.labels.campaign_view.live")}><Link to={() => dashboard.given(campaign).routeToAsset(productinstance)}>{runningIcon}</Link></Tooltip>)

    return <InstanceLabel instance={productinstance} noMemo={props.noMemo} tooltipForLineage={ref => <ProductInstanceLabel showCampaign instance={ref} />} 
        baseRoute={productRoute}
        {...rest}
        sourceLabel={props => <ProductLabel {...props} {...rest} noMemo={props.noMemo} showTitle={showTitle} tipTitle={tipTitle} options={options} linkTo={linkTo || (() => products.route(productinstance))} linkTarget={submissionType} product={instance?.source} />} />


}, ($: Props, $$: Props) => $.instance === $$.instance && sameLabel($,$$))
