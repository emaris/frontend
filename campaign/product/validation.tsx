
import { intlapi } from "apprise-frontend/intl/api"
import { tagapi } from "apprise-frontend/tag/api"
import { tenantPlural } from 'apprise-frontend/tenant/constants'
import { userPlural } from 'apprise-frontend/user/constants'
import { checkIt, Validation, withReport } from "apprise-frontend/utils/validation"
import { NamedAsset } from 'emaris-frontend/layout/components/AssetSection'
import { productPlural, productSingular, productType } from "emaris-frontend/product/constants"
import { ProductLabel } from 'emaris-frontend/product/Label'
import { requirementType } from 'emaris-frontend/requirement/constants'
import { RequirementLabel } from 'emaris-frontend/requirement/Label'
import { productapi } from '../../product/api'
import { EmarisState } from "../../state/model"
import { Campaign } from "../model"
import { requirementinstapi } from '../requirement/api'
import { productinstapi } from './api'
import { ProductInstance } from "./model"


export const productinstvalidationapi = (s: EmarisState) => (c: Campaign) => {

    const t = intlapi(s).getT()

    const { validateCategories } = tagapi(s)

    const singular = t(productSingular).toLowerCase()
    const plural = t(productPlural).toLowerCase()


    const partyPlural = t(tenantPlural).toLowerCase()

    const validateProfile = (edited: ProductInstance) => ({

        note: checkIt().nowOr(t("campaign.fields.note.msg"))

        ,

        lineage: checkIt().nowOr(t("common.fields.lineage.msg_nochoice", { singular }))

        ,

        source: checkIt().nowOr(t("campaign.fields.source.msg", { singular }))

        ,

        audience: checkIt().nowOr(
            t("common.fields.audience.msg"),
            t("common.fields.audience.help", { singular, plural: partyPlural })
        ),

        userProfile: checkIt().nowOr(
            t("common.fields.user_profile.msg"),
            t("common.fields.user_profile.help", { singular, plural: userPlural })
        )

        ,

        editable: checkIt().nowOr(
            t("common.fields.editable.help", { plural, parties: partyPlural })
        )

        ,

        versionable: checkIt().nowOr(
            t("common.fields.versionable.help", { plural, parties: partyPlural })
        )

        ,

        assessed: checkIt().nowOr(
            t("common.fields.assessed.help", { plural })
        )

        ,

        ...validateCategories(edited.tags).for(productType)

    })


    const validateAssetDependencies = (edited: ProductInstance): Record<string,Validation> => {

        
       const instances = [...requirementinstapi(s)(c).all(), ...productinstapi(s)(c).all()]

       const products = productapi(s)

        const product = products.safeLookup(edited.source!)
        const dependencies = product.properties.dependencies ?? []

        const labelOf = (asset:NamedAsset) => asset.type === requirementType ? <RequirementLabel noDecorations requirement={asset.asset} /> :<ProductLabel noDecorations product={asset.asset} />

        return dependencies
            .filter(({asset}) => !instances.some(i => i.source === asset))
            .map(asset => ({ id: asset.asset, validation: { 
                status: "error",
                msg: labelOf(asset)
        
        } as Validation }))
            .reduce((acc, { id, validation }) => ({ [id]: validation, ...acc }), {} as Record<string,Validation>) 
    }



    const self = {

        validateInstanceProfile: (edited: ProductInstance) => withReport(validateProfile(edited))

        ,

        validateAssetDependencies

        ,

        validateInstance: (edited: ProductInstance) => withReport({ 
            
            ...validateAssetDependencies(edited), 

            ...validateProfile(edited) })

    }


    return self


}