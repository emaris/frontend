import { intlapi } from "apprise-frontend/intl/api";
import { defaultLanguage } from "apprise-frontend/intl/model";
import { systemapi } from "apprise-frontend/system/api";
import { tagapi } from "apprise-frontend/tag/api";
import { TenantDto } from "apprise-frontend/tenant/model";
import { utilapi } from "apprise-frontend/utils/api";
import { indexMap, through } from "apprise-frontend/utils/common";
import { notify, showAndThrow } from "apprise-frontend/utils/feedback";
import { paramsInQuery, updateQuery } from "apprise-frontend/utils/routes";
import { productapi } from "emaris-frontend/product/api";
import { productPlural, productSingular, productType } from "emaris-frontend/product/constants";
import { EmarisState } from "emaris-frontend/state/model";
import { campaignapi } from "../api";
import { eventinstapi } from "../event/api";
import { Campaign, CampaignInstance, InstanceRef, noCampaignInstance } from "../model";
import { PartyInstance, partyinstmodelapi } from "../party/model";
import { CampaignNext } from '../state';
import { productinstcallapi } from "./calls";
import { ProductInstance, productinstmodelapi } from "./model";


export type ProductInstanceState = {

    products: {
        all: ProductInstance[]
        map: Record<string, ProductInstance>
        sourceMap: Record<string, ProductInstance>
    }

}

export const initialProductInstances = (): ProductInstanceState => ({

    products: {
        all: undefined!,
        map: undefined!,
        sourceMap: undefined!
    }
})


export const productinststateapi = (s: EmarisState) => (c: Campaign) => {


    const { toggleBusy } = systemapi(s)
    const t = intlapi(s).getT()

    const type = "productinstance"
    const [singular, plural] = [t(productSingular).toLowerCase(), t(productPlural).toLowerCase()]


    const call = () => productinstcallapi(s)(c)
    const model = () => productinstmodelapi(s)(c)
    const campaigns = () => campaignapi(s)
    const events = () => eventinstapi(s)(c)

    const paryinstancemodel = partyinstmodelapi(s)(c)

    const self = {

        livePush: (instances: ProductInstance[], type: 'change' | 'remove') => {

            const map = s.campaigns.instances[c.id]?.products.map ?? {}
            let current = s.campaigns.instances[c.id]?.products.all ?? []

            instances.forEach(instance => {

                switch (type) {

                    case "change":
                        current = map[instance.id] ? current.map(i => i.id === instance.id ? instance : i) : [...current, instance]
                        break;

                    case "remove": current = current.filter(i => i.id !== instance.id); break

                }

            })

            self.setAll(current)

        }


        ,

        setAll: (instances: ProductInstance[], props?: { noOverwrite: boolean }) => s.changeWith(s => {

            const campaign = s.campaigns.instances[c.id] ?? {}
            const products = campaign.products ?? initialProductInstances()

            if (products.all && props?.noOverwrite)
                return

            s.campaigns.instances[c.id] = { ...campaign, products: { ...products, all: instances, map: indexMap(instances).by(i => i.id), sourceMap: indexMap(instances).by(i => i.source) } }

        })

        ,

        addAll: (instances: ProductInstance[]) =>

            toggleBusy(`${type}.addall`, t("common.feedback.save_changes"))

                .then(_ => call().addAll(instances))
                .then(through(added => self.setAll([...self.all(), ...added])))
                // notifies the event system to generate automanaged events.
                .then(through(added => events().addManagedForInstances(added)))
                .then(through(() => notify(t('common.feedback.saved'))))

                .catch(e => showAndThrow(e, t("common.calls.add_many_error", { plural })))
                .finally(() => toggleBusy(`${type}.addall`))

        ,

        cloneAll: (instances: ProductInstance[], context: CampaignNext) => call().addAll(instances.map(i => model().clone(i, context))).then(self.setAll)

        ,

        areReady: () => !!self.all()

        ,

        fetchAll: (forceRefresh = false) =>

            Promise.all([productapi(s).fetchAll(),


            self.areReady() && !forceRefresh ?

                Promise.resolve(self.all())

                :

                toggleBusy(`${type}.fetchAll.${c.id}`, t("common.feedback.load", { plural }))

                    .then(_ => console.log(`fetching products for ${c.name[defaultLanguage]}...`))
                    .then(call().fetchAll) // load template dependency in parallel
                    .then(through($ => self.setAll($, { noOverwrite: !forceRefresh })))


                    .catch(e => showAndThrow(e, t("common.calls.fetch_all_error", { plural })))
                    .finally(() => toggleBusy(`${type}.fetchAll.${c.id}`))


            ]).then(([, instances]) => instances)



        ,

        lookup: (id: string | undefined) => id ? s.campaigns.instances[c.id]?.products?.map[id] : undefined

        ,

        safeLookup: (id: string | undefined) => self.lookup(id) ?? productinstmodelapi(s)(c).noInstance()

        ,

        lookupLineage: (ref: InstanceRef | undefined) => ref ? s.campaigns.instances[ref.campaign]?.products.all.find(p => p.source === ref.source) : undefined

        ,

        lookupBySource: (source: string | undefined) => source ? s.campaigns.instances[c.id]?.products?.sourceMap[source] : undefined
        //self.all().find(p => p.source !== undefined && p.source === source)

        ,

        safeLookupBySource: (source: string | undefined) => self.lookupBySource(source) ?? noCampaignInstance(productType, c.id) as ProductInstance


        ,


        detailParam: () => 'ri-drawer'

        ,

        // eslint-disable-next-line
        detailInRoute: () => paramsInQuery(location.search)[self.detailParam()] as string

        ,


        route: (p?: ProductInstance) =>

            // eslint-disable-next-line
            `${campaigns().routeTo(c)}/${productType}?${updateQuery(location.search).with(params => {
                
                params[self.detailParam()] = p ? p.id : null
                params['source'] = p?.source ?? null
            
            })}`

        ,

        all: () => s.campaigns.instances[c.id]?.products?.all

        ,

        allSorted: () => {

            return [...self.all() ?? []].sort(model().comparator)
        }

        ,

        allForParty: (party: PartyInstance, includeNotApplicable = paryinstancemodel.canSeeNotApplicable(party)) =>

            (includeNotApplicable ? self.allSorted() : self.allSorted()).filter(pi => self.isForParty(party, pi))

        ,

        isForParty: (party: PartyInstance | TenantDto, ci: CampaignInstance) => {

            const partyId = (party as PartyInstance).source ?? (party as TenantDto).id

            const audienceListCheckInclude = ci.audienceList ? ci.audienceList.includes.includes(partyId) || ci.audienceList.includes.length === 0 : true
            const audienceListCheckExclude = ci.audienceList ? !ci.audienceList.excludes.includes(partyId) || ci.audienceList.excludes.length === 0 : true

            const audienceListCheck = audienceListCheckExclude && audienceListCheckInclude

            return audienceListCheck && (ci.audience ? tagapi(s).expression(ci.audience).matches(party) : true)
            
        }

        ,

        save: (instance: ProductInstance, replacementOf: ProductInstance = instance) =>

            toggleBusy(`${type}.updateOne`, t("common.feedback.save_changes"))

                .then(_ => call().updateOne(instance))
                .then(() => self.all().map(p => p.id === replacementOf.id ? instance : p))
                .then(self.setAll)
                .then(() => notify(t('common.feedback.saved')))

                .catch(e => showAndThrow(e, t("common.calls.update_one_error", { singular })))
                .finally(() => toggleBusy(`${type}.updateOne`))



        ,

        remove: (instance: ProductInstance, onConfirm?: (...args) => void) =>

            utilapi(s).askConsent({

                title: t('common.consent.remove_one_title', { singular }),
                content: t("common.consent.remove_one_msg", { singular }),
                okText: t("common.consent.remove_one_confirm", { singular }),

                onOk: () => {


                    toggleBusy(`${type}.removeOne`, t("common.feedback.save_changes"))

                        .then(_ => call().removeOne(instance))
                        .then(() => self.all().filter(p => instance.id !== p.id))
                        .then(self.setAll)
                        .then(() => notify(t('common.feedback.saved')))
                        .then(() => onConfirm && onConfirm(instance))

                        .catch(e => showAndThrow(e, t("common.calls.remove_one_error", { singular })))
                        .finally(() => toggleBusy(`${type}.removeOne`))


                }
            })

        ,

        removeMany: (instances: ProductInstance[], onConfirm?: () => void) =>

            utilapi(s).askConsent({

                title: t('common.consent.remove_many_title', { count: instances.length, plural }),
                content: t("common.consent.remove_many_msg", { count: instances.length, plural }),
                okText: t("common.consent.remove_many_confirm", { count: instances.length, plural }),

                onOk: () => {

                    const ids = instances.map(i => i.id)

                    toggleBusy(`${type}.removeMany`, t("common.feedback.save_changes"))

                        .then(_ => call().removeMany(instances))
                        .then(() => self.all().filter(p => !ids.includes(p.id)))
                        .then(through(self.setAll))
                        .then(() => notify(t('common.feedback.saved')))
                        .then(through(() => onConfirm && onConfirm()))

                        .catch(e => showAndThrow(e, t("common.calls.remove_many_error", { plural })))
                        .finally(() => toggleBusy(`${type}.removeMany`))

                }
            })
    }

    return self
}