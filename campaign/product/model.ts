import { ParameterRef } from 'apprise-frontend/layout/parameters/model';
import { layoutRegistry } from 'apprise-frontend/layout/registry';
import { tagapi } from "apprise-frontend/tag/api";
import { TagExpression } from 'apprise-frontend/tag/model';
import { noTenant } from 'apprise-frontend/tenant/constants';
import { timeapi } from "apprise-frontend/time/api";
import { User } from 'apprise-frontend/user/model';
import { deepclone } from "apprise-frontend/utils/common";
import { Topic } from "apprise-messages/model";
import { productType } from "emaris-frontend/product/constants";
import moment from "moment-timezone";
import { productapi } from "../../product/api";
import { Product } from "../../product/model";
import { EmarisState } from "../../state/model";
import { productDeadlineEvent } from "../constants";
import { eventinstapi } from "../event/api";
import { AssetInstance, AssetInstanceProperties, Campaign, CampaignInstance } from "../model";
import { PartyInstance } from "../party/model";
import { CampaignNext } from '../state';
import { productinstapi } from "./api";

export type ProductInstanceDto = AssetInstance & {

    // requirements: string[]
    properties: AssetInstanceProperties & {

        userProfile?:  TagExpression
    }

}

export type ProductInstance = ProductInstanceDto       // future-proofs divergence from exchange model, starts, aligned.

export const productinstmodelapi = (s:EmarisState) => (c:Campaign) => {

    const products = productapi(s)
    
    const self = {

        noInstance: () => ({

            id:undefined!,
            instanceType:productType,
            source:"unknown",
            campaign:c.id,
            tags:[],
            properties:{
                editable: true,
                versionable: true,
                assessed: true
            }

        }) as ProductInstance

        ,

        stringify: (i:ProductInstance) => `${products.stringify(products.safeLookup(i.source))} ${i.properties.note} ${tagapi(s).stringifyRefs(i.tags)}`

        ,

        nameOf : (i:ProductInstance) => `${products.nameOf(products.safeLookup(i.source))}`

        ,
        
        generate: (product:Product) : ProductInstance => ( {
                                          id:undefined!,
                                          instanceType: productType,
                                          source:product.id,
                                          campaign:c.id,
                                        //   requirements: deepclone(products.requirementsFor(product)),
                                          tags:deepclone(product.tags),
                                          audience: product.audience,
                                          audienceList: product.audienceList,
                                          userProfile: product.userProfile,
                                          properties:{
                                            note: product.properties.note,
                                            editable: product.properties.editable,
                                            versionable: product.properties.versionable,
                                            assessed: product.properties.assessed
                                        }})
        
        ,
        
        
        clone: (instance:ProductInstance, campaign:CampaignNext) => {
            const {type} = campaign
            const clone = deepclone(instance)

            const parameterOverlay = clone.properties.parameterOverlay

            const overlays = parameterOverlay && 
                Object.keys(parameterOverlay).reduce( 
                    (acc, cur) => {
                        const param = parameterOverlay![cur] as ParameterRef
                        const clonedParam = type === 'branch' ? 
                            layoutRegistry(productType).lookupParameter(param.original.spec).clone({...param.original, value: param.value}, campaign)
                            :
                            {...param.original, value: param.value}
                        return clonedParam ? 
                            {...acc, [cur]: {original:param.original, value:clonedParam.value}} 
                        : acc
                    }
                ,{})
                
            return {...clone, 
                        id:undefined!,  
                        campaign:c.id, 
                        tags: type === 'branch' ? products.lookup(instance.source)?.tags ?? clone.tags : clone.tags, 
                        lineage: type === 'branch' ? {source:instance.source,campaign:instance.campaign} : undefined,
                        properties: {...clone.properties, parameterOverlay: overlays}
                    } 
        
        
        }


        
        ,

        comparator: (o1:ProductInstance,o2:ProductInstance) => products.comparator(products.safeLookup(o1.source)!,products.safeLookup(o2.source)!)

        ,

        deadlineOf: (p:ProductInstance) => eventinstapi(s)(c).allAbout(p.source).find(ei=>ei.source===productDeadlineEvent)

                ,

        deadlineDateOf: (r:ProductInstance) => eventinstapi(s)(c).absolute( self.deadlineOf(r)?.date )


        ,


        allDueDatesByProductId: () : { [_:string]:string} => {

            const {absolute,all} = eventinstapi(s)(c)
            
            return all().filter(ei=>ei.source===productDeadlineEvent)
                        .reduce((a,ei) =>({...a,[ei.target!]:absolute(ei.date)}), {} )

        },

        pastDue: (party?:PartyInstance) => {
            
            const now = timeapi(s).current()
            const {absolute,all} = eventinstapi(s)(c)
            
            const targets = (party ? productinstapi(s)(c).allForParty(party) : productinstapi(s)(c).all()).map(r=>r.source)

            return all().filter(ei=>ei.source===productDeadlineEvent 
                                    && targets.includes(ei.target!) 
                                    && ei.date && moment(new Date(absolute(ei.date)!)).isBefore(now))
        },


        audienceOf: (p:ProductInstance) => products.safeLookup(p.source).audience
             
        ,

        topics: (p: CampaignInstance): Topic[] => [{ type: productType, name:`${c.id}:${p.source}`  }]

        ,

        matches: (p:ProductInstance, u: User) => {

            const {given} = tagapi(s)
            
            return u.tenant === noTenant || given(u).matches(p.userProfile)
            
        }

    }

    return self

}