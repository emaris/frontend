
import { useEmarisState } from "emaris-frontend/state/hooks";
import { EmarisState } from "../../state/model";
import { Campaign } from "../model";
import { productinstmodelapi } from "./model";
import { productinststateapi } from "./state";
import { productinstvalidationapi } from "./validation";


export const useAllProductsInstances = () => productinstapi(useEmarisState())
export const useProductInstances = (c:Campaign) => productinstapi(useEmarisState())(c)


export const productinstapi = (s:EmarisState) => (c:Campaign) => ({


    ...productinststateapi(s)(c),
    ...productinstmodelapi(s)(c),
    ...productinstvalidationapi(s)(c)

    
})

