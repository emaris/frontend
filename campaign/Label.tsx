import { Tooltip } from "antd"
import { Button } from "apprise-frontend/components/Button"
import { Label, LabelProps, sameLabel, UnknownLabel } from "apprise-frontend/components/Label"
import { useConfig } from "apprise-frontend/config/state"
import { specialise } from "apprise-frontend/iam/model"
import { icns } from "apprise-frontend/icons"
import { useLocale } from "apprise-frontend/model/hooks"
import { useUsers } from "apprise-frontend/user/api"
import { useDashboards } from "emaris-frontend/dashboard/api"
import { useCampaignMode } from "emaris-frontend/dashboard/hooks"
import moment from "moment-timezone"
import * as React from "react"
import { useTranslation } from "react-i18next"
import { Link, useLocation } from "react-router-dom"
import { useCampaigns } from "./api"
import { campaignIcon, campaignRoute, campaignType, runningIcon } from "./constants"
import { Campaign } from "./model"
import { submissionType } from "./submission/constants"

type Props = LabelProps & {

    campaign: string | Campaign | undefined

    statusOnly?: boolean

    loadTrigger?: boolean

    noLiveView?: boolean

    targetMode?: 'default' | 'lineage'
}


const innerrouteRegexp = new RegExp(`${campaignRoute}/(.+?)(/|$)`.replace("/", "\\/"))


export const CampaignLabel = React.memo((props: Props) => {

    const { t } = useTranslation()

    const { pathname, search } = useLocation()

    const { logged } = useUsers()

    const { campaign, decorations = [], options = [], noLiveView = false, linkTo, loadTrigger, className = '', statusOnly, targetMode = 'default', ...rest } = props

    const { l } = useLocale()
    const config = useConfig()
    const campaigns = useCampaigns()
    const dashboard = useDashboards()
    const campaignMode = useCampaignMode()

    const { actions: { manage } } = campaigns

    const resolved = typeof campaign === 'string' ? campaigns.lookup(campaign) : campaign

    if (!campaign)
        return <UnknownLabel />


    if (!resolved)
        return <UnknownLabel tip={() => campaign as string} />

    const manageIt = specialise(manage, resolved.id)

    const isProtected = resolved ? resolved.predefined || resolved.guarded : false
    const readOnly = !logged.can(manageIt)

    const { label, start, end, started, ended } = campaigns.status(resolved)

    const statusClass = started ? ended ? 'campaign-ended' : 'campaign-started' : ''

    const duration = ended ? t("campaign.lifecycle.ended_since", { since: moment(end).fromNow() })
        : started ? t("campaign.lifecycle.started_since", { since: moment(start).fromNow() })
            : start ? t("campaign.lifecycle.start_in", { in: moment(start).fromNow() })
                : undefined

    const iconWithStatus = (icon = campaignIcon) => started ? <span className={`campaign-icon ${statusClass}`}>{icon}</span> : icon

    if (statusOnly)
        return <Label className={`status-only ${statusClass}`}
            linkTo={started ? dashboard.routeTo(resolved) : undefined}
            tip={duration ? () => duration : undefined}
            linkTarget={campaignType} title={label}
            icon={iconWithStatus(started ? runningIcon : campaignIcon)}
            {...rest} />

    const icon = started ? <Tooltip title={duration}>{iconWithStatus(campaignIcon)}</Tooltip> : iconWithStatus()

    if (targetMode === 'lineage') {
      
        return resolved.lineage ?
            <CampaignLabel campaign={resolved.lineage} decorations={decorations} noLiveView options={options} className={`${className}`} {...rest} />

            :

            <Label mode='tag' title={t("common.labels.none")} />
    }


    if (started && !noLiveView) {
        const icon = iconWithStatus(runningIcon)
        const contents = config.get().routedTypes?.includes(campaignType) ? <Link to={() => dashboard.routeTo(resolved)}>{icon}</Link> : icon
        if (config.get().routedTypes?.includes(submissionType))
            options.push(<Tooltip title={t("campaign.labels.campaign_view.live")}>{contents}</Tooltip>)

    }

    if (campaigns.isMuted(resolved))
        decorations.push(<Tooltip title={t("campaign.mute.muted")}>{icns.muted}</Tooltip>)

    if (campaignMode === 'dashboard' && config.get().routedTypes?.includes(campaignType))
        options.push(<Tooltip title={t("campaign.labels.campaign_live.design")}><Link to={() => campaigns.routeTo(resolved)}>{icns.edit}</Link></Tooltip>)

    // like routeTo() if the current route is not a campaign route, otherwise replaces the current campaign with the target one.
    // undefined, if the two campaigns coincide.
    const innerRouteTo = (target: Campaign) => {

        const regexpmatch = pathname.match(innerrouteRegexp)?.[1]
        const linkname = target.id

        return regexpmatch ?
                    regexpmatch === linkname ? undefined : `${pathname.replace(regexpmatch, linkname!)}${search}`
                : campaigns.routeTo(target)


    }

    const route = linkTo ?? innerRouteTo(resolved) 

    if (loadTrigger)
        decorations.push(<Button tooltip={t("common.feedback.load_details")} className="load-campaign" icn={icns.download} type="link" noborder onClick={() => campaigns.fetchOne(resolved)} />)

    if (resolved.lineage)
        decorations.push(<Tooltip title={<CampaignLabel mode="light" campaign={campaigns.lookup(resolved.lineage)} />}>{icns.branch}</Tooltip>)


    return <Label className={`${statusClass} ${className}`} linkTarget={campaignType} readonly={readOnly} isProtected={isProtected} title={l(resolved.name)} icon={icon} linkTo={route} decorations={decorations} options={options} {...rest} />

}, ($, $$) => $.campaign === $$.campaign && sameLabel($, $$))



type SuspendedLabelProps = LabelProps & {

    campaign: string | Campaign
}

export const SuspendedLabel = React.memo((props: SuspendedLabelProps) => {

    const {campaign, className='', ...rest} = props

    const { t } = useTranslation()
    const campaigns = useCampaigns()

    const currentCampaign = typeof campaign === 'string' ? campaigns.safeLookup(campaign) : campaign

    const suspended = campaigns.isSuspendSubmissions(currentCampaign)
    const archived = campaigns.isArchived(currentCampaign)

    if (!suspended || archived)
        return <></>

    return <Label mode='tag' className={`campaign-suspended ${className}`} icon={icns.pause} title={t("campaign.suspend.suspended")} {...rest} />


}, ($, $$) => $.campaign === $$.campaign && sameLabel($, $$))

type MutedLabelProps = LabelProps & {

    campaign: string | Campaign
}


export const MutedLabel = React.memo((props: MutedLabelProps) => {

    const {campaign, className='', ...rest} = props

    const { t } = useTranslation()
    const campaigns = useCampaigns()

    const resolved = typeof campaign === 'string' ? campaigns.safeLookup(campaign) : campaign

    if (!campaigns.isMuted(resolved))
        return <></>

    return <Label mode='tag' className={`campaign-muted ${className}`} icon={icns.muted} title={t("campaign.mute.muted")} {...rest} />


}, ($, $$) => $.campaign === $$.campaign && sameLabel($, $$))

type ArchiveLabelProps = SuspendedLabelProps

export const ArchiveLabel = React.memo((props: ArchiveLabelProps) => {

    const {campaign, className='', ...rest} = props

    const { t } = useTranslation()
    const campaigns = useCampaigns()

    const currentCampaign = typeof campaign === 'string' ? campaigns.safeLookup(campaign) : campaign

    const archived = campaigns.isArchived(currentCampaign)

    if (!archived)
        return <></>

    return <Label mode='tag' className={`campaign-archived ${className}`} icon={icns.archive} title={t("campaign.archive.archived")} {...rest} />


}, ($, $$) => $.campaign === $$.campaign && sameLabel($, $$))