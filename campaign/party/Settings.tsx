
import { Radio } from "antd"
import { Form } from "apprise-frontend/form/Form"
import { FormState, useFormState } from "apprise-frontend/form/hooks"
import { RadioBox } from "apprise-frontend/form/RadioBox"
import { specialise } from "apprise-frontend/iam/model"
import { SettingsForm } from "apprise-frontend/settings/SettingsForm"
import { useUsers } from "apprise-frontend/user/api"
import { Validation } from "apprise-frontend/utils/validation"
import * as React from "react"
import { useTranslation } from "react-i18next"
import { useCampaigns } from "../api"
import { useCurrentCampaign } from "../hooks"
import { approveCycleOptionDefaults, approveCycleOptions, CampaignSettings } from "../model"
import { usePartyInstances } from "./api"
import { PartyInstance } from "./model"


type LivePartySettingsProps = {

    party: PartyInstance
    onSave?: () => void
   
}

export const LivePartySettings = (props:LivePartySettingsProps) => {

    const {party,onSave=()=>{}} = props

    const campaign = useCurrentCampaign()
    const parties = usePartyInstances(campaign)

    const state = useFormState(party)

    const report = parties.validateInstance(state.edited) 

    return <SettingsForm state = {state} onSave={()=>parties.save(state.edited).then(onSave)} >
                <PartySettingsFields state={state} report={report} />
           </SettingsForm>


}


type PartySettingsProps = {

    state: FormState<PartyInstance>
    report: Partial<Record<keyof CampaignSettings,Validation>>


}

export const PartySettings = (props:PartySettingsProps) => {


    const {state,report} = props 
     

    return <Form state={state}>
                <PartySettingsFields state = {state} report={report} />
            </Form>

}

type PartySettingsFieldsProps = {
 
    state:FormState<PartyInstance>
    report: Partial<Record<keyof CampaignSettings,Validation>>
}

export const PartySettingsFields = (props:PartySettingsFieldsProps) => {

    const {t} = useTranslation()
    
    const {state,report} = props

    const {edited,change} = state

    const users = useUsers()
    const campaigns = useCampaigns()
    const campaign = useCurrentCampaign()
    const parties = usePartyInstances(campaign)

    const approvalOptionDefault = approveCycleOptionDefaults[parties.approveCycleDefault(edited) ?? 'none'] ?? `${t("common.labels.unknown")}??`

    const approvalOptions = Object.keys(approveCycleOptions).map(o=><Radio key={o} value={o}>{t(approveCycleOptions[o])}.</Radio>)
    
    approvalOptions.push(<Radio key={"defaults"} value={'default'}>{t( "campaign.fields.approval.options.default",{value:t(approvalOptionDefault)})}.</Radio>)

    const isAdmin = users.logged.hasNoTenant() && users.logged.can(specialise(campaigns.actions.manage,campaign.id))

    return <React.Fragment>

                 <RadioBox label={t("campaign.fields.approval.name")} validation={report.approveCycle}
                    value={ parties.approveCycle(edited,true) ?? 'default'  }
                    onChange={change((t, v) => parties.setApproveCycle(t, v==='default' ? undefined : v)) }>

                    {approvalOptions}

                </RadioBox>


                { isAdmin && 

                    <RadioBox label={t("campaign.fields.admin_can_edit.party.name")} validation={report.adminCanEdit}
                        value={ parties.canEdit(edited,true) ?? 'default'  }
                        onChange={change((t, v) => parties.setCanEdit(t, v==='default' ? undefined : v)) }>

                        <Radio value={true}>{t("campaign.fields.admin_can_edit.party.options.can")}.</Radio>
                        <Radio value={false}>{t("campaign.fields.admin_can_edit.party.options.cannot")}.</Radio>
                        <Radio value={'default'}>{t("campaign.fields.admin_can_edit.party.options.default",
                                        {value:t(parties.canEditDefault(edited)?
                                                "campaign.fields.admin_can_edit.party.options.can_short"
                                                : "campaign.fields.admin_can_edit.party.options.cannot_short" )})}.</Radio>
                    </RadioBox>
                }

                { isAdmin && 

                <RadioBox label={t("campaign.fields.admin_can_submit.party.name")} validation={report.adminCanSubmit}
                    value={ parties.canSubmit(edited,true) ?? 'default'  }
                    onChange={change((t, v) => parties.setCanSubmit(t, v==='default' ? undefined : v)) }>

                    <Radio value={true}>{t("campaign.fields.admin_can_submit.party.options.can")}.</Radio>
                    <Radio value={false}>{t("campaign.fields.admin_can_submit.party.options.cannot")}.</Radio>
                    <Radio value={'default'}>{t("campaign.fields.admin_can_submit.party.options.default",
                                    {value:t(parties.canSubmitDefault(edited)?
                                            "campaign.fields.admin_can_submit.party.options.can_short"
                                            : "campaign.fields.admin_can_submit.party.options.cannot_short" )})}.</Radio>
                </RadioBox>
                }


                <RadioBox label={t("campaign.fields.party_can_see_not_applicable.party.name")} validation={report.partyCanSeeNotApplicable}
                    value={ parties.canSeeNotApplicable(edited,true) ?? 'default'  }
                    onChange={change((t, v) => parties.setCanSeeNotApplicable(t, v==='default' ? undefined : v)) }>

                    <Radio value={true}>{t("campaign.fields.party_can_see_not_applicable.party.options.can")}.</Radio>
                    <Radio value={false}>{t("campaign.fields.party_can_see_not_applicable.party.options.cannot")}.</Radio>
                    <Radio value={'default'}>{t("campaign.fields.party_can_see_not_applicable.party.options.default",
                                    {value:t(parties.canSeeNotApplicableDefault(edited)?
                                            "campaign.fields.party_can_see_not_applicable.party.options.can_short"
                                            : "campaign.fields.party_can_see_not_applicable.party.options.cannot_short" )})}.</Radio>
                </RadioBox>

        </React.Fragment>

}