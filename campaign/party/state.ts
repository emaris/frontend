import { intlapi } from "apprise-frontend/intl/api";
import { defaultLanguage } from "apprise-frontend/intl/model";
import { l } from "apprise-frontend/model/multilang";
import { systemapi } from "apprise-frontend/system/api";
import { tenantapi } from "apprise-frontend/tenant/api";
import { tenantPlural, tenantSingular, tenantType } from "apprise-frontend/tenant/constants";
import { userapi } from "apprise-frontend/user/api";
import { utilapi } from "apprise-frontend/utils/api";
import { indexMap, through } from "apprise-frontend/utils/common";
import { notify, showAndThrow } from "apprise-frontend/utils/feedback";
import { paramsInQuery, updateQuery } from "apprise-frontend/utils/routes";
import { partyinstcallapi } from "emaris-frontend/campaign/party/calls";
import { PartyInstance, PartyInstanceDto } from "emaris-frontend/campaign/party/model";
import { EmarisState } from "emaris-frontend/state/model";
import { campaignapi } from "../api";
import { Campaign, CampaignInstance, InstanceRef } from "../model";
import { CampaignNext } from '../state';
import { profileapi } from '../submission/profile';
import { partyinstmodelapi } from "./model";


export type PartyInstanceState = {

    parties: {
        all: PartyInstance[]
        map: Record<string, PartyInstance>
    }

}

export const initialPartyInstances = (): PartyInstanceState => ({

    parties: {
        all: undefined!,
        map: undefined!
    }
})


const type = "partyinstance"


export const partyinststateapi = (s: EmarisState) => (c: Campaign) => {


    const system = () => systemapi(s)

    const t = intlapi(s).getT()

    const [singular, plural] = [t(tenantSingular).toLowerCase(), t(tenantPlural).toLowerCase()]


    const campaigns = () => campaignapi(s)
    const call = () => partyinstcallapi(s)(c)
    const model = () => partyinstmodelapi(s)(c)
    const tenants = () => tenantapi(s)

    const profile = () => profileapi(s)(c)

    const self = {

        stringify: (i: PartyInstance) => `${l(s)(tenants().safeLookup(i.source)?.name)}`

        ,

        livePush: (instances: PartyInstance[], type: 'change' | 'remove') => {

            const map = s.campaigns.instances[c.id]?.parties?.map ?? {}
            let current = s.campaigns.instances[c.id]?.parties?.all ?? []

            instances.forEach(instance => {

                switch (type) {

                    case "change":
                        current = map[instance.id] ? current.map(i => i.id === instance.id ? instance : i) : [...current, instance]
                        break;

                    case "remove": current = current.filter(i => i.id !== instance.id); break

                }

            })

            self.setAll(current)
        }

        ,

        setAll: (instances: PartyInstance[], props?: { noOverwrite: boolean }) => s.changeWith(s => {

            const campaign = s.campaigns.instances[c.id] ?? {}
            const parties = campaign.parties ?? initialPartyInstances()

            if (parties.all && props?.noOverwrite)
                return

            s.campaigns.instances[c.id] = { ...campaign, parties: { ...parties, all: instances, map: indexMap(instances).by(i => i.id) } }

        })

        ,

        addAll: (instances: PartyInstance[]) =>

            system().toggleBusy(`${type}.addall`, t("common.feedback.save_changes"))

                .then(_ => call().addAll(instances))
                .then((added: PartyInstanceDto[]) => [...self.all(), ...added])
                .then(self.setAll)
                .then(() => notify(t('common.feedback.saved')))

                .catch(e => showAndThrow(e, t("common.calls.add_many_error", { plural })))
                .finally(() => system().toggleBusy(`${type}.addall`))

        ,

        cloneAll: (instances: PartyInstance[], context: CampaignNext) => call().addAll(instances.map(i => model().clone(i, context))).then(self.setAll)

        ,

        areReady: () => !!self.all()

        ,

        fetchAll: (forceRefresh = false) =>

            self.areReady() && !forceRefresh ? Promise.resolve(self.all())

                :

                system().toggleBusy(`${type}.fetchAll.${c.id}`, t("common.feedback.load", { plural }))

                    .then(_ => console.log(`fetching parties for ${c.name[defaultLanguage]}...`))
                    .then(_ => Promise.all([call().fetchAll(), tenantapi(s).fetchAll()]))  // load template dependency in parallel
                    .then(([instances]) => instances)    // continues with instances only
                    .then(through($ => self.setAll($, { noOverwrite: !forceRefresh })))


                    .catch(e => showAndThrow(e, t("common.calls.fetch_all_error", { plural })))
                    .finally(() => system().toggleBusy(`${type}.fetchAll.${c.id}`))

        ,

        fetchAllQuietly: (forceRefresh = false) =>

            self.areReady() && !forceRefresh ? Promise.resolve(self.all())

                :

                system().toggleBusy(`${type}.fetchAll.${c.id}`, t("common.feedback.load", { plural }))

                    .then(_ => console.log(`fetching parties for ${c.name[defaultLanguage]}...`))
                    .then(_ => Promise.all([call().fetchAll(), tenantapi(s).fetchAll()]))  // load template dependency in parallel
                    .then(([instances]) => instances)    // continues with instances only
                    .then(through(self.setAll))


                    .catch(e => showAndThrow(e, t("common.calls.fetch_all_error", { plural })))
                    .finally(() => system().toggleBusy(`${type}.fetchAll.${c.id}`))

        ,

        lookup: (id: string | undefined) => id ? s.campaigns.instances[c.id]?.parties?.map[id] : undefined

        ,

        safeLookup: (id: string | undefined) => self.lookup(id) ?? model().noInstance()

        ,

        lookupBySource: (source: string | undefined) => self.all().find(p => p.source === source)

        ,


        safeLookupBySource: (id: string | undefined) => self.lookupBySource(id) ?? model().noInstance()

        ,


        detailParam: () => 'ti-drawer'

        ,

        // eslint-disable-next-line
        detailInRoute: () => paramsInQuery(location.search)[self.detailParam()] as string

        ,


        route: (p?: PartyInstance) =>

            // eslint-disable-next-line
            `${campaigns().routeTo(c)}/${tenantType}?${updateQuery(location.search).with(params => {
                
                params[self.detailParam()] = p ? p.id : null
                params['source'] = p?.source ?? null
            })}`

        ,

        lookupLineage: (ref: InstanceRef | undefined) => ref ? s.campaigns.instances[ref.campaign]?.parties.all.find(p => p.source === ref.source) : undefined



        ,

        all: () => s.campaigns.instances[c.id]?.parties?.all

        ,

        allSorted: () => {

            return [...self.all() ?? []].sort(model().comparator)
        }

        ,

        allForStats: () => {
            const { logged } = userapi(s)
            return logged.hasNoTenant() ? self.all().filter(p => !(c.properties.statExcludeList ?? []).includes(p.source)) : self.all()
        }

        ,


        allForAsset: (instance: CampaignInstance) => self.allSorted().filter(party => profile().profileOf(instance.instanceType).isForParty(party, instance))


        ,

        save: (instance: PartyInstance, replacementOf: PartyInstance = instance) =>

            system().toggleBusy(`${type}.updateOne`, t("common.feedback.save_changes"))

                .then(_ => call().updateOne(instance))
                .then(() => self.all().map(p => p.id === replacementOf.id ? instance : p))
                .then(self.setAll)
                .then(() => notify(t('common.feedback.saved')))

                .catch(e => showAndThrow(e, t("common.calls.update_one_error", { singular })))
                .finally(() => system().toggleBusy(`${type}.updateOne`))

        ,

        remove: (instance: PartyInstance, onConfirm?: (...args) => void) =>

            utilapi(s).askConsent({

                title: t('common.consent.remove_one_title', { singular }),
                content: t("common.consent.remove_one_msg", { singular }),
                okText: t("common.consent.remove_one_confirm", { singular }),

                onOk: () => {

                    system().toggleBusy(`${type}.removeOne`, t("common.feedback.save_changes"))

                        .then(_ => call().removeOne(instance))
                        .then(() => self.all().filter(p => instance.id !== p.id))
                        .then(self.setAll)
                        .then(() => notify(t('common.feedback.saved')))
                        .then(() => onConfirm && onConfirm(instance))

                        .catch(e => showAndThrow(e, t("common.calls.remove_one_error", { singular })))
                        .finally(() => system().toggleBusy(`${type}.removeOne`))


                }
            })

        ,

        removeMany: (instances: PartyInstance[], onConfirm?: () => void) =>


            utilapi(s).askConsent({

                title: t('common.consent.remove_many_title', { count: instances.length, plural }),
                content: t("common.consent.remove_many_msg", { count: instances.length, plural }),
                okText: t("common.consent.remove_many_confirm", { count: instances.length, plural }),

                onOk: () => {

                    const ids = instances.map(i => i.id)

                    system().toggleBusy(`${type}.removeMany`, t("common.feedback.save_changes"))

                        .then(_ => call().removeMany(instances))
                        .then(() => self.all().filter(p => !ids.includes(p.id)))
                        .then(through(self.setAll))
                        .then(() => notify(t('common.feedback.saved')))
                        .then(through(() => onConfirm && onConfirm()))

                        .catch(e => showAndThrow(e, t("common.calls.remove_many_error", { plural })))
                        .finally(() => system().toggleBusy(`${type}.removeMany`))

                }
            })
    }

    return self
}







