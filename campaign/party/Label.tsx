import { Tooltip } from "antd"
import { LabelProps, sameLabel, UnknownLabel } from "apprise-frontend/components/Label"
import { useConfig } from "apprise-frontend/config/state"
import { icns } from "apprise-frontend/icons"
import { TenantLabel } from "apprise-frontend/tenant/Label"
import { useDashboards } from "emaris-frontend/dashboard/api"
import { useCampaignMode } from "emaris-frontend/dashboard/hooks"
import React from "react"
import { useTranslation } from "react-i18next"
import { Link } from "react-router-dom"
import { useCampaigns } from "../api"
import { campaignSingular, campaignType, runningIcon } from "../constants"
import { InstanceLabel, InstanceLabelProps } from "../InstanceLabel"
import { CampaignLabel } from "../Label"
import { InstanceRef, isInstanceRef } from "../model"
import { submissionType } from "../submission/constants"
import { useParties } from "./api"
import { partyRoute } from "./constants"
import { PartyInstance } from "./model"

type Props = LabelProps & InstanceLabelProps & {

    instance: PartyInstance | InstanceRef | undefined
}


export const PartyInstanceLabel = React.memo((props: Props) => {


    const {t} = useTranslation()

    const { instance, linkTo, linkTarget, options = [], ...rest } = props

    const config = useConfig()
    const campaigns = useCampaigns()
    const dashboard = useDashboards()
    const campaignMode = useCampaignMode()
    const allParties = useParties()

    const campaign = campaigns.lookup(instance?.campaign)

    if (!campaign)
        return <UnknownLabel title={t("common.labels.unknown_one", { singular: t(campaignSingular).toLowerCase() })} />


    const parties = allParties(campaign)

    var partyinstance = instance as PartyInstance;

    if (isInstanceRef(instance))

        if (campaigns.isLoaded(instance.campaign))
            partyinstance = parties.lookupLineage(instance)!
        else
            return <CampaignLabel loadTrigger campaign={instance.campaign} />

    if (!partyinstance)
        return <UnknownLabel {...props} />

    if (campaignMode === 'dashboard' && config.get().routedTypes?.includes(campaignType))
        options.push(<Tooltip title={t("campaign.labels.campaign_view.design")}><Link to={() => parties.route(partyinstance)}>{icns.edit}</Link></Tooltip>)

    if (campaignMode === 'design' && config.get().routedTypes?.includes(submissionType))
        options.push(<Tooltip title={t("campaign.labels.campaign_view.live")}><Link to={() => dashboard.given(campaign).routeToParty(partyinstance)}>{runningIcon}</Link></Tooltip>)


    return <InstanceLabel instance={partyinstance}
        noMemo={props.noMemo}
        tooltipForLineage={ref => <PartyInstanceLabel showCampaign instance={ref} />}
        baseRoute={partyRoute}
        {...rest}
        sourceLabel={props => <TenantLabel {...rest} {...props} noMemo={props.noMemo} linkTarget={linkTarget ?? campaignType} options={options} linkTo={linkTo || (() => parties.route(partyinstance))} tenant={instance?.source} />}
    />

}, ($, $$) => $.instance === $$.instance && sameLabel($, $$))