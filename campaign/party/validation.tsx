
import { Paragraph } from "apprise-frontend/components/Typography"
import { intlapi } from "apprise-frontend/intl/api"
import { tagapi } from "apprise-frontend/tag/api"
import { tenantSingular, tenantType } from "apprise-frontend/tenant/constants"
import { checkIt, withReport } from "apprise-frontend/utils/validation"
import * as React from "react"
import { EmarisState } from "../../state/model"
import { Campaign } from "../model"
import { PartyInstance } from "./model"


export const partyinstvalidationapi = (s:EmarisState) => (_:Campaign) => ({

    validateInstance: (edited:PartyInstance) => {

        const t = intlapi(s).getT()
        const {validateCategories} = tagapi(s)

        const singular = t(tenantSingular).toLowerCase()

        return withReport({

            note: checkIt().nowOr(t("campaign.fields.note.msg"))
            
            ,

            lineage: checkIt().nowOr(t("common.fields.lineage.msg_nochoice",{singular}))

            ,

            source: checkIt().nowOr(t("campaign.fields.source.msg",{singular}))

            ,
    
            ...validateCategories(edited.tags).for(tenantType) 

            ,

            approveCycle: {
                            msg:t("campaign.fields.approval.msg"),
                            help:<>
                                    <Paragraph>{t("campaign.fields.approval.help1")}</Paragraph>
                                    <Paragraph spaced >{t("campaign.fields.approval.help2_campaigns")}</Paragraph>
                                </>}

            ,


            adminCanEdit : {

                msg:t("campaign.fields.admin_can_edit.msg"),
                help:<React.Fragment>
                        <Paragraph>{t("campaign.fields.admin_can_edit.help1")}</Paragraph>
                        <Paragraph spaced >{t("campaign.fields.admin_can_edit.help2")}</Paragraph>
                    </React.Fragment>
            }

            ,

            adminCanSubmit : {

                msg:t("campaign.fields.admin_can_submit.msg"),
                help:<React.Fragment>
                        <Paragraph>{t("campaign.fields.admin_can_submit.help1")}</Paragraph>
                        <Paragraph spaced >{t("campaign.fields.admin_can_submit.help2")}</Paragraph>
                    </React.Fragment>
            }

    
            ,
    
            partyCanSeeNotApplicable : {
                msg:t("campaign.fields.party_can_see_not_applicable.msg"),
                help:<React.Fragment>
                        <Paragraph>{t("campaign.fields.party_can_see_not_applicable.help1")}</Paragraph>
                        <Paragraph spaced>{t("campaign.fields.party_can_see_not_applicable.help2")}</Paragraph>
                    </React.Fragment>
            }
        


        })

    }
})