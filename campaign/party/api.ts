
import { EmarisState } from "../../state/model";
import { Campaign } from "../model";
import { partyinstmodelapi } from "./model";
import { partyinststateapi } from "./state";
import { partyinstvalidationapi } from "./validation";
import { useEmarisState } from "emaris-frontend/state/hooks";


export const useParties = () => partyinstapi(useEmarisState())
export const usePartyInstances = (c:Campaign) => partyinstapi(useEmarisState())(c)


export const partyinstapi = (s:EmarisState) => (c:Campaign) => ({


    ...partyinststateapi(s)(c),
    ...partyinstmodelapi(s)(c),
    ...partyinstvalidationapi(s)(c)

})

