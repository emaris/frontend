import { callapi } from "apprise-frontend/call/call";
import { domainService } from "../../constants";
import { EmarisState } from "../../state/model";
import { Campaign } from "../model";
import { PartyInstance } from "./model";




export const partyinstcallapi = (s:EmarisState) => (c: Campaign) => {

    const parties = `/partyinstance`
    const {at} = callapi(s)


   return {

        fetchAll : () : Promise<PartyInstance[]>  =>  at(`${parties}/search`,domainService).post({campaign:c.id})

        ,

        fetchOne : (source:string) : Promise<PartyInstance> => at(`${parties}/${source}?byref=true`,domainService).get()

        ,
        
        addAll : (instances:PartyInstance[]) : Promise<PartyInstance[]>  =>  at(parties,domainService).post(instances)

        ,

        updateOne: (instance:PartyInstance) : Promise<void>  => at(`${parties}/${instance.id}`,domainService).put(instance)

        ,
                    
        removeOne: (instance:PartyInstance) : Promise<void>  =>  at(`${parties}/${instance.id}`,domainService).delete()
        
        ,
        
        removeMany: (instances:PartyInstance[]) : Promise<void>  => at(`${parties}/remove`,domainService).post(instances.map(i=>i.id))
 
    }
}
