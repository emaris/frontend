import { layoutRegistry } from "apprise-frontend/layout/registry";
import { tenantapi } from "apprise-frontend/tenant/api";
import { tenantType } from "apprise-frontend/tenant/constants";
import { deepclone } from "apprise-frontend/utils/common";
import { withReport } from "apprise-frontend/utils/validation";
import { Topic } from "apprise-messages/model";
import { productType } from "emaris-frontend/product/constants";
import { requirementType } from "emaris-frontend/requirement/constants";
import { Tenant } from "../../../apprise-frontend/tenant/model";
import { EmarisState } from "../../state/model";
import { ApproveCycle, Campaign, CampaignInstance, campaignmodelapi, CampaignSettings, InstanceProperties, Overlay } from "../model";
import { tenantpreferencesapi } from "../preferences/api";
import { CampaignNext } from '../state';
import { partyinststateapi } from "./state";



export type PartyInstanceDto = CampaignInstance & {

    properties: PartyProperties

}        
export type PartyInstance =  PartyInstanceDto       // future-proofs divergence from exchange model, starts, aligned.

type PartyOverlays = { [instance in 'requirements' | 'products'  ] : Record<string, Overlay> }


export type PartyProperties = InstanceProperties & {

    settings: Partial<CampaignSettings>
    parameterOverlays: PartyOverlays
}

export const newPartyProperties = () =>({ 

    settings : {},

    parameterOverlays: { 

        requirements:{} as Record<string, Overlay>, 
        products:{} as Record<string, Overlay> 
    } as PartyOverlays
        
})



export const partyinstmodelapi = (s:EmarisState) => (c:Campaign) => {

    const tenants = tenantapi(s)

    const self = {

        stringify: (i:PartyInstance) => {

            const tenants = tenantapi(s)

            return `${tenants.stringify(tenants.lookup(i.source))}`
        }

        ,

        generate: (tenant:Tenant) : PartyInstance => ( {
            
            id: undefined!,
            instanceType:tenantType,
            source:tenant.id,
            campaign:c.id,
            tags:deepclone(tenant.tags), 
            properties: {

                ...newPartyProperties(),
                note: tenant.preferences.note
            }
        })

        ,

        noInstance: () => ({

            id: undefined!,
            instanceType:tenantType,
            source:"unknown",
            campaign:c.id,
            tags:[], 
            properties: newPartyProperties() 

        }) as PartyInstance

        
        ,
        
        
        clone: (instance:PartyInstance, campaign:CampaignNext) : PartyInstance => {

            const {type} = campaign

            const clone = deepclone(instance)

            const overlays = instance.properties.parameterOverlays ?? {}
            const productsOverlay = overlays.products ?? {}
            const requirementsOverlay = overlays.requirements ?? {}

            //PartyOverlays are a map of requirements/products.
            // Inside of each we have the a map of instanceid: Overlay {paramid: Param}
            // for this we need a double reduce.
            const clonedOverlays = {
                products: {...Object.keys(productsOverlay).reduce(
                                (acc1, cur1) => 
                                    (
                                        {...acc1, 
                                            [cur1]: Object.keys(productsOverlay[cur1]).reduce((acc2, cur2) => 
                                                    {
                                                        const param = productsOverlay[cur1][cur2]
                                                        const clonedParam = type === 'branch' ?
                                                                    layoutRegistry(productType).lookupParameter(param.original.spec).clone({...param.original, value: param.value}, campaign)
                                                                : param
                                                        return clonedParam ? {...acc2, [cur2]: clonedParam} : acc2
                                                    }, {})
                                        })
                                , {})},
                requirements: {...Object.keys(requirementsOverlay).reduce(
                                (acc1, cur1) => 
                                    (
                                        {...acc1, 
                                            [cur1]: Object.keys(requirementsOverlay[cur1]).reduce((acc2, cur2) => 
                                                    {
                                                        const param = requirementsOverlay[cur1][cur2]
                                                        const clonedParam = type === 'branch' ?
                                                                    layoutRegistry(requirementType).lookupParameter(param.original.spec).clone({...param.original, value: param.value}, campaign)
                                                                : param
                                                        return clonedParam ? {...acc2, [cur2]: clonedParam} : acc2
                                                    }, {})
                                        })
                                , {})}
            } as PartyOverlays


            
            return {
                    ...clone, 
                    id:undefined!, 
                    campaign:c.id, 
                    tags: type === 'branch' ? tenants.lookup(instance.source)?.tags ?? clone.tags  : clone.tags,
                    lineage: type === 'branch' ? {source:instance.source,campaign:instance.campaign} : undefined,
                    properties : {...clone.properties, parameterOverlays: clonedOverlays}
                }
        
        }
        
        ,

        comparator: (o1:PartyInstance,o2:PartyInstance) => {

           return tenants.comparator(tenants.safeLookup(o1.source)!,tenants.safeLookup(o2.source)!)
        }


        ,

        validateInstance: (p:PartyInstance)  => withReport({}) // nothing to validate for now

        ,



        approveCycle : (p:PartyInstance, noDelegation?:boolean) =>  p.properties.settings.approveCycle ?? (noDelegation ? undefined : self.approveCycleDefault(p))
 
        ,

        approveCycleDefault : (p:PartyInstance) => tenantpreferencesapi(s).approveCycleFor(p.source) ?? campaignmodelapi(s).approveCycleFor(p.campaign)
 
        ,

        setApproveCycle : (p:PartyInstance,value: ApproveCycle|undefined ) => {
            
            if (value)
                 p.properties.settings.approveCycle = value
            else 
                delete p.properties.settings.approveCycle
        }

        ,

        canEdit: (p:PartyInstance,noDelegation?:boolean) => p.properties.settings.adminCanEdit ??  (noDelegation ? undefined : self.canEditDefault(p)  )

        ,

        canEditFor: (source:string) => self.canEdit(partyinststateapi(s)(c).safeLookupBySource(source))

        ,

        canEditDefault: (p:PartyInstance) => campaignmodelapi(s).canEditFor(p.campaign)

        ,

        setCanEdit: (p:PartyInstance, value:boolean| undefined) => {
            
            if (value===undefined)
                delete p.properties.settings.adminCanEdit
            else  
                p.properties.settings.adminCanEdit = value
        
        }

        ,

        canSubmit: (p:PartyInstance, noDelegation?:boolean) => p.properties.settings.adminCanSubmit ??  (noDelegation ? undefined : self.canSubmitDefault(p)  )

        ,

        canSubmitFor: (source:string) => self.canSubmit(partyinststateapi(s)(c).safeLookupBySource(source))

        ,


        canSubmitDefault: (p:PartyInstance) => campaignmodelapi(s).canSubmitFor(p.campaign)

        ,


        setCanSubmit: (p:PartyInstance, value:boolean|undefined) => {
            
            if (value===undefined)
                delete p.properties.settings.adminCanSubmit
            
            else  
                p.properties.settings.adminCanSubmit = value
        
        }

        ,

        setCanSeeNotApplicable: (p:PartyInstance, value:boolean|undefined) => {
            
            if (value===undefined)
                delete p.properties.settings.partyCanSeeNotApplicable
            
            else  
                p.properties.settings.partyCanSeeNotApplicable = value
        
        }

        ,

        canSeeNotApplicable: (p:PartyInstance, noDelegation?:boolean) => p.properties.settings.partyCanSeeNotApplicable ??  (noDelegation ? undefined : self.canSeeNotApplicableDefault(p))

        ,

        canSeeNotApplicableDefault: (p:PartyInstance) => campaignmodelapi(s).canPartySeeNotApplicable(p.campaign)

        ,

        topics: (p: PartyInstance): Topic[] => [{ type: tenantType, name:`${c.id}:${p.source}`  }]
        
    }

    return self

}