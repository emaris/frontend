
import { Icon, Radio } from "antd"
import { Label } from "apprise-frontend/components/Label"
import { Form } from "apprise-frontend/form/Form"
import { FormState } from "apprise-frontend/form/hooks"
import { RadioBox } from "apprise-frontend/form/RadioBox"
import { SliderBox } from "apprise-frontend/form/SliderBox"
import { Switch } from "apprise-frontend/form/Switch"
import { BaseSettings, SettingsProps } from "apprise-frontend/settings/model"
import { CategoryBox } from "apprise-frontend/tag/CategoryBox"
import { categoryLabelIcon } from "apprise-frontend/tag/constants"
import { ZonePickerBox } from "apprise-frontend/time/TimeZonePicker"
import { Validation } from "apprise-frontend/utils/validation"
import moment from "moment-timezone"
import * as React from "react"
import { useTranslation } from "react-i18next"
import { useCampaigns } from "./api"
import { campaignType, complianceType, defaultRelativeDate, defaultStatisticalHorizon, defaultTimelinessScale, timelinessType } from "./constants"
import { PureRelativeDateBox } from "./event/DateBox"
import { approveCycleOptions, Campaign, CampaignAppSettings, CampaignSettings } from "./model"
import { useSubmissions } from "./submission/api"


type CampaignSettingsFieldsProps = {

    state: FormState<Campaign>
    report: Partial<Record<keyof CampaignSettings, Validation>>

}

const horizonSteps = 5

// setting fields to mount inside live drawer or design form.
export const CampaignSettingsFields = (props: CampaignSettingsFieldsProps) => {

    const { t } = useTranslation()

    const { state, report } = props

    const { edited, change } = state

    const campaigns = useCampaigns()

    const submissions = useSubmissions(edited)

    // may not have been loaded yet
    const assessmentsExist = submissions.allTrails()?.flatMap(t => t.submissions).some(s => s.lifecycle.compliance?.state)
    const timelinessAssessmentExist = submissions.allTrails()?.flatMap(t => t.submissions).some(s => s.lifecycle.compliance?.timeliness)

    const now = new Date()

    const maxHorizonDays = moment(now).add(1.5, 'year').diff(moment(now), 'days')

    const computedMaxHorizonDays = maxHorizonDays - (maxHorizonDays % horizonSteps)

    const campaignEnd = campaigns.endDate(campaigns.safeLookup(edited.id))

    return <React.Fragment>

        <CategoryBox disabled={assessmentsExist} label={t("campaign.fields.compliance_scale.name")} validation={report.complianceScale}
            type={complianceType}
            allowClear
            undefinedOption={<Label icon={<Icon style={{ color: 'lightgray' }} component={categoryLabelIcon} />} title={t("campaign.fields.compliance_scale.no_scale")} />}
            onChange={change(campaigns.setComplianceScale)}>

            {campaigns.complianceScale(edited)}

        </CategoryBox>

        <CategoryBox disabled={timelinessAssessmentExist} label={t("campaign.fields.timeliness_scale.name")} validation={report.timelinessScale}
            type={timelinessType}
            allowClear
            onChange={change(campaigns.setTimelinessScale)}>

            {campaigns.timelinessScale(edited)}

        </CategoryBox>

        <RadioBox label={t("campaign.fields.approval.name")} validation={report.approveCycle}
            value={campaigns.approveCycle(edited) ?? 'none'}
            onChange={change((t, v) => campaigns.setApprovalCycle(t, v === 'none' ? undefined : v))}>
            {
                Object.keys(approveCycleOptions).map(o =>

                    <Radio key={o} value={o}>{t(approveCycleOptions[o])}.</Radio>)

            }
        </RadioBox>

        <Switch label={t("campaign.fields.admin_can_edit.name")} validation={report.adminCanEdit} onChange={change(campaigns.setCanEdit)}>{campaigns.canEdit(edited)}</Switch>
        <Switch label={t("campaign.fields.admin_can_submit.name")} validation={report.adminCanSubmit} onChange={change(campaigns.setCanSubmit)}>{campaigns.canSubmit(edited)}</Switch>
        <Switch label={t("campaign.fields.party_can_see_not_applicable.name")} validation={report.partyCanSeeNotApplicable} onChange={change(campaigns.setPartyCanSeeNotApplicable)}>{campaigns.canPartySeeNotApplicable(edited)}</Switch>

        <SliderBox width={300}
            label={t("campaign.fields.statistical_horizon.name")}
            validation={report.statisticalHorizon}
            tipFormatter={days => (campaignEnd && days === computedMaxHorizonDays) ? t("campaign.labels.end_of_campaign") : t("campaign.fields.statistical_horizon.slider_tip", { days })}
            step={horizonSteps}
            max={computedMaxHorizonDays}
            value={campaigns.statisticalHorizon(edited)}
            onChange={change(campaigns.setStatisticalHorizon)}
            showValuesFormatter={(days: number) => t("dashboard.summary.performance.horizon.reminder", { horizon: days })}
        />

        <Switch label={t("campaign.fields.suspend_on_end.name")} validation={report.suspendOnEnd} onChange={change(campaigns.setSuspendOnEnd)}>{campaigns.isSuspendOnEnd(edited)}</Switch>

        <ZonePickerBox
            value={edited.properties.timeZone}
            onChange={change((t, v) => campaigns.setTimeZone(t, v))}
        />

        <PureRelativeDateBox
            asFormField
            mode='relative'
            label={t("campaign.fields.default_relative_date.name")}
            validation={report.defaultRelativeDate}
            relativeDate={campaigns.defaultRelativeDate(edited) || defaultRelativeDate}
            onChange={change(campaigns.setDefaultRelativeDate)} />


    </React.Fragment>

}

export type ApplicationSettings = BaseSettings & CampaignAppSettings

export const ApplicationSettingsFields = (props: SettingsProps<ApplicationSettings>) => {

    const { t } = useTranslation()

    const { state, report } = props

    const { edited, change } = state

    const now = new Date()

    const maxHorizonDays = moment(now).add(1.5, 'year').diff(moment(now), 'days')

    const computedMaxHorizonDays = maxHorizonDays - (maxHorizonDays % horizonSteps)

    return <Form state={state} sidebar>

        <CategoryBox label={t("campaign.fields.compliance_scale.name")} validation={report.complianceScale}
            type={complianceType}
            allowClear
            undefinedOption={<Label icon={<Icon style={{ color: 'lightgray' }} component={categoryLabelIcon} />} title={t("campaign.fields.compliance_scale.no_scale")} />}
            onChange={change((t, v) => { v ? t[campaignType].complianceScale = v : delete t[campaignType].complianceScale })}>

            {edited.campaign.complianceScale}

        </CategoryBox>

        <CategoryBox label={t("campaign.fields.timeliness_scale.name")} validation={report.timelinessScale}
            type={timelinessType}
            allowClear
            onChange={change((t, v) => { v ? t[campaignType].timelinessScale = v : delete t[campaignType].timelinessScale })}>

            {edited.campaign.timelinessScale ?? defaultTimelinessScale}

        </CategoryBox>

        <Switch label={t("campaign.fields.liveguard.name")} validation={report.liveguard} onChange={change((t, v) => t[campaignType].liveGuard = v)}>{edited[campaignType].liveGuard}</Switch>

        <RadioBox label={t("campaign.fields.approval.name")} validation={report.approveCycle}
            value={edited[campaignType].approveCycle ?? 'none'}
            onChange={change((t, v) => t[campaignType].approveCycle = v === 'none' ? undefined : v)}>
            {
                Object.keys(approveCycleOptions).map(o =>

                    <Radio key={o} value={o}>{t(approveCycleOptions[o])}.</Radio>)

            }
        </RadioBox>

        <Switch label={t("campaign.fields.admin_can_edit.name")} validation={report.adminCanEdit} onChange={change((t, v) => t[campaignType].adminCanEdit = v)}>{edited[campaignType].adminCanEdit}</Switch>
        <Switch label={t("campaign.fields.admin_can_submit.name")} validation={report.adminCanSubmit} onChange={change((t, v) => t[campaignType].adminCanSubmit = v)}>{edited[campaignType].adminCanSubmit}</Switch>


        <SliderBox width={300} label={t("campaign.fields.statistical_horizon.name")}
            validation={report.statisticalHorizon}
            tipFormatter={days => days === computedMaxHorizonDays ? t("campaign.labels.end_of_campaign") : t("campaign.fields.statistical_horizon.slider_tip", { days })}
            step={horizonSteps}
            max={computedMaxHorizonDays}
            value={edited[campaignType].statisticalHorizon ?? defaultStatisticalHorizon}
            onChange={change((t, v) => t[campaignType].statisticalHorizon = v)}
            showValuesFormatter={(days: number) => t("dashboard.summary.performance.horizon.reminder", { horizon: days })}
        />

        <Switch label={t("campaign.fields.suspend_on_end.name")} validation={report.suspendOnEnd} onChange={change((t, v) => t[campaignType].suspendOnEnd = v)}>{edited[campaignType].suspendOnEnd ?? true}</Switch>

        <ZonePickerBox
            value={edited[campaignType].timeZone}
            onChange={change((t, v) => t[campaignType].timeZone = v)}
            validation={report.defaultTimeZone}
        />

        <PureRelativeDateBox
            asFormField
            mode='relative'
            label={t("campaign.fields.default_relative_date.name")}
            validation={report.defaultRelativeDate}
            relativeDate={edited[campaignType].defaultRelativeDate}
            onChange={change((t, v) => t[campaignType].defaultRelativeDate = v)} />


    </Form>
}