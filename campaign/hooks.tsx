

import { ConfigContext, useCurrentConfig } from 'apprise-frontend/config/state'
import { StateContext, useAppState } from 'apprise-frontend/state/model'
import { EmarisState } from 'emaris-frontend/state/model'
import * as React from "react"
import { campaignType } from './constants'
import { Campaign } from "./model"
import { submissionType } from './submission/constants'


const CampaignContext = React.createContext<Campaign>(undefined!)

export const ModifiableCampaignContext = React.createContext<{ campaign: Campaign, setCampaign: (_: Campaign) => void }>(undefined!)


export const useCurrentCampaign = () => React.useContext(CampaignContext)

export const useModifiableCampaign = () => React.useContext(ModifiableCampaignContext)


// mounts the current campaign in context.
// if the campaign is archived, also overlayes the archived versions of its dependencies over application state. 
export const CurrentCampaignContext = (props: React.PropsWithChildren<{ campaign: Campaign }>) => {

    const { campaign } = props

    // first, overlay state if required.

    const base = useAppState() as EmarisState

    const archived = campaign.lifecycle.state === 'archived'

    const overlay = () => archived ?

        {
            ...base, ...{

                requirements: base.campaigns.instances[campaign.id].archive?.requirements ?? { all: [], map: {}},
                products: base.campaigns.instances[campaign.id].archive?.products ?? { all: [], map: {}},
                tenants: base.campaigns.instances[campaign.id].archive?.tenants ?? { all: [], map: {}},
                events: base.campaigns.instances[campaign.id].archive?.events ?? { all: [], map: {}},

            }
        }

        : base


    const baseConfig = useCurrentConfig()

    const configOverlay = () => archived ? { ...baseConfig, routedTypes: baseConfig.routedTypes?.includes(campaignType) ? [submissionType,campaignType] : [submissionType] } : baseConfig



    // eslint-disable-next-line
    const state = React.useMemo(overlay, [archived,base, campaign])

    // eslint-disable-next-line
    const config = React.useMemo(configOverlay, [archived,baseConfig, campaign])

    // second, mount the overlayed app state so that useAppState() or useEmarisState() get it in lieu of the original one.
    // all hooks and APIs are based on those base hooks.

    return <CampaignContext.Provider value={campaign}>
        <ConfigContext.Provider value={config}>
            <StateContext.Provider value={state}>
                {props.children}
            </StateContext.Provider>
        </ConfigContext.Provider>
    </CampaignContext.Provider>
}

