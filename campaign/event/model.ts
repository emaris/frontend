
import { intlapi } from 'apprise-frontend/intl/api';
import { compareDates, deepclone } from "apprise-frontend/utils/common";
import { eventapi } from "emaris-frontend/event/api";
import { eventType } from "emaris-frontend/event/constants";
import { Event } from "emaris-frontend/event/model";
import moment from "moment-timezone";
import shortid from "shortid";
import { EmarisState } from "../../state/model";
import { campaignType } from "../constants";
import { Campaign, CampaignInstance, InstanceProperties } from "../model";
import { CampaignNext } from '../state';
import { eventinstapi } from "./api";
import { eventinstprofileapi } from "./profile";

export type EventInstanceDto = CampaignInstance & {

    type: string
    target: string | undefined
    date: EventDate
    notifiedOn?: string
    properties: InstanceProperties & {relatable? : boolean}
    

}

export type EventDate = undefined | AbsoluteDate | RelativeDate

export type AbsoluteDate = {

    kind: 'absolute'        // backend model with type info to drive deserialsiation
    value: string
    branchType?: 'pinned' | 'recurring' | 'single'

}

export type RelativeDate = {

    kind: 'relative'        // backend model with type info to drive deserialsiation
    period: Period
    target: string

}

export type Period = {

    number: number,
    unit: 'months' | 'days' | 'weeks',
    direction: 'before' | 'after'
}

// event instances have client-side identifiers, so that the UI can create web of relatonships ahead of storage.
export const newId = () => `EVI-${shortid()}`

export const isRelative = (date: EventDate | undefined): date is RelativeDate => (date as RelativeDate)?.kind === 'relative'
export const isAbsolute = (date: EventDate | undefined): date is AbsoluteDate => (date as AbsoluteDate)?.kind === 'absolute'

export const absoluteOf = (date: string | undefined): AbsoluteDate | undefined => date ? ({ kind: 'absolute', value: date}) : undefined

export const isTemporal = (i: CampaignInstance | undefined): i is EventInstance => i ? i.instanceType === eventType && (i as EventInstance).type === eventType : false

export const asDuration = (p: Period) => moment.duration(p.direction === 'before' ? -p.number : p.number, p.unit)

// export const defaultRelativeDate = (target?: string): RelativeDate => ({ kind: 'relative', period: { number: 1, unit: 'months', direction: 'after' }, target: target! })

export type EventInstance = EventInstanceDto    // future-proofs divergence from exchange model, starts, aligned.

export const eventinstmodelapi = (s: EmarisState) => (c: Campaign) => {

    const events = eventapi(s)

    const t = intlapi(s).getT()

    const self = {

        //  turns all dates into absolute dates, following relative date targets recursively.
        //  optionally, EventDatelves targets against an overlay before using the global state.
        absolute: (date: EventDate | string, overlay?: EventInstance[]): string | undefined => {

            const absoluteRec = (date: EventDate | string, visited: string[], overlay?: EventInstance[]): string | undefined => {


                if (!date)
                    return undefined

                if (typeof date === 'string')
                    return date

                if (!isRelative(date))
                    return (date as AbsoluteDate).value

                if (!(date?.period?.number >= 0 ))
                    return undefined

                const target = overlay?.find(e => e.id === date.target) ?? eventinstapi(s)(c).lookup(date.target)

                if (target && visited.includes(target.id))
                    return undefined;

                // resolve target date 'recursively'.
                const targetDate = absoluteRec(target?.date, target ? [target.id, ...visited] : visited, overlay)

                if (!targetDate)
                    return undefined

                return moment(new Date(targetDate)).add(asDuration(date.period)).format();

            }

            return absoluteRec(date, [], overlay)


        },

        defaultRelativeDate : (target?:string) : RelativeDate => c.properties.defaultRelativeDate ?  
                    {...c.properties.defaultRelativeDate, target: target!} : 
                    { kind: 'relative', period: { number: 1, unit: 'months', direction: 'after' }, target: target! }

        ,

        dateOf: (source: string, target?: string) =>

            eventinstapi(s)(c).all().filter(e => e.source === source && (target === undefined || e.target === target)).map(e => self.absolute(e.date)).find(d => d)


        ,


        stringify: (i: EventInstance | undefined):string => i ? `${events.stringify(events.lookup(i.source))}` : ``

        ,

        generate: (event: Event, target?: string): EventInstance => {
            const instance = {
                instanceType: eventType,
                id: newId(),
                type: event.type,
                target: target ?? (event.type === campaignType ? c.id : undefined),
                source: event.id,
                campaign: c.id,
                date: event.type === eventType ? self.defaultRelativeDate(target) : undefined,
                tags: deepclone(event.tags),
                notified: false,
                properties: {
                    note: deepclone(event.properties.note),
                    relatable: event.properties.relatable || false
                }
            }

            // console.log("instantiating",event,"with",target, "returning", instance)

            return instance
        }

        ,


        clone: (instance: EventInstance, context:CampaignNext): EventInstance => {

            const id = newId();

            const clone = deepclone(instance)

            const date = context.type === 'branch' ?
                instance.date?.kind === 'absolute' ? 
                    instance.date.branchType === 'recurring' || instance.date.branchType === undefined?
                        {...clone.date , value: moment(instance.date.value).add(context.timeOffset || 0, 'years').format()} as AbsoluteDate //absolute recurring, then shift 
                    :
                    instance.date.branchType === 'pinned' ?
                       clone.date // keep absolute pinned
                    :
                    undefined // branched not pinned and not absolute ... delete it

                :
                clone.date // branch but relative date. Keep it
            :
            clone.date // is a clone. keep it anyway


            return {
                ...clone,
                id,
                target: instance.type === campaignType ? c.id : instance.target,
                campaign: c.id,
                // date: isRelative(instance.date) ? deepclone(instance.date) : undefined, // blanks absolute dates
                tags: context.type === 'branch' ? events.lookup(instance.source)?.tags ?? clone.tags : clone.tags,
                date,
                lineage: { source: instance.id, campaign: instance.campaign }
            }

        }

        ,

        comparator: (o1: EventInstance, o2: EventInstance) => {

            const profile = eventinstprofileapi(s)(c)

            return (profile.given(o1).name() ?? '').localeCompare(profile.given(o2).name() ?? '') || events.comparator(events.lookup(o1.source)!, events.lookup(o2.source)!)


        }

        ,

        // sorts by date, but groups events with their temporals.
        temporalAwareDateComparator: (o1: EventInstance, o2: EventInstance) =>

            self.temporalAwareDateComparatorOver(eventinstapi(s)(c).all())(o1,o2)


        ,
        
        // like above, but resolves temporals and dates using a given population rather than what's in state.
        temporalAwareDateComparatorOver: (all: EventInstance[]) => (o1: EventInstance, o2: EventInstance) => {

            const lookup = target => all.find(e => e.id === target)!

            const temporalLast = (o1: EventInstance, o2: EventInstance) => isTemporal(o1) ? isTemporal(o2) ? 0 : 1 : isTemporal(o2) ? -1 : 0

            // unfold temporals to their targets,   
            const oo1 = isTemporal(o1) ? lookup(o1.target) : o1
            const oo2 = isTemporal(o2) ? lookup(o2.target) : o2

            const value = self.dateComparatorOver(all)(oo1, oo2) || 
                        ( oo1.id===oo2.id ? temporalLast(o1, o2) || self.dateComparatorOver(all)(o1, o2) 
                        : 
                        self.comparator(oo1,oo2) 
                        ) 

            return value

        }


        ,

        // like date comparator, but resolves date using a given population rather than what's in state.
        dateComparatorOver: (all: EventInstance[]) => (o1: EventInstance, o2: EventInstance, nullFirst: boolean = true) => compareDates(self.absolute(o1.date, all), self.absolute(o2.date, all), nullFirst)

        ,

        dateComparator: (o1: EventInstance, o2: EventInstance, nullFirst: boolean = true) => compareDates(self.absolute(o1.date), self.absolute(o2.date), nullFirst)

        ,

        // by date, but temporals last after other events
        biasedDateComparator: (o1: EventInstance, o2: EventInstance, nullFirst: boolean = true) =>

            isTemporal(o1) ?

                (isTemporal(o2) ?

                    // both temporals
                    self.dateComparator(o1, o2, nullFirst)

                    :

                    1)

                : isTemporal(o2) ?

                    -1

                    :

                    // both non-temporal
                    self.dateComparator(o1, o2, nullFirst)


        ,
        
        duedateComparator: (dl1: EventInstance, dl2: EventInstance, nullFirst: boolean = false) => {

                const d1 = self.absolute(dl1?.date)
                const d2 = self.absolute(dl2?.date)

                // compare as dates if we have at least one.
                if (d1 || d2)
                    return compareDates(d1,d2,nullFirst)

                // otheriwse compare the string translations (coordinates with label rendering)
                const s1 = dl1 ? t("common.labels.unknown_due_date") : t("common.labels.no_due_date")
                const s2 = dl2 ? t("common.labels.unknown_due_date") : t("common.labels.no_due_date")

                return s1.localeCompare(s2)
            
        
        }
    }

    return self

}