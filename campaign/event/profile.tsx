import { eventapi } from "emaris-frontend/event/api";
import { eventPlural, eventSingular, eventType } from "emaris-frontend/event/constants";
import { l } from "apprise-frontend/model/multilang";
import { productapi } from "emaris-frontend/product/api";
import { productPlural, productSingular, productType } from "emaris-frontend/product/constants";
import { ProductLabel } from "emaris-frontend/product/Label";
import { requirementapi } from "emaris-frontend/requirement/api";
import { requirementPlural, requirementSingular, requirementType } from "emaris-frontend/requirement/constants";
import { EmarisState } from "emaris-frontend/state/model";
import * as React from "react";
import { RequirementLabel } from "../../requirement/Label";
import { campaignPlural, campaignSingular, campaignType } from "../constants";
import { CampaignLabel } from "../Label";
import { Campaign, CampaignInstance } from "../model";
import { ProductInstanceLabel } from "../product/Label";
import { productinststateapi } from "../product/state";
import { RequirementInstanceLabel } from "../requirement/Label";
import { requirementinststateapi } from "../requirement/state";
import { EventInstanceLabel } from "./Label";
import { EventInstance } from "./model";
import { eventinststateapi } from "./state";

export type TargetProfile = {

    // resolves the target and returns a string for comparisons and filtering.
    name: () => string | undefined
    typeName: () => { singular: string, plural: string }
    lookup: (target?: string) => CampaignInstance | Campaign | undefined

    // all possible values of the target type, and their rendering as options.
    // used to select one in detail form.
    allTargets: () => string[]
    optionOf: (target: string | undefined) => JSX.Element | undefined

    // renders a target.
    labelOf: (target: string | undefined, props?) => JSX.Element | undefined
}




export type ValidationProfile = {

    [type: string]: { allSources: { [sourceid: string]: CampaignInstance | Campaign } }


}

// given an instance, implements a suite of generic functions based on the target type.
export const eventinstprofileapi = (s: EmarisState) => (c: Campaign) => {

    const self = {


        validationProfile: (): ValidationProfile => ({


            [productType]: {
                allSources: (productinststateapi(s)(c).all() ?? []).reduce((acc, i) => ({ ...acc, [i.source]: i }), {})
            }

            ,

            [eventType]: { allSources: (eventinststateapi(s)(c).all() ?? []).reduce((acc, i) => ({ ...acc, [i.source]: i }), {}) }

            ,

            [requirementType]: { allSources: (requirementinststateapi(s)(c).all() ?? []).reduce((acc, i) => ({ ...acc, [i.source]: i }), {}) }

            ,

            [campaignType]: { allSources: { [c.id]: c } }


        })
        ,

        givenType: (type: string | undefined, target?: string): TargetProfile => {

            var impl

            switch (type) {

                case requirementType: {

                    const requirements = requirementapi(s)

                    impl = {


                        name: () => l(s)(requirements.safeLookup(target)?.name),
                        typeName: () => ({ singular: requirementSingular, plural: requirementPlural }),

                        lookup: (target) => requirementinststateapi(s)(c).all().find(i => i.source === target),

                        allTargets: () => requirementinststateapi(s)(c).all().map(i => requirements.safeLookup(i.source)).filter(i => i).map(i => i!.id),
                        optionOf: id => <RequirementLabel mode="light" noLink noDecorations requirement={id} />,

                        labelOf: (t, props) => <RequirementInstanceLabel {...props} instance={impl.lookup(t)} />

                    }

                    return impl
                }

                // temporals are different: have other event instances as their targets (instance ids, not template ids).
                case eventType:

                    {
                        impl = {

                            // name of template plus name of target instance, which will be about the name of its target campaign,requirement,or product.
                            name: () => {

                                const targetInstance = eventinststateapi(s)(c).lookup(target)

                                return targetInstance ? `${l(s)(eventapi(s).lookup(targetInstance.source)?.name)} ${self.given(targetInstance).name()}` : ''

                            },
                            typeName: () => ({ singular: eventSingular, plural: eventPlural }),
                            lookup: (target) => requirementinststateapi(s)(c).all().find(i => i.id === target),

                            // temmporals cannot target other temporals (eg. reminder of a reminder)
                            allTargets: () => eventinststateapi(s)(c).all().filter(i => i.type !== eventType).map(i => i.id),
                            optionOf: id => <EventInstanceLabel mode="light" combined noLink noDecorations noOptions instance={id} />,

                            labelOf: (target, props) => <EventInstanceLabel {...props} combined instance={target} />
                        }

                        return impl
                    }

                case productType: {

                    const products = productapi(s)

                    impl = {

                        name: () => l(s)(products.safeLookup(target)?.name),
                        typeName: () => ({ singular: productSingular, plural: productPlural }),

                        allTargets: () => productinststateapi(s)(c).all().map(i => products.safeLookup(i.source)).filter(i => i).map(i => i!.id),
                        optionOf: id => <ProductLabel mode="light" noLink product={id} />,
                        lookup: (target) => productinststateapi(s)(c).all().find(i => i.source === target),

                        labelOf: (target, props) => <ProductInstanceLabel {...props} instance={impl.lookup(target)} />
                    }

                    return impl
                }
                case campaignType:
                default:


                    impl = {

                        name: () => l(s)(c.name),
                        typeName: () => ({ singular: campaignSingular, plural: campaignPlural }),
                        lookup: () => c,

                        allTargets: () => [c.id],
                        optionOf: id => <CampaignLabel mode="light" noLink campaign={id} />,

                        labelOf: (_, props) => <CampaignLabel {...props} campaign={c} />
                    }

                    return impl
            }

        }


        ,

        given: (instance: EventInstance | string): TargetProfile => {

            const resolved = typeof instance === 'string' ? eventinststateapi(s)(c).lookup(instance)! : instance

            return self.givenType(resolved.type, resolved.target)

        }

    }

    return self;


}