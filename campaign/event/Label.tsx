import { Tooltip } from "antd"
import { UnknownLabel } from "apprise-frontend/components/Label"
import { eventRoute } from "emaris-frontend/event/constants"
import { EventLabel } from "emaris-frontend/event/Label"
import { icns } from "apprise-frontend/icons"
import { useTime } from "apprise-frontend/time/api"
import { useCurrentCampaign } from "emaris-frontend/campaign/hooks"
import React from "react"
import { useTranslation } from "react-i18next"
import { useCampaigns } from "../api"
import { campaignSingular, campaignType } from "../constants"
import { InstanceLabel, InstanceLabelProps } from "../InstanceLabel"
import { CampaignLabel } from "../Label"
import { InstanceRef, isInstanceRef } from "../model"
import { useAllEventInstances } from "./api"
import { EventInstance } from "./model"

type Props = InstanceLabelProps & {

    instance: string | EventInstance | InstanceRef | undefined
    combined?: boolean
    noDate?: boolean
}

export const EventInstanceLabel = (props: Props) => {

    const { t } = useTranslation()

    const time = useTime()
    const campaigns = useCampaigns()

    // if we received an id, we look in context for a campaign.
    const currentcampaign = useCurrentCampaign()

    const allEvents = useAllEventInstances()

    const { instance, combined, noDate, decorations = [], noDecorations, linkTarget, ...rest } = props

    if (!instance)
     return <UnknownLabel {...props} />

    // if we have full instance we resolve its campaign, otheriwise use current one.
    const campaign = typeof instance === 'string' ? currentcampaign : campaigns.lookup(instance?.campaign)

    if (!campaign)
        return <UnknownLabel title={t("common.labels.unknown_one", { singular: t(campaignSingular).toLowerCase() })} />

    const events = allEvents(campaign)

    // resolve instance if we don't have it already.
    var eventinstance = instance as EventInstance;

    if (isInstanceRef(instance))

        if (campaigns.isLoaded(instance.campaign))
            eventinstance = events.lookupLineage(instance)!
        else
            return <CampaignLabel loadTrigger campaign={instance.campaign} />

    else
     if (typeof instance === 'string')
        eventinstance = events.lookup(instance)!

    if (!eventinstance)
     return <UnknownLabel />

    if (!noDecorations && !noDate && eventinstance.date)
        decorations.push(<Tooltip title={time.format(events.absolute(eventinstance.date), 'long')}>{icns.bell}</Tooltip>)

    const currentcampaignEvent = currentcampaign && currentcampaign.id === eventinstance.campaign && eventinstance.type === campaignType

    const instanceLabel = props => <EventLabel {...rest} {...props} decorations={[...decorations, ...props.decorations]}
        mode={combined ? 'light' : undefined}
        linkTo={() => rest.linkTo || events.route(eventinstance)} linkTarget={linkTarget ?? campaignType} event={eventinstance?.source} />

    const usecombined = combined && !currentcampaignEvent && eventinstance.type

    return <InstanceLabel
        instance={eventinstance}
        baseRoute={eventRoute}
        {...rest}
        sourceLabel={sourceprops => usecombined ?

            <>
                {instanceLabel(sourceprops)} &nbsp;|&nbsp; { events.given(eventinstance).labelOf(eventinstance.target, { mode: 'light', noOptions: props.noOptions, noDecorations:props.noDecorations, noLink: props.noLink, linkTo: rest.linkTo, noIcon: true })}
            </>
            :
            instanceLabel(sourceprops)} />



}