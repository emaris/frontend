import { intlapi } from "apprise-frontend/intl/api"
import { tagapi } from "apprise-frontend/tag/api"
import { timeapi } from 'apprise-frontend/time/api'
import { check, checkIt, empty, notdefined, provided, ValidationCheck, withReport } from "apprise-frontend/utils/validation"
import { eventSingular, eventType } from "emaris-frontend/event/constants"
import { EmarisState } from "emaris-frontend/state/model"
import moment from 'moment'
import { campaignType } from "../constants"
import { Campaign } from "../model"
import { eventinstapi } from "./api"
import { EventDate, EventInstance, isRelative, RelativeDate } from "./model"
import { eventinstprofileapi, ValidationProfile } from "./profile"


export const eventinstvalidationapi = (s:EmarisState) => (c:Campaign) => ({


    validateInstance: (edited:EventInstance, profiles: ValidationProfile ) => {

        const t = intlapi(s).getT()
        const {validateCategories} = tagapi(s)
       
        const singular = t(eventSingular).toLowerCase()
       
        const eventInstances = eventinstapi(s)(c);

        const profile = profiles[edited.type || campaignType]
        const targetProfile = eventinstprofileapi(s)(c).given(edited)


        const targetExists : ValidationCheck<any> = {

            predicate: target => edited.type===eventType ? !eventInstances.lookup(target) : !profile.allSources[target],
            msg: ()=>t("campaign.feedback.missing")
        }

        return withReport({

            note: checkIt().nowOr(t("campaign.fields.note.msg"))

            ,
        
            target: check(edited.target!)
                        .with(provided(!!edited.type,empty(t)))
                        .with(provided(!!edited.type,targetExists))
                        .nowOr(t("common.fields.target.msg",{singular:t(targetProfile.typeName().singular).toLowerCase()}))


            ,

            lineage: checkIt().nowOr(t("common.fields.lineage.msg_nochoice",{singular}))

            ,

            source: checkIt().nowOr(t("campaign.fields.source.msg",{singular}))

            ,

            relatable: checkIt().nowOr(
                t("event.fields.relatable.msg"),
                t("event.fields.relatable.help",{singular}),
            )
            
            ,

            notified: checkIt().nowOr(
                edited.notifiedOn ? t("event.fields.notified.msg_notified",{relative:moment(edited.notifiedOn).from(timeapi(s).current())})
                : t("event.fields.notified.msg_unnotified") ,
                t("event.fields.notified.help",{singular}),
            ),

            date: check(edited.date).with(notdefined(t), {status:'warning'})
                                    .with(provided(edited.type!==eventType && isRelative(edited.date),{...notdefined(t),predicate: (d: EventDate) => notdefined(t).predicate((d as RelativeDate).target)}))
                                    .nowOr(isRelative(edited.date) ? t("campaign.fields.date.msg") : t("common.fields.date.msg"))

            ,
    
            ...validateCategories(edited.tags).for(eventType) 

        })

    }
})