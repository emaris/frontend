
import { useEmarisState } from "emaris-frontend/state/hooks";
import { EmarisState } from "../../state/model";
import { Campaign } from "../model";
import { eventinstmodelapi } from "./model";
import { eventinstprofileapi } from "./profile";
import { eventinststateapi } from "./state";
import { eventinstvalidationapi } from "./validation";


export const useAllEventInstances = () => eventinstapi(useEmarisState())
export const useEventInstances = (c:Campaign) => eventinstapi(useEmarisState())(c)

export const eventinstapi = (s:EmarisState) => (c:Campaign) => ({

    ...eventinststateapi(s)(c),
    ...eventinstmodelapi(s)(c),
    ...eventinstprofileapi(s)(c),
    ...eventinstvalidationapi(s)(c)

})

