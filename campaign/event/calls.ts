import { callapi } from "apprise-frontend/call/call";
import { domainService } from "../../constants";
import { EmarisState } from "../../state/model";
import { Campaign } from "../model";
import { EventInstance } from "./model";


export type Changes = {
    added : EventInstance[]
    updated : EventInstance[]
    removed : string[]
}

export const eventinstcallapi = (s:EmarisState) => (c: Campaign) => {

    const events = `/eventinstance`
    const {at} = callapi(s)


   return {

        fetchAll : () : Promise<EventInstance[]>  =>  at(`${events}/search`,domainService).post({campaign:c.id})

        ,
        
        addAll : (instances:EventInstance[]) : Promise<EventInstance[]>  =>  at(events,domainService).post(instances)

        ,

        updateOne: (instance:EventInstance) : Promise<void>  => at(`${events}/${instance.id}`,domainService).put(instance)

        ,

        updateMany: (changes:Changes) : Promise<EventInstance[]>  => at(events,domainService).put(changes)
          
        ,
        
        removeMany: (instances:EventInstance[]) : Promise<void>  => at(`${events}/remove`,domainService).post(instances.map(i=>i.id))
 
    }
}
