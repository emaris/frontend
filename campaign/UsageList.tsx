
import { Placeholder } from "apprise-frontend/components/Placeholder"
import { Column, VirtualTable } from "apprise-frontend/components/VirtualTable"
import { useLocale } from "apprise-frontend/model/hooks"
import { TagList } from "apprise-frontend/tag/Label"
import { useAsyncRender } from "apprise-frontend/utils/hooks"
import * as React from "react"
import { useTranslation } from "react-i18next"
import { useHistory } from "react-router-dom"
import { useCampaigns } from "./api"
import { campaignApiRoute } from "./constants"
import { CampaignLabel } from "./Label"
import { Campaign, CampaignDto } from "./model"

type Props = {

    type:string
    target:string
    height?:number

}

type ComponentProps = {

  
    height?: number
    campaigns:Campaign[]

}

export const useCampaignUsage = (props:Props) => {

    const [filtered,setFiltered] = React.useState<Campaign[]>(undefined!)

    const campaigns = useCampaigns()

    const {type,target,height} = props

    const [list] = useAsyncRender({

        when:!!filtered,
        task: ()=>campaigns.allWith(type,target).then(setFiltered),
        content: <CampaignUsageList campaigns={filtered} height={height} />,
        placeholder:Placeholder.list,
        errorPlaceholder: "nodata"
    })


    return [list,filtered] as [JSX.Element,CampaignDto[]]

}

const CampaignUsageList = (props:ComponentProps) => {

    const history=useHistory()
    const {t} = useTranslation()
    const {l} = useLocale()

   
    const {campaigns, height} = props

    return <VirtualTable<Campaign>  height={height} selectable={false} filtered={campaigns.length>15} 
                            data={campaigns} rowKey="id"
                            onDoubleClick={c => history.push(`${campaignApiRoute}/${c.id}`)} >

                        <Column width={250} title={t("common.fields.name_multi.name")} dataKey="name" 
                                dataGetter={(r:Campaign)=>l(r.name)} cellRenderer={r=><CampaignLabel campaign={r.rowData}/>} />

                        <Column flexGrow={1} title={t("common.fields.tags.name")} dataKey="tags" dataGetter={(r:Campaign)=><TagList taglist={r.tags} />} />

                        </VirtualTable>


}