

import { Paragraph } from "apprise-frontend/components/Typography";
import { configapi } from "apprise-frontend/config/state";
import { intlapi } from "apprise-frontend/intl/api";
import { contextCategory } from "apprise-frontend/system/constants";
import { tagapi } from "apprise-frontend/tag/api";
import { check, checkIt, notdefined, requireLanguages, reservedKeyword, stringLength, uniqueLanguages, withReport } from "apprise-frontend/utils/validation";
import { campaignType } from "emaris-frontend/campaign/constants";
import * as React from "react";
import { EmarisState } from "../state/model";
import { campaignapi } from "./api";
import { eventinstapi } from "./event/api";
import { Campaign, CampaignSettings } from "./model";
import { campaignmodule } from "./module";
import { partyinstapi } from "./party/api";
import { productinstapi } from "./product/api";
import { requirementinstapi } from "./requirement/api";

export type CampaignValidation = ReturnType<typeof campaignvalidationapi>

export const campaignvalidationapi = (s:EmarisState) => {


    const self = {

        validateProfile : (edited:Campaign, initial:Campaign) => { 


            const t = intlapi(s).getT()
            const {validateCategories} = tagapi(s)
            const {all} = campaignapi(s)

            const requiredLangs = configapi(s).get().intl.required || [];
        
            const singular= t(campaignmodule.nameSingular).toLowerCase()
            const plural= t(campaignmodule.namePlural).toLowerCase()

        
            return withReport({
        
                active: checkIt().nowOr( t("common.fields.active.msg"), t("common.fields.active.help",{plural})),
        
                name: check(edited.name).with(notdefined(t))
                            .with(requireLanguages(requiredLangs,t))
                            .with(reservedKeyword(t))
                            .with(stringLength(t))
                            .with(uniqueLanguages(all().filter(t=>t.id!==edited.id).map(t=>t.name),t)).nowOr(
                                t("common.fields.name_multi.msg"),
                                t("common.fields.name_multi.help",{plural,requiredLangs})),
        
        
                description: check(edited.description).nowOr(
                    t("common.fields.description_multi.msg"),
                    t("common.fields.description_multi.help",{plural}),
                )
                
                ,

                lineage: checkIt().nowOr(
                    t("common.fields.lineage.msg",{singular}),
                    t("campaign.fields.lineage.help"),
                )
                
                ,

                note: check(edited.properties.note ? edited.properties.note[s.logged.tenant] : '')
                        .nowOr(t("common.fields.note.msg"),t("common.fields.note.help", { singular }))
                
                ,

                muted: checkIt().nowOr(
                    t("campaign.fields.muted.msg"),
                    t("campaign.fields.muted.help"),
                )

                ,
        
                ...validateCategories(edited.tags).include(contextCategory)
                
                ,
        
                ...validateCategories(edited.tags).for(campaignType) 

                ,

                approveCycle: { 
                                msg:t("campaign.fields.approval.msg"),
                                help:<React.Fragment>
                                        <Paragraph>{t("campaign.fields.approval.help1")}</Paragraph>
                                        <Paragraph spaced >{t("campaign.fields.approval.help2_campaigns")}</Paragraph>
                                    </React.Fragment>
                                }
    
                ,
    
                adminCanEdit : {
    
                        msg:t("campaign.fields.admin_can_edit.msg"),
                        help:<React.Fragment>
                                <Paragraph>{t("campaign.fields.admin_can_edit.help1")}</Paragraph>
                                <Paragraph spaced >{t("campaign.fields.admin_can_edit.help2")}</Paragraph>
                            </React.Fragment>
                }
    
                ,

                defaultRelativeDate: checkIt().nowOr(
                    t("campaign.fields.default_relative_date.msg"),
                    t("campaign.fields.default_relative_date.help",{plural}),
                ),
    
    
                adminCanSubmit : {
    
                    msg:t("campaign.fields.admin_can_submit.msg"),
                    help:<React.Fragment>
                            <Paragraph>{t("campaign.fields.admin_can_submit.help1")}</Paragraph>
                            <Paragraph spaced >{t("campaign.fields.admin_can_submit.help2")}</Paragraph>
                        </React.Fragment>
                }
    
                ,
    
                complianceScale : {
                    msg:t("campaign.fields.compliance_scale.msg"),
                    help:<React.Fragment>
                            <Paragraph>{t("campaign.fields.compliance_scale.help1")}</Paragraph>
                            <Paragraph spaced>{t("campaign.fields.compliance_scale.help2")}</Paragraph>
                        </React.Fragment>
                }
    
                ,
    
                timelinessScale : {
                    msg:t("campaign.fields.timeliness_scale.msg"),
                    help:<React.Fragment>
                            <Paragraph>{t("campaign.fields.timeliness_scale.help1")}</Paragraph>
                            <Paragraph spaced>{t("campaign.fields.timeliness_scale.help2")}</Paragraph>
                        </React.Fragment>
                }
    
                ,
    
                statisticalHorizon : {
                    msg:t("campaign.fields.statistical_horizon.msg"),
                    help:<React.Fragment>
                            <Paragraph>{t("campaign.fields.statistical_horizon.help1")}</Paragraph>
                        </React.Fragment>
                }

                ,

                suspendOnEnd : {

                    msg:t("campaign.fields.suspend_on_end.msg"),
                    help:<Paragraph>{t("campaign.fields.suspend_on_end.help")}</Paragraph>
                        
                }
    
                ,

                suspendSubmissions : {

                    msg:t("campaign.fields.suspend_submissions.msg"),
                    help:<Paragraph>{t("campaign.fields.suspend_submissions.help")}</Paragraph>
                        
                }
    
                ,
    
                partyCanSeeNotApplicable : {
                    msg:t("campaign.fields.party_can_see_not_applicable.msg"),
                    help:<React.Fragment>
                            <Paragraph>{t("campaign.fields.party_can_see_not_applicable.help1")}</Paragraph>
                            <Paragraph spaced>{t("campaign.fields.party_can_see_not_applicable.help2")}</Paragraph>
                        </React.Fragment>
                }

                ,

                statExcludeList: {
                    msg:t("campaign.fields.stat_exclude_list.msg"),
                    help:<Paragraph>{t("campaign.fields.stat_exclude_list.help")}</Paragraph>
                }

            

        })
        
        
        }

        ,

        validateApplicationSettings: (_:Partial<CampaignSettings>) => {

            const t = intlapi(s).getT()

            return {


                liveguard : {

                    msg:t("campaign.fields.liveguard.msg"),
                    help:<Paragraph>{t("campaign.fields.liveguard.help")}</Paragraph>
                        
                }
                ,

                approveCycle: { 
                    msg:t("campaign.fields.approval.msg"),
                    help:<React.Fragment>
                            <Paragraph>{t("campaign.fields.approval.help1")}</Paragraph>
                        </React.Fragment>
                }

                ,

                adminCanEdit : {

                    msg:t("campaign.fields.admin_can_edit.msg"),
                    help:<React.Fragment>
                            <Paragraph>{t("campaign.fields.admin_can_edit.help1")}</Paragraph>
                        </React.Fragment>
                }

                ,


                adminCanSubmit : {

                    msg:t("campaign.fields.admin_can_submit.msg"),
                    help:<React.Fragment>
                        <Paragraph>{t("campaign.fields.admin_can_submit.help1")}</Paragraph>
                    </React.Fragment>
                }

                ,

                complianceScale : {
                    msg:t("campaign.fields.compliance_scale.msg"),
                    help:<React.Fragment>
                            <Paragraph>{t("campaign.fields.compliance_scale.help1")}</Paragraph>
                        </React.Fragment>
                }

                ,

                timelinessScale : {
                    msg:t("campaign.fields.timeliness_scale.msg"),
                    help:<React.Fragment>
                            <Paragraph>{t("campaign.fields.timeliness_scale.help1")}</Paragraph>
                        </React.Fragment>
                }

                ,

                statisticalHorizon : {
                    msg:t("campaign.fields.statistical_horizon.msg"),
                    help:<React.Fragment>
                            <Paragraph>{t("campaign.fields.statistical_horizon.help1")}</Paragraph>
                        </React.Fragment>
                }

                ,
                suspendOnEnd : {

                    msg:t("campaign.fields.suspend_on_end.msg"),
                    help:<Paragraph>{t("campaign.fields.suspend_on_end.help")}</Paragraph>
                        
                }
                ,

                defaultTimeZone : {
                    msg:t("common.fields.timezone.msg"),
                    help:<React.Fragment>
                            <Paragraph>{t("common.fields.timezone.help2")}</Paragraph>
                        </React.Fragment>
                }

            }
        }

        ,

        validateParties : (edited:Campaign) => { 

            const t = intlapi(s).getT()

            const {all,validateInstance} = partyinstapi(s)(edited)

            const report =  all().map(p=>({p,e:validateInstance(p).errors()})).reduce((acc,{p,e})=>({[p.id]:{status:e>0?"error":"success",msg:t("common.feedback.issues",{count:e})},...acc}),{})

            return withReport(report)
        
        
        }
        
        ,


        validateRequirements : (edited:Campaign) => { 

            const t = intlapi(s).getT()
           
            const {all,validateInstance} = requirementinstapi(s)(edited)

            return withReport({
        
            ...all().map(p=>({p,e:validateInstance(p).errors()})).reduce((acc,{p,e})=>({[p.id]:{status:e>0?"error":"success",msg:t("common.feedback.issues",{count:e})},...acc}),{})
        })
        
        
        }
        
        ,

        validateProducts : (edited:Campaign) => { 


            const t = intlapi(s).getT()

            const {all,validateInstance} = productinstapi(s)(edited)

            return withReport({
        
            ...all().map(p=>({p,e:validateInstance(p).errors()})).reduce((acc,{p,e})=>({[p.id]:{status:e>0?"error":"success",msg:t("common.feedback.issues",{count:e})},...acc}),{})
        })
        
        
        }

        ,

        validateEvents : (edited:Campaign) => { 

            const t = intlapi(s).getT()
           
            const {all,validateInstance, validationProfile} =eventinstapi(s)(edited)

            const profile = validationProfile()

            return withReport({
        
            ...all().map(i=>({i,e:validateInstance(i,profile).errors()}))
                    .reduce((acc,{i,e})=>({[i.id]:{status:e>0?"error":"success",msg:t("common.feedback.issues",{count:e})},...acc}),{})
        })
        
        
        }
        

    }   

    return self;

}