
import { EmarisState } from "../../state/model";
import { Campaign } from "../model";
import { requirementinstmodelapi } from "./model";
import { requirementinststateapi } from "./state";
import { requirementinstvalidationapi } from "./validation";
import { useEmarisState } from "emaris-frontend/state/hooks";

export const useAllRequirementInstances = () => requirementinstapi(useEmarisState())
export const useRequirementInstances = (c:Campaign) => requirementinstapi(useEmarisState())(c)

export const requirementinstapi = (s:EmarisState) => (c:Campaign) => ({


    ...requirementinststateapi(s)(c),
    ...requirementinstmodelapi(s)(c),
    ...requirementinstvalidationapi(s)(c)


})

