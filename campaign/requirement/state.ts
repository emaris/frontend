import { intlapi } from "apprise-frontend/intl/api";
import { defaultLanguage } from "apprise-frontend/intl/model";
import { systemapi } from "apprise-frontend/system/api";
import { tagapi } from "apprise-frontend/tag/api";
import { TenantDto } from "apprise-frontend/tenant/model";
import { utilapi } from "apprise-frontend/utils/api";
import { indexMap, through } from "apprise-frontend/utils/common";
import { notify, showAndThrow } from "apprise-frontend/utils/feedback";
import { paramsInQuery, updateQuery } from "apprise-frontend/utils/routes";
import { requirementapi } from "emaris-frontend/requirement/api";
import { requirementPlural, requirementSingular, requirementType } from "emaris-frontend/requirement/constants";
import { EmarisState } from "emaris-frontend/state/model";
import { campaignapi } from "../api";
import { eventinstapi } from "../event/api";
import { Campaign, CampaignInstance, InstanceRef, noCampaignInstance } from "../model";
import { PartyInstance, partyinstmodelapi } from "../party/model";
import { CampaignNext } from '../state';
import { requirementinstcallapi } from "./calls";
import { RequirementInstance, requirementinstmodelapi } from "./model";



export type RequirementInstanceState = {

    requirements: {
        all: RequirementInstance[]
        map: Record<string, RequirementInstance>
        sourceMap: Record<string, RequirementInstance>
    }

}

export const initialRequirementInstances = (): RequirementInstanceState => ({

    requirements: {
        all: undefined!,
        map: undefined!,
        sourceMap: undefined!
    }
})


export const requirementinststateapi = (s: EmarisState) => (c: Campaign) => {

    const { toggleBusy } = systemapi(s)
    const t = intlapi(s).getT()

    const [singular, plural] = [t(requirementSingular).toLowerCase(), t(requirementPlural).toLowerCase()]


    const type = "reqinstance"
    const call = () => requirementinstcallapi(s)(c)
    const model = () => requirementinstmodelapi(s)(c)

    const campaigns = () => campaignapi(s)
    const events = () => eventinstapi(s)(c)

    const paryinstancemodel = () => partyinstmodelapi(s)(c)

    const self = {

        livePush: (instances: RequirementInstance[], type: 'change' | 'remove') => {

        
            const map = s.campaigns.instances[c.id]?.requirements?.map ?? {}
            let current =  s.campaigns.instances[c.id]?.requirements?.all ?? []

            instances.forEach(instance => {

                switch (type) {

                    case "change" : 
                        current = map[instance.id] ? current.map(i => i.id === instance.id ? instance : i) : [...current, instance]
                        break;
                    
                    case "remove": current = current.filter(i => i.id !== instance.id); break

                }

            })

            self.setAll(current)


        }

        ,

        setAll: (instances: RequirementInstance[], props?: { noOverwrite: boolean }) => s.changeWith(s => {

            const campaign = s.campaigns.instances[c.id] ?? {}

            const requirements = campaign.requirements ?? initialRequirementInstances()

            if (requirements.all && props?.noOverwrite)
                return

            s.campaigns.instances[c.id] = { ...campaign, requirements: { ...requirements, all: instances, map: indexMap(instances).by(i => i.id), sourceMap: indexMap(instances).by(i => i.source) } }

        })

        ,

        addAll: (instances: RequirementInstance[]) =>

            toggleBusy(`${type}.addall`, t("common.feedback.save_changes"))

                .then(_ => call().addAll(instances))
                .then(through(added => self.setAll([...self.all(), ...added])))
                // notifies the event system to generate automanaged events.
                .then(through(added => events().addManagedForInstances(added)))
                .then(through(() => notify(t('common.feedback.saved'))))

                .catch(e => showAndThrow(e, t("common.calls.add_many_error", { plural })))
                .finally(() => toggleBusy(`${type}.addall`))

        ,

        cloneAll: (instances: RequirementInstance[], context: CampaignNext) => call().addAll(instances.map(i => model().clone(i, context))).then(self.setAll)

        ,

        areReady: () => !!self.all()

        ,

        fetchAll: (forceRefresh = false) =>

            Promise.all([

                requirementapi(s).fetchAll(),

                self.areReady() && !forceRefresh ? Promise.resolve(self.all())

                    :

                    toggleBusy(`${type}.fetchAll.${c.id}`, t("common.feedback.load", { plural }))

                        .then(_ => console.log(`fetching requirements for ${c.name[defaultLanguage]}...`))
                        .then(call().fetchAll) // load template dependency in parallel

                        .then(through($ => self.setAll($, { noOverwrite: !forceRefresh })))


                        .catch(e => showAndThrow(e, t("common.calls.fetch_all_error", { plural })))
                        .finally(() => toggleBusy(`${type}.fetchAll.${c.id}`))

            ])
                .then(([, instances]) => instances)  // continue with instances only

        ,

        lookup: (id: string | undefined) => id ? s.campaigns.instances[c.id]?.requirements?.map[id] : undefined

        ,

        safeLookup: (id: string | undefined) => self.lookup(id) ?? requirementinstmodelapi(s)(c).noInstance()

        ,

        lookupLineage: (ref: InstanceRef | undefined) => ref ? s.campaigns.instances[ref.campaign]?.requirements?.all.find(p => p.source === ref.source) : undefined

        ,

        lookupBySource: (source: string | undefined) => source ? s.campaigns.instances[c.id]?.requirements?.sourceMap[source] : undefined

        ,

        safeLookupBySource: (source: string | undefined) => self.lookupBySource(source) ?? noCampaignInstance(requirementType, c.id) as RequirementInstance

        ,

        detailParam: () => 'ri-drawer'

        ,

        // eslint-disable-next-line
        detailInRoute: () => paramsInQuery(location.search)[self.detailParam()] as string

        ,

        route: (p?: RequirementInstance) =>

            // eslint-disable-next-line
            `${campaigns().routeTo(c)}/${requirementType}?${updateQuery(location.search).with(params => {
                
                params[self.detailParam()] = p ? p.id : null
                params['source'] = p?.source ?? null
            
            })}`

        ,


        all: () => s.campaigns.instances[c.id]?.requirements?.all

        ,

        allSorted: () => [...self.all() ?? []].sort(model().comparator)

        ,

        allForParty: (party: PartyInstance, includeNotApplicable = paryinstancemodel().canSeeNotApplicable(party)) =>

            includeNotApplicable ? self.allSorted() : self.allSorted().filter(ci => self.isForParty(party, ci))


        ,

        isForParty: (party: PartyInstance | TenantDto, ci: CampaignInstance): boolean => {

            const partyId = (party as PartyInstance).source ?? (party as TenantDto).id

            const audienceListCheckInclude = ci.audienceList ? ci.audienceList.includes.includes(partyId) || ci.audienceList.includes.length === 0 : true
            const audienceListCheckExclude = ci.audienceList ? !ci.audienceList.excludes.includes(partyId) || ci.audienceList.excludes.length === 0 : true
            
            const audienceListCheck = audienceListCheckExclude && audienceListCheckInclude

            return audienceListCheck && (ci.audience ? tagapi(s).expression(ci.audience).matches(party) : true)
            
        }

        ,

        save: (instance: RequirementInstance, replacementOf: RequirementInstance = instance) =>

            toggleBusy(`${type}.updateOne`, t("common.feedback.save_changes"))

                .then(() => call().updateOne(instance))
                .then(() => self.all().map(p => p.id === replacementOf.id ? instance : p))
                .then(self.setAll)
                .then(() => notify(t('common.feedback.saved')))

                .catch(e => showAndThrow(e, t("common.calls.update_one_error", { singular })))
                .finally(() => toggleBusy(`${type}.updateOne`))
        ,

        remove: (instance: RequirementInstance, onConfirm?: (...args) => void) =>

            utilapi(s).askConsent({

                title: t('common.consent.remove_one_title', { singular }),
                content: t("common.consent.remove_one_msg", { singular }),
                okText: t("common.consent.remove_one_confirm", { singular }),

                onOk: () => {


                    toggleBusy(`${type}.removeOne`, t("common.feedback.save_changes"))

                        .then(_ => call().removeOne(instance))
                        .then(() => self.all().filter(p => instance.id !== p.id))
                        .then(self.setAll)
                        .then(() => notify(t('common.feedback.saved')))
                        .then(() => onConfirm && onConfirm(instance))

                        .catch(e => showAndThrow(e, t("common.calls.remove_one_error", { singular })))
                        .finally(() => toggleBusy(`${type}.removeOne`))


                }
            })

        ,


        removeMany: (instances: RequirementInstance[], onConfirm?: () => void) =>

            utilapi(s).askConsent({

                title: t('common.consent.remove_many_title', { count: instances.length, plural }),
                content: t("common.consent.remove_many_msg", { count: instances.length, plural }),
                okText: t("common.consent.remove_many_confirm", { count: instances.length, plural }),

                onOk: () => {

                    const ids = instances.map(p => p.id)

                    toggleBusy(`${type}.removeMany`, t("common.feedback.save_changes"))

                        .then(_ => call().removeMany(instances))
                        .then(() => self.all().filter(p => !ids.includes(p.id)))
                        .then(self.setAll)
                        .then(() => notify(t('common.feedback.saved')))
                        .then(through(() => onConfirm && onConfirm()))

                        .catch(e => showAndThrow(e, t("common.calls.remove_many_error", { plural })))
                        .finally(() => toggleBusy(`${type}.removeMany`))

                }
            })

    }

    return self
}






