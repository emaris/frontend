import { Tooltip } from "antd"
import { Label, sameLabel, UnknownLabel } from "apprise-frontend/components/Label"
import { useConfig } from "apprise-frontend/config/state"
import { icns } from "apprise-frontend/icons"
import { TimeLabel } from "apprise-frontend/time/Label"
import { useDashboards } from "emaris-frontend/dashboard/api"
import { useCampaignMode } from "emaris-frontend/dashboard/hooks"
import { requirementRoute } from "emaris-frontend/requirement/constants"
import React from "react"
import { useTranslation } from "react-i18next"
import { Link } from "react-router-dom"
import { RequirementLabel } from "../../requirement/Label"
import { useCampaigns } from "../api"
import { campaignSingular, campaignType, runningIcon } from "../constants"
import { useAllEventInstances } from "../event/api"
import { InstanceLabel, InstanceLabelProps } from "../InstanceLabel"
import { CampaignLabel } from "../Label"
import { InstanceRef, isInstanceRef } from "../model"
import { submissionType } from "../submission/constants"
import { useAllRequirementInstances } from "./api"
import { RequirementInstance } from "./model"

type Props = InstanceLabelProps & {

    instance: RequirementInstance | InstanceRef | undefined
    lineage?: boolean
    deadlineOnly?: boolean
    deadlineMode?: 'default' | 'relative'
    showTitle?: boolean
    tipTitle?: boolean
}

const emptySpace = <div style={{width: 13}} />

export const RequirementInstanceLabel = React.memo((props: Props) => {

    const { instance, options = [], deadlineOnly, deadlineMode, linkTo, linkTarget, showTitle=false, tipTitle=false,...rest } = props

    const { t } = useTranslation()

    const config = useConfig()
    const campaigns = useCampaigns()
    const dashboard = useDashboards()
    const campaignMode = useCampaignMode()
    const allRequirements = useAllRequirementInstances()
    const allEvents = useAllEventInstances()

    const campaign = campaigns.lookup(instance?.campaign)

    if (!campaign)
        return <UnknownLabel title={t("common.labels.unknown_one", { singular: t(campaignSingular).toLowerCase() })} />

    const campaignTimeZone = campaign.properties.timeZone ? {'original' : campaign.properties.timeZone} : undefined

    const requirements = allRequirements(campaign)

    var requirementinstance = instance as RequirementInstance;

    if (isInstanceRef(instance))

        if (campaigns.isLoaded(instance.campaign))
            requirementinstance = requirements.lookupLineage(instance)!
        else
            return <CampaignLabel loadTrigger campaign={instance.campaign} />

    if (!requirementinstance)
        return <UnknownLabel {...props} />

    const events = allEvents(campaign)

    if (deadlineOnly) {

        const deadline = requirements.deadlineOf(requirementinstance)

        if (deadline) {
            const date = events.absolute(deadline.date)
            return date ? <TimeLabel accentFrame='weeks' format='numbers' displayMode={deadlineMode} render={props.mode === 'tag' ? date => t('campaign.date.due', { date }) : undefined} value={date} timezones={campaignTimeZone} {...rest} />
                    : <Label {...props} noIcon noLink title={t("common.labels.unknown_due_date")} disabled />
        }
        else 
        
            return <Label {...props} noIcon noLink title={t("common.labels.no_due_date")} disabled />

    }

    if (campaignMode === 'dashboard' && config.get().routedTypes?.includes(campaignType))
        options.push(<Tooltip title={t("campaign.labels.campaign_view.design")}><Link to={() => requirements.route(requirementinstance)}>{icns.edit}</Link></Tooltip>)
    else 
        options.push(emptySpace)

    if (campaignMode === 'design' && config.get().routedTypes?.includes(submissionType))
        options.push(<Tooltip title={t("campaign.labels.campaign_view.live")}><Link to={() => dashboard.given(campaign).routeToAsset(requirementinstance)}>{runningIcon}</Link></Tooltip>)

   return <InstanceLabel instance={requirementinstance} noMemo={props.noMemo} tooltipForLineage={ref => <RequirementInstanceLabel showCampaign instance={ref} />}

        baseRoute={requirementRoute}
        {...rest}
        sourceLabel={props => <RequirementLabel {...props} noMemo={props.noMemo} showTitle={showTitle} tipTitle={tipTitle} options={options} linkTo={linkTo || (() => requirements.route(requirementinstance))} linkTarget={submissionType} {...rest} requirement={instance?.source} />} />


}, ($, $$) => $.instance === $$.instance && sameLabel($, $$))