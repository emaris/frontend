import { callapi } from "apprise-frontend/call/call";
import { domainService } from "../../constants";
import { EmarisState } from "../../state/model";
import { Campaign } from "../model";
import { RequirementInstance } from "./model";


export const requirementinstcallapi = (s:EmarisState) => (c: Campaign) => {

    const requirements = `/requirementinstance`
   
    const {at} = callapi(s)


   return {


        fetchAll : () : Promise<RequirementInstance[]>  =>  at(`${requirements}/search`,domainService).post({campaign:c.id})
        
        , 

        fetchOne : (id:string) : Promise<RequirementInstance>  =>  at(`${requirements}/${id}?byref=true`,domainService).get()

        , 

        addAll : (instances:RequirementInstance[]) : Promise<RequirementInstance[]>  =>  at(requirements,domainService).post(instances)
        ,

        updateOne: (instance:RequirementInstance) : Promise<void>  => at(`${requirements}/${instance.id}`,domainService).put(instance)
        
        ,
                    
        removeOne: (instance:RequirementInstance) : Promise<void>  => at(`${requirements}/${instance.id}`,domainService).delete()
        
        ,

        removeMany: (instances:RequirementInstance[]) : Promise<void>  =>  at(`${requirements}/remove`,domainService).post(instances.map(i=>i.id))
 
    }
}
