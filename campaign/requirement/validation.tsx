
import { intlapi } from "apprise-frontend/intl/api"
import { tagapi } from "apprise-frontend/tag/api"
import { check, checkIt, empty, ValidationCheck, withReport } from "apprise-frontend/utils/validation"
import { requirementapi } from "emaris-frontend/requirement/api"
import { requirementPlural, requirementSingular, requirementType } from "emaris-frontend/requirement/constants"
import { EmarisState } from "../../state/model"
import { Campaign } from "../model"
import { RequirementInstance } from "./model"
import { tenantPlural } from "apprise-frontend/tenant/constants"
import { userPlural } from 'apprise-frontend/user/constants'


export const requirementinstvalidationapi = (s:EmarisState) => (c:Campaign) => ({

    validateInstance: (edited:RequirementInstance) => {

        const t = intlapi(s).getT()
        const {validateCategories} = tagapi(s)
        const {all} = requirementapi(s)

        const singular = t(requirementSingular).toLowerCase()
        const plural = t(requirementPlural).toLowerCase()

        const targetExists : ValidationCheck<any> = {

            predicate: target => !all().find(i=>i.id===target),
            msg: ()=>"campaign.feedback.missing"
        }

        const partyPlural = t(tenantPlural).toLowerCase()

        return withReport({

            note: checkIt().nowOr(t("campaign.fields.note.msg"))
            
            ,

            source_editable: check(edited.source!)
                    .with(empty(t))
                    .with(targetExists)
                    .nowOr(t("campaign.fields.source.msg"))

            ,
                    
            
            source_readonly: checkIt().nowOr(t("campaign.fields.source.msg",{singular}))
            ,
    
            lineage: checkIt().nowOr(t("common.fields.lineage.msg_nochoice",{singular}))
            
            ,

            audience: checkIt().nowOr(
                t("common.fields.audience.msg"), 
                t("common.fields.audience.help",{singular,plural:partyPlural})
            ),

            userProfile: checkIt().nowOr(
                t("common.fields.user_profile.msg"), 
                t("common.fields.user_profile.help",{singular,plural:userPlural})
            )
            
            ,  

            editable: checkIt().nowOr(
                t("common.fields.editable.help", {plural, parties: partyPlural})
            )

            ,  

            versionable: checkIt().nowOr(
                t("common.fields.versionable.help", {plural, parties: partyPlural})
            )

            ,  

            assessed: checkIt().nowOr(
                t("common.fields.assessed.help", {plural})
            ) 

            ,

            ...validateCategories(edited.tags).for(requirementType) 

        })

    }
})