import { ParameterRef } from "apprise-frontend/layout/parameters/model";
import { layoutRegistry } from "apprise-frontend/layout/registry";
import { tagapi } from "apprise-frontend/tag/api";
import { noTenant } from 'apprise-frontend/tenant/constants';
import { timeapi } from "apprise-frontend/time/api";
import { User } from 'apprise-frontend/user/model';
import { deepclone } from "apprise-frontend/utils/common";
import { withReport } from "apprise-frontend/utils/validation";
import { Topic } from "apprise-messages/model";
import { requirementapi } from "emaris-frontend/requirement/api";
import { requirementType } from "emaris-frontend/requirement/constants";
import moment from "moment-timezone";
import { Requirement } from "../../requirement/model";
import { EmarisState } from "../../state/model";
import { requirementDeadlineEvent } from "../constants";
import { eventinstapi } from "../event/api";
import { AssetInstance, Campaign, CampaignInstance } from "../model";
import { PartyInstance } from "../party/model";
import { CampaignNext } from '../state';
import { requirementinstapi } from "./api";

 // future-proofs custom fields, starts.generic. 
export type RequirementInstanceDto = AssetInstance 

export type RequirementInstance = RequirementInstanceDto    // future-proofs divergence from exchange model, starts, aligned.



export const requirementinstmodelapi = (s:EmarisState) => (c:Campaign) =>{

    const requirements = requirementapi(s)
    
    const self = {

        noInstance: () => ({

            id:undefined!,
            instanceType:requirementType,
            source:"unknown",
            campaign:c.id,
            tags:[],
            properties:{
                editable: true,
                versionable: true,
                assessed: true
            }

        }) as RequirementInstance

        ,

        stringify: (i:RequirementInstance) => `${requirements.stringify(requirements.safeLookup(i?.source))}`

        ,

        nameOf: (i:RequirementInstance) => `${requirements.nameOf(requirements.safeLookup(i?.source))}`

        ,
        
        generate: (requirement:Requirement) : RequirementInstance => ( {

            id:undefined!,
            instanceType:requirementType,
            source:requirement.id,
            campaign:c.id,
            tags:deepclone(requirement.tags),
            audience:requirement.audience,
            audienceList: requirement.audienceList,
            userProfile: requirement.userProfile,
            properties:{
                note:requirement.properties.note,
                editable: requirement.properties.editable,
                versionable: requirement.properties.versionable,
                assessed: requirement.properties.assessed
            }})
        
        ,

        clone: (instance:RequirementInstance, campaign:CampaignNext) => {
            const {type} = campaign
            const clone = deepclone(instance)

            const parameterOverlay = clone.properties.parameterOverlay
            const overlays = parameterOverlay && 
                Object.keys(parameterOverlay).reduce( 
                    (acc, cur) => {
                        const param = parameterOverlay![cur] as ParameterRef
                        const clonedParam = type === 'branch' ? 
                            layoutRegistry(requirementType).lookupParameter(param.original.spec).clone({...param.original, value: param.value}, campaign)
                            :
                            {...param.original, value: param.value}
                        return clonedParam ? 
                            {...acc, [cur]: {original:param.original, value:clonedParam ? clonedParam.value : clonedParam}} 
                        : acc
                    }
                ,{})


            return {...clone, 
                    id:undefined!,  
                    campaign:c.id, 
                    tags: type === 'branch' ? requirements.lookup(instance.source)?.tags ?? clone.tags : clone.tags, 
                    lineage: type === 'branch' ? {source:instance.source,campaign:instance.campaign} : undefined,
                    properties: {...clone.properties, parameterOverlay: overlays}
                    
            } 
                
        }

        ,

        comparator: (o1:RequirementInstance,o2:RequirementInstance) => requirements.comparator(requirements.safeLookup(o1.source)!,requirements.safeLookup(o2.source)!)

        ,


        deadlineOf: (r:RequirementInstance) => eventinstapi(s)(c).allAbout(r.source).find(ei=>ei.source===requirementDeadlineEvent)


        ,

        allDueDatesByRequirementId: () : { [_:string]:string} => {

            const {absolute,all} = eventinstapi(s)(c)
            
            return all().filter(ei=>ei.source===requirementDeadlineEvent)
                        .reduce((a,ei) =>({...a,[ei.target!]:absolute(ei.date)}), {} )

        }
        
        ,

        pastDue: (party?:PartyInstance) => {
            
            const now = timeapi(s).current()
            const {absolute,all} = eventinstapi(s)(c)

            const targets = (party ? requirementinstapi(s)(c).allForParty(party) : requirementinstapi(s)(c).all()).map(r=>r.source)

            return all().filter(ei=>ei.source===requirementDeadlineEvent && 
                                    targets.includes(ei.target!) 
                                    && ei.date 
                                    && moment(new Date(absolute(ei.date)!)).isBefore(now))
        }
        ,

        deadlineDateOf: (r:RequirementInstance) => eventinstapi(s)(c).absolute( self.deadlineOf(r)?.date )

        ,

        audienceOf: (r:RequirementInstance) => requirements.safeLookup(r.source).audience

        ,

        validateInstance: (r:RequirementInstance)  => withReport({})  // nothing to vaidate for now.

        ,

        topics: (r: CampaignInstance): Topic[] => [{ type: requirementType, name:`${c.id}:${r.source}`  }]

        ,

        matches: (r:RequirementInstance, u: User) => {

            const {given} = tagapi(s)
            
            return u.tenant === noTenant || given(u).matches(r.userProfile)
            
        }
    }

    return self

}