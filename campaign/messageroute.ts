import { tagapi } from "apprise-frontend/tag/api";
import { tenantcalls } from "apprise-frontend/tenant/calls";
import { tenantType } from "apprise-frontend/tenant/constants";
import { TenantDto } from "apprise-frontend/tenant/model";
import { userapi } from "apprise-frontend/user/api";
import { messageType } from "apprise-messages/constants";
import { Message, Topic } from "apprise-messages/model";
import { dashboardRoute } from "emaris-frontend/dashboard/constants";
import { productcalls } from "emaris-frontend/product/calls";
import { productType } from "emaris-frontend/product/constants";
import { ProductDto } from "emaris-frontend/product/model";
import { requirementcalls } from "emaris-frontend/requirement/calls";
import { requirementType } from "emaris-frontend/requirement/constants";
import { RequirementDto } from "emaris-frontend/requirement/model";
import { EmarisState } from "emaris-frontend/state/model";
import { campaigncalls } from "./calls";
import { campaignType } from "./constants";
import { Campaign } from "./model";
import { partyinstcallapi } from "./party/calls";
import { productinstcallapi } from "./product/calls";
import { requirementinstcallapi } from "./requirement/calls";
import { profileapi } from "./submission/profile";

export const messagerouteapi = (state:EmarisState) => (message:Message) => {  

    const logged = userapi(state).logged

    const self = {
        getCampaign : (id:string) : Promise<Campaign> => campaigncalls(state).fetchOne(id).then(c=>c.campaign),
        getTenant : (id:string) : Promise<TenantDto> => tenantcalls(state).fetchOne(id),
        
        getRequirement : (id:string) => requirementcalls(state).fetchOne(id),
        getProduct : (id:string) => productcalls(state).fetchOne(id),
        getPartyInstance : (campaign: string) => (party:string) => self.getCampaign(campaign).then(c=>partyinstcallapi(state)(c).fetchOne(`${party}@${campaign}`).then(p=>
                ({partyinstance: p, campaign: c})
            ))
        ,

        getRequirementInstance : (campaign: string) => (id:string) =>
            self.getCampaign(campaign).then(c=>requirementinstcallapi(state)(c).fetchOne(`${id}@${campaign}`).then(requirementinstance=>({campaign: c, requirementinstance})))
        ,
        getProductInstance : (campaign: string) => (id:string) =>
            self.getCampaign(campaign).then(c=>productinstcallapi(state)(c).fetchOne(`${id}@${campaign}`).then(productinstance=>({campaign: c, productinstance})))
        ,


        getTopicByType : (type:string) : Topic | undefined => message.topics.find(t=>t.type === type),
        buildRouteToCampaign : (c:Campaign) => `${dashboardRoute}/${c.id}`,
        buildRouteToAsset : (c:Campaign) => (type: typeof requirementType | typeof productType, asset : RequirementDto | ProductDto) => `${self.buildRouteToCampaign(c)}/${type}/${asset.id}`,
        buildRouteToParty : (c:Campaign) => (party: TenantDto) => `${self.buildRouteToCampaign(c)}/${tenantType}/${party.id}`,
        buildRouteToSubmission : (c:Campaign) => (party:TenantDto) => (assetType: typeof requirementType | typeof productType, asset : RequirementDto | ProductDto) =>
                `${self.buildRouteToParty(c)(party)}/${assetType}/${asset.id}/latest`
        ,

        topicsTypes : () => message.topics.map(t=>t.type),

        submissionMessageRoute : async() => {

            const assetType = self.topicsTypes().includes(requirementType) ? requirementType : productType

            const assetTopic = self.getTopicByType(assetType)
            const partyTopic = self.getTopicByType(tenantType)
            const campaignTopic = self.getTopicByType(campaignType)

            const pid = partyTopic!.name.split(":").pop()!
            const aid = assetTopic!.name.split(":").pop()!
            const [cid] = campaignTopic!.name.split(":") 

            const campaign = await self.getCampaign(cid)

            const submissions = profileapi(state)(campaign)
            const profiles = { 
        
                [requirementType] : submissions.profileOf(requirementType),
                [productType] : submissions.profileOf(productType)
            
            }

            const party = await self.getTenant(pid)

            const asset =  assetType === requirementType ? await self.getRequirement(aid) : await self.getProduct(aid)

            const assetInstance = assetType ? assetType === 'requirement' ? (await self.getRequirementInstance(campaign.id)(asset.id)).requirementinstance : (await self.getProductInstance(campaign.id)(asset.id)).productinstance : undefined

            const toRoute = assetInstance ? profiles[requirementType].isForParty(party, assetInstance) : true

            return toRoute ? {route:  `${self.buildRouteToSubmission(campaign)(party)(assetType, asset)}?tab=${messageType}`} 
                    : undefined


        }

        ,

        assetMessageRoute : () => {
            const assetType = self.topicsTypes().includes(requirementType) ? requirementType : productType
            return assetType === requirementType ? self.requirementMessageRoute() : self.productMessageRoute()
        }

        ,

        requirementMessageRoute : () => {
            const assetTopic = self.getTopicByType(requirementType)
            const campaignTopic = self.getTopicByType(campaignType)

            const rid = assetTopic!.name.split(":").pop()!
            const [cid] = campaignTopic!.name.split(":") 

            if (logged.hasNoTenant()) {
                return self.getCampaign(cid).then(campaign=>
                    self.getRequirement(rid).then(requirement=>({requirement, campaign})).then(fetched=>
                        ({route: `${self.buildRouteToAsset(fetched.campaign)(requirementType, fetched.requirement)}/${messageType}`})    
                    ))
            } else {
                return self.getTenant(logged.tenant).then(party=>
                            self.getPartyInstance(cid)(party.id).then(res=>({...res, party})).then(fetched=>
                                self.getRequirementInstance(cid)(rid).then(res=>({...fetched, ...res})).then(fetched=>
                                    self.getRequirement(rid).then(requirement=>({...fetched, requirement})).then(fetched=>{
                                        if (fetched.requirementinstance.audience && !tagapi(state).expression(fetched.requirementinstance.audience).matches(fetched.partyinstance))
                                            return undefined
                                        return {route: `${self.buildRouteToSubmission(fetched.campaign)(fetched.party)(requirementType,fetched.requirement)}?tab=${messageType}`}    
                                    })    
                                )
                            ) 
                        )
            }
        }

        ,

        productMessageRoute : () => {
            const assetTopic = self.getTopicByType(productType)
            const campaignTopic = self.getTopicByType(campaignType)

            const pid = assetTopic!.name.split(":").pop()!
            const [cid] = campaignTopic!.name.split(":") 

            if (logged.hasNoTenant() ) {
                return self.getCampaign(cid).then(campaign=>
                    self.getProduct(pid).then(product=>({product, campaign})).then(fetched=>
                        ({route: `${self.buildRouteToAsset(fetched.campaign)(productType, fetched.product)}/${messageType}`})    
                    ))
            } else {
                return self.getTenant(logged.tenant).then(party=>
                            self.getPartyInstance(cid)(party.id).then(res=>({...res, party})).then(fetched=>
                                self.getProductInstance(cid)(pid).then(res=>({...fetched, ...res})).then(fetched=>
                                    self.getProduct(pid).then(product=>({...fetched, product})).then(fetched=>{
                                        if (fetched.productinstance.audience && !tagapi(state).expression(fetched.productinstance.audience).matches(fetched.partyinstance))
                                            return undefined
                                        return {route: `${self.buildRouteToSubmission(fetched.campaign)(fetched.party)(productType,fetched.product)}?tab=${messageType}`}    
                                    })    
                                )
                            ) 
                        )
            }
        }
        
        ,

        partyMessageRoute : () => {
            
            const partyTopic = self.getTopicByType(tenantType)
            const campaignTopic = self.getTopicByType(campaignType)

            const pid = partyTopic!.name.split(":").pop()!
            const [cid] = campaignTopic!.name.split(":") 

            return (logged.hasNoTenant() || logged.tenant === pid) ? //If has a tenant and the tenant is different from the message topic for parties then return undefined, otherwise is either IOTC or a user of which the message belongs to.
                        self.getCampaign(cid).then(campaign=>
                            self.getTenant(pid).then(party=>({campaign, party})).then(fetched=>
                                ({route: `${self.buildRouteToParty(fetched.campaign)(fetched.party)}/${messageType}` })
                            )
                        )
                    :
                        undefined
        }
        
        ,

        campaignMessageRoute : () => {
            const campaignTopic = self.getTopicByType(campaignType)
            const [cid] = campaignTopic!.name.split(":") 

            return logged.hasNoTenant() ? 
                        self.getCampaign(cid).then(campaign=>
                            ({route: `${self.buildRouteToCampaign (campaign)}/${messageType}`})
                        )
                    :
                        self.getCampaign(cid).then(campaign=>
                            self.getTenant(logged.tenant).then(party=>
                                ({route: `${self.buildRouteToParty(campaign)(party)}/${messageType}` })
                            )
                        )
        }

    }
    
    return self;
}
