
import { callapi } from "apprise-frontend/call/call"
import { domainService } from "../constants"
import { EmarisState } from "../state/model"
import { Campaign, CampaignDto, campaignmodelapi, Summary } from "./model"

const campaigns = "/campaign"

export type CampaignAndSummary = {

    campaign: CampaignDto
    summary: Summary

}

export type CampaignAndArchive = {

    campaign: CampaignDto
    archive: CampaignArchive

}

export type ArchiveEntry = {

    details: any
    type: string
}

export type CampaignArchive = ArchiveEntry[]


export const campaigncalls = (s:EmarisState) => {

    const {at} = callapi(s)
    const { extern, intern } = campaignmodelapi(s)

    return {


        fetchAll: () => at(campaigns,domainService).get<CampaignAndSummary[]>().then(all=>all.map(cs=>intern({...cs.campaign,summary:cs.summary})))
        
        , 

        fetchOne: (id:String) =>  at(`${campaigns}/${id}`,domainService).get<CampaignAndArchive>().then(c => ({...c, campaign: intern(c.campaign)})) 
        
        ,

        fetchWith: (filter:{type:string,id:string}) => at(`${campaigns}/search`,domainService).post<string[]>(filter)

        ,

        add: (campaign:Campaign)  =>  at(campaigns,domainService).post<Campaign>(extern(campaign)).then(intern)
        
        ,
    
        update: (campaign:Campaign) => at(`${campaigns}/${campaign.id}`,domainService).put<CampaignAndArchive>(extern(campaign)).then(c => ({...c, campaign: intern(c.campaign)})) 
        
        , 

        delete: (id:string) => at(`${campaigns}/${id}`,domainService).delete()


    }

}