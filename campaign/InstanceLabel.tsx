import { Tooltip } from "antd"
import { LabelProps, sameLabel, UnknownLabel } from "apprise-frontend/components/Label"
import { useConfig } from "apprise-frontend/config/state"
import { icns } from "apprise-frontend/icons"
import { useUsers } from "apprise-frontend/user/api"
import React from "react"
import { useCampaigns } from "./api"
import { CampaignLabel } from "./Label"
import { CampaignInstance, InstanceRef } from "./model"

export type InstanceLabelProps = LabelProps & {

    showCampaign?: boolean
    noLineage?: boolean

    children?: any
}

export type InternalProps = InstanceLabelProps & {

    instance: CampaignInstance | undefined
    sourceLabel: (props: LabelProps) => JSX.Element

    tooltipForLineage?: (ref: InstanceRef) => JSX.Element | undefined
    baseRoute: string

}

export const InstanceLabel = React.memo((props: InternalProps) => {

   
    const {logged} = useUsers();
    const config = useConfig()
    const campaigns = useCampaigns()
        
    const { instance, sourceLabel, decorations = [], showCampaign, linkTarget,  tooltipForLineage, noLineage } = props

    if (!instance)
        return <UnknownLabel />

   
    const campaign = campaigns.lookup(instance.campaign)

    if (!campaign)
        return <UnknownLabel tip={() => instance.source} />

    if (!props.noDecorations && props.mode !== 'light') {

        if (instance.properties?.note?.[logged.tenant])
            decorations.push(<Tooltip title={instance.properties.note[logged.tenant]}>{icns.note}</Tooltip>)
            

        if (!noLineage && instance.lineage && (!linkTarget || config.get().routedTypes?.includes(linkTarget))) {
           
            const tip = tooltipForLineage?.(instance.lineage)

            //const tip = campaigns.isLoaded(instance.lineage.campaign) ? tooltipForLineage?.(instance.lineage) :  <CampaignLabel loadTrigger campaign={instance.lineage.campaign} /> 

            decorations.push(<Tooltip overlayClassName="long-tooltip" title={tip}>{icns.branch}</Tooltip>)
        }

    }

    return <span style={{ display: "flex", alignItems: "center" }}>
        {sourceLabel({ decorations })}
        {showCampaign && <>&nbsp;&nbsp;|&nbsp;&nbsp;<CampaignLabel campaign={campaign} /></>}
    </span>

},($, $$) => $.instance === $$.instance && sameLabel($, $$))