import { TemplatedMailContent } from 'apprise-frontend/mail/model'
import { Multilang, MultilangDto } from 'apprise-frontend/model/multilang'
import { tenantapi } from 'apprise-frontend/tenant/api'
import { productapi } from 'emaris-frontend/product/api'
import { Product } from 'emaris-frontend/product/model'
import { requirementapi } from 'emaris-frontend/requirement/api'
import { requirementType } from 'emaris-frontend/requirement/constants'
import { Requirement } from 'emaris-frontend/requirement/model'
import { EmarisState } from 'emaris-frontend/state/model'
import { campaignapi } from '../api'
import { subchangeMailTemplate } from '../constants'
import { Submission, submissionmodelapi, Trail } from '../submission/model'



export const mailtemplateapi =  (s: EmarisState) => {

    const tenants = tenantapi(s)
    const campaigns = campaignapi(s)
    const requirement = requirementapi(s)
    const products = productapi(s)


    const self = {


            submissionChange: (submission: Submission, trail: Trail) : TemplatedMailContent => {

                const campaign = campaigns.safeLookup(trail.key.campaign)

                const model = submissionmodelapi(s)(campaign)

                const compliance = model.complianceProfile(submission)
                
                const tenant = tenants.safeLookup(trail.key.party)

                let asset: Requirement | Product = undefined!
                let assetTitle: MultilangDto = undefined!

                if (trail.key.assetType === requirementType) 
                
                    asset=requirement.safeLookup(trail.key.asset)
                
                else 
                    asset =  products.safeLookup(trail.key.asset)
                
 
                assetTitle = asset.description
         
                const parameters = {
                    tenant: tenant.name as Multilang,
                    tenantId: tenant.id,
                    campaign: campaign.name as Multilang,
                    campaignId: campaign.id,
                    asset: asset.name as Multilang,
                    assetTitle,
                    assetId: asset.id,
                    assetType: trail.key.assetType,
                    status: submission.lifecycle.state as string,
                    assessment: compliance?.name as Multilang,
                    observation: compliance?.complianceObservation as Multilang
                }

                return { template: subchangeMailTemplate, parameters }
            }
            

    }


    return self

}