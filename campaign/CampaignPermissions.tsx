

import { ChangesProps } from 'apprise-frontend/iam/permission'
import { usePermissionDrawer } from 'apprise-frontend/iam/PermissionForm'
import { ResourceProps, StateProps, SubjectProps } from 'apprise-frontend/iam/PermissionTable'
import { useLocale } from 'apprise-frontend/model/hooks'
import { User } from 'apprise-frontend/user/model'
import { UserPermissions } from 'apprise-frontend/user/UserPermissionTable'
import * as React from 'react'
import { useTranslation } from 'react-i18next'
import { campaignActions } from './actions'
import { useCampaigns } from './api'
import { campaignType } from './constants'
import { CampaignLabel } from './Label'
import { Campaign } from './model'
import { campaignmodule } from './module'


type PermissionsProps = ChangesProps & Partial<ResourceProps<Campaign>> & Partial<SubjectProps<User>> & Partial<StateProps<User,Campaign>> & {
    edited? : User
}


//  wraps UserPermissions for Requirements, so as to inject appropriate defaults.

export const CampaignPermissions =  (props:PermissionsProps) => {

    const {t} = useTranslation()
    const {l} = useLocale()
    
    const campaigns = useCampaigns()
    

    return  <UserPermissions {...props}
                id="campaign-permissions" 
                resourceSingular={props.resourceSingular || t(campaignmodule.nameSingular)}
                resourcePlural={props.resourcePlural || t(campaignmodule.namePlural)}
                renderResource={props.renderResource || ((t:Campaign) => <CampaignLabel campaign={t} /> )}
                resourceId={props.resourceId || ((r:Campaign) => r.id) }
                resourceText={t=>l(t.name)}
                renderResourceOption={props.renderResourceOption || ((r:Campaign) => <CampaignLabel noLink campaign={r} />)} 
                resourceRange={props.resourceRange || campaigns.all() } 
                resourceType={campaignType}

                actions={Object.values(campaignActions)} />
                
}


export const useCampaignPermissions = () => usePermissionDrawer(CampaignPermissions, { types:[campaignType] } )