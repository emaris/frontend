import { Tag } from "antd"
import { Label, LabelProps, sameLabel, UnknownLabel } from "apprise-frontend/components/Label"
import { useLocale } from "apprise-frontend/model/hooks"
import { moduleRegistry } from "apprise-frontend/module"
import { userapi, useUsers } from "apprise-frontend/user/api"
import * as React from "react"
import { useTranslation } from "react-i18next"
import { useLocation } from "react-router-dom"
import { eventapi, useEvents } from "./api"
import { eventIcon, eventRoute, eventType } from "./constants"
import { Event } from "./model"



type Props = LabelProps & {

    event: string | Event | undefined
}


const innerrouteRegexp = new RegExp(`${eventRoute}\\/(.+?)(\\?|\\/|$)`)

export const EventLabel = (props: Props) => {

    const {event} = props
    const location = useLocation()
    const events = useEvents()
    const users = useUsers()

    const resolved = typeof event === 'string' ? events.lookup(event) : event
    
    if (!event)
        return <UnknownLabel />

    if (!resolved)
        return <UnknownLabel tip={() => event as string} />

    return <PureEventLabel {...props} event={resolved} events={events} users={users} location={location} />

}


type PureProps = Omit<Props,'event'> & {

    event: Event
    users: ReturnType<typeof userapi>
    events: ReturnType<typeof eventapi>
    location: ReturnType<typeof useLocation>
}

export const PureEventLabel = React.memo((props: PureProps) => {

    const { l } = useLocale()

    const { users, events, location, event, linkTo, inactive, linkTarget, ...rest } = props

    const { pathname, search } = location

    // like routeTo() if the current route is not a campaign route, otherwise replaces the current campaign with the target one.
    // undefined, if the two campaigns coincide.
    const innerRouteTo = (target: Event) => {

        const regexpmatch = pathname.match(innerrouteRegexp)?.[1]
        const linkname = target?.id

        return regexpmatch ?
                    regexpmatch === linkname ? undefined : `${pathname.replace(regexpmatch, linkname!)}${search}`
                : events.routeTo(target)


    }

    const route = linkTo ?? innerRouteTo(event) 

    const inactiveState = event.lifecycle.state === 'inactive'

    return <Label {...rest} inactive={inactive ?? inactiveState} title={l(event.name)} isProtected={event.guarded} linkTarget={linkTarget ?? eventType} icon={eventIcon} linkTo={route} />


},($, $$) => $.event === $$.event && sameLabel($, $$))

type TypeLabelProps = {
    type: string | undefined
    light?: boolean
}

export const EventTypeLabel = (props: TypeLabelProps) => {

    const { t } = useTranslation()

    const { type, light } = props

    const module = moduleRegistry.get(type)

    const content = module ?

        <Label icon={module.icon} title={t(module.nameSingular)} />

        :

        <UnknownLabel title={type} tip={t("common.labels.unknown")} />

    return light ? content : <Tag>{content}</Tag>
}