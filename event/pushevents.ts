import { askReloadOnPushChange, askReloadOnPushRemove } from 'apprise-frontend/push/constants';
import { PushEventSlot } from 'apprise-frontend/push/module';
import { EmarisState } from 'emaris-frontend/state/model';
import shortid from 'shortid';
import { eventRoute, eventSingular, eventType } from './constants';
import { Event } from './model';
import { eventstateapi } from './state';

export type EventChangeEvent = {

    event: Event
    type: 'add' | 'remove' | 'change'

}


export const pushEventSlot: PushEventSlot<EmarisState, EventChangeEvent> = {

    onSubcribe: () => {

        console.log("subscribing for event changes...")

        return [{

            topics: [eventType]

            ,

            onEvent: (pushevent: EventChangeEvent, s: EmarisState, history, location) => {

                const { event, type } = pushevent

                console.log(`received ${type} event for events ${event.name.en}...`, { event })

                const events = eventstateapi(s)

                const currentUrl = `${location.pathname}${location.search}`


                // note: we can't tell if the user is designing an instance as that route currently doesn't mention the template (its source).
                // we should change the route for that.
                const detailOnScreen = currentUrl.includes(event.id)

                switch (type) {

                    case 'add': {

                        events.setAll([...events.all(), event])

                        break
                    }
                    case 'change': {

                        const change = () => events.setAll(events.all().map(c => c.id === event.id ? event : c))
                        const changeAndRefresh = () => { change(); history.push(currentUrl, shortid()) } // uses location state to send down a signal for remount.

                        if (detailOnScreen)
                            askReloadOnPushChange(s, { singular: eventSingular, onCancel: change, onOk: changeAndRefresh })

                        else change()

                        break
                    }

                    case 'remove': {

                        const fallbackRoute = eventRoute

                        const remove = () => events.setAll(events.all().filter(r => r.id !== event.id))
                        const leaveAndRemove = () => { history.push(fallbackRoute); remove() }

                        if (detailOnScreen)
                            askReloadOnPushRemove(s, { singular: eventSingular, onOk: leaveAndRemove })

                        else remove()

                        break
                    }

                }

            }

        }]
    }
}