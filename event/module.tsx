import { iamType } from "apprise-frontend/iam/constants"
import { IamSlot } from "apprise-frontend/iam/module"
import { Module } from "apprise-frontend/module"
import { pushEventType } from 'apprise-frontend/push/constants'
import { BaseState } from "apprise-frontend/state/model"
import { tagType } from "apprise-frontend/tag/constants"
import { TagSlot } from "apprise-frontend/tag/module"
import { noTenant } from "apprise-frontend/tenant/constants"
import { userType } from "apprise-frontend/user/constants"
import { ComponentProps, UserSlot } from "apprise-frontend/user/module"
import * as React from "react"
import { eventIcon, eventPlural, eventSingular, eventType } from './constants'
import { EventLoader } from "./Loader"
import { EventPermissions } from "./Permissions"
import { pushEventSlot } from './pushevents'



export const eventmodule : Module = {

    icon: eventIcon,
    type: eventType,
    
    dependencies: () => [tagType],

    nameSingular: eventSingular,
    namePlural: eventPlural,

    [tagType] :  {
           
        enable: true
      
    } as TagSlot,


    [iamType]: {
         
        actions: {}
      
    }  as IamSlot,

    [eventType] : {
        
        enable:true
        
    } as EventSlot

    ,


    [pushEventType] : pushEventSlot

    ,

    [userType]: {

        // include only if ...
        renderIf: (s: ComponentProps & BaseState) => {

              // tags cannot be handled by tenant users
              const someTenantSubject = s?.subjectRange?.some(s=>s.tenant!==noTenant)

              return !someTenantSubject 

        },

        permissionComponent: props=><EventLoader>
                                        <EventPermissions {...props}/>
                                    </EventLoader>
  
  } as UserSlot

}

export type EventSlot = {

    enable: boolean

}