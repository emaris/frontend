import { any } from "apprise-frontend/iam/model";
import { eventIcon, eventType } from "./constants";

const baseAction = { icon:eventIcon, type: eventType, resource :any }

export const eventActions = {  

    manage: {...baseAction,  labels:["manage"], shortName:"event.actions.manage.short",name:"event.actions.manage.name", description: "event.actions.manage.desc"},
}