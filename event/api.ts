import { useEmarisState } from "emaris-frontend/state/hooks";
import { EmarisState } from "emaris-frontend/state/model";
import { eventActions } from "./actions";
import { eventcalls } from "./calls";
import { eventmodelapi } from "./model";
import { eventstateapi } from "./state";
import { eventvalidationapi } from "./validation";



export const useEvents = () => eventapi(useEmarisState())

export const eventapi = (s:EmarisState) => ({

    ...eventstateapi(s),
    ...eventvalidationapi(s),
    ...eventmodelapi(s),
    
    calls: eventcalls(s),
    actions: eventActions 
})


export const events = {

    breadcrumbResolver: (id:string,s: EmarisState) => eventapi(s).lookup(id)?.name
}