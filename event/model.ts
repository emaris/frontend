import { intlapi } from 'apprise-frontend/intl/api'
import { Lifecycle, newLifecycle } from "apprise-frontend/model/lifecycle"
import { compareMultilang, l, MultilangDto, newMultiLang, noMultilang } from "apprise-frontend/model/multilang"
import { Module, moduleRegistry } from "apprise-frontend/module"
import { newTagged, TagExpression, Tagged } from "apprise-frontend/tag/model"
import { deepclone } from "apprise-frontend/utils/common"
import { campaignType } from 'emaris-frontend/campaign/constants'
import { EmarisState } from "emaris-frontend/state/model"
import shortid from "shortid"
import { eventSingular, eventType } from "./constants"
import { EventSlot } from "./module"

export type EventState = 'active' | 'inactive'

type PredefProps = 'guarded' | 'cardinality'

export type EventDto = Tagged & {
    
    id: string

    guarded?: boolean
    predefined?:boolean

    lifecycle: Lifecycle<EventState>

    name: MultilangDto
    description: MultilangDto

    type: string,
    cardinality: EventCardinality

    managed?: boolean,
    managedTypes?: string[]                 // managed if target has this type (only for metaevents, when type=event)
    managedExpression?: TagExpression,      // managed if target matches the expression
   
    predefinedProperties: PredefProps[]

    properties: EventProperties
}

export type EventCardinality = { initial: number, max: number | undefined }

export type CardinalityNames = 'any' | 'zeroOrOne' | 'one' | 'atLeastOne'

export const eventcardinalities: { [key in CardinalityNames]: EventCardinality } = {

    any: { initial: 0, max: undefined },
    zeroOrOne: { initial: 0, max: 1 },
    one: { initial: 1, max: 1 },
    atLeastOne: { initial: 1, max: undefined },


}

export type EventProperties = {

    note?:Record<string, string>
    relatable?: boolean
  
  } & {[key:string]:any}

export type Event = EventDto

export const noEvent = (s:EmarisState) : Event  => {

    const t = intlapi(s).getT()
    
    return {...newEvent(), id: `${newEventId()}-unknown`, type: campaignType, name:noMultilang(t('common.labels.unknown_one',{singular:t(eventSingular)}))}

}

export const newEvent = () : Event => ({
    
    ...newTagged,

    id: undefined!,

    lifecycle:newLifecycle('inactive'),
    type: undefined!,

    cardinality: eventcardinalities["one"],

    name:newMultiLang(),
    description:newMultiLang(),

    predefinedProperties: [],

    properties:{relatable: false}
    

}) as Event

export const completeEventOnAdd  = (event:Event) =>  ({...event, id:newEventId()})

export const newEventId = () =>`E-${shortid()}`

export const eventmodelapi = (s:EmarisState) => {

    const self = {

        stringify: (e:Event | undefined ) => e ? `${l(s)(e.name)} ${l(s)(e.description)??''} ${e.type}}` : ''

        ,

        newEvent : (type:string) : Event => ({id:undefined!,type,name:newMultiLang(),description:newMultiLang()}) as any
        
        ,

        clone: (e:Event) : Event => ({...deepclone(e), id:undefined!, predefined:false, predefinedProperties:[], lifecycle:newLifecycle(`inactive`)})
        
        ,

        registeredModules: ():Module[] => moduleRegistry.allWith(eventType).filter(m=>(m[eventType] as EventSlot).enable)

        ,
    
        comparator: (o1:Event,o2:Event) => compareMultilang(s)(o1.name,o2.name)
    }

    return self
}