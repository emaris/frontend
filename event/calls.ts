import { callapi } from "apprise-frontend/call/call";
import { EmarisState } from "emaris-frontend/state/model";
import { Event } from "./model";

const svc="admin"
const events="/event"

export const eventcalls = (s:EmarisState) => {

    const {at} = callapi(s)

    return {

        fetchAll: () => at(events,svc).get<Event[]>()
        
        , 
    
        fetchOne: (id:String) =>  at(`${events}/${id}`,svc).get<Event>()
        
        ,

        add: (event:Event) =>  at(events,svc).post<Event>(event)
        
        ,

        update: (event:Event) => at(`${events}/${event.id}`,svc).put<Event>(event)
        
        , 

        delete: (id:string) : Promise<void> => at(`${events}/${id}`,svc).delete()


    }
}