
import * as React from 'react'
import { useTranslation } from 'react-i18next'
import { ChangesProps } from 'apprise-frontend/iam/permission'
import { usePermissionDrawer } from 'apprise-frontend/iam/PermissionForm'
import { ResourceProps, StateProps, SubjectProps } from 'apprise-frontend/iam/PermissionTable'
import { User } from 'apprise-frontend/user/model'
import { UserPermissions } from 'apprise-frontend/user/UserPermissionTable'
import { eventActions } from './actions'
import { eventPlural, eventSingular, eventType } from './constants'
import { EventLabel } from './Label'
import { Event } from './model'
import { useEvents } from './api'
import { any } from 'apprise-frontend/iam/model'
import { useLocale } from 'apprise-frontend/model/hooks'

type PermissionsProps = ChangesProps & Partial<ResourceProps<Event>> & Partial<SubjectProps<User>> & Partial<StateProps<User,Event>> & {
    edited? : User
}

export const EventPermissions =  (props: PermissionsProps) => {

    const {t} = useTranslation()
    const {l} = useLocale()
    
    const events = useEvents()
    
    return <UserPermissions {...props}
                                 id="event-permissions" 
                                 resourceSingular={props.resourceSingular || t(eventSingular) }
                                 renderResource={props.renderResource || ((e:Event) => <EventLabel event={e} /> )}
                                 resourcePlural={props.resourcePlural || t(eventPlural) }
                                 resourceText={t=>l(t.name)}
                                 resourceId={props.resourceId || ((e:Event)=>e.id)}
                                 renderResourceOption={props.renderResourceOption || ((e:Event) => <EventLabel noLink event={e} />)} 
                                 resourceRange={props.resourceRange || events.all() } 
                                 resourceType={eventType}

                                 actions={Object.values(eventActions)} />

}


export const useEventPermissions = () => usePermissionDrawer(EventPermissions, { types:[eventType, any] }  )