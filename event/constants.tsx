import { Icon } from 'antd'
import { icns } from 'apprise-frontend/icons'
import * as React from 'react'
import * as anticns from "react-icons/ai"


export const eventSingular = "event.module.name_singular"
export const eventPlural = "event.module.name_plural"

export const eventRoute="/event"
export const eventType="event"
export const eventIcon =  icns.clock
export const dateIcon = icns.calendar
export const relativeIcon = icns.link
export const pinnedDateIcon = <Icon component={anticns.AiOutlineEnvironment} />
export const recurringDateIcon = <Icon component={anticns.AiOutlineHistory} />
export const singleUseDateIcon = icns.singleUse

export const pendingIcon = <Icon component={anticns.AiOutlineHourglass} />
export const missingIcon = <Icon component={anticns.AiOutlineMinusCircle} />
