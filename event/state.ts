import { intlapi } from "apprise-frontend/intl/api";
import { systemapi } from "apprise-frontend/system/api";
import { utilapi } from "apprise-frontend/utils/api";
import { through } from "apprise-frontend/utils/common";
import { notify, showAndThrow } from "apprise-frontend/utils/feedback";
import { EmarisState } from "emaris-frontend/state/model";
import { eventcalls } from "./calls";
import { eventPlural, eventRoute, eventSingular, eventType } from "./constants";
import { completeEventOnAdd, Event, EventDto, eventmodelapi, newEvent, noEvent } from "./model";


type EventNext = {
    model: Event
}

export type EventState = {
    events: {
        all: Event[];
        next?: EventNext
    }
};

export const initialEvents: EventState = {
    events: {
        all: undefined!,
        next: undefined
    }
}


export const eventstateapi = (s: EmarisState) => {

    const model = eventmodelapi(s)

    const { toggleBusy } = systemapi(s)
    const call = eventcalls(s)
    const t = intlapi(s).getT()
    const type = eventType
    const [singular, plural] = [t(eventSingular), t(eventPlural)].map(n => n.toLowerCase())

    const self = {

        areReady: () => !!s.events.all

        ,

        all: () => s.events.all as EventDto[]

        ,

        allOf: (...types: (string | undefined)[]) => self.all().filter(e => (types ?? []).includes(e.type))

        ,

        allSorted: () => [...self.all()].sort(model.comparator)

        ,

        lookup: (id: string | undefined) => self.all()?.find(e => e.id === id)

        ,

        safeLookup: (id: string | undefined) => self.lookup(id) ?? noEvent(s)

        ,

        setAll: (events: Event[], props?: { noOverwrite: boolean }) => s.changeWith(s => {

            if (s.events.all && props?.noOverwrite)
                return

            s.events.all = events

        })

        ,

        fetchAll: (forceRefresh = false): Promise<Event[]> =>

            self.areReady() && !forceRefresh ? Promise.resolve(self.all())

                :

                toggleBusy(`${type}.fetchAll`, t("common.feedback.load", { plural }))

                    .then(_ => console.log(`fetching events...`))
                    .then(_ => call.fetchAll())
                    .then(through($ => self.setAll($, { noOverwrite: true })))


                    .catch(e => showAndThrow(e, t("common.calls.fetch_all_error", { plural })))
                    .finally(() => toggleBusy(`${type}.fetchAll`))


        ,

        fetchOne: (id: string): Promise<Event> =>

            toggleBusy(`${type}.fetchOne`, t("common.feedback.load_one", { singular }))

                .then(_ => call.fetchOne(id))
                .then(through(fetched => { if (!fetched) throw Error(`Invalid resource lookup for ${id}, returned undefined.`) }))
                .then(through(fetched => self.setAll(self.all().map(u => u.id === id ? fetched : u))))

                .catch(e => showAndThrow(e, t("common.calls.fetch_one_error", { singular })))
                .finally(() => toggleBusy(`${type}.fetchOne`))

        ,

        routeTo: (e: Event) => e ? `${eventRoute}/${e.id}` : eventRoute


        ,

        next: () => {
            const next = s.events.next || { model: newEvent() }
            return next;
        },

        resetNext: () => {
            self.setNext(undefined)
        }
        ,

        setNext: (t: EventNext | Event | undefined): EventNext | undefined => {
            const model = t ? t.hasOwnProperty('model') ? t as EventNext : { model: t as Event } : undefined

            s.changeWith(s => s.events.next = model)
            return model

        },

        save: (event: Event): Promise<Event> =>

            toggleBusy(`${type}.save`, t("common.feedback.save_changes"))


                .then(() => event.id ?

                    call.update(event)
                        .then(through(saved => self.setAll(self.all().map(r => r.id === event.id ? saved : r))))

                    :

                    call.add(completeEventOnAdd(event))
                        .then(through(saved => self.setAll([saved, ...self.all()])))

                )
                .then(through(() => notify(t('common.feedback.saved'))))

                .catch(e => showAndThrow(e, t("common.calls.save_one_error", { singular })))
                .finally(() => toggleBusy(`${type}.save`))


        ,

        remove: (id: string, onConfirm: () => void, challenge?: boolean) =>

            utilapi(s).askConsent({

                title: t('common.consent.remove_one_title', { singular }),
                content: t('common.consent.remove_one_msg', { singular }),
                okText: t('common.consent.remove_one_confirm', { singular }),

                okChallenge: challenge ? t('common.consent.remove_challenge',{singular}) : undefined,
                
                onOk: () => {


                    toggleBusy(`${type}.removeOne`, t("common.feedback.save_changes"))

                        .then(_ => call.delete(id))
                        .then(_ => self.all().filter(u => id !== u.id))
                        .then(self.setAll)
                        .then(_ => notify(t('common.feedback.saved')))
                        .then(_ => onConfirm && onConfirm())

                        .catch(e => showAndThrow(e, t("common.calls.remove_one_error", { singular })))
                        .finally(() => toggleBusy(`${type}.removeOne`))


                }
            }),

        removeAll: (list: string[], onConfirm?: () => void) =>

            utilapi(s).askConsent({


                title: t('common.consent.remove_many_title', { count: list.length, plural }),
                content: t('common.consent.remove_many_msg', { plural }),
                okText: t('common.consent.remove_many_confirm', { count: list.length, plural }),

                onOk: () => {


                    toggleBusy(`${type}.removeAll`, t("common.feedback.save_changes"))

                        .then(_ => list.forEach(id => call.delete(id)))
                        .then(_ => self.all().filter(u => !list.includes(u.id)))
                        .then(self.setAll)
                        .then(_ => notify(t('common.feedback.saved')))
                        .then(_ => onConfirm && onConfirm())

                        .catch(e => showAndThrow(e, t("common.calls.remove_one_error", { singular })))
                        .finally(() => toggleBusy(`${type}.removeAll`))


                }
            })



    }

    return self
}