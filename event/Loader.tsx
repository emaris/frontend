
import * as React from "react"
import { useAsyncRender } from "apprise-frontend/utils/hooks"
import { useEvents } from "./api"

type Props = React.PropsWithChildren<{

  placeholder?: React.ReactNode

}>


export const EventLoader = (props:Props) => {

    const {areReady,fetchAll} = useEvents()

    const [render] = useAsyncRender({
      when:areReady(),
      task:fetchAll,
      content:props.children,
      placeholder:props.placeholder
  
    })
  
    return render 
  
  }